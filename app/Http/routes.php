<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

if (version_compare(PHP_VERSION, '7.3.0', '<=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

Route::get('/', function () {
    return view('welcome');
});

Route::get('/mailsme', function () {
    return view('mails.valid');
});

/* ================== Homepage + Admin Routes ================== */

require __DIR__.'/admin_routes.php';


Route::post('/savedevis', [
    'as' => 'savedevis',
    'uses' => 'LA\BriefsController@savedevis',
]);

// Ajax checklist
Route::post('/ajaxChecklist', 'HomeController@ajaxChecklist');
Route::post('/ajaxChecklistUpdate', 'HomeController@ajaxChecklistUpdate');

# route for old admin
Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
    //page de profil by me
    Route::get(config('laraadmin.adminRoute'). '/profil', 'LA\UsersController@profil');
    Route::post(config('laraadmin.adminRoute'). '/profil/update/info', 'LA\UsersController@updateInfo');
    Route::post(config('laraadmin.adminRoute'). '/profil/update/pass', 'LA\UsersController@updatePass');
    Route::post(config('laraadmin.adminRoute'). '/profil/update/avatar', 'LA\UsersController@updateAvatar');


	// # routes admin for marketing chief
	Route::get(config('laraadmin.adminRoute'). '/dashboard/marketing', 'MarketingController@index');
    Route::get(config('laraadmin.adminRoute'). '/marketing/brief/attente', 'MarketingController@attente');
    Route::get(config('laraadmin.adminRoute'). '/marketing/brief/valid', 'MarketingController@valid');
    Route::get(config('laraadmin.adminRoute'). '/marketing/brief/reject', 'MarketingController@reject');
    Route::get(config('laraadmin.adminRoute'). '/marketing/notif', 'MarketingController@notif');
    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/valid', 'MarketingController@assetvalid');
    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/attente', 'MarketingController@assetattents');
    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/reject', 'MarketingController@assetreject');
    Route::get(config('laraadmin.adminRoute'). '/marketing/bc/waiting', 'MarketingController@bc');
    Route::get(config('laraadmin.adminRoute'). '/marketing/bc/valid', 'MarketingController@bcValid');
    Route::get(config('laraadmin.adminRoute'). '/marketing/bc/reject', 'MarketingController@bcReject');

    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/waiting/valid', 'MarketingController@waitingAsset');
    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/waiting/{id}/valid', 'MarketingController@validAsset');
    Route::post(config('laraadmin.adminRoute'). '/marketing/asset/reject', 'MarketingController@rejectAsset');
    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/waiting/{id}/valid/prod', 'MarketingController@validAssetProd');
    Route::post(config('laraadmin.adminRoute'). '/marketing/asset/reject/prod', 'MarketingController@rejectAssetProd');

    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/waiting/{id}/valid/livre', 'MarketingController@validAssetLivre');
    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/waiting/{id}/valid/retro', 'MarketingController@validAssetRetro');
    Route::post(config('laraadmin.adminRoute'). '/marketing/asset/reject/livre', 'MarketingController@rejectAssetLivre');
    Route::post(config('laraadmin.adminRoute'). '/marketing/asset/reject/retro', 'MarketingController@rejectAssetRetro');
    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/waiting/{id}/valid/rapportop', 'MarketingController@validAssetRapportOp');
    Route::post(config('laraadmin.adminRoute'). '/marketing/asset/reject/rapportop', 'MarketingController@rejectAssetRapportOp');

    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/waiting/{id}/valid/plan', 'MarketingController@validAssetPlan');
    Route::get(config('laraadmin.adminRoute'). '/marketing/asset/waiting/{id}/valid/ggle', 'MarketingController@validAssetGgle');
    Route::post(config('laraadmin.adminRoute'). '/marketing/asset/reject/plan', 'MarketingController@rejectAssetPlan');
    Route::post(config('laraadmin.adminRoute'). '/marketing/asset/reject/ggle', 'MarketingController@rejectAssetGgle');

    Route::get(config('laraadmin.adminRoute'). '/marketing/annonceurs', 'MarketingController@annonceurs');
    Route::post(config('laraadmin.adminRoute'). '/marketing/annonceurs', 'MarketingController@addannonceurs');

    Route::post(config('laraadmin.adminRoute'). '/marketing/delete/devis', 'MarketingController@deleteDevis');

    // # routes admin for leads
	Route::get(config('laraadmin.adminRoute'). '/dashboard/lead', 'AdminleadController@index');
    Route::get(config('laraadmin.adminRoute'). '/lead/asset/waiting', 'AdminleadController@waitingAsset');
    Route::get(config('laraadmin.adminRoute'). '/lead/asset/valid', 'AdminleadController@validAsset');
    Route::get(config('laraadmin.adminRoute'). '/lead/asset/reject', 'AdminleadController@rejectAsset');

    Route::get(config('laraadmin.adminRoute'). '/lead/brief/inprogress', 'AdminleadController@inprogress');
    Route::get(config('laraadmin.adminRoute'). '/lead/brief/waiting', 'AdminleadController@waiting');
    Route::get(config('laraadmin.adminRoute'). '/lead/brief/rejected', 'AdminleadController@reject');
    Route::get(config('laraadmin.adminRoute'). '/lead/brief/waitingsbrief', 'AdminleadController@waitingBrief');

    Route::get(config('laraadmin.adminRoute'). '/lead/bc/waiting', 'AdminleadController@bc');
    Route::get(config('laraadmin.adminRoute'). '/lead/bc/valid', 'AdminleadController@bcValid');
    Route::get(config('laraadmin.adminRoute'). '/lead/bc/reject', 'AdminleadController@bcReject');

    Route::get(config('laraadmin.adminRoute'). '/lead/notif', 'AdminleadController@notif');
    Route::get(config('laraadmin.adminRoute'). '/lead/assetDevis/waiting/{id}/valid', 'AdminleadController@validAssetDevisUser');
    Route::post(config('laraadmin.adminRoute'). '/lead/assetDevis/waiting/rejet', 'AdminleadController@cancelAssetDevis');
    Route::get(config('laraadmin.adminRoute'). '/lead/assetDevis/waiting/{id}/valid/retro', 'AdminleadController@validAssetRetro');
    Route::post(config('laraadmin.adminRoute'). '/lead/assetDevis/waiting/rejet/retro', 'AdminleadController@cancelAssetRetro');

    Route::get(config('laraadmin.adminRoute'). '/lead/assetDevis_controle/waiting/{id}/valid', 'AdminleadController@validAssetDevisUserControle');
    Route::get(config('laraadmin.adminRoute'). '/lead/assetBcp_controle/waiting/{id}/valid', 'AdminleadController@validAssetBcpControle');
    Route::post(config('laraadmin.adminRoute'). '/lead/assetDevis_controle/waiting/rejet', 'AdminleadController@cancelAssetDevisControle');
    Route::post(config('laraadmin.adminRoute'). '/lead/assetBcp_controle/waiting/rejet', 'AdminleadController@cancelAssetBcpControle');
    Route::get(config('laraadmin.adminRoute'). '/lead/assetDevis_controle/waiting/{id}/valid/retro', 'AdminleadController@validAssetRetroControle');
    Route::post(config('laraadmin.adminRoute'). '/lead/assetDevis_controle/waiting/rejet/retro', 'AdminleadController@cancelAssetRetroControle');

    Route::get(config('laraadmin.adminRoute'). '/lead/assetBcp/waiting/{id}/valid', 'AdminleadController@validAssetBcp');
    Route::post(config('laraadmin.adminRoute'). '/lead/assetBcp/waiting/rejet', 'AdminleadController@cancelAssetBcp');

	// # routes admin for current users
    Route::get(config('laraadmin.adminRoute'). '/dashboard/user', 'CurrentUserController@index');
    Route::get(config('laraadmin.adminRoute'). '/acceptache/{idtache}', 'CurrentUserController@accept');
    Route::get(config('laraadmin.adminRoute'). '/user/assigntach', 'CurrentUserController@assign');
    Route::get(config('laraadmin.adminRoute'). '/user/inprogresstach', 'CurrentUserController@progess');
    Route::get(config('laraadmin.adminRoute'). '/user/finish', 'CurrentUserController@finish');
    Route::get(config('laraadmin.adminRoute'). '/user/attent/finish', 'CurrentUserController@termintache');
    Route::get(config('laraadmin.adminRoute'). '/user/{id}/termine', 'CurrentUserController@sendtachvalidate');
    Route::get(config('laraadmin.adminRoute'). '/user/rejet', 'CurrentUserController@rejet');
    Route::get(config('laraadmin.adminRoute'). '/user/notif', 'CurrentUserController@notif');

	// # routes admin for head of departments
    Route::get(config('laraadmin.adminRoute'). '/dashboard', 'HeadDepartmentController@index');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/tacheAssign', 'HeadDepartmentController@tacheAssignDep');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/{id}/tacheAssign/edit', 'HeadDepartmentController@tacheAssignDepEdit');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/tacheAssign/update', 'HeadDepartmentController@tacheAssignDepUpdate');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/brief/assign', 'HeadDepartmentController@assign');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/brief/{id}/reassignation', 'HeadDepartmentController@reassign');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/users_dep', 'HeadDepartmentController@user');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/notif', 'HeadDepartmentController@notif');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/asset/waiting/valid', 'HeadDepartmentController@waitingAsset');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/asset/waiting/{id}/valid', 'HeadDepartmentController@validAsset');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/asset/waiting/{id}/valid/fiche', 'HeadDepartmentController@validAssetFiche');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/asset/reject', 'HeadDepartmentController@rejectAsset');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/asset/reject/fiche', 'HeadDepartmentController@rejectAssetFiche');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/asset/waiting/{id}/valid/prod', 'HeadDepartmentController@validAssetProd');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/asset/reject/prod', 'HeadDepartmentController@rejectAssetProd');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/asset/waiting/{id}/valid/proto', 'HeadDepartmentController@validAssetProto');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/asset/reject/proto', 'HeadDepartmentController@rejectAssetProto');

    Route::get(config('laraadmin.adminRoute'). '/dashboard/op/asset/waiting/valid', 'HeadDepartmentController@waitingAssetOp');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/op/asset/waiting/{id}/valid/retro', 'HeadDepartmentController@validAssetRetro');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/op/asset/waiting/{id}/valid/livre', 'HeadDepartmentController@validAssetLivre');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/op/asset/reject/retro', 'HeadDepartmentController@rejectAssetRetro');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/op/asset/reject/livre', 'HeadDepartmentController@rejectAssetLivre');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/op/asset/waiting/{id}/valid/rapportop', 'HeadDepartmentController@validAssetRapportOP');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/op/asset/reject/rapportop', 'HeadDepartmentController@rejectAssetRapportOP');

    Route::get(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/waiting/valid', 'HeadDepartmentController@waitingAssetCdp');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/waiting/{id}/valid/plan', 'HeadDepartmentController@validAssetPlan');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/waiting/{id}/valid/ggle', 'HeadDepartmentController@validAssetGgle');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/waiting/{id}/valid/rapport', 'HeadDepartmentController@validAssetRapport');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/waiting/{id}/valid/rapportrim', 'HeadDepartmentController@validAssetRapportrim');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/reject/plan', 'HeadDepartmentController@rejectAssetPlan');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/reject/ggle', 'HeadDepartmentController@rejectAssetGgle');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/reject/rapport', 'HeadDepartmentController@rejectAssetRapport');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/reject/rapportrim', 'HeadDepartmentController@rejectAssetRapportrim');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/cpd/digit', 'HeadDepartmentController@assetDigitCdp');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/cpd/digit/createtache', 'HeadDepartmentController@digiTacheCdp');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/cpd/digit/createtache', 'HeadDepartmentController@storeTache');

    Route::get(config('laraadmin.adminRoute'). '/dashboard/rd/asset/waiting/valid', 'HeadDepartmentController@waitingAssetRd');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/rd/asset/waiting/{id}/valid/rapport', 'HeadDepartmentController@validAssetRapportRd');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/rd/asset/waiting/{id}/valid/rapportrim', 'HeadDepartmentController@validAssetRapportrimRd');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/rd/asset/reject/rapportrim', 'HeadDepartmentController@rejectAssetRapportrimRd');
    Route::post(config('laraadmin.adminRoute'). '/dashboard/rd/asset/reject/rapport', 'HeadDepartmentController@rejectAssetRapportRd');

    Route::post(config('laraadmin.adminRoute'). '/dashboard/brief/waiting/rejet', 'HeadDepartmentController@cancelBrief');
    Route::get(config('laraadmin.adminRoute'). '/dashboard/brief/waiting/{id}/valid', 'HeadDepartmentController@validBrief');

    Route::get(config('laraadmin.adminRoute'). '/dashboard/myasset/rejet', 'HeadDepartmentController@myAssetRejet');

    // # routes admin for lead validator
    Route::get(config('laraadmin.adminRoute'). '/', 'AdminleadvalidController@index');
    Route::get(config('laraadmin.adminRoute'). '/brief/waiting', 'AdminleadvalidController@waiting');
    Route::post(config('laraadmin.adminRoute'). '/brief/waiting/rejet', 'AdminleadvalidController@cancelBrief');
    Route::get(config('laraadmin.adminRoute'). '/brief/waiting/{id}/valid', 'AdminleadvalidController@validBrief');
    Route::get(config('laraadmin.adminRoute'). '/brief/inprogress', 'AdminleadinprogressController@inprogress');
    Route::get(config('laraadmin.adminRoute'). '/brief/rejected', 'AdminleadrejectController@reject');
    Route::get(config('laraadmin.adminRoute'). '/briefwaitg/waitingsbrief', 'AdminleadvalidController@waitingBrief');
    Route::get(config('laraadmin.adminRoute'). '/briefwaitg/waitingsbrief/{id}/valid', 'AdminleadvalidController@validDeBrief');

    Route::get(config('laraadmin.adminRoute'). '/asset/waiting', 'AdminleadvalidController@waitingAsset');
    Route::get(config('laraadmin.adminRoute'). '/asset/valid', 'AdminleadvalidController@validAsset');
    Route::get(config('laraadmin.adminRoute'). '/asset/reject', 'AdminleadvalidController@rejectAsset');
    Route::get(config('laraadmin.adminRoute'). '/asset/waiting/{id}/valid', 'AdminleadvalidController@validAssetUser');
    Route::get(config('laraadmin.adminRoute'). '/assetDevis/waiting/{id}/valid', 'AdminleadvalidController@validAssetDevisUser');
    Route::get(config('laraadmin.adminRoute'). '/assetOP/waiting/{id}/valid', 'AdminleadvalidController@validAssetOpUser');
    Route::post(config('laraadmin.adminRoute'). '/asset/waiting/rejet', 'AdminleadvalidController@cancelAsset');
    Route::post(config('laraadmin.adminRoute'). '/assetDevis/waiting/rejet', 'AdminleadvalidController@cancelAssetDevis');
    Route::post(config('laraadmin.adminRoute'). '/assetOp/waiting/rejet', 'AdminleadvalidController@cancelAssetOp');
    Route::get(config('laraadmin.adminRoute'). '/assetBcp/waiting/{id}/valid', 'AdminleadvalidController@validAssetBcp');
    Route::post(config('laraadmin.adminRoute'). '/assetBcp/waiting/rejet', 'AdminleadvalidController@cancelAssetBcp');

    Route::get(config('laraadmin.adminRoute'). '/bc/waiting', 'AdminleadvalidController@bc');
    Route::get(config('laraadmin.adminRoute'). '/bc/valid', 'AdminleadvalidController@bcValid');
    Route::get(config('laraadmin.adminRoute'). '/bc/reject', 'AdminleadvalidController@bcReject');
    Route::get(config('laraadmin.adminRoute'). '/bc/waiting/{id}/valid', 'AdminleadvalidController@validBC');
    Route::post(config('laraadmin.adminRoute'). '/bc/waiting/rejet', 'AdminleadvalidController@cancelBC');
    Route::get(config('laraadmin.adminRoute'). '/soumi/waiting/{id}/valid', 'AdminleadvalidController@validSoumi');
    Route::post(config('laraadmin.adminRoute'). '/soumi/waiting/rejet', 'AdminleadvalidController@cancelSoumi');

    Route::get(config('laraadmin.adminRoute'). '/niveau3/waiting', 'AdminleadvalidController@nv3');

    Route::get(config('laraadmin.adminRoute'). '/admin/notif', 'AdminleadvalidController@notif');
    Route::get(config('laraadmin.adminRoute') . '/admin/projets_list', 'AdminleadvalidController@listsvalid');

    Route::get(config('laraadmin.adminRoute'). '/devis/attent/del', 'AdminleadvalidController@devisDel');
    Route::get(config('laraadmin.adminRoute'). '/devis/attent/{id}/del', 'AdminleadvalidController@devisDelete');


    // # routes admin for trafic users
    Route::get(config('laraadmin.adminRoute'). '/dashboard/trafic', 'TraficUserController@index');
    Route::get(config('laraadmin.adminRoute'). '/trafic/notif', 'TraficUserController@notif');
    Route::get(config('laraadmin.adminRoute'). '/trafic/tacheassign', 'TraficUserController@tacheAssign');
    Route::get(config('laraadmin.adminRoute'). '/trafic/{id}/edite', 'TraficUserController@tacheEdit');
    Route::post(config('laraadmin.adminRoute'). '/trafic/update', 'TraficUserController@tacheUpdate');

});