<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\Assetype;
use App\Models\Debrief;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Tach;

class TachesController extends Controller
{
	public $show_action = true;
	public $view_col = 'libelle';
	public $listing_cols = ['id', 'libelle', 'description', 'brief_id', 'assigne_id', 'assignant_id', 'status'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Taches', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Taches', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Taches.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Taches');
		
		if(Module::hasAccess($module->id)) {
			return View('la.taches.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect("/");
        }
	}

	/**
	 * Show the form for creating a new tach.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created tach in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	    //dd($request->all());
		if(Module::hasAccess("Taches", "create")) {
		
			$rules = Module::validateRules("Taches", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			$insert_id = Module::insert("Taches", $request);

            $notif = new Notification();
            $notif->libelle = 'tache_assigne';
            $notif->send_id = $request['assignant_id'];
            $notif->recip_id = $request['assigne_id'];
            //$notif->brief_id = 'tache_assigne';
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Une tâche vous êtes assignée",
            );
            $findUserMail = User::where('id',$request['assigne_id'])->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Tâche assigné');
            });

            return redirect()->back();
			//return redirect()->route(config('laraadmin.adminRoute') . '.taches.index');
			
		} else {
            return redirect("/");
		}
	}

	/**
	 * Display the specified tach.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Taches", "view")) {
			
			$tach = Tach::find($id);
			$debrieftache = Debrief::where('id',$tach->debrief_id)->first();
			$assetypes = Assetype::where("status",1)->get();
			//dd($tach);
            //$assetUser='';
            //les assets par user
            //if(Auth::user()->role_id==6){
                $assetUser=Asset::where('debrief_id',$tach->debrief_id)->where('user_id',Auth::user()->id)->get();
            //}
            //dd($assetUser);
            $mytaches=Tach::where('id',$id)->with('userTache')->with('userAssignant')->first();

			if(isset($tach->id)) {
				$module = Module::get('Taches');
				$module->row = $tach;
				
				return view('la.taches.show', [
                    'module' => $module,
                    'mytaches' => $mytaches,
                    'debrieftache'=>$debrieftache,
					'view_col' => $this->view_col,
                    'assetUser'=>$assetUser,
                    'assetypes'=>$assetypes,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('tach', $tach);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("tach"),
				]);
			}
		} else {
            return redirect("/");
		}
	}

	/**
	 * Show the form for editing the specified tach.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Taches", "edit")) {			
			$tach = Tach::find($id);
			//dd($tach);
            $usersDepartmts ='';
            if(Auth::user()->role_id == 5){
                $usersDepartmts = User::where('departement_id',Auth::user()->departement_id)->where('role_id','!=',Auth::user()->role_id)->get();
            }

			if(isset($tach->id)) {	
				$module = Module::get('Taches');
				
				$module->row = $tach;
				
				return view('la.taches.edit', [
                    'module' => $module,
                    'usersDepartmts' => $usersDepartmts,
					'view_col' => $this->view_col,
				])->with('tach', $tach);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("tach"),
				]);
			}
		} else {
            return redirect("/");
		}
	}

	/**
	 * Update the specified tach in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
	    //dd($request->all());
		if(Module::hasAccess("Taches", "edit")) {
			
			$rules = Module::validateRules("Taches", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Taches", $request, $id);

            $notif = new Notification();
            $notif->libelle = 'tache_assigne';
            $notif->send_id = $request['assignant_id'];
            $notif->recip_id = $request['assigne_id'];
            //$notif->brief_id = 'tache_assigne';
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Une tâche vous êtes assignée",
            );
            $findUserMail = User::where('id',$request['assigne_id'])->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Tâche assigné');
            });

            return redirect(config('laraadmin.adminRoute') . '/debriefs/'.$request['debrief_id'].'#tache');
            //return redirect()->route(config('laraadmin.adminRoute') . '.taches.index');
			
		} else {
            return redirect("/");
		}
	}

	/**
	 * Remove the specified tach from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Taches", "delete")) {
			Tach::find($id)->delete();
			$assets = Asset::where('tache_id',$id)->pluck('id')->toArray();
			if(count($assets)>0){
                //dd($assets);
                Asset::whereIn('id',$assets)->delete();
                Notification::whereIn('asset_id',$assets)->delete();
                return redirect()->back();
            }
			// Redirecting to index() method
            return redirect()->back();
            //return redirect()->route(config('laraadmin.adminRoute') . '.taches.index');
		} else {
            return redirect("/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('taches')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Taches');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/taches/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Taches", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/taches/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Taches", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.taches.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
