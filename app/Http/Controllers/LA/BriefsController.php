<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\Debrief;
use App\Models\Department;
use App\Models\Notification;
use App\Models\Projet;
use App\Models\Tach;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Brief;
use App\Devi;

class BriefsController extends Controller
{
	public $show_action = true;
	public $view_col = 'libelle';
	public $listing_cols = ['id', 'libelle', 'dateDentree', 'dateDeSortie', 'taf', 'pj', 'dep', 'project_id', 'userId'];
    public $listing_cols_tache = ['id', 'libelle', 'description', 'assigne','status'];

	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Briefs', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Briefs', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Briefs.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Briefs');
		
		if(Module::hasAccess($module->id)) {
			return View('la.briefs.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect("/");
        }
	}

	/**
	 * Show the form for creating a new brief.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	public function assetDevis($value='')
	{
		# code...
	}


	public function savedevis(Request $request){
		//dd($request->all());
		$file=$request->file('file');
        //$fileName=$file->getClientOriginalName();
        $fileextensions=$file->getClientOriginalExtension();
        // $fileSize=$file->getClientSize();

        $aleatoire=str_random(8);
        //$uploadName = htmlspecialchars(trim(strtolower($fileName)));
        $upload_nom =$aleatoire . '.' . $fileextensions;
        $upload_dest= 'devis/';
        //$fileextension=strtolower($fileextensions);
        //$extensions_autorisees=array('jpeg','jpg','gif','tiff','bmp','png');

        $devi= new Devi();
        $devi->brief_id = $request['idbrief'];
        $devi->file = $upload_nom;
        $devi->save();

        $file->move($upload_dest,$upload_nom);

        return redirect()->back();
	}

	/**
	 * Store a newly created brief in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
        //dd($request->all());
		if(Module::hasAccess("Briefs", "create")) {
		
			$rules = Module::validateRules("Briefs", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			$insert_id = Module::insert("Briefs", $request);


			$file=$request->file('file');

            $asset = implode(",", $request['asset']);
            $project = Projet::find($request['project_id']);
            $d1 = date_parse_from_format("d/m/Y",$request['dateDentree']);
            $d2 = date_parse_from_format("d/m/Y",$request['dateDeSortie']);
            $dateEntre = date("Y-m-d", strtotime($d1['year']."-".$d1['month']."-".$d1['day']));
            $dateSortie = date("Y-m-d", strtotime($d2['year']."-".$d2['month']."-".$d2['day']));

            $insert = new Debrief();
            $insert->libelle = $request['libelle'];
            $insert->proposition = $request['proposition'];
            $insert->cible = $request['cible'];
            $insert->dateDentree = $dateEntre;
            $insert->dateDeSortie = $dateSortie;
            $insert->taf = $request['taf'];
            $insert->dep = $request['dep'];
            $insert->user_id = $request['user_id'];
            $insert->brief_id = $insert_id;
            $insert->project_id = $request['project_id'];
            $insert->asset=$asset;
            $insert->status='1';
            $insert->niveau=$project->niveau;
            //dd($file);
            if($file){
                $folderName ='briefFiles/';
                $originalExt = $file->getClientOriginalExtension();
                $picture = str_replace(' ','',$request['libelle']).str_random(2).'.'. $originalExt;
                $insert->pj = $picture;
                $insert->save();
                $file->move($folderName,$picture);
            }else{
                $insert->save();
            }

            $usersValid = User::select('id','email')->where('post','DCOMM')->get();
            //dd($usersValid[0]->id);

            foreach ($usersValid as $user){
                $notif = new Notification();
                $notif->libelle = 'brief_a_valide';
                $notif->send_id = Auth::user()->id;
                $notif->recip_id = $user->id;
                $notif->debrief_id = $insert_id;
                $notif->vue = '0';
                $notif->save();

                //NOTIFICATION PAR MAIL
                $data=array(
                    "notifmessage"=>"Brief en attente de validation",
                );
                //$findUserMail = User::where('id',$debr->user_id)->first();
                $emailMail = $user->email;
                Mail::send('mails/valid',$data,function ($message) use($emailMail){
                    $message->to($emailMail);
                    $message->subject('Brief en attente de validation');
                });
            }

            return redirect()->back();
			//return redirect()->route(config('laraadmin.adminRoute') . '.briefs.index');
			
		} else {
            return redirect("/");
		}
	}

	/**
	 * Display the specified brief.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Briefs", "view")) {
			
			$brief = Brief::find($id);
			$briefID=$id;
			$alldepartments = Department::whereIn('id',['6','9'])->get();
			$project_id = $brief->project_id;
			$projectCkeck = Projet::find($project_id);
            //$taches = Tach::where('brief_id', $id)->with('userTache')->get();
            $debriefs = Debrief::where('brief_id', $id)->with('userDebrief')->with('brief')->orderBy('id','desc')->get();
            $debriefsCount = Debrief::where('project_id', $project_id)->where('niveau',$projectCkeck->niveau)->count();
            //dd($debriefsCount);
            //$usersDepartmts ='';
            //if(Auth::user()->role_id == 5){
              //  $usersDepartmts = User::where('departement_id',Auth::user()->departement_id)->where('role_id','!=',Auth::user()->role_id)->get();
            //}

			if(isset($brief->id)) {
				$module = Module::get('Briefs');
                //$moduleTache = Module::get('Taches');
                $moduleDebrief = Module::get('Debriefs');
                //dd($moduleDebrief);
				$module->row = $brief;
				
				return view('la.briefs.show', [
                    'module' => $module,
                    //'moduleTache' => $moduleTache,
                    'moduleDebrief'=>$moduleDebrief,
					'view_col' => $this->view_col,
                    'show_actions' => $this->show_action,
					'no_header' => true,
                    'project_id' => $project_id,
                    'nvproject' =>$projectCkeck,
                    'debriefsCount' => $debriefsCount,
					'no_padding' => "no-padding",
                    'listing_cols_tache' => $this->listing_cols_tache,
                    'alldepartments'=>$alldepartments,
                    //'usersDepartmts' => $usersDepartmts,
                    //'taches' => $taches,
                    'briefID'=>$briefID,
                    'debriefs'=>$debriefs
				])->with('brief', $brief);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("brief"),
				]);
			}
		} else {
            return redirect("/");
		}
	}

	/**
	 * Show the form for editing the specified brief.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Briefs", "edit")) {			
			$brief = Brief::find($id);
			if(isset($brief->id)) {	
				$module = Module::get('Briefs');
				
				$module->row = $brief;
				
				return view('la.briefs.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('brief', $brief);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("brief"),
				]);
			}
		} else {
			return redirect("/");
		}
	}

	/**
	 * Update the specified brief in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
        //dd($request->all());
		if(Module::hasAccess("Briefs", "edit")) {
			
			$rules = Module::validateRules("Briefs", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Briefs", $request, $id);
            $asset = implode(",", $request['asset']);
			$insert = Brief::find($id);
            $insert->status = $request['status'];
            $insert->asset=$asset;
            $insert->save();
            //dd($request['project_id']);

            $debrifone = Debrief::where('brief_id',$id)->first();
            if($debrifone){
                $d1 = date_parse_from_format("d/m/Y",$request['dateDentree']);
                $d2 = date_parse_from_format("d/m/Y",$request['dateDeSortie']);
                $dateEntre = date("Y-m-d", strtotime($d1['year']."-".$d1['month']."-".$d1['day']));
                $dateSortie = date("Y-m-d", strtotime($d2['year']."-".$d2['month']."-".$d2['day']));
                $debrifone->status = $request['status'];
                $debrifone->asset=$asset;
                $debrifone->libelle = $request['libelle'];
                $debrifone->proposition = $request['proposition'];
                $debrifone->cible = $request['cible'];
                $debrifone->dateDentree = $dateEntre;
                $debrifone->dateDeSortie = $dateSortie;
                $debrifone->taf = $request['taf'];
                $debrifone->dep = $request['dep'];
                $debrifone->user_id = $request['user_id'];
                $debrifone->brief_id = $insert_id;
                $debrifone->status='1';
                $debrifone->save();

            }
            //dd($debrifone);

            $usersValid = User::select('id','email')->where('post','DCOMM')->get();
            //dd($usersValid[0]->id);

            foreach ($usersValid as $user){
                $notif = new Notification();
                $notif->libelle = 'brief_a_valide';
                $notif->send_id = Auth::user()->id;
                $notif->recip_id = $user->id;
                $notif->vue = '0';
                $notif->save();

                //NOTIFICATION PAR MAIL
                $data=array(
                    "notifmessage"=>"Brief en attente de validation",
                );
                //$findUserMail = User::where('id',$debr->user_id)->first();
                $emailMail = $user->email;
                Mail::send('mails/valid',$data,function ($message) use($emailMail){
                    $message->to($emailMail);
                    $message->subject('Brief en attente de validation');
                });
            }


			return redirect(config('laraadmin.adminRoute').'/projets/'.$request['project_id']);
			
		} else {
			return redirect("/");
		}
	}

    public function pj(Request $request)
    {
        //dd($request->all());
        $file=$request->file('pj');

        if($file){
            $insert_id= Brief::find($request['brief_id']);

            $folderName ='briefFiles/';
            $originalExt = $file->getClientOriginalExtension();
            $picture = str_replace(' ','',$insert_id->libelle).str_random(2).'.'. $originalExt;

            if ($insert_id->pj!='[]') {
                unlink($folderName.$insert_id->pj);
            }
            $insert_id->pj = $picture;
            //$insert_id->status = $request['status'];
            $file->move($folderName,$picture);
            $insert_id->save();

            $usersValid = User::select('id','email')->where('post','DCOMM')->get();
            //dd($usersValid[0]->id);

            foreach ($usersValid as $user){
                $notif = new Notification();
                $notif->libelle = 'brief_a_valide';
                $notif->send_id = Auth::user()->id;
                $notif->recip_id = $user->id;
                $notif->brief_id = $insert_id;
                $notif->vue = '0';
                $notif->save();

                //NOTIFICATION PAR MAIL
                $data=array(
                    "notifmessage"=>"Brief en attente de validation",
                );
                //$findUserMail = User::where('id',$debr->user_id)->first();
                $emailMail = $user->email;
                Mail::send('mails/valid',$data,function ($message) use($emailMail){
                    $message->to($emailMail);
                    $message->subject('Brief en attente de validation');
                });
            }
            return redirect(config('laraadmin.adminRoute').'/projets/'.$request['project_id']);
        }else{
            return redirect(config('laraadmin.adminRoute').'/projets/'.$request['project_id']);
        }

    }

	/**
	 * Remove the specified brief from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Briefs", "delete")) {
			Brief::find($id)->delete();
			
			// Redirecting to index() method
            return redirect()->back();
			//return redirect()->route(config('laraadmin.adminRoute') . '.briefs.index');
		} else {
            return redirect("/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('briefs')->select($this->listing_cols)->orderBy('id','desc')->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Briefs');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/briefs/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Briefs", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/briefs/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Briefs", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.briefs.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
