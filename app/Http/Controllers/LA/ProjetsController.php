<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Models\Annonceur;
use App\Models\Asset;
use App\Models\Assetop;
use App\Models\Assetype;
use App\Models\Bcommande;
use App\Models\Checklist;
use App\Models\Debrief;
use App\Models\Department;
use App\Models\Devi;
use App\Models\Mail;
use App\Models\Notification;
use App\Models\Soumission;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Projet;
use App\Models\Brief;

class ProjetsController extends Controller
{
	public $show_action = true;
	public $view_col = 'operation';
	public $listing_cols = ['id', 'operation', 'slug', 'annonceur', 'dateDentree']; /*'userId',, 'dateDeSortie'*/
	public $listing_cols_brief = ['id', 'libelle','proposition','cible', 'taf', 'dateDentree', 'status'];
    //public $listing_cols_bc = ['id', 'libelle', 'path', 'Commercial'];

	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Projets', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Projets', $this->listing_cols);
		}
	}

	/**
	 * Display a listing of the Projets.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
	    //dd($this->listing_cols);
		$module = Module::get('Projets');
        if(Auth::user()->role_id==4){
		    $projects = Projet::where('niveau',1)->where('userId',Auth::user()->id)->with('annonceurProject')->orderBy('id','desc')->get();
        }else{
            $projects = Projet::where('niveau',1)->with('annonceurProject')->orderBy('id','desc')->get();
        }
        //dd($projects);
		if(Module::hasAccess($module->id)) {
			return View('la.projets.index', [
				'show_actions' => $this->show_action,
                'listing_cols' => $this->listing_cols,
                'projects' => $projects,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new projet.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created projet in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	    //dd($request->all());
		if(Module::hasAccess("Projets", "create")) {

			$rules = Module::validateRules("Projets", $request);

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			$libelannonceur = Annonceur::find($request['annonceur']);
			$cunt = Projet::where('annonceur',$request['annonceur'])->count();
			$id= $cunt + 1 ;
			$lt3 = substr($libelannonceur->name, 0, 3);
            $dat = new \DateTime('now');
            $datnow = $dat->format('dmy');
            $slug = $lt3.$datnow.$id;

            $insert_id = new Projet();
            $insert_id->slug = $slug ;
            $insert_id->operation = $request['operation'];
            $insert_id->annonceur = $request['annonceur'];
            $d2 = date_parse_from_format("d/m/Y",$request['dateDentree']);
            $dateEntre = date("Y-m-d", strtotime($d2['year']."-".$d2['month']."-".$d2['day']));
            $insert_id->dateDentree = $dateEntre;
            $insert_id->userId = $request['userId'];
            $insert_id->status = '1';
            $insert_id->save();

			//dd($cunt+1,$libelannonceur->name,$lt3,$datnow);
			//$insert_id = Module::insert("Projets", $request);

			return redirect()->route(config('laraadmin.adminRoute') . '.projets.index');

		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified projet.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Projets", "view")) {

            $projet = Projet::find($id);
            $briefs = Brief::where('project_id', $id)->orderBy('id','desc')->get();
            $debriefs = Debrief::where('project_id', $id)->orderBy('id','desc')->get();
            $bc = Bcommande::where('project_id',$id)->with('userBC')->orderBy('id','desc')->get();
            $dp = Devi::where('project_id',$id)->with('userDP')->orderBy('id','desc')->get();
            $mails = Mail::where('project_id',$id)->with('userMail')->orderBy('id','desc')->get();

            $alldepartments = Department::whereIn('id',['6','9'])->get();

            $checklistAlluser = DB::table('project_checklist')
                ->join('checklists', 'checklists.id', '=', 'project_checklist.checklist_id')
                ->where('project_checklist.project_id', $id)
                ->whereNull('project_checklist.deleted_at')
                ->select('checklists.*','project_checklist.id as idtp')->orderBy('checklists.niveau','desc')->get();
            //dd($checklistAlluser);
            $sql = DB::table('checklists')->where('departement',Auth::user()->departement_id)->where('niveau',$projet->niveau)->get();
            foreach($sql as $value) {
                $show_checklist[] = DB::table('project_checklist')
                    ->where('checklist_id', $value->id)
                    ->where('project_id', $projet->id)
                    ->whereNull('deleted_at')->get();

                $uncheck[] = DB::table('project_checklist')
                    ->join('checklists', 'checklists.id', '=', 'project_checklist.checklist_id')
                    ->where('project_checklist.checklist_id', $value->id)
                    ->where('project_checklist.project_id', $projet->id)
                    ->where('project_checklist.deleted_at','!=', null)
                    ->select('*')
                    ->get();
            }

            //Liste des check uncheck
            $uncheck = array_filter($uncheck);
            $show_checklist = array_filter($show_checklist);

            $soumis = Soumission::where('project_id',$id)->with('userSomi')->with('projectSomi')->orderBy('id','desc')->get();
            $iddebrief1 = Debrief::where('project_id',$id)->pluck('id')->toArray();

            $assetColors = ['maroon','green','red','olive','orange','navy','purple','yellow','blackmini','kaki','myblue','myviolet','fonce','myred','myorange','mygreen','myyello','myblue2'];
            //TEAMWORK
                //COMMERCIAL
                $userComms = User::where('id',$projet->userId)->with('departUsers')->get();
                //ADD ASSET
                $uAsset = Asset::whereIn('debrief_id',$iddebrief1)->pluck('user_id')->toArray();
                $userAsserts = User::whereIn('id',$uAsset)->with('departUsers')->get();
                //ADD DEPARTMT
                $uDep = Debrief::where('project_id', $id)->pluck('dep')->toArray();
                $userDep = User::whereIn('departement_id',$uDep)->with('departUsers')->where('role_id',5)->get();

			if(isset($projet->id)){
				$module = Module::get('Projets');
                $moduleBrief = Module::get('Briefs');
                $moduleMail = Module::get('Mails');
                //$moduleOP = Module::get('Assetops');
                //$moduleBC = Module::get('Bcommandes');
				$module->row = $projet;

				return view('la.projets.show', [
					'module' => $module,
                    'moduleBrief' => $moduleBrief,
                    'moduleMail' => $moduleMail,
                    //'moduleOP' => $moduleOP,
                    //'moduleBC' => $moduleBC,
                    'userComms' => $userComms,
                    'userAsserts' => $userAsserts,
                    'userDep' => $userDep,
                    'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding",
					'listing_cols_brief' => $this->listing_cols_brief,
					'show_actions' => $this->show_action,
                    'briefs' => $briefs,
                    'debriefs' => $debriefs,
                    'listsFormCheck' => $sql,
                    'checklistsShow' => $show_checklist,
                    'uncheck' => $uncheck,
                    'checklistAlluser'=>$checklistAlluser,
                    'alldepartments'=>$alldepartments,
                    /*'listsFormCheck' => $checklists,
                    'checklistsShow' => $queryShow,*/
                    'assetcolors'=>$assetColors,
                    'bc' => $bc,
                    'dp' => $dp,
                    'mails' => $mails,
                    'soumis' => $soumis,
				])->with('projet', $projet);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("projet"),
				]);
            }

		} else {
            return redirect("/");
		}
	}

	/**
	 * Show the form for editing the specified projet.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Projets", "edit")) {
			$projet = Projet::find($id);
			if(isset($projet->id)) {
				$module = Module::get('Projets');

				$module->row = $projet;

				return view('la.projets.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('projet', $projet);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("projet"),
				]);
			}
		} else {
            return redirect("/");
		}
	}

	/**
	 * Update the specified projet in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Projets", "edit")) {

			$rules = Module::validateRules("Projets", $request, true);

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}

			$insert_id = Module::updateRow("Projets", $request, $id);

            //return redirect()->route(config('laraadmin.adminRoute') . '.projets.index');
            return redirect()->back()->with('success','Le Job Bag a été modifié');

		} else {
            return redirect("/");
		}
	}

	/**
	 * Remove the specified projet from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Projets", "delete")) {
            $brief = Brief::where('project_id',$id)->count();
		    if($brief == 0){
                Projet::find($id)->delete();
                return redirect()->back()->with('success','Le Job Bag a été supprimé');
            }
            return redirect()->back()->with('error','Le Job Bag ne peut pas etre supprimé');
			// Redirecting to index() method
			//return redirect()->route(config('laraadmin.adminRoute') . '.projets.index');
		} else {
            return redirect("/");
		}
	}

	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('projets')->where('niveau',1)->select($this->listing_cols)->orderBy('id','desc')->whereNull('deleted_at');
		//dd($values);
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Projets');

		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) {
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/projets/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}

			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Projets", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/projets/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}

				if(Module::hasAccess("Projets", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.projets.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

	public function second(){
        if(Auth::user()->role_id==4){
            $projects = Projet::where('niveau',2)->where('userId',Auth::user()->id)->with('annonceurProject')->get();
        }else{
            $projects = Projet::where('niveau',2)->with('annonceurProject')->get();
        }
        //dd($projects);
        return view('la.projets.niv2',compact('projects'));
    }

    public function third(){
        if(Auth::user()->role_id==4){
            $projects = Projet::where('niveau',3)->where('userId',Auth::user()->id)->with('annonceurProject')->get();
        }else{
            $projects = Projet::where('niveau',3)->with('annonceurProject')->get();
        }
        //dd($projects);
        return view('la.projets.niv3',compact('projects'));
    }

    public function lists(){
        if(Auth::user()->role_id==4){
            $project1 = Projet::where('niveau',1)->where('userId',Auth::user()->id)->with('userProject')->with('annonceurProject')->get();
            $project2 = Projet::where('niveau',2)->where('userId',Auth::user()->id)->with('userProject')->with('annonceurProject')->get();
            $project3 = Projet::where('niveau',3)->where('userId',Auth::user()->id)->with('userProject')->with('annonceurProject')->get();
        }else{
            $project1 = Projet::where('niveau',1)->with('userProject')->with('annonceurProject')->get();
            $project2 = Projet::where('niveau',2)->with('userProject')->with('annonceurProject')->get();
            $project3 = Projet::where('niveau',3)->with('userProject')->with('annonceurProject')->get();
        }
        //dd($projects);
        return view('la.projets.lists',compact('project1','project2','project3'));
    }

    public function pjtadd(){
        $module = Module::get('Projets');
        if(Auth::user()->role_id==4){
            $annoncs = Annonceur::where('user_id',Auth::user()->id)->get();
        }else{
            $annoncs = Annonceur::get();
        }
        //dd(Auth::user()->role_id);
        if(Module::hasAccess($module->id)) {
            return View('la.projets.pjtadd', [
                'module' => $module,
                'annoncs' => $annoncs
            ]);
        } else {
            return redirect("/");
        }
        //return view('la.projets.pjtadd');
    }

    public function showasset($id,$type){
        //dd($id,$type);
        $libelAsset = Assetype::where('name',$type)->first();
        $asset_type = $libelAsset->libelle;

        $idpjt = $id;
        $debriefid = Debrief::where('project_id',$id)->select('id')->get()->toArray();
        $asset_data = Asset::whereIn('debrief_id',$debriefid)->where('asset_type',$type)->get();
        return view('la.projets.showasset',compact('asset_data','asset_type','idpjt'));
        //dd($asset_data);
    }

    public function showassetrq(Request $request){
        if (!isset($request)){
            return url('/');
        }
        //dump($request->all());
        $idd = $request['idpjt'];
        $iddebrief = Debrief::where('project_id', $idd)->pluck('id')->toArray();
        $debrief = $request['debrief'];
        $nv = $request['niveau'];

        $databasetable = DB::table('assets');

        if(isset($debrief) && !empty($debrief))
            {$databasetable->where('debrief_id',$debrief);}

        if(isset($nv) && !empty($nv))
            {$databasetable->where('niveau',$nv);}

        if(empty($debrief) && empty($nv))
            {$databasetable->whereIn('debrief_id',$iddebrief);}

        $asset_data = $databasetable->get();
        //dd($asset_data);

        return view('la.projets.showassetbypjt',compact('asset_data','idd'));
        //dd($asset_data);
    }

    public function addcheck(Request $request){
        //dd($request->all());
        foreach ($request['check'] as $check){
            $insertCheck = DB::table('project_checklist')->insert(['checklist_id'=>$check,'project_id' => $request['project_id']]);;
        }
        return redirect()->back();
    }

    public function delcheck($id){
        //dd($id);
        $nv1 = DB::table('project_checklist')
            ->where('id', $id)
            ->delete();
        return redirect()->back();
    }

    public function changephasetwo($id){
        //dd($id);
        if($id){
            $project = Projet::find($id);
            if(isset($project->id)){
                $project->niveau = '2';
                $project->save();

                $findUserMail2 = User::where('post','ACHAT')->first();
                $findUserMail3 = User::where('post','OP')->first();
                if($findUserMail2){
                    $notif = new Notification();
                    $notif->libelle = 'new_phase2';
                    $notif->send_id = Auth::user()->id;
                    $notif->recip_id = $findUserMail2->id;
                    $notif->vue = '0';
                    $notif->save();

                    $data2=array(
                        "notifmessage"=>"Un job bag en passer en phase 2",
                    );
                    $emailMail2 = $findUserMail2->email;
                    \Illuminate\Support\Facades\Mail::send('mails/valid',$data2,function ($message) use($emailMail2){
                        $message->to($emailMail2);
                        $message->subject('PROJECT EN PHASE 2');
                    });
                }
                if($findUserMail3){
                    $notif = new Notification();
                    $notif->libelle = 'new_phase2';
                    $notif->send_id = Auth::user()->id;
                    $notif->recip_id = $findUserMail3->id;
                    $notif->vue = '0';
                    $notif->save();

                    $data2=array(
                        "notifmessage"=>"Un job bag en passer en phase 2",
                    );
                    $emailMail2 = $findUserMail2->email;
                    \Illuminate\Support\Facades\Mail::send('mails/valid',$data2,function ($message) use($findUserMail3){
                        $message->to($findUserMail3);
                        $message->subject('PROJECT EN PHASE 2');
                    });
                }


                return redirect()->back()->with('sucess','Le project est en phase 2');
            }
            return redirect()->back()->with('error','Désolé ! veuillez choisir un project');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez choisir un project');
        }
    }

    public function changephasethree($id){
        if($id){
            $project = Projet::find($id);
            if(isset($project->id)){
                $project->niveau = '3';
                $project->save();
                return redirect()->back()->with('sucess','Le project est en phase 3');
            }
            return redirect()->back()->with('error','Désolé ! veuillez choisir un project');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez choisir un project');
        }
    }

    /*public function sendnv3(Request $request){
        //dd($request->all());
        $usersValid = User::select('id')->where('role_id',7)->get();
        foreach ($usersValid as $user){
            $notif = new Notification();
            $notif->libelle = 'nv3_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $user->id;
            $notif->project_id = $request['project_id'];
            $notif->vue = '0';
            $notif->save();
        }
        return redirect()->back();
    }*/

    public function typAssetOP(){
        $asset = [ 'fichefabric','retroplanning','bcp' ];
        $listAssetypes = Assetype::select('name','libelle')->whereIn('name',$asset)->get();
        return $assetypes = $listAssetypes->toArray();
    }

    public function nbAssetByBrief($id,$type){
        $iddebrief1 = Debrief::where('project_id',$id)->pluck('id')->toArray();
        $countAsset = Asset::whereIn('debrief_id',$iddebrief1)->where('asset_type',$type)->count();
        return $countAsset ;
    }
}
