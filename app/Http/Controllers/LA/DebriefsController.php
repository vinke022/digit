<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Models\Asset;
use App\Models\Mail;
use App\Models\Projet;
use App\Models\Tach;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Debrief;

class DebriefsController extends Controller
{
	public $show_action = true;
	public $view_col = 'libelle';
	public $listing_cols = ['id', 'libelle', 'dateDentree', 'dateDeSortie', 'taf', 'path', 'dep', 'project_id', 'user_id', 'brief_id', 'status', 'asset'];
    public $listing_cols_tache = ['id', 'libelle', 'description', 'assigne','status'];

	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Debriefs', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Debriefs', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Debriefs.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Debriefs');
		
		if(Module::hasAccess($module->id)) {
			return View('la.debriefs.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new debrief.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created debrief in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	    //dd($request->all());
		if(Module::hasAccess("Debriefs", "create")) {
		
			$rules = Module::validateRules("Debriefs", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			//$insert_id = Module::insert("Debriefs", $request);
            $file=$request->file('file');
            $asset = implode(",", $request['asset']);
            //dd($file);

            $d1 = date_parse_from_format("d/m/Y",$request['dateDentree']);
            $d2 = date_parse_from_format("d/m/Y",$request['dateDeSortie']);
            $dateEntre = date("Y-m-d", strtotime($d1['year']."-".$d1['month']."-".$d1['day']));
            $dateSortie = date("Y-m-d", strtotime($d2['year']."-".$d2['month']."-".$d2['day']));

            $insert= new Debrief();
            $project = Projet::find($request['project_id']);
            $insert->libelle = $request['libelle'];
            $insert->proposition = $request['proposition'];
            $insert->cible = $request['cible'];
            $insert->dateDentree = $dateEntre;
            $insert->dateDeSortie = $dateSortie;
            $insert->taf = $request['taf'];
            $insert->dep = $request['dep'];
            $insert->user_id = $request['user_id'];
            $insert->brief_id = $request['brief_id'];
            $insert->project_id = $request['project_id'];
            $insert->asset=$asset;
            $insert->status='1';
            $insert->niveau=$project->niveau;

            if($file){
                $folderName ='briefFiles/';
                $originalExt = $file->getClientOriginalExtension();
                $picture = str_replace(' ','',$request['libelle']).str_random(2).'.'. $originalExt;
                $insert->pj = $picture;
                $file->move($folderName,$picture);
            }

            $insert->save();

            //GERER LES NOTIFS

            return redirect()->back();
			//return redirect()->route(config('laraadmin.adminRoute') . '.debriefs.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified debrief.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Debriefs", "view")) {
			
			$debrief = Debrief::find($id);
            $brief_id= $debrief->brief_id;

            $mails = Mail::where('debrief_id',$id)->with('userMail')->orderBy('id','desc')->get();
            $taches = Tach::where('debrief_id', $id)->where('assignant_id',Auth::user()->id)->with('userTache')->get();
            $usersDepartmts ='';
            if(Auth::user()->role_id == 5 or Auth::user()->role_id == 8 ){
                if(Auth::user()->post == 'TRAFIC'){
                    $usersDepartmts = User::whereNotIn('role_id',[1,3,4,7,8])->get();
                }else{
                    $usersDepartmts = User::where('departement_id',Auth::user()->departement_id)->get();
                    //->where('role_id','!=',Auth::user()->role_id)
                }
            }

            $assetColors = ['maroon','green','yellow','red','olive','orange','navy','purple','blackmini','kaki','myblue','myviolet','fonce','myred','myorange','mygreen','myyello','myblue2'];

            $assetReco = Asset::where('debrief_id',$id)->where('asset_type','reco')->count();


			if(isset($debrief->id)) {
				$module = Module::get('Debriefs');
                $moduleTache = Module::get('Taches');
                $moduleMail = Module::get('Mails');
				$module->row = $debrief;
				
				return view('la.debriefs.show', [
					'module' => $module,
                    'moduleTache' => $moduleTache,
                    'moduleMail' => $moduleMail,
                    'show_actions' => $this->show_action,
					'view_col' => $this->view_col,
					'no_header' => true,

                    'assetcolors'=>$assetColors,
                    //'assetDigit' => $assetDigit,
                    'usersDepartmts' => $usersDepartmts,
                    'listing_cols_tache' => $this->listing_cols_tache,
                    'taches' => $taches,
                    'assetReco' => $assetReco,
                    'mails' => $mails,
                    'brief_id' => $brief_id,
					'no_padding' => "no-padding"
				])->with('debrief', $debrief);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("debrief"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified debrief.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Debriefs", "edit")) {			
			$debrief = Debrief::find($id);
			if(isset($debrief->id)) {	
				$module = Module::get('Debriefs');
				
				$module->row = $debrief;
				
				return view('la.debriefs.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('debrief', $debrief);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("debrief"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified debrief in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
        //dd($request->all());
		if(Module::hasAccess("Debriefs", "edit")) {
			
			$rules = Module::validateRules("Debriefs", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}

            $d1 = date_parse_from_format("d/m/Y",$request['dateDentree']);
            $d2 = date_parse_from_format("d/m/Y",$request['dateDeSortie']);
            $dateEntre = date("Y-m-d", strtotime($d1['year']."-".$d1['month']."-".$d1['day']));
            $dateSortie = date("Y-m-d", strtotime($d2['year']."-".$d2['month']."-".$d2['day']));
            $asset = implode(",", $request['asset']);
			//$insert_id = Module::updateRow("Debriefs", $request, $id);
            $insert = Debrief::find($id);
            $insert->libelle = $request['libelle'];
            $insert->proposition = $request['proposition'];
            $insert->cible = $request['cible'];
            $insert->dateDentree = $dateEntre;
            $insert->dateDeSortie = $dateSortie;
            $insert->taf = $request['taf'];
            $insert->dep = $request['dep'];
            //$insert->user_id = $request['user_id'];
            //$insert->brief_id = $request['brief_id'];
            //$insert->project_id = $request['project_id'];
            $insert->asset=$asset;
            $insert->status='1';
            //$insert->niveau=$project->niveau;
            $insert->save();
			
			//return redirect()->route(config('laraadmin.adminRoute') . '.debriefs.index');
            return redirect()->back()->with('success','Le debrief a été modifié');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified debrief from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Debriefs", "delete")) {
			Debrief::find($id)->delete();
			
			// Redirecting to index() method
			//return redirect()->route(config('laraadmin.adminRoute') . '.debriefs.index');
            return redirect()->back();
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('debriefs')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Debriefs');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/debriefs/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Debriefs", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/debriefs/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Debriefs", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.debriefs.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

    public function pj(Request $request)
    {
        //dd($request->all());
        $file=$request->file('pj');

        if($file){
            $insert_id= Debrief::find($request['debrief_id']);

            $folderName ='briefFiles/';
            $originalExt = $file->getClientOriginalExtension();
            $picture = str_replace(' ','',$insert_id->libelle).str_random(2).'.'. $originalExt;

            if ($insert_id->pj!='[]') {
                unlink($folderName.$insert_id->pj);
            }
            $insert_id->pj = $picture;
            //$insert_id->status = $request['status'];
            $file->move($folderName,$picture);
            $insert_id->save();

            $usersValid = User::select('id')->where('post','DCOMM')->get();
            //dd($usersValid[0]->id);

            //GERER LA NOTIF
            /*foreach ($usersValid as $user){
                $notif = new Notification();
                $notif->libelle = 'brief_a_valide';
                $notif->send_id = Auth::user()->id;
                $notif->recip_id = $user->id;
                $notif->brief_id = $insert_id;
                $notif->vue = '0';
                $notif->save();
            }*/
            return redirect(config('laraadmin.adminRoute').'/projets/'.$request['project_id']);
        }else{
            return redirect(config('laraadmin.adminRoute').'/projets/'.$request['project_id']);
        }

    }

    public function nbAssetByBrief($id,$type){
        $countAsset = Asset::where('debrief_id',$id)->where('asset_type',$type)->count();
        return $countAsset ;
    }
}
