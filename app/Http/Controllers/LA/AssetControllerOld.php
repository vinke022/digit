<?php

namespace App\Http\Controllers\LA;

use App\Asset;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AssetController extends Controller
{
    public function __construct() {
		$this->middleware('auth');
	}

	public function index($id,$type)
	{
		$assets = Asset::where('brief_id',$id)->where('type',$type)->get();
		//dd($assets);
		return view('la.assets.index',compact('assets'));
		
	}
}
