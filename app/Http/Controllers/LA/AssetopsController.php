<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Assetop;

class AssetopsController extends Controller
{
	public $show_action = true;
	public $view_col = 'libelle';
	public $listing_cols = ['id', 'libelle', 'path', 'ext', 'status', 'type', 'project_id', 'user_id'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Assetops', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Assetops', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Assetops.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Assetops');
		
		if(Module::hasAccess($module->id)) {
			return View('la.assetops.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new assetop.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created assetop in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	    //dd($request->all());
		if(Module::hasAccess("Assetops", "create")) {
            $file=$request->file('file');
		
			$rules = Module::validateRules("Assetops", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			//$insert_id = Module::insert("Assetops", $request);
            $originalExt = $file->getClientOriginalExtension();
            $extension = $originalExt ?: 'png';
            $folderName ='assetFiles/';
            $picture = str_replace(' ','',$request['libelle']).str_random(2).'.'. $extension;

            $insert_id = new Assetop();
            $insert_id->libelle = $request['libelle'] ;
            $insert_id->project_id = $request['project_id'] ;
            $insert_id->user_id = $request['user_id'];
            $insert_id->type = $request['type'];
            $insert_id->ext = $originalExt ;
            $insert_id->path = $picture;
            $insert_id->save() ;

            $file->move($folderName,$picture);

            //GERER lES NOTIFI ICI

            return redirect()->back();
			
			//return redirect()->route(config('laraadmin.adminRoute') . '.assetops.index');
			
		} else {
			return redirect("/");
		}
	}

	/**
	 * Display the specified assetop.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Assetops", "view")) {
			
			$assetop = Assetop::find($id);
			if(isset($assetop->id)) {
				$module = Module::get('Assetops');
				$module->row = $assetop;
				
				return view('la.assetops.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('assetop', $assetop);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("assetop"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified assetop.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Assetops", "edit")) {			
			$assetop = Assetop::find($id);
			if(isset($assetop->id)) {	
				$module = Module::get('Assetops');
				
				$module->row = $assetop;
				
				return view('la.assetops.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('assetop', $assetop);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("assetop"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified assetop in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Assetops", "edit")) {
			
			$rules = Module::validateRules("Assetops", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Assetops", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.assetops.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified assetop from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Assetops", "delete")) {
			Assetop::find($id)->delete();
			
			// Redirecting to index() method
            return redirect()->back();
            //return redirect()->route(config('laraadmin.adminRoute') . '.assetops.index');
		} else {
			return redirect("/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('assetops')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Assetops');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/assetops/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Assetops", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/assetops/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Assetops", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.assetops.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
