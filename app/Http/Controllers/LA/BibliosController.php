<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Biblio;

class BibliosController extends Controller
{
	public $show_action = true;
	public $view_col = 'libelle';
	public $listing_cols = ['id', 'libelle', 'type', 'path', 'ext'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Biblios', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Biblios', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Biblios.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Biblios');
		
		if(Module::hasAccess($module->id)) {
			return View('la.biblios.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new biblio.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created biblio in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	    //dd($request->all());
		if(Module::hasAccess("Biblios", "create")) {
            $file=$request->file('file');
		
			$rules = Module::validateRules("Biblios", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			//$insert_id = Module::insert("Biblios", $request);
            $originalExt = $file->getClientOriginalExtension();
            $extension = $originalExt ?: 'png';
            $folderName ='bibliosFiles/';
            $picture = str_replace(' ','',$request['libelle']).str_random(2).'.'. $extension;

            $insert_id = new Biblio();
            $insert_id->libelle = $request['libelle'] ;
            $insert_id->type = $request['type'] ;
            $insert_id->ext = $originalExt ;
            $insert_id->path = $picture;
            $insert_id->save() ;

            $file->move($folderName,$picture);


			return redirect()->route(config('laraadmin.adminRoute') . '.biblios.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified biblio.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Biblios", "view")) {
			
			$biblio = Biblio::find($id);
			if(isset($biblio->id)) {
				$module = Module::get('Biblios');
				$module->row = $biblio;
				
				return view('la.biblios.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('biblio', $biblio);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("biblio"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified biblio.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Biblios", "edit")) {			
			$biblio = Biblio::find($id);
			if(isset($biblio->id)) {	
				$module = Module::get('Biblios');
				
				$module->row = $biblio;
				
				return view('la.biblios.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('biblio', $biblio);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("biblio"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified biblio in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Biblios", "edit")) {
			
			$rules = Module::validateRules("Biblios", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Biblios", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.biblios.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified biblio from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Biblios", "delete")) {
			Biblio::find($id)->delete();
			
			// Redirecting to index() method
			return redirect()->route(config('laraadmin.adminRoute') . '.biblios.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('biblios')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Biblios');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/biblios/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				/*if(Module::hasAccess("Biblios", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/biblios/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}*/
				
				if(Module::hasAccess("Biblios", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.biblios.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
