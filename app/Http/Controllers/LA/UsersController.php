<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Support\Facades\Hash;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\User;

class UsersController extends Controller
{
    public $show_action = true;
    public $view_col = 'name';
    public $listing_cols = ['id', 'name','email', 'role_id', 'departement_id', 'post'];

    public function __construct() {
        // Field Access of Listing Columns
        if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
            $this->middleware(function ($request, $next) {
                $this->listing_cols = ModuleFields::listingColumnAccessScan('Users', $this->listing_cols);
                return $next($request);
            });
        } else {
            $this->listing_cols = ModuleFields::listingColumnAccessScan('Users', $this->listing_cols);
        }
    }

    /**
     * Display a listing of the Users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $module = Module::get('Users');

        if(Module::hasAccess($module->id)) {
            return View('la.users.index', [
                'show_actions' => $this->show_action,
                'listing_cols' => $this->listing_cols,
                'module' => $module
            ]);
        } else {
            return redirect("/");
        }
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created user in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Module::hasAccess("Users", "create")) {

            $rules = Module::validateRules("Users", $request);

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $role_id = $request->role_id;

            $insert_id = Module::insert("Users", $request);

            //Store user_id & role_id in role_users
            $query = DB::table('role_user')
                ->select('*')
                ->where('user_id', $insert_id)
                ->count();

            if($query == 0) {
                DB::insert('insert into role_user (user_id, role_id) values (?, ?)', array($insert_id, $role_id));
            }
            else{
                return redirect()->route(config('laraadmin.adminRoute') . '.users.index');
            }

            return redirect()->route(config('laraadmin.adminRoute') . '.users.index');

        } else {
            return redirect("/");
        }
    }

    /**
     * Display the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Module::hasAccess("Users", "view")) {

            $user = User::find($id);
            if(isset($user->id)) {
                $module = Module::get('Users');
                $module->row = $user;

                return view('la.users.show', [
                    'module' => $module,
                    'view_col' => $this->view_col,
                    'no_header' => true,
                    'no_padding' => "no-padding"
                ])->with('user', $user);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("user"),
                ]);
            }
        } else {
            return redirect("/");
        }
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Module::hasAccess("Users", "edit")) {
            $user = User::find($id);
            if(isset($user->id)) {
                $module = Module::get('Users');

                $module->row = $user;

                return view('la.users.edit', [
                    'module' => $module,
                    'view_col' => $this->view_col,
                ])->with('user', $user);
            } else {
                return view('errors.404', [
                    'record_id' => $id,
                    'record_name' => ucfirst("user"),
                ]);
            }
        } else {
            return redirect("/");
        }
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Module::hasAccess("Users", "edit")) {

            $rules = Module::validateRules("Users", $request, true);

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();;
            }

            $insert_id = Module::updateRow("Users", $request, $id);

            //Update user_id & role_id in role_users
            $role_id = $request->role_id;
            $query = DB::table('role_user')
                ->select('*')
                ->where('user_id', $id)
                ->count();

            if($query == 0) {
                DB::insert('insert into role_user (user_id, role_id) values (?, ?)', array($insert_id, $role_id));
            }
            else{
                DB::update('update role_user set role_id ='.$role_id.' where user_id = ?', array($id));
            }

            return redirect()->route(config('laraadmin.adminRoute') . '.users.index');

        } else {
            return redirect("/");
        }
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Module::hasAccess("Users", "delete")) {
            User::find($id)->delete();
            DB::delete('delete from role_user where user_id = ?', array($id));

            // Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '.users.index');
        } else {
            return redirect("/");
        }
    }

    /**
     * Datatable Ajax fetch
     *
     * @return
     */
    public function dtajax()
    {
        $values = DB::table('users')->select($this->listing_cols)->whereNull('deleted_at');
        $out = Datatables::of($values)->make();
        $data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Users');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/users/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Users", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/users/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Users", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.users.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

    /**
     * @return array
     */
    public function profil()
    {
        return view('la.users.profile');
    }

    public function updateInfo(Request $request){
        //dd($request->all());
        $reponse = $request->except(['_token']);
        $user = User::where('id',Auth::user()->id)->firstOrFail();
        $user->name = $reponse['nom'];
        $user->save();
        return redirect()->back()->with('success','✔ Félicitation ! modification a été modifié');
    }

    public function updatePass(Request $request){
        $reponse = $request->except(['_token']);

        $oldpass = $reponse['motpass'];
        $newpass = $reponse['newpass'];
        $newpassconfirm = $reponse['confirmpass'];
        $passUser = Auth::user()->password;
        if($newpass===$newpassconfirm)
        {
            if(Hash::check($oldpass,$passUser) ){

                $user = User::find(Auth::user()->id);
                $user->password = Hash::make($newpass);
                $user->save();

                return redirect()->back()->with('success','Félicitation votre mot de passe a été mise a jour');
            }else{
                return redirect()->back()->with('error','Désolé ! votre mot de passe actuel est erronée');
            }
        }else{
            return redirect()->back()->with('error','Les mots de passe sont différents');
        }
    }

    public function updateAvatar(Request $request){
        $file=$request->file('fileUser');
        $fileSize=$file->getSize();
        if($fileSize <= 3000263):
            $extension = $file->getClientOriginalExtension() ?: 'png';
            //$folderName = '../public_html/piges/panneau';
            $folderName ='userAvatar/';
            $picture = str_random(8).'.'. $extension;

            if (!empty(Auth::user()->img)) {
                unlink($folderName.Auth::user()->img);
            }
            $users= User::find(Auth::user()->id);
            $users->img = $picture;
            $users->save();

            $file->move($folderName,$picture);

            return redirect()->back()->with('success','Félicitation ! votre avatar a été mise à jours');
        else:
            return redirect()->back()->with('error','Désolé ! La taille de l\'image est trop éléve. Maximum 3Mb');
        endif;
    }
}
