<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Models\Assetype;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Assetvalidation;

class AssetvalidationsController extends Controller
{
	public $show_action = true;
	public $view_col = 'Post';
	public $listing_cols = ['id', 'name', 'Post', 'nombre', 'assetype'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Assetvalidations', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Assetvalidations', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Assetvalidations.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Assetvalidations');
		
		if(Module::hasAccess($module->id)) {
			return View('la.assetvalidations.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new assetvalidation.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created assetvalidation in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if(Module::hasAccess("Assetvalidations", "create")) {
		
			//$rules = Module::validateRules("Assetvalidations", $request);
			
			//$validator = Validator::make($request->all(), $rules);
            //dd($request->all());
            $nbre = $request->nombre;
            $asset_type=$request->asset_type;
            $valideur = $request->valideur;

            if(count($valideur)!= $nbre){
                return redirect()->back()->with('error','veillez saisir les validateurs');
            }
            //find type asset
            $oldvalid = Assetvalidation::where('assetype',$asset_type)->first();
            if($oldvalid){
                return redirect()->back()->with('error','Le type d\'asset existe déjà');
            }

            $validateur = json_encode($request->valideur);
            //dd($validateur);
            $insert_id = new Assetvalidation();
            $insert_id->nombre=$nbre;
            $insert_id->post=$validateur;
            $insert_id->assetype=$asset_type;
            $insert_id->save();

			//$insert_id = Module::insert("Assetvalidations", $request);
            return redirect()->back()->with('success','Les validateurs ont bien été enregistrées');
			//return redirect()->route(config('laraadmin.adminRoute') . '.assetvalidations.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Display the specified assetvalidation.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Assetvalidations", "view")) {
			
			$assetvalidation = Assetvalidation::find($id);
			if(isset($assetvalidation->id)) {
				$module = Module::get('Assetvalidations');
				$module->row = $assetvalidation;
				
				return view('la.assetvalidations.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('assetvalidation', $assetvalidation);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("assetvalidation"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified assetvalidation.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Assetvalidations", "edit")) {			
			$assetvalidation = Assetvalidation::find($id);
            $assetypes = Assetype::where("status",1)->get();
			if(isset($assetvalidation->id)) {
				$module = Module::get('Assetvalidations');
				
				$module->row = $assetvalidation;
				
				return view('la.assetvalidations.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
                    'assetypes'=>$assetypes,
				])->with('assetvalidation', $assetvalidation);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("assetvalidation"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified assetvalidation in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Assetvalidations", "edit")) {
		    //dd($request->all() , $id);

            $nbre = $request->nombre;
            $asset_type=$request->assetype;
            $valideur = $request->valideur;

            if(count($valideur)!= $nbre){
                return redirect()->back()->with('error','veillez saisir les validateurs');
            }
            //find type asset
//            $oldvalid = Assetvalidation::where('assetype',$asset_type)->first();
//            if($oldvalid){
//                return redirect()->back()->with('error','Le type d\'asset existe déjà');
//            }

            $validateur = json_encode($valideur);
            //dd($validateur);
            $insert_id = Assetvalidation::find($id);
            $insert_id->nombre=$nbre;
            $insert_id->post=$validateur;
            $insert_id->assetype=$asset_type;
            $insert_id->save();
			
			//$rules = Module::validateRules("Assetvalidations", $request, true);
			
			//$validator = Validator::make($request->all(), $rules);
			
			/*if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}*/
			
			//$insert_id = Module::updateRow("Assetvalidations", $request, $id);
			
			//return redirect()->route(config('laraadmin.adminRoute') . '.assetvalidations.index');
            return redirect(config('laraadmin.adminRoute').'/assign/validation')->with('success','Les validateurs ont bien été modifiés');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified assetvalidation from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Assetvalidations", "delete")) {
			Assetvalidation::find($id)->delete();
			
			// Redirecting to index() method
            return redirect()->route(config('laraadmin.adminRoute') . '/assign/validation');
            //return redirect()->route(config('laraadmin.adminRoute') . '.assetvalidations.index');
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('assetvalidations')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Assetvalidations');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/assetvalidations/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Assetvalidations", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/assetvalidations/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Assetvalidations", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.assetvalidations.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}

}
