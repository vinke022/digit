<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Devi;

class DevisController extends Controller
{
	public $show_action = true;
	public $view_col = 'libelle';
	public $listing_cols = ['id', 'libelle', 'path', 'project_id', 'user_id', 'ext', 'status'];
	
	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Devis', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Devis', $this->listing_cols);
		}
	}
	
	/**
	 * Display a listing of the Devis.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$module = Module::get('Devis');
		
		if(Module::hasAccess($module->id)) {
			return View('la.devis.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new devi.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created devi in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	    //dd($request->all());
		if(Module::hasAccess("Devis", "create")) {
            $file=$request->file('file');
		
			$rules = Module::validateRules("Devis", $request);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}
			
			//$insert_id = Module::insert("Devis", $request);
            $originalExt = $file->getClientOriginalExtension();
            $extension = $originalExt ?: 'png';
            $folderName ='devisFiles/';
            //$picture = str_replace(' ','',$request['libelle']).str_random(2).'.'. $extension;
            $picture = str_random(8).'.'. $extension;

            $insert_id = new Devi();
            $insert_id->libelle = $request['libelle'] ;
            $insert_id->project_id = $request['project_id'] ;
            $insert_id->user_id = $request['user_id'];
            $insert_id->ext = $originalExt ;
            $insert_id->path = $picture;
            $insert_id->save() ;

            $file->move($folderName,$picture);

            //GERER lES NOTIFI ICI
            $usersValid = User::where('post','DSTRAT')->first();

            $notif = new Notification();
            $notif->libelle = 'asset_devis_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $usersValid->id;
            $notif->devi_id = $insert_id->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Un Devis en attente de validation",
            );
            //$findUserMail = User::where('id',$debr->user_id)->first();
            $emailMail = $usersValid->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Devis en attente de validation');
            });

            return redirect()->back();
			//return redirect()->route(config('laraadmin.adminRoute') . '.devis.index');
			
		} else {
            return redirect("/");
		}
	}

	/**
	 * Display the specified devi.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Devis", "view")) {
			
			$devi = Devi::find($id);
			if(isset($devi->id)) {
				$module = Module::get('Devis');
				$module->row = $devi;
				
				return view('la.devis.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('devi', $devi);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("devi"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Show the form for editing the specified devi.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Devis", "edit")) {			
			$devi = Devi::find($id);
			if(isset($devi->id)) {	
				$module = Module::get('Devis');
				
				$module->row = $devi;
				
				return view('la.devis.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('devi', $devi);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("devi"),
				]);
			}
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Update the specified devi in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Devis", "edit")) {
			
			$rules = Module::validateRules("Devis", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Devis", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.devis.index');
			
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}

	/**
	 * Remove the specified devi from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
	    //dd($id);
		if(Module::hasAccess("Devis", "delete")) {
			Devi::find($id)->delete();
			Notification::where('devi_id',$id)->delete();
			//\DB::table('notifications')->where('devi_id',$id)->delete();
			// Redirecting to index() method
			return redirect()->back();
		} else {
			return redirect(config('laraadmin.adminRoute')."/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('devis')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Devis');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/devis/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Devis", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/devis/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Devis", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.devis.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
