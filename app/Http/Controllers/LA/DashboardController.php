<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Asset;
use App\Models\Assetvalidation;
use App\Models\Assetype;
use App\Models\Biblio;
use App\Models\Brief;
use App\Models\Debrief;
use App\Models\Projet;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use DB;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function time(){
        $allBrief = Debrief::all();
        //dd($allBrief);
        return view('la.timesheet',compact('allBrief'));
    }

    public function biblio(){
        return view('la.bibliotheque');
    }

    public function biblioTempl(){
        return view('la.biblio_template');
    }

    public function biblioDa(){
        return view('la.biblio_da');
    }

    public function biblioHowto(){
        return view('la.biblio_how');
    }

    public function showbiblio($id){
        $type = $id;
        $biblio = Biblio::where('type',$type)->get();

        //dd($biblio);
        return view('la.biblioshow',compact('biblio','type'));
    }

    public function itemUser($id){
        $user = DB::table('users')->where('id',$id)->first();
        if($user){
            return $user->name;
        }
        return $user = null;
    }

    public function assetDigit(){
        $asset = ['planpubli','rapport','rapportrim'];
        $listAssetypes = Assetype::select('name','libelle')->whereIn('name',$asset)->get();
        $assetypes = $listAssetypes->toArray();
        return view('la.asset_digital',compact('assetypes'));
    }

    public function assetDigitData($type){
        $assets = Asset::where('asset_type',$type)->orderBy('id','desc')->get();
        return $assets;
        //return view('la.asset_digital',compact('assetPlanning','assetRapport','assetRapportrim'));
    }
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $userId = Auth::user()->id;
        $query = DB::table('role_user')
            ->join('users', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->select('roles.*')
            ->where('users.id', $userId)
            ->whereNull('users.deleted_at')
            ->whereNull('roles.deleted_at')
            ->get();
            
        foreach ($query as $value) {
            $roleName = strtolower($value->name); 
        }
        
        switch ($roleName) {
            case 'admin_lead':
                //return view('admin_lead.dashboard');
                return redirect()->action('AdminleadController@index');
                break;

            case 'marketing':
                //return view('marketing.dashboard');
                return redirect()->action('MarketingController@index');
                break;

            case 'head_department':
                //dd($roleName);
                //return view('head_department.dashboard');
                return redirect()->action('HeadDepartmentController@index');
                break;

            case 'current_user':
                //return view('current_user.dashboard');
                return redirect()->action('CurrentUserController@index');
                break;

            case 'admin_lead_valid':
                //return view('admin_lead_valid.dashboard');
                return redirect()->action('AdminleadvalidController@index');
                break;

            case 'user_trafic':
                //return view('admin_lead_valid.dashboard');
                return redirect()->action('TraficUserController@index');
                break;

            case 'super_admin':
                $projectEncours = Projet::where('dateDeSortie',null)->count();
                $allbriefsBriefsAttente = Brief::where('status','1')->orderBy('id', 'desc')->take(5)->get();
                $allbriefsAttente = Debrief::where('status','1')->orderBy('id', 'desc')->take(5)->get();
                $allbriefsvalid = Debrief::where('status','2')->orderBy('id', 'desc')->take(5)->get();
                $assetWaiting = Asset::where('status','1')->with('debrief')->with('user')->take(5)->orderBy('id','desc')->get();
                return view('la.dashboard',compact('allbriefsBriefsAttente','projectEncours','assetWaiting','allProjectEncours','allbriefsAttente','allbriefsvalid'));
                break;
        }
    }

    public function validassign(){
        $assetypes = Assetype::where("status",1)->get();
        $posts = User::where('post','!=','null')->where('post','!=','')->orderBy('post','ASC')->select('post')->get();
        $assetvalidateur = Assetvalidation::all();
        //dd($assetvalidateur);
        return view('la.assignvalidation',compact('assetypes','posts','assetvalidateur'));
    }

    public function nameLibelle($type){
        $assetypes = Assetype::where("name",$type)->first();
        return $assetypes->libelle;
    }

    public function storeAssignValid(Request $request){

    }
}