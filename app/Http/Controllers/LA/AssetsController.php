<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers\LA;

use App\Http\Controllers\Controller;
use App\Models\Assetvalidation;
use App\Models\Assetype;
use App\Models\Brief;
use App\Models\Debrief;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use Datatables;
use Collective\Html\FormFacade as Form;
use Dwij\Laraadmin\Models\Module;
use Dwij\Laraadmin\Models\ModuleFields;

use App\Models\Asset;
use App\Models\Upload;

class AssetsController extends Controller
{
	public $show_action = true;
	public $view_col = 'name';
    public $listing_cols = ['libelle','path'];
    //public $listing_cols = ['id', 'libelle', 'asset_type', 'brief_id', 'path'];

	public function __construct() {
		// Field Access of Listing Columns
		if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
			$this->middleware(function ($request, $next) {
				$this->listing_cols = ModuleFields::listingColumnAccessScan('Assets', $this->listing_cols);
				return $next($request);
			});
		} else {
			$this->listing_cols = ModuleFields::listingColumnAccessScan('Assets', $this->listing_cols);
		}
	}

	/**
	 * Display a listing of the Assets.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($id,$type)
	{
		$module = Module::get('Assets');

        $asset_datas = Asset::where('debrief_id',$id)->where('asset_type',$type)->get();
        $libelAsset = Assetype::where('name',$type)->first();
        //$table = [] ;
        /*foreach ($asset_datas as $asset){
            //$table[]= $asset->path;
            foreach ($asset->path as $k=>$item){
                dd($k);
            }
        }
        die();*/
        /*//dd($asset_datas[0]->path);
        foreach ($asset_datas as $asset){
            dd(explode('"',$asset->path));
            /*foreach ($asset->path as $img){
                dd($img);
            }*/
        //}
        //$img = Upload::find($asset_datas->path);
        //dd($img);*/

		if(Module::hasAccess($module->id)) {
			return View('la.assets.index', [
				'show_actions' => $this->show_action,
				'listing_cols' => $this->listing_cols,
                'asset_type' =>$libelAsset->libelle,
                'asset_id' =>$id,
                'asset_data' => $asset_datas,
				'module' => $module
			]);
		} else {
            return redirect(config('laraadmin.adminRoute')."/");
        }
	}

	/**
	 * Show the form for creating a new asset.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created asset in database.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
	    //$i = array_map('intval', $request->path);
        //dd($file=$request->file('file'));
        //dd($request->all());
		if(Module::hasAccess("Assets", "create")) {

            $file=$request->file('file');

		    if(empty($request['libelle']) or !isset($file)){
                return redirect()->back()->with('error','Veuillez saisir tous les champs');
            }

			$rules = Module::validateRules("Assets", $request);

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();
			}

			$originalExt = $file->getClientOriginalExtension();
            $extension = $originalExt ?: 'png';
            //$folderName = '../public_html/piges/panneau';
            $folderName ='assetFiles/';
            $picture = str_random(8).'.'. $extension;

            $niveauPjt = Debrief::where('id',$request['debrief_id'])->with('project')->first();
            //dd($niveauPjt->project->niveau);

            if($request['asset_type'] == 'reco'){
                $extensions_autorisees = array('pdf','ppt','pptx');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','reco')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];
                    //dd(auth());
                    //dd(json_decode($usevalid->Post)[0]);

                    $notif = new Notification();
                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }
                    $notif->libelle = 'reco_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset RECOMMANDATION en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    if($emailMail) {
                        Mail::send('mails/valid', $data, function ($message) use ($emailMail) {
                            $message->to($emailMail);
                            $message->subject('Asset en attente de validation');
                        });
                    }
                    //}
                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'crea'){
                $extensions_autorisees = array('jpeg','jpg','gif','tiff','bmp','png','mp3','wma','aac','ogg','avi','mpeg','mov','mp4','flv','pdf','ppt','pptx');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){

                    //foreach($file as $file):
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'];
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','crea')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'crea_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset CREA en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    if($emailMail){
                        Mail::send('mails/valid',$data,function ($message) use($emailMail){
                            $message->to($emailMail);
                            $message->subject('Asset CREA en attente de validation');
                        });
                    }

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'flowplane'){
                $extensions_autorisees = array('xls','xlsx','csv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);
                    //$insert_id = Module::insert("Assets", $request);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','flowplane')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'flowplane_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset FLOW PLAN en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset FLOW PLAN en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'planmedia'){
                $extensions_autorisees = array('xls','xlsx','csv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);
                    //$insert_id = Module::insert("Assets", $request);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','planmedia')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'planmedia_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();
                    //}
                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset PLAN MEDIA en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset PLAN MEDIA en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'fichefabric'){
                //dd($request->all());
                $extensions_autorisees = array('pdf','ppt','pptx');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);
                    //$insert_id = Module::insert("Assets", $request);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','fichefabric')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'fichefabric_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset FICHE DE FABRICATION en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset FICHE DE FABRICATION en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'production'){
                $extensions_autorisees = array('jpeg','jpg','gif','tiff','bmp','png','mp3','wma','aac','ogg','avi','mpeg','mov','mp4','flv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'];
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','production')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'production_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();
                    //}

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset PRODUCTION en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    if($emailMail) {
                        Mail::send('mails/valid', $data, function ($message) use ($emailMail) {
                            $message->to($emailMail);
                            $message->subject('Asset PRODUCTION en attente de validation');
                        });
                    }
                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'retroplanning'){
                $extensions_autorisees = array('xls','xlsx','csv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'];
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;
                    $folderName ='assetFiles/';
                    $file->move($folderName,$picture);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','retroplanning')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'retroplanning_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset RETROPLANNING en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset RETROPLANNING en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'livrable'){
                $extensions_autorisees = array('xls','xlsx','csv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'];
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save();
                    $folderName ='assetFiles/';
                    $file->move($folderName,$picture);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','livrable')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'livrable_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();
                    //}

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset LIVRABLE en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset LIVRABLE en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'rapportop'){
                $extensions_autorisees = array('xls','xlsx','csv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'];
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save();
                    $folderName ='assetFiles/';
                    $file->move($folderName,$picture);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','rapportop')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'rapportop_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();
                    //}

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset RAPPORT OPE en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset RAPPORT OPE en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'planpubli'){
                $extensions_autorisees = array('xls','xlsx','csv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','planpubli')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'planpubli_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset PLANNING DE PUBLICATION en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset PLANNING DE PUBLICATION en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'gglads'){
                $extensions_autorisees = array('jpeg','jpg','gif','tiff','bmp','png','mp3','wma','aac','ogg','avi','mpeg','mov','mp4','flv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);
                    //$insert_id = Module::insert("Assets", $request);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','gglads')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'gglads_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset VISUEL GOOGLE ADS en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset VISUEL GOOGLE ADS en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'rapport'){
                $extensions_autorisees = array('pdf','ppt','pptx');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);
                    //$insert_id = Module::insert("Assets", $request);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','rapport')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'rapport_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset RAPPORT en attente de validation",
                    );
                    //$findUserMail = User::where('id',$niveauPjt->user_id)->first();
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset RAPPORT en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'rapportrim'){
                $extensions_autorisees = array('pdf','ppt','pptx');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);
                    //$insert_id = Module::insert("Assets", $request);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','rapportrim')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'rapportrim_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset RAPPORT TRIMESTRIEL en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset RAPPORT TRIMESTRIEL en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'prototype'){
                $extensions_autorisees = array('pdf','ppt','pptx','txt');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);
                    //$insert_id = Module::insert("Assets", $request);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','prototype')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'prototype_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset PROTOTYPE en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset PROTOTYPE en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'bcp'){
                $extensions_autorisees = array('pdf','ppt','pptx');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);
                    //$insert_id = Module::insert("Assets", $request);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','bcp')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'bcp_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset BON DE COMMANDE PRESTATAIRE en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset BON DE COMMANDE PRESTATAIRE en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'planstrat'){
                $extensions_autorisees = array('xls','xlsx','csv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','planstrat')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'planstrat_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset PLANNING EDITORIAL STATEGIQUE en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset PLANNING EDITORIAL STATEGIQUE en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            if($request['asset_type'] == 'plancrea'){
                $extensions_autorisees = array('xls','xlsx','csv');
                $originalExt = strtolower($originalExt);
                if (in_array($originalExt ,$extensions_autorisees)){
                    $asset = new Asset();
                    $asset->asset_type = $request['asset_type'];
                    $asset->libelle = $request['libelle'] ;
                    $asset->debrief_id = $request['debrief_id'] ;
                    $asset->niveau = $niveauPjt->project->niveau ;
                    $asset->user_id = $request['user_id'];
                    $asset->tache_id = $request['tache_id'];
                    $asset->ext = $originalExt ;
                    $asset->path = $picture;
                    $asset->save() ;

                    $file->move($folderName,$picture);

                    //NOUVELLE NOTIFICATION
                    $usevalid = Assetvalidation::where('assetype','plancrea')->first();
                    $firstvalid = json_decode($usevalid->Post)[0];

                    $notif = new Notification();

                    if($firstvalid=='DC'){
                        $notif->recip_id = $niveauPjt->user_id;
                    }else{
                        $usersValid = User::where('post',$firstvalid)->first();
                        $notif->recip_id = $usersValid->id;
                    }

                    $notif->libelle = 'plancrea_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $asset->id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset PLANNING EDITORIAL CREATIF en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail){
                        $message->to($emailMail);
                        $message->subject('Asset PLANNING EDITORIAL CREATIF en attente de validation');
                    });

                    return redirect()->back()->with('success','l\'asset à bien été bien ajouté');
                }else{
                    return redirect()->back()->with('error','Désolé ! l\'extension du fichier n\'est pas autoriser');
                }
            }

            //return redirect()->route(config('laraadmin.adminRoute') . '.assets.index');

		} else {
            return redirect("/");
		}
	}

	/**
	 * Display the specified asset.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		if(Module::hasAccess("Assets", "view")) {
			
			$asset = Asset::find($id);
			if(isset($asset->id)) {
				$module = Module::get('Assets');
				$module->row = $asset;
				
				return view('la.assets.show', [
					'module' => $module,
					'view_col' => $this->view_col,
					'no_header' => true,
					'no_padding' => "no-padding"
				])->with('asset', $asset);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("asset"),
				]);
			}
		} else {
            return redirect("/");
		}
	}

	/**
	 * Show the form for editing the specified asset.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		if(Module::hasAccess("Assets", "edit")) {			
			$asset = Asset::find($id);
			if(isset($asset->id)) {	
				$module = Module::get('Assets');
				
				$module->row = $asset;
				
				return view('la.assets.edit', [
					'module' => $module,
					'view_col' => $this->view_col,
				])->with('asset', $asset);
			} else {
				return view('errors.404', [
					'record_id' => $id,
					'record_name' => ucfirst("asset"),
				]);
			}
		} else {
            return redirect("/");
		}
	}

	/**
	 * Update the specified asset in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		if(Module::hasAccess("Assets", "edit")) {
			
			$rules = Module::validateRules("Assets", $request, true);
			
			$validator = Validator::make($request->all(), $rules);
			
			if ($validator->fails()) {
				return redirect()->back()->withErrors($validator)->withInput();;
			}
			
			$insert_id = Module::updateRow("Assets", $request, $id);
			
			return redirect()->route(config('laraadmin.adminRoute') . '.assets.index');
			
		} else {
            return redirect("/");
		}
	}

	/**
	 * Remove the specified asset from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		if(Module::hasAccess("Assets", "delete")) {
			Asset::find($id)->delete();
            Notification::where('asset_id',$id)->delete();
			// Redirecting to index() method
            return redirect()->back();
			//return redirect()->route(config('laraadmin.adminRoute') . '.assets.index');
		} else {
            return redirect("/");
		}
	}
	
	/**
	 * Datatable Ajax fetch
	 *
	 * @return
	 */
	public function dtajax()
	{
		$values = DB::table('assets')->select($this->listing_cols)->whereNull('deleted_at');
		$out = Datatables::of($values)->make();
		$data = $out->getData();

		$fields_popup = ModuleFields::getModuleFields('Assets');
		
		for($i=0; $i < count($data->data); $i++) {
			for ($j=0; $j < count($this->listing_cols); $j++) { 
				$col = $this->listing_cols[$j];
				if($fields_popup[$col] != null && starts_with($fields_popup[$col]->popup_vals, "@")) {
					$data->data[$i][$j] = ModuleFields::getFieldValue($fields_popup[$col], $data->data[$i][$j]);
				}
				if($col == $this->view_col) {
					$data->data[$i][$j] = '<a href="'.url(config('laraadmin.adminRoute') . '/assets/'.$data->data[$i][0]).'">'.$data->data[$i][$j].'</a>';
				}
				// else if($col == "author") {
				//    $data->data[$i][$j];
				// }
			}
			
			if($this->show_action) {
				$output = '';
				if(Module::hasAccess("Assets", "edit")) {
					$output .= '<a href="'.url(config('laraadmin.adminRoute') . '/assets/'.$data->data[$i][0].'/edit').'" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>';
				}
				
				if(Module::hasAccess("Assets", "delete")) {
					$output .= Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $data->data[$i][0]], 'method' => 'delete', 'style'=>'display:inline']);
					$output .= ' <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>';
					$output .= Form::close();
				}
				$data->data[$i][] = (string)$output;
			}
		}
		$out->setData($data);
		return $out;
	}
}
