<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Annonceur;
use App\Models\Asset;
use App\Models\Assetop;
use App\Models\Bcommande;
use App\Models\Debrief;
use App\Models\Devi;
use App\Models\Notification;
use App\Models\Tach;
use App\Models\User;
use App\Models\Validation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;

use Auth;
use DB;

use App\Models\Projet;
use App\Models\Brief;
use Illuminate\Support\Facades\Mail;

class AdminleadController extends Controller
{
    public function onProgress()
    {
    	$data = Projet::where("status", "En attente")->get();

    	return $data;
    }

    public function showAnnonceurPjt($id){
        $data = Annonceur::where('id',$id)->select('name')->first();
        return $data->name;
    }

    public function index(){
        //dd('df');
        $projectEncours = Projet::where('dateDeSortie',null)->count();
        //dd($projectEncours);
        $allbriefsBriefsAttente = Brief::where('status','1')->with('userBF')->with('project')->orderBy('id', 'desc')->take(5)->get();
        $allbriefsAttente = Debrief::where('status','1')->with('userBF')->with('project')->orderBy('id', 'desc')->take(5)->get();
        $allbriefsvalid = Debrief::where('status','2')->with('userBF')->with('project')->orderBy('id', 'desc')->take(5)->get();
        //$assetWaiting = Asset::where('status','1')->where('asset_type','retroplanning')->with('debrief')->with('user')->take(5)->orderBy('id','desc')->get();
        $deviWaiting = Devi::where('status','1')->take(5)->orderBy('id','desc')->get();
        $projectEtat = Projet::orderBy('niveau','desc')->get();
        //dd($allbriefsAttente);
        return view('admin_lead.dashboard',compact('deviWaiting','projectEtat','allbriefsBriefsAttente','allbriefsAttente','allbriefsvalid','projectEncours','assetWaiting'));
    }

    public function waitingAsset(){

        if(Auth::user()->post=='DGA' or Auth::user()->post=='CONTROLE'){
            $assetWaiting = Asset::where('status','1')->with('debrief')->with('user')->orderBy('id','desc')->get();
            $assetWaitingReco = Asset::where('status','1')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
            $assetWaitingCrea = Asset::where('status','1')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
            $assetWaitingFlow = Asset::where('status','1')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
            $assetWaitingPlan = Asset::where('status','1')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
            //$assetWaitingDigit = Asset::where('status','1')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();
            //$assetWaitingDevis = Devi::where('status','1')->with('project')->with('userDP')->orderBy('id','desc')->get();
            $assetWaitingDevis = Notification::where('recip_id',Auth::user()->id)->where('devi_id','!=','0')->where('libelle','asset_devis_a_valide')->with('devis')->get();
            $assetWaitingRetro = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_retro_a_valide')->with('asset')->get();
            $assetWaitingPresta = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','bcp_a_valide')->with('asset')->get();
            //dd($assetWaitingRetro);
            //$assetWaitingOP = Assetop::where('status','1')->with('project')->with('userOP')->orderBy('id','desc')->get();
            //dd($assetWaiting);
        }else{
            $assetWaiting = Asset::where('status','1')->with('debrief')->with('user')->orderBy('id','desc')->get();
            $assetWaitingReco = Asset::where('status','1')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
            $assetWaitingCrea = Asset::where('status','1')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
            $assetWaitingFlow = Asset::where('status','1')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
            $assetWaitingPlan = Asset::where('status','1')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
//            $assetWaitingDigit = Asset::where('status','1')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();
            $assetWaitingDevis = Devi::where('status','1')->with('project')->with('userDP')->orderBy('id','desc')->get();
            $assetWaitingRetro='';
            $assetWaitingPresta='';
            //$assetWaitingDevis = Notification::where('recip_id',Auth::user()->id)->where('devi_id','!=','0')->where('libelle','asset_devis_a_valide')->with('devis')->get();
            //dd($assetWaitingDevis);
//            $assetWaitingOP = Assetop::where('status','1')->with('project')->with('userOP')->orderBy('id','desc')->get();
            //dd($assetWaiting);
        }

        return view('admin_lead.asset_waiting',compact('assetWaitingPresta','assetWaitingRetro','assetWaitingOP','assetWaiting','assetWaitingCrea','assetWaitingDevis','assetWaitingDigit','assetWaitingFlow','assetWaitingReco','assetWaitingPlan'));
    }

    public function validAsset(){
        $assetValid = Asset::where('status','2')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingReco = Asset::where('status','2')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDevis = Devi::where('status','2')->with('project')->with('userDP')->orderBy('id','desc')->get();
        $assetWaitingOP = Assetop::where('status','2')->with('project')->with('userOP')->orderBy('id','desc')->get();
        $assetWaitingCrea = Asset::where('status','2')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingFlow = Asset::where('status','2')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingPlan = Asset::where('status','2')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDigit = Asset::where('status','2')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();

        return view('admin_lead.asset_valid',compact('assetWaitingOP','assetValid','assetWaitingCrea','assetWaitingDevis','assetWaitingDigit','assetWaitingFlow','assetWaitingReco','assetWaitingPlan'));
    }

    public function rejectAsset(){
        $assetReject = Asset::where('status','0')->with('debrief')->with('user')->with('lastReject')->orderBy('id','desc')->get();
        $assetWaitingReco = Asset::where('status','0')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDevis = Devi::where('status','0')->with('project')->with('userDP')->orderBy('id','desc')->get();
        $assetWaitingOP = Assetop::where('status','1')->with('project')->with('userOP')->orderBy('id','desc')->get();
        $assetWaitingCrea = Asset::where('status','0')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingFlow = Asset::where('status','0')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingPlan = Asset::where('status','0')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDigit = Asset::where('status','0')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();

        return view('admin_lead.asset_reject',compact('assetWaitingOP','assetReject','assetWaitingCrea','assetWaitingDevis','assetWaitingDigit','assetWaitingFlow','assetWaitingReco','assetWaitingPlan'));
    }

    public function inprogress(){
        $allbriefsValid = Debrief::where('status','2')->with('userBF')->with('project')->orderBy('id', 'desc')->get();
        //dd($allbriefsAttente);
        return view('admin_lead.brief_inprogress',compact('allbriefsValid'));
    }

    public function waiting(){
        $allbriefsAttente = Debrief::where('status','1')->with('userBF')->with('project')->orderBy('id', 'desc')->get();
        //dd($allbriefsAttente);
        return view('admin_lead.brief_waiting',compact('allbriefsAttente'));
    }

    public function waitingBrief(){
        //dd('sale');
        $allbriefsAttente = Brief::where('status','1')->with('project')->with('userBF')->orderBy('id', 'desc')->get();
        //dd($allbriefsAttente);
        return view('admin_lead.waitingBrief',compact('allbriefsAttente'));
    }

    public function reject(){
        $allbriefsReject = Debrief::where('status','0')->with('userBF')->with('project')->with('lastReject')->orderBy('id', 'desc')->get();
        //dd($allbriefsReject);
        return view('admin_lead.brief_reject',compact('allbriefsReject'));
    }

    public function bc(){
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','1')->with('userBC')->with('project')->get();
        return view('admin_lead.bc_waiting',compact('bcommandes'));
    }

    public function bcValid(){
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','2')->with('userBC')->with('project')->get();
        return view('admin_lead.bc',compact('bcommandes'));
    }

    public function bcReject(){
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','0')->with('lastReject')->with('project')->with('userBC')->get();
        return view('admin_lead.bc_reject',compact('bcommandes'));
    }

    public function notif(){
        $notifs = Notification::where('recip_id',Auth::user()->id)->take(50)->orderBy('id','desc')->get();
        //dd($notifs);
        $var=\DB::table('notifications')->where('recip_id',Auth::user()->id)->update(['vue' =>'1']);

        return view('admin_lead.notif',compact('notifs'));
    }

    public function validAssetDevisUser($id){
        //dd($id);
        if($id){

            $allnotif = Notification::find($id);
            $asset = Devi::where('id',$allnotif->devi_id)->first();
            $asset->status = '2';
            $asset->save();

            $notif = new Notification();
            $notif->libelle = 'asset_devis_valide';
            $notif->send_id = $allnotif->recip_id;
            $notif->recip_id = $allnotif->send_id;
            $notif->devi_id = $allnotif->devi_id;
            $notif->vue = '0';
            $notif->save();

            $notif = new Notification();
            $notif->libelle = 'asset_devis_valide';
            $notif->send_id = $allnotif->recip_id;
            $notif->recip_id = $asset->user_id;
            $notif->devi_id = $allnotif->devi_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le DEVIS a été validé",
            );
            $findUserMail = User::where('id',$allnotif->send_id)->first();
            $findUserMail2 = User::where('id',$asset->user_id)->first();
            $emailMail = [$findUserMail->email,$findUserMail2->email];
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Devis validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','le devis a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function validAssetDevisUserControle($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);

            $DGA = User::where('post','DGA')->first();
            $notif = new Notification();
            $notif->libelle = 'asset_devis_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $DGA->id;
            $notif->devi_id = $allnotif->devi_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Un Devis ou Proforma en attente de validation",
            );
            //$findUserMail = User::where('id',$debr->user_id)->first();
            $emailMail = $DGA->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('DEVIS OU PROFORMA en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','le devis a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function cancelAssetDevis(Request $request){
        //dd($request->all());
        $allnotif = Notification::find($request['idDevis']);

        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->devi_id = $allnotif->devi_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Devi::find($allnotif->devi_id);
        $file->status='0';
        $file->save();


        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_devis_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_devis_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"le DEVIS a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Devis rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','le devis a été rejecté');

    }

    public function cancelAssetDevisControle(Request $request){
        //dd($request->all());
        $allnotif = Notification::find($request['idDevisControle']);

        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->devi_id = $allnotif->devi_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Devi::find($allnotif->devi_id);
        $file->status='0';
        $file->save();


        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_devis_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_devis_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"le DEVIS a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Devis rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','le devis a été rejecté');

    }

    public function validAssetRetro($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            $asset = Asset::where('id',$allnotif->asset_id)->first();
            //dd($asset);
            $asset->status = '2';
            $asset->save();

            $notif = new Notification();
            $notif->libelle = 'asset_retro_valide';
            $notif->send_id = $allnotif->recip_id;
            $notif->recip_id = $allnotif->send_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            $notif = new Notification();
            $notif->libelle = 'asset_retro_valide';
            $notif->send_id = $allnotif->recip_id;
            $notif->recip_id = $asset->user_id;
            $notif->devi_id = $allnotif->devi_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"le retro planning livrable a été validé",
            );
            $findUserMail = User::where('id',$allnotif->send_id)->first();
            $findUserMail2 = User::where('id',$asset->user_id)->first();
            $emailMail = [$findUserMail->email,$findUserMail2->email];
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Retro planning validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','le devis a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function validAssetRetroControle($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);
            $asset = Asset::find($allnotif->asset_id);
            $DGA = User::where('post','DGA')->first();

            $notif = new Notification();
            $notif->libelle = 'asset_retro_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $DGA->id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le Retro planning en attente de validation",
            );
            //$findUserMail = User::where('id',$allnotif->send_id)->first();
            $emailMail = $DGA->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Retro planning en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','le retroplanning a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function cancelAssetRetro(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['fileid']);

        $asset = Asset::find($allnotif->asset_id);
        $asset->status = '0';
        $asset->save();

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_retro_rejete';
        $notif->send_id = $allnotif->recip_id;
        $notif->recip_id = $allnotif->send_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_retro_rejete';
        $notif->send_id = $allnotif->recip_id;
        $notif->recip_id = $asset->user_id;
        $notif->devi_id = $allnotif->devi_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"le retro planning livrable a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$asset->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Retro planning rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','le retro planning a été rejecté');

    }

    public function cancelAssetRetroControle(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['fileidControle']);

        $asset = Asset::find($allnotif->asset_id);
        $asset->status = '0';
        $asset->save();

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_retro_rejete';
        $notif->send_id = $allnotif->recip_id;
        $notif->recip_id = $allnotif->send_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_retro_rejete';
        $notif->send_id = $allnotif->recip_id;
        $notif->recip_id = $asset->user_id;
        $notif->devi_id = $allnotif->devi_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"le retro planning livrable a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$asset->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Retro planning rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','le retro planning a été rejecté');

    }

    public function validAssetBcp($id){
        //dd($id);
        if($id){

            $allnotif = Notification::find($id);
            $asset = Asset::where('id',$allnotif->asset_id)->first();
            $asset->status = '2';
            $asset->save();

            if($asset->asset_type != 'devis'){
                $tach = Tach::find($asset->tache_id);
                $tach->status = '2';
                $tach->save();
            }

            $notif = new Notification();
            $notif->libelle = 'bcp_valide';
            $notif->send_id = $allnotif->recip_id;
            $notif->recip_id = $allnotif->send_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            $notif = new Notification();
            $notif->libelle = 'bcp_valide';
            $notif->send_id = $allnotif->recip_id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"le Bon de commande prestataire a été validé",
            );
            $findUserMail = User::where('id',$allnotif->send_id)->first();
            $findUserMail2 = User::where('id',$asset->user_id)->first();
            $emailMail = [$findUserMail->email,$findUserMail2->email];
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Bon de commande prestataire rejeté');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','le bon de commande prestataire a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function validAssetBcpControle($id){
        //dd($id);
        if($id){

            $allnotif = Notification::find($id);
            $DGA = User::where('post','DGA')->first();
            //$DGA = User::where('post','CONTROLE')->first();
            //dd($DGA);
            $notif = new Notification();
            $notif->libelle = 'bcp_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $DGA->id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Bon de commande prestataire en attente de validation",
            );
            $emailMail = $DGA->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('BON DE COMMANDE en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','le bon de commande prestataire a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function cancelAssetBcp(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idbcp']);

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();


        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'bcp_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'bcp_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"le Bon de commande prestataire a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Bon de commande prestataire rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','le devis a été rejecté');

    }

    public function cancelAssetBcpControle(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idbcpControle']);

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();


        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'bcp_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'bcp_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"le Bon de commande prestataire a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Bon de commande prestataire rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','le devis a été rejecté');

    }

}
