<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Assetvalidation;
use App\Models\Assetype;
use App\Models\Debrief;
use App\Models\Devi;
use App\Models\Notification;
use App\Models\Tach;
use App\Models\User;
use App\Models\Validation;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AssetsvalidController extends Controller
{

    public function typAssetForVue(){
        /*return $assetypes = [
            ['slug'=>'reco','libelle'=>'RECO'],
            ['slug'=>'crea','libelle'=>'CREA'],
            ['slug'=>'flowplane','libelle'=>'FLOW PLAN'],
            ['slug'=>'planmedia','libelle'=>'PLAN MEDIA'],
            ['slug'=>'fichefabric','libelle'=>'FICHE DE FABRICATION'],
            ['slug'=>'retroplanning','libelle'=>'RETROPLANNING'],
            ['slug'=>'bcp','libelle'=>'BON DE COMMANDE PRESTATAIRE'],
            ['slug'=>'rapportop','libelle'=>'RAPPORT OPERATIONNEL'],
            ['slug'=>'livrable','libelle'=>'LIVRABLE'],
            ['slug'=>'rapport','libelle'=>'RAPPORT DIGITAL MENSUEL'],
            ['slug'=>'rapportrim','libelle'=>'RAPPORT DIGITAL CAMPAGNE'],
            ['slug'=>'production','libelle'=>'PRODUCTION'],
            ['slug'=>'planpubli','libelle'=>'PLANNING DE PUBLICATION'],
            ['slug'=>'gglads','libelle'=>'VISUEL GOOGLE ADS'],
            ['slug'=>'prototype','libelle'=>'PROTOTYPE & RENDU']
        ];*/
        $listAssetypes = Assetype::select('name','libelle')->get();
        $assetypes = $listAssetypes->toArray();
        return $assetypes;
    }

    public function index(){
        $assetypes = $this->typAssetForVue();
        $assetWaitingDevis = Notification::where('recip_id',Auth::user()->id)->where('devi_id','!=','0')->where('libelle','asset_devis_a_valide')->with('devis')->get();

        return view('asset_waiting',compact('assetypes','assetWaitingDevis'));
    }

    public function assetvalider(){
        $assetypes = $this->typAssetForVue();
        $assetWaitingDevis = Devi::where('status','2')->with('project')->with('userDP')->orderBy('id','desc')->get();
        return view('asset_valider',compact('assetypes','assetWaitingDevis'));
    }

    public function assetrejeter(){
        $assetypes = $this->typAssetForVue();
        $assetWaitingDevis = Devi::where('status','0')->with('project')->with('userDP')->orderBy('id','desc')->get();
        return view('asset_rejeter',compact('assetypes','assetWaitingDevis'));
    }

    public function checkAssetValidation($slug){
        //dd($slug);
        $asset = Assetvalidation::where('assetype',$slug)->first();
        $arrayAsset = json_decode($asset->Post);
        //dd($arrayAsset);
        if(in_array(Auth::user()->post,$arrayAsset)){
            return true ;
        }else{
            return false;
        }
    }

    public function getNotif($slug){
        $assetWaiting = Notification::where('recip_id',Auth::user()->id)->where('libelle',$slug.'_a_valide')->with('asset')->get();
        //dd($assetWaitingDevis);
        return $assetWaiting;
    }

    public function getAssetWaiting($slug){
        $assetWaiting = Asset::where('status','1')->where('asset_type',$slug)->with('debrief')->with('user')->orderBy('id','desc')->get();
        //dd($assetWaitingDevis);
        return $assetWaiting;
    }

    public function getAssetValid($slug){
        $assetValider = Asset::where('status','2')->where('asset_type',$slug)->with('debrief')->with('user')->orderBy('id','desc')->get();
        return $assetValider;
    }

    public function getAssetReject($slug){
        $assetValider = Asset::where('status','0')->where('asset_type',$slug)->with('debrief')->with('user')->orderBy('id','desc')->get();
        return $assetValider;
    }

    public function validAsset($id,$type){
        //dd($id,$type);

        if(isset($id) and isset($type)){
            $allnotif = Notification::find($id);
            $typeAsset = Assetype::where('name',$type)->first();
            $typeAssetLibelle = $typeAsset->libelle;

            $post = Assetvalidation::where('assetype',$type)->first();
            //dd($post->nombre);
            $getpost = json_decode($post->Post);

            if(in_array(Auth::user()->post ,$getpost)) {
                $keyPost = array_search(Auth::user()->post, $getpost);
                //dd($getpost, $keyPost + 1);

                //SI L'USER CONNECTER EST LA DERNIERE PERSONNE A VALIDER
                if($post->nombre == $keyPost+1){
                    //VALID DIRETEMENT LE ASSET
                    $asset = Asset::find($allnotif->asset_id);
                    $asset->status = '2';
                    $asset->save();

                    //NOTIFICATION PAR LE SYSTEM
                    $notif = new Notification();
                    $notif->libelle = $type.'_valide';
                    //$notif->libelle = 'asset_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->recip_id = $asset->user_id;
                    $notif->asset_id = $allnotif->asset_id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Votre ASSET ".$typeAssetLibelle." a été validé",
                    );
                    $findUserMail = User::where('id',$asset->user_id)->first();
                    $emailMail = $findUserMail->email;
                    Mail::send('mails/valid',$data,function ($message) use($emailMail,$typeAssetLibelle){
                        $message->to($emailMail);
                        $message->subject('ASSET '.$typeAssetLibelle.' validé');
                    });

                    $allnotif->delete();
                    return redirect()->back()->with('success','l\'asset '.$typeAssetLibelle.' a été validé');

                }else{

                    //NOUVELLE NOTIF POUR LE VALIDATEUR SUIVANT
                    $notif = new Notification();

                    //SAVOIR SI C'EST UN DC
                    if($getpost[$keyPost+1]=='DC'){

                            //SELECTIONNER L'IDENTIFIANT DU DC
                            $assetget = Asset::where('id',$allnotif->asset_id)->first();
                            $tach = Tach::where('id',$assetget->tache_id)->first();
                            $niveauPjt = Debrief::where('id',$tach->debrief_id)->first();
                            //dd($niveauPjt->user_id);
                            $notif->recip_id = $niveauPjt->user_id;

                        }else{
                            //dd($usersValid->id);
                            $usersValid = User::where('post',$getpost[$keyPost+1])->first();
                            $notif->recip_id = $usersValid->id;
                        }

                    $notif->libelle = $type.'_a_valide';
                    //$notif->libelle = 'asset_a_valide';
                    $notif->send_id = Auth::user()->id;
                    $notif->asset_id = $allnotif->asset_id;
                    $notif->vue = '0';
                    $notif->save();

                    //NOTIFICATION PAR MAIL
                    $data=array(
                        "notifmessage"=>"Vous avez un asset ".$typeAssetLibelle." en attente de validation",
                    );
                    $emailUser = User::where('id',$notif->recip_id)->first();
                    $emailMail = $emailUser->email;
                    if($emailMail) {
                        Mail::send('mails/valid', $data, function ($message) use ($emailMail,$typeAssetLibelle) {
                            $message->to($emailMail);
                            $message->subject('Asset '.$typeAssetLibelle.' en attente de validation');
                        });
                    }

                    $allnotif->delete();

                    return redirect()->back()->with('success','l\'asset '.$typeAssetLibelle.' a été validé');
                }

            }else{
                return redirect()->back()->with('error','Désolé ! une erreur est survenue veuillez contacter l\'administrateur');
            }

        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }

    public function rejectAsset(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idAsset']);

        $typeAsset = Assetype::where('name',$request['typeAsset'])->first();
        $typeAssetLibelle = $typeAsset->libelle;

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        $notif = new Notification();
        $notif->libelle = $request['typeAsset'].'_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        $allnotif->delete();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"l'asset ".$typeAssetLibelle." a été rejeté",
        );
        $findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail,$typeAssetLibelle){
            $message->to($emailMail);
            $message->subject('Asset '.$typeAssetLibelle.' rejeté');
        });

        return redirect()->back()->with('success','l\'asset a été rejecté');
    }
}
