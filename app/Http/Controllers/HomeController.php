<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $roleCount = \App\Role::count();
		if($roleCount != 0) {
			if($roleCount != 0) {
				return view('home');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}
    }

    public function ajaxChecklist(Request $request)
    {
        $id_checked = $request->id_checked;
        $project_id  = $request->project_id;

        $find = DB::table('project_checklist')
            ->where('checklist_id', $id_checked)
            ->where('project_id', $project_id)
            ->where('deleted_at', '!=', NULL)->first();
        if(count($find) > 0){
            DB::table('project_checklist')
                ->where('checklist_id', $id_checked)
                ->where('project_id', $project_id)
                ->update(['deleted_at' => NULL]);
        }
        else{
            DB::table('project_checklist')
                ->insert(['checklist_id'=>$id_checked,'project_id' => $project_id]);
        }

        return response()->json([
            'res' => 'success'
        ], 200);
    }

    public function ajaxChecklistUpdate(Request $request)
    {
        $id_checked = $request->id_checked;
        $project_id  = $request->project_id;

        DB::table('project_checklist')
            ->where('checklist_id', $id_checked)
            ->where('project_id', $project_id)
            ->whereNull('deleted_at')
            ->update(['deleted_at' => date('Y-m-d H:i:s')]);


        return response()->json([
            'res' => 'success'
        ], 200);
    }
}