<?php

namespace App\Http\Controllers;

use App\Models\Annonceur;
use App\Models\Asset;
use App\Models\Assetop;
use App\Models\Bcommande;
use App\Models\Brief;
use App\Models\Debrief;
use App\Models\Devi;
use App\Models\Notification;
use App\Models\Projet;
use App\Models\Soumission;
use App\Models\Tach;
use App\Models\User;
use App\Models\Validation;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AdminleadvalidController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showAnnonceurPjt($id){
        $data = Annonceur::where('id',$id)->select('name')->first();
        return $data->name;
    }

    public function index(){
        //dd('ee');
        //$projectEncours = Projet::where('dateDeSortie', '>=',Carbon::today())->count();
        $projectEncours = Projet::where('dateDeSortie',null)->count();
        //dd($projectEncours);
        $allbriefsBriefsAttente = Brief::where('status','1')->with('project')->with('userBF')->orderBy('id', 'desc')->take(5)->get();
        $allbriefsAttente = Debrief::where('status','1')->with('project')->with('userBF')->orderBy('id', 'desc')->take(5)->get();
        $allbriefsvalid = Debrief::where('status','2')->with('project')->with('userBF')->orderBy('id', 'desc')->take(5)->get();
        //$assetWaiting = Asset::where('status','1')->where('asset_type','reco')->where('asset_type','flowplane')->where('asset_type','planmedia')->with('debrief')->with('user')->take(5)->orderBy('id','desc')->get();
        $deviWaiting = Devi::where('status','1')->take(5)->orderBy('id','desc')->get();
        $projectEtat = Projet::all();
        //dd($allbriefsAttente); 'projectEncours',
        return view('admin_lead_valid.dashboard',compact('deviWaiting','allbriefsBriefsAttente','allbriefsAttente','allbriefsvalid','assetWaiting','projectEtat','projectEncours'));
    }

    public function devisDel(){
        $deviWaiting = Devi::where('status','3')->with('userDP')->with('project')->orderBy('id','desc')->get();
        //dd($deviWaiting);
        return view('admin_lead_valid.devisdel',compact('deviWaiting'));
    }

    public function devisDelete($id){
        if($id){
            $devis = Devi::find($id);
            $devis->delete();
            return redirect()->back()->with('success','le devis a été supprimé');
        }else{
            return redirect()->back()->with('success','le devis n\'a été supprimé');
        }
    }

    public function waiting(){
        $allbriefsAttente = Debrief::where('status','1')->with('project')->with('userBF')->orderBy('id', 'desc')->get();
        //dd($allbriefsAttente);
        return view('admin_lead_valid.waiting',compact('allbriefsAttente'));
    }

    public function waitingBrief(){
        //dd('sale');
        $allbriefsAttente = Brief::where('status','1')->with('project')->with('userBF')->orderBy('id', 'desc')->get();
        //dd($allbriefsAttente);
        return view('admin_lead_valid.waitingBrief',compact('allbriefsAttente'));
    }

    public function validDeBrief($id){
        //dd($id);
        $debriefs = Debrief::where('brief_id',$id)->where('status','1')->with('project')->with('userBF')->orderBy('id', 'desc')->get();
        return view('admin_lead_valid.debriefWaiting',compact('debriefs'));
    }

    public function cancelBrief(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->debrief_id = $request['idbrief'];
        $rejet->observe = $observation;
        $rejet->save();

        $file = Debrief::find($request['idbrief']);
        $file->status='0';
        $file->save();

        $fileBrief = Brief::find($file->brief_id);
        if($fileBrief->status == '1' or $fileBrief->status == '0'){
            $fileBrief->status='0';
            $fileBrief->save();
        }

        $notif = new Notification();
        $notif->libelle = 'brief_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->debrief_id = $request['idbrief'];
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Votre DEBRIEF a été rejeté",
        );
        $findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Debrief rejeté');
        });

        return redirect()->back()->with('success','l\'asset a été rejeté');

    }

    public function validBrief($id){
        $debrief = Debrief::find($id);
        $debrief->status='3';
        $debrief->save();

//        $file = Brief::find($debrief->brief_id);
//        if($file->status == '1'){
//            $file->status='3';
//            $file->save();
//        }

        $userDep = User::where('departement_id',$debrief->dep)->where('role_id',5)->first();
        $notif = new Notification();
        $notif->libelle = 'brief_assigne';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $userDep->id;
        $notif->debrief_id = $id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Un DEBRIEF vous êtes assigné",
        );
        //$findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $userDep->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Debrief assigné');
        });


        return redirect()->back()->with('success','le debrief a été validé');
    }

    public function waitingAsset(){
        //$assetWaiting = Asset::where('status','1')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingReco = Asset::where('status','1')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
//        $assetWaitingCrea = Asset::where('status','1')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingFlow = Asset::where('status','1')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingPlan = Asset::where('status','1')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
//        $assetWaitingDigit = Asset::where('status','1')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDevis = Notification::where('recip_id',Auth::user()->id)->where('devi_id','!=','0')->where('libelle','asset_devis_a_valide')->with('devis')->get();
        $assetWaitingPresta = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','bcp_a_valide')->with('asset')->get();
        //dd($assetWaitingDevis);
        //$assetWaitingDevis = Devi::where('status','1')->with('project')->with('userDP')->orderBy('id','desc')->get();
        //$assetWaitingOP = Assetop::where('status','1')->with('project')->with('userOP')->orderBy('id','desc')->get();
        //dd($assetWaitingOP);
        return view('admin_lead_valid.asset_waiting',compact('assetWaitingPresta','assetWaitingDevis','assetWaitingFlow','assetWaitingReco','assetWaitingPlan'));
    }

    public function validAsset(){
        //$assetValid = Asset::where('status','2')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingReco = Asset::where('status','2')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDevis = Devi::where('status','2')->with('project')->with('userDP')->orderBy('id','desc')->get();
        //$assetWaitingOP = Assetop::where('status','2')->with('project')->with('userOP')->orderBy('id','desc')->get();
        //$assetWaitingCrea = Asset::where('status','2')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingFlow = Asset::where('status','2')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingPlan = Asset::where('status','2')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
        //$assetWaitingDigit = Asset::where('status','2')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();

        return view('admin_lead_valid.asset_valid',compact('assetWaitingOP','assetValid','assetWaitingCrea','assetWaitingDevis','assetWaitingDigit','assetWaitingFlow','assetWaitingReco','assetWaitingPlan'));
    }

    public function rejectAsset(){
        //$assetReject = Asset::where('status','0')->with('debrief')->with('user')->with('lastReject')->orderBy('id','desc')->get();
        $assetWaitingReco = Asset::where('status','0')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDevis = Devi::where('status','0')->with('project')->with('userDP')->orderBy('id','desc')->get();
        //$assetWaitingOP = Assetop::where('status','1')->with('project')->with('userOP')->orderBy('id','desc')->get();
        //$assetWaitingCrea = Asset::where('status','0')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingFlow = Asset::where('status','0')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingPlan = Asset::where('status','0')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingPresta = Asset::where('status','0')->where('asset_type','bcp')->with('debrief')->with('user')->orderBy('id','desc')->get();
        //$assetWaitingDigit = Asset::where('status','0')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();

        return view('admin_lead_valid.asset_reject',compact('assetWaitingPresta','assetWaitingOP','assetReject','assetWaitingCrea','assetWaitingDevis','assetWaitingDigit','assetWaitingFlow','assetWaitingReco','assetWaitingPlan'));
    }

    public function validAssetUser($id){
        if($id){
            $asset = Asset::find($id);
            $asset->status = '2';
            $asset->save();

            if($asset->asset_type != 'devis'){
                $tach = Tach::find($asset->tache_id);
                $tach->status = '2';
                $tach->save();
            }

            $notif = new Notification();
            $notif->libelle = 'asset_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Votre ASSET a été validé",
            );
            $findUserMail = User::where('id',$asset->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('ASSET validé');
            });

            $notif = new Notification();
            $debr = Debrief::where('id',$asset->debrief_id)->first();
            $notif->libelle = 'asset_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $debr->user_id;
            $notif->asset_id = $id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Un ASSET a été validé",
            );
            $findUserMail = User::where('id',$debr->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('ASSET validé');
            });


            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }

    public function validAssetDevisUser($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //$asset = Devi::where('id',$allnotif->devi_id)->first();
            //$DGA = User::where('post','DGA')->first();
            $DGA = User::where('post','CONTROLE')->first();
            //dd($DGA);
            $notif = new Notification();
            $notif->libelle = 'asset_devis_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $DGA->id;
            $notif->devi_id = $allnotif->devi_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Un Devis ou Proforma en attente de validation",
            );
            //$findUserMail = User::where('id',$debr->user_id)->first();
            $emailMail = $DGA->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('DEVIS OU PROFORMA en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','le devis a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function validAssetOpUser($id){
        //dd($id);
        if($id){
            $asset = Assetop::find($id);
            $asset->status = '2';
            $asset->save();

            #GERE LA NOTIF
            /*$notif = new Notification();
            $notif->libelle = 'assetDevis_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->vue = '0';
            $notif->save();*/

            return redirect()->back()->with('success','l\'asset OP a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function cancelAsset(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $request['idAsset'];
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($request['idAsset']);
        $file->status='0';
        $file->save();

        $notif = new Notification();
        $notif->libelle = 'asset_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->asset_id = $request['idAsset'];
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"l'ASSET a été rejeté",
        );
        $findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('ASSET rejeté');
        });

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function cancelAssetDevis(Request $request){
        //dd($allnotif);
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idDevis']);

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->devi_id = $allnotif->devi_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Devi::find($allnotif->devi_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_devis_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"le DEVIS a été rejeté",
        );
        $findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('DEVIS rejeté');
        });

        $allnotif->delete();
        return redirect()->back()->with('success','le devis a été rejecté');

    }

    public function cancelAssetOp(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->assetop_id = $request['idOp'];
        $rejet->observe = $observation;
        $rejet->save();

        $file = Assetop::find($request['idOp']);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        /*$notif = new Notification();
        $notif->libelle = 'devis_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();*/

        return redirect()->back()->with('success','l\'asset OP a été rejecté');

    }

    public function validAssetBcp($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //$DGA = User::where('post','DGA')->first();
            $DGA = User::where('post','CONTROLE')->first();
            //dd($DGA);
            $notif = new Notification();
            $notif->libelle = 'bcp_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $DGA->id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Bon de commande prestataire en attente de validation",
            );
            //$findUserMail = User::where('id',$allnotif->asset_id)->first();
            $emailMail = $DGA->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('BON DE COMMANDE en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset bon de commande prestataire a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function cancelAssetBcp(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idbcp']);

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'bcp_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Bon de commande prestataire a été rejeté",
        );
        $findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('BON DE COMMANDE PRESTATAIRE rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset bon de commande prestataire a été rejecté');

    }

    public function notif(){
        $notifs = Notification::where('recip_id',Auth::user()->id)->take(50)->orderBy('id','desc')->get();
        //dd($notifs);
        $var=\DB::table('notifications')->where('recip_id',Auth::user()->id)->update(['vue' =>'1']);

        return view('admin_lead_valid.notif',compact('notifs'));
    }

    public function bc(){
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','1')->with('project')->with('userBC')->get();
        return view('admin_lead_valid.bc_waiting',compact('bcommandes'));
    }

    public function bcValid(){
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','2')->with('project')->with('userBC')->get();
        return view('admin_lead_valid.bc',compact('bcommandes'));
    }

    public function bcReject(){
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','0')->with('project')->with('lastReject')->with('userBC')->get();
        return view('admin_lead_valid.bc_reject',compact('bcommandes'));
    }

    public function validBC($id){
        if($id){
            $bc = Bcommande::find($id);
            $bc->status = '2';
            $bc->save();

            $pjt = Projet::find($bc->project_id);

            if($pjt->niveau == 1){
                $pjt->niveau = 2;
                $pjt->save();
            }

            $notif = new Notification();
            $notif->libelle = 'bc_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $bc->user_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le BON DE COMMANDE a été validé",
            );
            $findUserMail = User::where('id',$bc->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('BON DE COMMANDE validé');
            });

            $findUserMail2 = User::where('post','ACHAT')->first();
            $findUserMail3 = User::where('post','OP')->first();
            if($findUserMail2){
                $notif = new Notification();
                $notif->libelle = 'new_phase2';
                $notif->send_id = Auth::user()->id;
                $notif->recip_id = $findUserMail2->id;
                $notif->vue = '0';
                $notif->save();

                $data2=array(
                    "notifmessage"=>"Un job bag en passer en phase 2",
                );
                $emailMail2 = $findUserMail2->email;
                Mail::send('mails/valid',$data2,function ($message) use($emailMail2){
                    $message->to($emailMail2);
                    $message->subject('PROJECT EN PHASE 2');
                });
            }

            if($findUserMail3){
                $notif = new Notification();
                $notif->libelle = 'new_phase2';
                $notif->send_id = Auth::user()->id;
                $notif->recip_id = $findUserMail3->id;
                $notif->vue = '0';
                $notif->save();

                $data2=array(
                    "notifmessage"=>"Un job bag en passer en phase 2",
                );
                $emailMail2 = $findUserMail3->email;
                Mail::send('mails/valid',$data2,function ($message) use($emailMail3){
                    $message->to($emailMail3);
                    $message->subject('PROJECT EN PHASE 2');
                });
            }

            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function cancelBC(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->bc_id = $request['idbc'];
        $rejet->observe = $observation;
        $rejet->save();

        $bc = Bcommande::find($request['idbc']);
        $bc->status='0';
        $bc->save();

        $notif = new Notification();
        $notif->libelle = 'bc_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $bc->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le BON DE COMMANDE a été rejeté",
        );
        $findUserMail = User::where('id',$bc->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('BON DE COMMANDE rejeté');
        });

        return redirect()->back();

    }

    public function validSoumi($id){
        //dd($id);
        if($id){
            $bc = Soumission::find($id);
            $bc->status = '2';
            $bc->save();

            $pjt = Projet::find($bc->project_id);

            if($pjt->niveau == 2){
                $pjt->niveau = 3;
                $pjt->save();
            }

            $notif = new Notification();
            $notif->libelle = 'soumi_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $bc->user_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"JOB BAG PHASE 3 - Le job bag soumis est en phase 3 a été validé",
            );
            $findUserMail = User::where('id',$bc->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('JOB BAG PHASE 3');
            });

            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function cancelSoumi(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->soumi_id = $request['idsoumi'];
        $rejet->observe = $observation;
        $rejet->save();

        $bc = Soumission::find($request['idsoumi']);
        $bc->status='0';
        $bc->save();

        $notif = new Notification();
        $notif->libelle = 'soumi_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $bc->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"JOB BAG PHASE 3 - Le job bag soumis est en phase 3 a été rejeté",
        );
        $findUserMail = User::where('id',$bc->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('JOB BAG PHASE 3');
        });

        return redirect()->back();

    }

    public function nv3(){
        $attentnv3 = Soumission::with('userSomi')->with('projectSomi')->get();
        return view('admin_lead_valid.nv3_waiting',compact('attentnv3'));
    }

    public function listsvalid(){
        //dd("de");
        $project1 = Projet::where('niveau',1)->with('annonceurProject')->get();
        $project2 = Projet::where('niveau',2)->with('annonceurProject')->get();
        $project3 = Projet::where('niveau',3)->with('annonceurProject')->get();
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','1')->with('project')->with('userBC')->get();
        $soumettres = Soumission::orderBy('id','desc')->where('status','1')->with('projectSomi')->with('userSomi')->get();
        //dd($bcommandes);
        return view('admin_lead_valid.lists',compact('soumettres','project1','project2','project3','bcommandes'));
    }


}
