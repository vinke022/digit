<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Debrief;
use App\Models\Notification;
use App\Models\Tach;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class TraficUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //dd(Auth::user()->role_id);
        $taches = Tach::where('status','1')->with('userTache')->with('userAssignant')->orderBy('id','desc')->take(5)->get();
        $tachesAssign = Tach::where('status','0')->with('userTache')->with('userAssignant')->orderBy('id','desc')->take(5)->get();
        $assetValid = Asset::where('status','2')->count();
        $assetReject = Asset::where('status','0')->count();
        $briefAssigne = Debrief::where('dep',Auth::user()->departement_id)->with('project')->with('userBF')->where('status','2')->take(5)->orderBy('id','desc')->get();
        $assetEncours = Asset::where('status','1')->with('debrief')->with('user')->orderBy('id','desc')->take(5)->get();
        //dd($taches);
        return view('user_trafic.dashboard',compact('assetEncours','briefAssigne','taches','tachesAssign','assetValid','assetReject'));
    }

    public function tacheAssign(){
        $taches = Tach::where('status','0')->with('userTache')->with('userAssignant')->orderBy('id','desc')->get();
        return view('user_trafic.assign',compact('taches'));
    }

    public function tacheEdit($id){
        $taches = Tach::find($id);
        //$user = User::where('id',$taches->assignant_id)->first();
        //$usersDepartmts = User::where('departement_id',$user->departement_id)->where('role_id','!=',$user->role_id)->get();
        $usersDepartmts = User::whereNotIn('role_id',[1,3,4,7])->get();
        //dd($usersDepartmts);

        return view('user_trafic.editassign',compact('taches','usersDepartmts'));
    }

    public function tacheUpdate(Request $request){
        //dd($request->all());
        $insert_id = Tach::find($request['id']);
        $insert_id->assigne_id = $request['assigne_id'];
        $insert_id->save();

        return redirect(config('laraadmin.adminRoute') . '/trafic/tacheassign');

    }

    public function checkTachAssetTraf($id){
        $asset = Asset::where('tache_id',$id)->first();
        if($asset){
            $data = '1';
            return $data;
        }else{
            $data = '0';
            return $data;
        }

    }

    public function notif(){
        $notifs = Notification::where('recip_id',Auth::user()->id)->orderBy('id','desc')->take(50)->get();
        //dd($notifs);
        $var=\DB::table('notifications')->where('recip_id',Auth::user()->id)->update(['vue' =>'1']);

        return view('user_trafic.notif',compact('notifs'));
    }
}
