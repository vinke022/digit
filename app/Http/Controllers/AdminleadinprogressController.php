<?php

namespace App\Http\Controllers;

use App\Models\Brief;
use App\Models\Debrief;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminleadinprogressController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function inprogress(){
        $allbriefsValid = Debrief::where('status','2')->with('userBF')->with('project')->orderBy('id', 'desc')->get();
        //dd($allbriefsAttente);
        return view('admin_lead_valid.inprogress',compact('allbriefsValid'));
    }
}
