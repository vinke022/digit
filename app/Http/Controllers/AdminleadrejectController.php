<?php

namespace App\Http\Controllers;

use App\Models\Brief;
use App\Models\Debrief;
use Illuminate\Http\Request;

use App\Http\Requests;

class AdminleadrejectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function reject(){
        $allbriefsReject = Debrief::where('status','0')->with('userBF')->with('project')->with('lastReject')->orderBy('id', 'desc')->get();
        //dd($allbriefsReject);
        return view('admin_lead_valid.reject',compact('allbriefsReject'));
    }
}
