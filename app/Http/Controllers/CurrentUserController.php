<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\Notification;
use App\Models\Tach;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CurrentUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $taches = Tach::where('assigne_id',Auth::user()->id)->where('status','1')->where('temp','0')->orderBy('id','desc')->take(5)->get();
        $tachesAssign = Tach::where('assigne_id',Auth::user()->id)->where('status','0')->orderBy('id','desc')->take(5)->get();
        $assetValid = Asset::where('user_id',Auth::user()->id)->where('status','2')->count();
        $assetReject = Asset::where('user_id',Auth::user()->id)->where('status','0')->count();
        //dd($taches);
        return view('current_user.dashboard',compact('taches','tachesAssign','assetValid','assetReject'));
    }

    public function checkTachAsset($id){
        $asset = Asset::where('tache_id',$id)->first();
        if($asset){
            $data = '1';
            return $data;
        }else{
            $data = '0';
            return $data;
        }

    }

    public function assign(){
        $tachesAssign = Tach::where('assigne_id',Auth::user()->id)->where('status','0')->orderBy('id','desc')->get();
        return view('current_user.assign',compact('tachesAssign'));
    }

    public function progess(){
        $tachesEncours = Tach::where('assigne_id',Auth::user()->id)->where('status','1')->where('temp','0')->orderBy('id','desc')->get();
        return view('current_user.progress',compact('tachesEncours'));
    }

    public function termintache(){
        $tachesEncours = Tach::where('assigne_id',Auth::user()->id)->where('status','1')->where('temp','1')->orderBy('id','desc')->get();
        return view('current_user.finish_attent',compact('tachesEncours'));
    }

    public function sendtachvalidate($id){
        //dd($id);
        $asset = Asset::where('tache_id',$id)->first();
        //dd($asset);
        if($asset){
            $tache = Tach::find($id);
            $tache->temp = '1';
            $tache->save();
            return redirect('/')->with('success','Tâche en attente de validation');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez ajouter un asset');
        }
    }

    public function finish(){
        $tachesTerminer = Tach::where('assigne_id',Auth::user()->id)->where('status','2')->orderBy('id','desc')->get();
        return view('current_user.finish',compact('tachesTerminer'));
    }

    public function rejet(){
        $tachesRejet = Asset::where('user_id',Auth::user()->id)->with('lastReject')->where('status','0')->orderBy('id','desc')->get();
        return view('current_user.rejet',compact('tachesRejet'));
    }

    public function accept($id){
        if(isset($id)){
            $tache = Tach::find($id);
            $tache->status='1';
            $tache->save();

            $notif = new Notification();
            $notif->libelle = 'tache_demarrer';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $tache->assignant_id;
            $notif->debrief_id = $tache->debrief_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"La tache assigné à ".$notif->userAssigneNotif->name." a démarré",
            );
            $findUserMail = User::where('id',$tache->assignant_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Tâche debutée');
            });
            //dd($tache);
            return redirect()->back();
        }else{
            return redirect()->back();
        }
    }

    public function notif(){
        $notifs = Notification::where('recip_id',Auth::user()->id)->orderBy('id','desc')->take(50)->get();
        //dd($notifs);
        $var=\DB::table('notifications')->where('recip_id',Auth::user()->id)->update(['vue' =>'1']);

        return view('current_user.notif',compact('notifs'));
    }
}
