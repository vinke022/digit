<?php

namespace App\Http\Controllers;

use App\Models\Annonceur;
use App\Models\Asset;
use App\Models\Bcommande;
use App\Models\Brief;
use App\Models\Debrief;
use App\Models\Devi;
use App\Models\Notification;
use App\Models\Tach;
use App\Models\User;
use App\Models\Validation;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MarketingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showAnnonceurPjt($id){
        $data = Annonceur::where('id',$id)->select('name')->first();
        return $data->name;
    }

    public function index(){
        if(Auth::user()->post=='DCOMM'){
            //$allbriefsBriefsAttente = Brief::where('status','1')->with('project')->with('userBF')->orderBy('id', 'desc')->take(5)->get();
            $allbriefsAttente = Debrief::where('status','1')->with('project')->with('userBF')->orderBy('id', 'desc')->take(5)->get();
            $allbriefsvalid = Debrief::where('status','2')->with('project')->with('userBF')->orderBy('id', 'desc')->take(5)->get();
            return view('marketing.dashboard',compact('allbriefsAttente','allbriefsvalid'));
        }else{
            $allbriefsAttente = Debrief::where('status','1')->with('project')->with('userBF')->where('user_id',Auth::user()->id)->orderBy('id', 'desc')->take(5)->get();
            $allbriefsRejects = Debrief::where('status','0')->with('project')->with('userBF')->where('user_id',Auth::user()->id)->orderBy('id', 'desc')->take(5)->get();
            $allbriefsReject = Debrief::where('status','0')->with('project')->with('userBF')->where('user_id',Auth::user()->id)->count();
            $tachesencours = Tach::where('status','1')->count();
            return view('marketing.dashboard',compact('allbriefsAttente','allbriefsReject','tachesencours','allbriefsRejects'));
        }
    }

    public function attente(){
        $stat=["1","3"];
        if(Auth::user()->post=='DCOMM'){
            $allbriefsAttente = Debrief::where('status','1')->with('project')->with('userBF')->orderBy('id', 'desc')->get();
            //dd($allbriefsAttente);
        }else{
            $allbriefsAttente = Debrief::whereIn('status',$stat)->with('project')->with('userBF')->where('user_id',Auth::user()->id)->orderBy('id', 'desc')->get();
        }
        return view('marketing.attente',compact('allbriefsAttente'));
    }

    public function valid(){
        if(Auth::user()->post=='DCOMM'){
            $allbriefsvalid = Debrief::where('status','2')->with('project')->with('userBF')->orderBy('id', 'desc')->get();
        }else{
            $allbriefsvalid = Debrief::where('status','2')->with('project')->with('userBF')->where('user_id',Auth::user()->id)->orderBy('id', 'desc')->get();
        }
        return view('marketing.valid',compact('allbriefsvalid'));
    }

    public function reject(){
        if(Auth::user()->post=='DCOMM'){
            $allbriefsreject = Debrief::where('status','0')->with('project')->with('userBF')->with('lastReject')->orderBy('id', 'desc')->get();
        }else{
            $allbriefsreject = Debrief::where('status','0')->with('project')->with('userBF')->where('user_id',Auth::user()->id)->with('lastReject')->orderBy('id', 'desc')->get();
        }
        return view('marketing.reject',compact('allbriefsreject'));
    }

    public function assetvalid(){
        if(Auth::user()->post=='DCOMM'){
            $assetValid = Devi::where('status','2')->with('project')->with('userDP')->orderBy('id','desc')->get();
        }else{
            $assetValid = Devi::where('status','2')->where('user_id',Auth::user()->id)->with('project')->with('userDP')->orderBy('id','desc')->get();
        }
        return view('marketing.asset_valid',compact('assetValid'));
    }

    public function assetattents(){
        if(Auth::user()->post=='DCOMM'){
            $assetAttente = Devi::where('status','1')->with('project')->with('userDP')->orderBy('id','desc')->get();
        }else{
            $assetAttente = Devi::where('status','1')->where('user_id',Auth::user()->id)->with('project')->with('userDP')->orderBy('id','desc')->get();
        }
        return view('marketing.asset_attente',compact('assetAttente'));
    }

    public function assetreject(){
        if(Auth::user()->post=='DCOMM'){
            $assetReject = Devi::where('status','0')->with('project')->with('userDP')->with('lastReject')->orderBy('id','desc')->get();
        }else{
            $assetReject = Devi::where('status','0')->with('project')->with('userDP')->where('user_id',Auth::user()->id)->with('lastReject')->orderBy('id','desc')->get();
        }
        return view('marketing.asset_reject',compact('assetReject'));
    }

    public function bc(){
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','1')->with('userBC')->with('project')->get();
        return view('marketing.bc_waiting',compact('bcommandes'));
    }

    public function bcValid(){
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','2')->with('userBC')->with('project')->get();
        return view('marketing.bc',compact('bcommandes'));
    }

    public function bcReject(){
        $bcommandes =Bcommande::orderBy('id','desc')->where('status','0')->with('lastReject')->with('project')->with('userBC')->get();
        return view('marketing.bc_reject',compact('bcommandes'));
    }

    public function notif(){
        $notifs = Notification::where('recip_id',Auth::user()->id)->orderBy('id','desc')->take(50)->get();
        //dd($notifs);
        $var=\DB::table('notifications')->where('recip_id',Auth::user()->id)->update(['vue' =>'1']);

        return view('marketing.notif',compact('notifs'));
    }

    public function waitingAsset(){
        /*$assetWaiting = Asset::where('status','1')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingReco = Asset::where('status','1')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingCrea = Asset::where('status','1')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingFlow = Asset::where('status','1')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingPlan = Asset::where('status','1')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDigit = Asset::where('status','1')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();
        */
        $assetWaitingCrea = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_crea_a_valide')->with('asset')->get();
        $assetWaitingProd = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_prod_a_valide')->with('asset')->get();
        $assetWaitingLivr = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_livrable_a_valide')->with('asset')->get();
        $assetWaitingRetro = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_retro_a_valide')->with('asset')->get();
        $assetWaitingPlanning = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_planpubli_a_valide')->with('asset')->get();
        $assetWaitingGoogle = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_gglads_a_valide')->with('asset')->get();
        $assetWaitingRapportOp = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_rapportop_a_valide')->with('asset')->get();

        //dd($assetWaitingCrea);
        //$assetWaitingDevis = Devi::where('status','1')->with('project')->with('userDP')->orderBy('id','desc')->get();
        //$assetWaitingOP = Assetop::where('status','1')->with('project')->with('userOP')->orderBy('id','desc')->get();
        //dd($assetWaitingOP);
        return view('marketing.asset_valid_waiting',compact('assetWaitingRapportOp','assetWaitingPlanning','assetWaitingGoogle','assetWaitingCrea','assetWaitingProd','assetWaitingLivr','assetWaitingRetro'));
    }

    public function validAsset($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            $DA = User::where('post','DA')->first();
            $notif = new Notification();
            $notif->libelle = 'asset_crea_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $DA->id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Créa en attente de validation",
            );
            //$findUserMail = User::where('id',$tache->assignant_id)->first();
            $emailMail = $DA->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Crea en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function rejectAsset(Request $request){
        //dd($request->all());
        $allnotif = Notification::find($request['idAsset']);
        //dd($allnotif);
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_crea_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"La Créa a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Crea rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function validAssetProd($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            $DA = User::where('post','DA')->first();

            $notif = new Notification();
            $notif->libelle = 'asset_prod_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $DA->id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"L'asset Production en attente de validation",
            );
            //$findUserMail = User::where('id',$allnotif->send_id)->first();
            $emailMail = $DA->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Asset Production en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function rejectAssetProd(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['prodfile']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_prod_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"L'asset Production a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Asset Production rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function validAssetRetro($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);
            $asset = Asset::find($allnotif->asset_id);
            //$DGA = User::where('post','DGA')->first();
            $DGA = User::where('post','CONTROLE')->first();

            $notif = new Notification();
            $notif->libelle = 'asset_retro_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $DGA->id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le Retro planning en attente de validation",
            );
            //$findUserMail = User::where('id',$allnotif->send_id)->first();
            $emailMail = $DGA->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Retro planning en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function validAssetLivre($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);

            $asset = Asset::find($allnotif->asset_id);
            $asset->status='2';
            $asset->save();

            $notif = new Notification();
            $notif->libelle = 'asset_livrable_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le Livrable a été validé",
            );
            $findUserMail = User::where('id',$asset->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Livrable validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }

    public function rejectAssetLivre(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['livrefile']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_livrable_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_livrable_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le Livrable a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Livrable rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function validAssetRapportOp($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);

            $asset = Asset::find($allnotif->asset_id);
            $asset->status='2';
            $asset->save();

            $notif = new Notification();
            $notif->libelle = 'asset_rapportop_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            $notif = new Notification();
            $notif->libelle = 'asset_rapportop_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $allnotif->send_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le Rapport Op a été validé",
            );
            $findUserMail = User::where('id',$asset->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Rapport Op validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }

    public function rejectAssetRapportOp(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['rapportopid']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_rapportop_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_rapportop_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le Rapport Op a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Livrable rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function rejectAssetRetro(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idretro']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_retro_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_retro_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le Retro planning a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Retro planning rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function validAssetPlan($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);
            $asset = Asset::find($allnotif->asset_id);
            $asset->status = '2';
            $asset->save();

            if($asset->asset_type != 'devis'){
                $tach = Tach::find($asset->tache_id);
                $tach->status = '2';
                $tach->save();
            }

            $notif = new Notification();
            $notif->libelle = 'asset_planpubli_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $allnotif->send_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            $notif = new Notification();
            $notif->libelle = 'asset_planpubli_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le Planning de publication a été validé",
            );
            $findUserMail = User::where('id',$allnotif->send_id)->first();
            $findUserMail2 = User::where('id',$asset->user_id)->first();
            $emailMail = [$findUserMail->email,$findUserMail2->email];
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Planning de publication validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }
    public function validAssetGgle($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);
            $asset = Asset::find($allnotif->asset_id);
            $asset->status = '2';
            $asset->save();

            if($asset->asset_type != 'devis'){
                $tach = Tach::find($asset->tache_id);
                $tach->status = '2';
                $tach->save();
            }

            $notif = new Notification();
            $notif->libelle = 'asset_gglads_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $allnotif->send_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            $notif = new Notification();
            $notif->libelle = 'asset_gglads_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Visuel Google Ads a été validé",
            );
            $findUserMail = User::where('id',$allnotif->send_id)->first();
            $findUserMail2 = User::where('id',$asset->user_id)->first();
            $emailMail = [$findUserMail->email,$findUserMail2->email];
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Visuel Google Ads validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }
    public function rejectAssetPlan(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }
        $allnotif = Notification::find($request['idplan']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_planpubli_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_planpubli_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le Planning de publication a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Planning de publication rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }
    public function rejectAssetGgle(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }
        $allnotif = Notification::find($request['ggleid']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_gglads_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_gglads_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Visuel Google Ads a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Visuel Google Ads rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function annonceurs(){
        $annonceurs = Annonceur::where('user_id',Auth::user()->id)->get();
        //dd($annonceurs);
        return view('marketing.annonceurs',compact('annonceurs'));

    }

    public function addannonceurs(Request $request){

    }

    public function deleteDevis(Request $request){
        //dd($request->all());
        $id = $request['idrapport'];
        if($id){
            $devis = Devi::find($id);

            if($devis->status != '2'){
                $devis->motif = $request['message'];
                $devis->status = '3';
                $devis->save();

                $usersValid = User::select('id','email')->where('role_id',7)->first();

                #GERE LA NOTIF
                $notif = new Notification();
                $notif->libelle = 'devis_a_supprimer';
                $notif->send_id = Auth::user()->id;
                $notif->recip_id = $usersValid->id;
                $notif->vue = '0';
                $notif->save();

                //NOTIFICATION PAR MAIL
                $data=array(
                    "notifmessage"=>"Un devis en attente de suppression",
                );
                //$findUserMail = User::where('id',$debr->user_id)->first();
                $emailMail = $usersValid->email;
                Mail::send('mails/valid',$data,function ($message) use($emailMail){
                    $message->to($emailMail);
                    $message->subject('Bon de commande en attente de validation');
                });
            }else{
                $devis->delete();
            }



            return redirect()->back()->with('success','Attente de validation');
        }else{
            return redirect()->back()->with('error','Une erreur est survenu veuillez recommencer');
        }
    }


}
