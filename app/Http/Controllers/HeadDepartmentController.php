<?php

namespace App\Http\Controllers;

use App\Models\Annonceur;
use App\Models\Asset;
use App\Models\Assetype;
use App\Models\Brief;
use App\Models\Debrief;
use App\Models\Notification;
use App\Models\Projet;
use App\Models\Tach;
use App\Models\User;
use App\Models\Validation;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HeadDepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showAnnonceurPjt($id){
        $data = Annonceur::where('id',$id)->select('name')->first();
        return $data->name;
    }

    public function index(){
        //dd("e");->where('role_id','!=',Auth::user()->role_id)
        $usersDepartmts = User::where('departement_id',Auth::user()->departement_id)->with('tacheUsers')->get();
        $usersDepartmtsEncours = User::where('departement_id',Auth::user()->departement_id)->with('tacheUsersEncours')->get();
        $IdusersDepartmts = User::where('departement_id',Auth::user()->departement_id)->pluck('id')->toArray();
        $tachencours = Tach::where('assignant_id',Auth::user()->id)->where('status','1')->count();
        $usersDepartmtsAssigne = Tach::whereIn('assigne_id',$IdusersDepartmts)->with('userTache')->with('userAssignant')->where('status','0')->get();
        //dd($usersDepartmtsAssigne);
        $briefAssigne = Debrief::where('dep',Auth::user()->departement_id)->with('project')->with('userBF')->where('status','2')->take(5)->orderBy('id','desc')->get();
        //$briefAttente = Brief::where('dep',Auth::user()->departement_id)->where('status','1')->take(5)->orderBy('id','desc')->get();
        $usersDepartmtsId = array();
        if(count($usersDepartmts)>0){
            foreach ($usersDepartmts as $user){
                $usersDepartmtsId[] =$user->id ;
            }
        }
        $assetEncours = Asset::whereIn('user_id',$usersDepartmtsId)->where('status','1')->with('debrief')->with('user')->orderBy('id','desc')->take(5)->get();
        //dd($assetEncours);
        return view('head_department.dashboard',compact('usersDepartmtsAssigne','usersDepartmts','briefAssigne','assetEncours','usersDepartmtsEncours','tachencours'));
    }

    public function assign(){
        $briefAssigne = Debrief::where('dep',Auth::user()->departement_id)->where('status','2')->with('project')->with('userBF')->orderBy('id','desc')->get();
        return view('head_department.brief_assign',compact('briefAssigne'));
    }

    public function reassign($id){
        if($id){
            $reassign = Debrief::find($id);
            $reassign->dep='9';
            $reassign->save();

            $usersValid = User::select('id','email')->where('post','TRAFIC')->first();

            $notif = new Notification();
            $notif->libelle = 'brief_requalifier';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $usersValid->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Brief en attente de validation",
            );
            //$findUserMail = User::where('id',$debr->user_id)->first();
            $emailMail = $usersValid->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Brief requalifier');
            });

            return redirect()->back()->with('success','le debrief a été réassigné');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function tacheAssignDep(){
        $IdusersDepartmts = User::where('departement_id',Auth::user()->departement_id)->pluck('id')->toArray();
        $usersDepartmtsAssigne = Tach::whereIn('assigne_id',$IdusersDepartmts)->with('userTache')->with('userAssignant')->get();
        //->where('status','0')
        return view('head_department.assign',compact('usersDepartmtsAssigne'));
    }

    public function tacheAssignDepEdit($id){
        $taches = Tach::find($id);
        $usersDepartmts = User::where('departement_id',Auth::user()->departement_id)->get();
        //dd($usersDepartmts);
        return view('head_department.editassign',compact('taches','usersDepartmts'));
    }

    public function tacheAssignDepUpdate(Request $request){
        //dd($request->all());
        $insert_id = Tach::find($request['id']);
        $insert_id->assigne_id = $request['assigne_id'];
        $insert_id->save();
        //GERE NOTIFICATION D4ASSIGNATION
        $notif = new Notification();
        $notif->libelle = 'tache_assigne';
        $notif->send_id = $request['assignant_id'];
        $notif->recip_id = $request['assigne_id'];
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Une tâche vous est assignée",
        );
        $findUserMail = User::where('id',$request['assigne_id'])->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Tâche assignée');
        });

        return redirect('/');
    }

    public function user(){
        $usersDepartmts = User::where('departement_id',Auth::user()->departement_id)->where('role_id','!=',Auth::user()->role_id)->with('tacheUsers')->get();
        return view('head_department.users',compact('usersDepartmts'));
    }

    public function notif(){
        $notifs = Notification::where('recip_id',Auth::user()->id)->with('userAssigneNotif')->orderBy('id','desc')->take(50)->get();
        //dd($notifs);
        $var=\DB::table('notifications')->where('recip_id',Auth::user()->id)->update(['vue' =>'1']);

        return view('head_department.notif',compact('notifs'));
    }

    public function waitingAsset(){
        /*$assetWaiting = Asset::where('status','1')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingReco = Asset::where('status','1')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingCrea = Asset::where('status','1')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingFlow = Asset::where('status','1')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingPlan = Asset::where('status','1')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDigit = Asset::where('status','1')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();
        */
        $assetWaitingFiche = Asset::where('status','1')->where('asset_type','fichefabric')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingProd = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_prod_a_valide')->with('asset')->get();
        $assetWaitingCrea = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_crea_a_valide')->with('asset')->get();
        $assetWaitingProto = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_prototype_a_valide')->with('asset')->get();
        //dd($assetWaitingCrea);
        //$assetWaitingDevis = Devi::where('status','1')->with('project')->with('userDP')->orderBy('id','desc')->get();
        //$assetWaitingOP = Assetop::where('status','1')->with('project')->with('userOP')->orderBy('id','desc')->get();
        //dd($assetWaitingOP);
        return view('head_department.asset_valid_waiting',compact('assetWaitingProto','assetWaitingCrea','assetWaitingFiche','assetWaitingProd'));
    }

    public function waitingAssetOp(){
        /*$assetWaiting = Asset::where('status','1')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingReco = Asset::where('status','1')->where('asset_type','reco')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingCrea = Asset::where('status','1')->where('asset_type','crea')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingFlow = Asset::where('status','1')->where('asset_type','flowplane')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingPlan = Asset::where('status','1')->where('asset_type','planmedia')->with('debrief')->with('user')->orderBy('id','desc')->get();
        $assetWaitingDigit = Asset::where('status','1')->where('asset_type','digital')->with('debrief')->with('user')->orderBy('id','desc')->get();
        */
        $assetWaitingRapportOp = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_rapportop_a_valide')->with('asset')->get();
        $assetWaitingLivr = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_livrable_a_valide')->with('asset')->get();
        $assetWaitingRetro = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_retro_a_valide')->with('asset')->get();
        //dd($assetWaitingCrea);
        //$assetWaitingDevis = Devi::where('status','1')->with('project')->with('userDP')->orderBy('id','desc')->get();
        //$assetWaitingOP = Assetop::where('status','1')->with('project')->with('userOP')->orderBy('id','desc')->get();
        //dd($assetWaitingOP);
        return view('head_department.asset_valid_waiting_op',compact('assetWaitingLivr','assetWaitingRetro','assetWaitingRapportOp'));
    }

    public function waitingAssetCdp(){

        $assetWaitingPlanning = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_planpubli_a_valide')->with('asset')->get();
        $assetWaitingGoogle = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_gglads_a_valide')->with('asset')->get();
        $assetWaitingRapport = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_rapport_a_valide')->with('asset')->get();
        $assetWaitingRapportTrim = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_rapportrim_a_valide')->with('asset')->get();
        //dd($assetWaitingCrea);
        return view('head_department.asset_valid_waiting_cpd',compact('assetWaitingPlanning','assetWaitingGoogle','assetWaitingRapport','assetWaitingRapportTrim'));
    }

    public function waitingAssetRd(){

        $assetWaitingRapport = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_rapport_a_valide')->with('asset')->get();
        $assetWaitingRapportTrim = Notification::where('recip_id',Auth::user()->id)->where('asset_id','!=','0')->where('libelle','asset_rapportrim_a_valide')->with('asset')->get();
        //dd($assetWaitingCrea);
        return view('head_department.asset_valid_waiting_rd',compact('assetWaitingRapport','assetWaitingRapportTrim'));
    }

    public function validAsset($id){
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);
            $asset = Asset::find($allnotif->asset_id);
            $asset->status = '2';
            $asset->save();

            if($asset->asset_type != 'devis'){
                $tach = Tach::find($asset->tache_id);
                if($tach){
                    $tach->status = '2';
                    $tach->save();
                }
            }

            $notif = new Notification();
            $notif->libelle = 'asset_crea_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $allnotif->send_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            $notif = new Notification();
            $notif->libelle = 'asset_crea_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"La Créa a été validé",
            );
            $findUserMail = User::where('id',$allnotif->send_id)->first();
            $findUserMail2 = User::where('id',$asset->user_id)->first();
            $emailMail = [$findUserMail->email,$findUserMail2->email];
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Créa validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }

    public function validAssetFiche($id){
        if($id){
            $asset = Asset::find($id);
            $asset->status = '2';
            $asset->save();

            $tach = Tach::find($asset->tache_id);
            $tach->status = '2';
            $tach->save();

            $notif = new Notification();
            $notif->libelle = 'asset_fiche_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"La Fiche de fabrication a été validé",
            );
            $findUserMail = User::where('id',$asset->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Fiche de fabrication validé');
            });

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }

    public function rejectAsset(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idAsset']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_crea_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_crea_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"La Créa a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Créa rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function rejectAssetFiche(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $file = Asset::find($request['fileid']);
        $file->status='0';
        $file->save();

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $request['fileid'];
        $rejet->observe = $observation;
        $rejet->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_fiche_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"La Fiche de fabrication a été rejeté",
        );
        $findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Fiche de fabrication rejeté');
        });

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function validAssetProd($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);

            $asset = Asset::find($allnotif->asset_id);
            $asset->status = '2';
            $asset->save();

            $tach = Tach::find($asset->tache_id);
            $tach->status = '2';
            $tach->save();

            $notif = new Notification();
            $notif->libelle = 'asset_prod_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            $DA = User::where('post','DA')->first();
            $notif = new Notification();
            $notif->libelle = 'asset_prod_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $allnotif->send_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"L'asset Production a été validé",
            );
            $findUserMail = User::where('id',$allnotif->send_id)->first();
            $findUserMail2 = User::where('id',$asset->user_id)->first();
            $emailMail = [$findUserMail->email,$findUserMail2->email];
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Asset Production validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function validAssetProto($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);

            $asset = Asset::find($allnotif->asset_id);
            $asset->status = '2';
            $asset->save();

            $tach = Tach::find($asset->tache_id);
            $tach->status = '2';
            $tach->save();

            $notif = new Notification();
            $notif->libelle = 'asset_prototype_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();


            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"L'asset Prototype a été validé",
            );
            $findUserMail = User::where('id',$asset->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Prototype validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function rejectAssetProd(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['prodfile']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_prod_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_prod_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"L'asset Production a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Asset Production rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function rejectAssetProto(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['protofile']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_prototype_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"L'asset Prototype a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Prototype rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function validAssetRetro($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);

            $asset = Asset::find($allnotif->asset_id);
            $debrief = Debrief::where('id',$asset->debrief_id)->first();

            $notif = new Notification();
            $notif->libelle = 'asset_retro_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $debrief->user_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le Retro planning en attente de validation",
            );
            $findUserMail = User::where('id',$debrief->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Retro planning en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function validAssetLivre($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);

            $asset = Asset::find($allnotif->asset_id);
            $debrief = Debrief::where('id',$asset->debrief_id)->first();

            $notif = new Notification();
            $notif->libelle = 'asset_livrable_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $debrief->user_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"L'asset Livrable en attente de validation",
            );
            $findUserMail = User::where('id',$debrief->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Livrable en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }

    public function validAssetRapportOP($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);

            $asset = Asset::find($allnotif->asset_id);
            $debrief = Debrief::where('id',$asset->debrief_id)->first();

            $notif = new Notification();
            $notif->libelle = 'asset_rapportop_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $debrief->user_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"L'asset RAPPORT OPE en attente de validation",
            );
            $findUserMail = User::where('id',$debrief->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Rapport Op en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }

    }

    public function rejectAssetRetro(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idretro']);
        //$allnotif = Notification::find($request['prodfile']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_retro_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_retro_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le Retro planning a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Retro planning rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function rejectAssetLivre(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['livrefile']);
        //$allnotif = Notification::find($request['prodfile']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_livrable_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_livrable_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"L'asset Production a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Asset Production rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    public function rejectAssetRapportOP(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['rapportopfile']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_rapportop_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"L'asset Rapport Op a été rejeté",
        );
        $findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Asset Rapport Op rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    //CHEF DE PROJET DIGITAL
    public function validAssetPlan($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);
            $asset = Asset::find($allnotif->asset_id);
            $debrief = Debrief::where('id',$asset->debrief_id)->first();

            $notif = new Notification();
            $notif->libelle = 'asset_planpubli_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $debrief->user_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"L'asset Planning de publication en attente de validation",
            );
            $findUserMail = User::where('id',$debrief->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Planning de publication en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }
    public function validAssetGgle($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);
            $asset = Asset::find($allnotif->asset_id);
            $debrief = Debrief::where('id',$asset->debrief_id)->first();

            $notif = new Notification();
            $notif->libelle = 'asset_gglads_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $debrief->user_id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Visuel Google Ads en attent de validation",
            );
            $findUserMail = User::where('id',$debrief->user_id)->first();
            $emailMail = $findUserMail->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Visuel Google Ads en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }
    public function validAssetRapport($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);

            $asset = Asset::find($allnotif->asset_id);
            $user = User::where('post','RD')->first();

            $notif = new Notification();
            $notif->libelle = 'asset_rapport_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $user->id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Un Rapport est en attente de validation",
            );
            //$findUserMail = User::where('id',$debrief->user_id)->first();
            $emailMail = $user->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Rapport en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function validAssetRapportrim($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);

            $asset = Asset::find($allnotif->asset_id);
            $user = User::where('post','RD')->first();

            $notif = new Notification();
            $notif->libelle = 'asset_rapportrim_a_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $user->id;
            $notif->asset_id = $asset->id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Un Rapport trimestriel est en attente de validation",
            );
            //$findUserMail = User::where('id',$debrief->user_id)->first();
            $emailMail = $user->email;
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Rapport en attente de validation');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }

    public function rejectAssetPlan(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idplan']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_planpubli_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"le Planning de publication a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Planning de publication rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }
    public function rejectAssetGgle(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['ggleid']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_gglads_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Visuel Google Ads a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Visuel Google Ads rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }
    public function rejectAssetRapport(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idrapport']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_rapport_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le Rapport a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Rapport rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }
    public function rejectAssetRapportrim(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $allnotif = Notification::find($request['idrapportrim']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_rapportrim_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le Rapport a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Rapport rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }

    //RD
    public function validAssetRapportRd($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);
            $asset = Asset::find($allnotif->asset_id);
            $asset->status = '2';
            $asset->save();

            if($asset->asset_type != 'devis'){
                $tach = Tach::find($asset->tache_id);
                $tach->status = '2';
                $tach->save();
            }

            $notif = new Notification();
            $notif->libelle = 'asset_rapport_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $allnotif->send_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            $notif = new Notification();
            $notif->libelle = 'asset_rapport_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le Rapport a été validé",
            );
            $findUserMail = User::where('id',$allnotif->send_id)->first();
            $findUserMail2 = User::where('id',$asset->user_id)->first();
            $emailMail = [$findUserMail->email,$findUserMail2->email];
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Rapport validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }
    public function rejectAssetRapportRd(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }


        $allnotif = Notification::find($request['idrapport']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_rapport_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_rapport_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le Rapport a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Rapport rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }
    public function validAssetRapportrimRd($id){
        //dd($id);
        if($id){
            $allnotif = Notification::find($id);
            //dd($allnotif);
            $asset = Asset::find($allnotif->asset_id);
            $asset->status = '2';
            $asset->save();

            if($asset->asset_type != 'devis'){
                $tach = Tach::find($asset->tache_id);
                $tach->status = '2';
                $tach->save();
            }

            $notif = new Notification();
            $notif->libelle = 'asset_rapportrim_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $allnotif->send_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            $notif = new Notification();
            $notif->libelle = 'asset_rapportrim_valide';
            $notif->send_id = Auth::user()->id;
            $notif->recip_id = $asset->user_id;
            $notif->asset_id = $allnotif->asset_id;
            $notif->vue = '0';
            $notif->save();

            //NOTIFICATION PAR MAIL
            $data=array(
                "notifmessage"=>"Le Rapport a été validé",
            );
            $findUserMail = User::where('id',$allnotif->send_id)->first();
            $findUserMail2 = User::where('id',$asset->user_id)->first();
            $emailMail = [$findUserMail->email,$findUserMail2->email];
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Rapport validé');
            });

            $allnotif->delete();

            return redirect()->back()->with('success','l\'asset a été validé');
        }else{
            return redirect()->back()->with('error','Désolé ! veuillez recommencez');
        }
    }
    public function rejectAssetRapportrimRd(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }


        $allnotif = Notification::find($request['idrapportrim']);
        //dd($allnotif);
        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->asset_id = $allnotif->asset_id;
        $rejet->observe = $observation;
        $rejet->save();

        $file = Asset::find($allnotif->asset_id);
        $file->status='0';
        $file->save();

        #GERE LA NOTIF
        $notif = new Notification();
        $notif->libelle = 'asset_rapportrim_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $allnotif->send_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        $notif = new Notification();
        $notif->libelle = 'asset_rapportrim_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->asset_id = $allnotif->asset_id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le Rapport a été rejeté",
        );
        $findUserMail = User::where('id',$allnotif->send_id)->first();
        $findUserMail2 = User::where('id',$file->user_id)->first();
        $emailMail = [$findUserMail->email,$findUserMail2->email];
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Rapport rejeté');
        });

        $allnotif->delete();

        return redirect()->back()->with('success','l\'asset a été rejecté');

    }


    public function validBrief($id){
        //dd($id);
        $debrief = Debrief::find($id);
        $debrief->status='2';
        $debrief->save();

        $file = Brief::find($debrief->brief_id);
        if($file->status == '1'){
            $file->status='2';
            $file->save();
        }

        $notif = new Notification();
        $notif->libelle = 'brief_valide';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->debrief_id = $debrief->id;
        $notif->vue = '0';
        $notif->save();

        if($debrief->dep==6){
            $userdep = User::where('post','RSTRAT')->first();
        }else{
            $userdep = User::where('post','TRAFIC')->first();
        }
        $notif = new Notification();
        $notif->libelle = 'brief_assigner';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $userdep->id;
        $notif->debrief_id = $debrief->id;
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le DEBRIEF a été validé",
        );
        $findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Debrief validé');
        });

        $data=array(
            "notifmessage"=>"DEBRIEF assigné",
        );
        $emailMail = $userdep->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Debrief assigné');
        });

        return redirect()->back()->with('success','le debrief a été validé');
    }

    public function cancelBrief(Request $request){
        //dd($request->all());
        $observation = trim(htmlspecialchars($request['message']));
        if(empty($observation)){
            return redirect()->back()->with('error','Veuillez saisir le motif');
        }

        $rejet = new Validation();
        $rejet->user_id = Auth::user()->id;
        $rejet->debrief_id = $request['idbrief'];
        $rejet->observe = $observation;
        $rejet->save();

        $file = Debrief::find($request['idbrief']);
        $file->status='0';
        $file->save();

        $fileBrief = Brief::find($file->brief_id);
        if($fileBrief->status == '1' or $fileBrief->status == '0'){
            $fileBrief->status='0';
            $fileBrief->save();
        }

        $notif = new Notification();
        $notif->libelle = 'brief_rejete';
        $notif->send_id = Auth::user()->id;
        $notif->recip_id = $file->user_id;
        $notif->debrief_id = $request['idbrief'];
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Le DEBRIEF a été rejeté",
        );
        $findUserMail = User::where('id',$file->user_id)->first();
        $emailMail = $findUserMail->email;
        if($emailMail){
            Mail::send('mails/valid',$data,function ($message) use($emailMail){
                $message->to($emailMail);
                $message->subject('Debrief rejeté');
            });
        }


        return redirect()->back()->with('success','l\'asset a été rejeté');

    }

    public function myAssetRejet(){
        $tachesRejet = Asset::where('user_id',Auth::user()->id)->with('lastReject')->where('status','0')->orderBy('id','desc')->get();
        return view('head_department.asset_rejet',compact('tachesRejet'));
    }

    public function assetDigitCdp(){
        $asset = ['planpubli','rapport','rapportrim'];
        $listAssetypes = Assetype::select('name','libelle')->whereIn('name',$asset)->get();
        $assetypes = $listAssetypes->toArray();

        return view('head_department.asset_digital_cpd',compact('assetypes'));
    }

    public function showAnnonceurByAsset($id){
        //select project
        $pjt = Debrief::where('id',$id)->first();
        $annonceur = Projet::where('id',$pjt->project_id)->with('annonceurProject')->first();
        return $annonceur->annonceurProject->name;
    }

    public function digiTacheCdp(){
        $annonceurs = Annonceur::all();
        $usersDepartmts = User::where('departement_id',Auth::user()->departement_id)->get();
        $IdusersDepartmts = User::where('departement_id',Auth::user()->departement_id)->pluck('id')->toArray();
        $usersDepartmtsAssigne = Tach::whereIn('assigne_id',$IdusersDepartmts)->with('userTache')->with('userAssignant')->orderBy('id','desc')->get();
        return view('head_department.asset_digital_tache_cpd',compact('usersDepartmtsAssigne','usersDepartmts','annonceurs'));
    }

    public function storeTache(Request $request){
        //dd($request->all());
        $iDebrief='0';
        $pjt = Projet::where('annonceur',$request->annonceur_id)->first();
        if($pjt){
            $debrief_id = Debrief::where('project_id',$pjt->id)->first();
            if($debrief_id){
                $iDebrief=$debrief_id->id;
            }
        }

        if(empty($request->annonceur_id)){
            return redirect()->back()->with('error','Veuillez saisir l\'annonceur');
        }
        if(empty($request->libelle)){
            return redirect()->back()->with('error','Veuillez saisir le libelle');
        }
        if(empty($request->description)){
            return redirect()->back()->with('error','Veuillez saisir la description');
        }
        if(empty($request->assigne_id)){
            return redirect()->back()->with('error','Veuillez saisir l\'assigner');
        }

        $tachesDigital = new Tach();
        $tachesDigital->libelle = $request->libelle;
        $tachesDigital->description = $request->description;
        $tachesDigital->assigne_id = $request->assigne_id;
        $tachesDigital->assignant_id = $request->assignant_id;
        $tachesDigital->debrief_id = $iDebrief;
        $tachesDigital->save();

        $notif = new Notification();
        $notif->libelle = 'tache_assigne';
        $notif->send_id = $request['assignant_id'];
        $notif->recip_id = $request['assigne_id'];
        $notif->vue = '0';
        $notif->save();

        //NOTIFICATION PAR MAIL
        $data=array(
            "notifmessage"=>"Une tâche vous êtes assignée",
        );
        $findUserMail = User::where('id',$request['assigne_id'])->first();
        $emailMail = $findUserMail->email;
        Mail::send('mails/valid',$data,function ($message) use($emailMail){
            $message->to($emailMail);
            $message->subject('Tâche assigné');
        });

        return redirect()->back()->with('success','La tâche a été créée');

    }

}
