<?php

/* ================== Homepage ================== */
Route::auth();
Route::get('/login', function(){
    return view('auth.v1-1.login');
});
Route::post('/loginUser', 'Auth\AuthController@loginUser');

/* ================== Access Uploaded Files ================== */
Route::get('files/{hash}/{name}', 'LA\UploadsController@get_file');

/*
|--------------------------------------------------------------------------
| Admin Application Routes
|--------------------------------------------------------------------------
*/

$as = "";
if(\Dwij\Laraadmin\Helpers\LAHelper::laravel_ver() == 5.3) {
	$as = config('laraadmin.adminRoute').'.';
	
	// Routes for Laravel 5.3
	Route::get('/logout', 'Auth\LoginController@logout');
}

Route::group(['as' => $as, 'middleware' => ['auth', 'permission:ADMIN_PANEL']], function () {
	
	/* ================== Dashboard ================== */
	Route::get('/', 'LA\DashboardController@index');

	/* ================== Assets ================== */
	Route::get(config('laraadmin.adminRoute'). '/assets/{id}/{type}', 'LA\AssetController@index');


	Route::get(config('laraadmin.adminRoute'), 'LA\DashboardController@index');
	Route::get(config('laraadmin.adminRoute'). '/dashboard', 'LA\DashboardController@index');
	

    Route::get(config('laraadmin.adminRoute'). '/planning', 'LA\DashboardController@time');
    Route::get(config('laraadmin.adminRoute'). '/bibliotheques', 'LA\DashboardController@biblio');
    Route::get(config('laraadmin.adminRoute'). '/bibliotheques/template', 'LA\DashboardController@biblioTempl');
    Route::get(config('laraadmin.adminRoute'). '/bibliotheques/da', 'LA\DashboardController@biblioDa');
    Route::get(config('laraadmin.adminRoute'). '/bibliotheques/howto', 'LA\DashboardController@biblioHowto');
    Route::get(config('laraadmin.adminRoute'). '/bibliotheques/{type}/show', 'LA\DashboardController@showbiblio');
    Route::get(config('laraadmin.adminRoute'). '/asset/digital', 'LA\DashboardController@assetDigit');

	/* ================== Users ================== */
	Route::resource(config('laraadmin.adminRoute') . '/users', 'LA\UsersController');
	Route::get(config('laraadmin.adminRoute') . '/user_dt_ajax', 'LA\UsersController@dtajax');
	
	/* ================== Uploads ================== */
	Route::resource(config('laraadmin.adminRoute') . '/uploads', 'LA\UploadsController');
	Route::post(config('laraadmin.adminRoute') . '/upload_files', 'LA\UploadsController@upload_files');
	Route::get(config('laraadmin.adminRoute') . '/uploaded_files', 'LA\UploadsController@uploaded_files');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_caption', 'LA\UploadsController@update_caption');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_filename', 'LA\UploadsController@update_filename');
	Route::post(config('laraadmin.adminRoute') . '/uploads_update_public', 'LA\UploadsController@update_public');
	Route::post(config('laraadmin.adminRoute') . '/uploads_delete_file', 'LA\UploadsController@delete_file');
	
	/* ================== Roles ================== */
	Route::resource(config('laraadmin.adminRoute') . '/roles', 'LA\RolesController');
	Route::get(config('laraadmin.adminRoute') . '/role_dt_ajax', 'LA\RolesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_module_role_permissions/{id}', 'LA\RolesController@save_module_role_permissions');
	
	/* ================== Permissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/permissions', 'LA\PermissionsController');
	Route::get(config('laraadmin.adminRoute') . '/permission_dt_ajax', 'LA\PermissionsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/save_permissions/{id}', 'LA\PermissionsController@save_permissions');
	
	/* ================== Departments ================== */
	Route::resource(config('laraadmin.adminRoute') . '/departments', 'LA\DepartmentsController');
	Route::get(config('laraadmin.adminRoute') . '/department_dt_ajax', 'LA\DepartmentsController@dtajax');
	
	/* ================== Employees ================== */
	Route::resource(config('laraadmin.adminRoute') . '/employees', 'LA\EmployeesController');
	Route::get(config('laraadmin.adminRoute') . '/employee_dt_ajax', 'LA\EmployeesController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/change_password/{id}', 'LA\EmployeesController@change_password');
	
	/* ================== Organizations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/organizations', 'LA\OrganizationsController');
	Route::get(config('laraadmin.adminRoute') . '/organization_dt_ajax', 'LA\OrganizationsController@dtajax');

	/* ================== Backups ================== */
	Route::resource(config('laraadmin.adminRoute') . '/backups', 'LA\BackupsController');
	Route::get(config('laraadmin.adminRoute') . '/backup_dt_ajax', 'LA\BackupsController@dtajax');
	Route::post(config('laraadmin.adminRoute') . '/create_backup_ajax', 'LA\BackupsController@create_backup_ajax');
	Route::get(config('laraadmin.adminRoute') . '/downloadBackup/{id}', 'LA\BackupsController@downloadBackup');

	/* ================== Annonceurs ================== */
	Route::resource(config('laraadmin.adminRoute') . '/annonceurs', 'LA\AnnonceursController');
	Route::get(config('laraadmin.adminRoute') . '/annonceur_dt_ajax', 'LA\AnnonceursController@dtajax');

	/* ================== Projets ================== */
	Route::resource(config('laraadmin.adminRoute') . '/projets', 'LA\ProjetsController');
    Route::get(config('laraadmin.adminRoute') . '/projet_dt_ajax', 'LA\ProjetsController@dtajax');
    Route::get(config('laraadmin.adminRoute') . '/projets_second', 'LA\ProjetsController@second');
    Route::get(config('laraadmin.adminRoute') . '/projets_third', 'LA\ProjetsController@third');
    Route::get(config('laraadmin.adminRoute') . '/projets_list', 'LA\ProjetsController@lists');
    Route::get(config('laraadmin.adminRoute') . '/projets_add', 'LA\ProjetsController@pjtadd');
    Route::get(config('laraadmin.adminRoute') . '/projets/assets/{id}/{typeasset}', 'LA\ProjetsController@showasset');
    Route::post(config('laraadmin.adminRoute') . '/projets/addcheck', 'LA\ProjetsController@addcheck');
    Route::get(config('laraadmin.adminRoute') . '/projets/delcheck/{id}', 'LA\ProjetsController@delcheck');
    Route::get(config('laraadmin.adminRoute') . '/projets/assets/show', 'LA\ProjetsController@showassetrq')->name('searchassetspjt');

    Route::get(config('laraadmin.adminRoute') . '/projets/change/{id}/phase', 'LA\ProjetsController@changephasetwo');
    Route::get(config('laraadmin.adminRoute') . '/projets/changetwo/{id}/phase', 'LA\ProjetsController@changephasethree');
    //Route::post(config('laraadmin.adminRoute') . '/projets/nv3', 'LA\ProjetsController@sendnv3');



	/* ================== Briefs ================== */
	Route::resource(config('laraadmin.adminRoute') . '/briefs', 'LA\BriefsController');
	Route::get(config('laraadmin.adminRoute') . '/brief_dt_ajax', 'LA\BriefsController@dtajax');
    Route::post(config('laraadmin.adminRoute') . '/briefs/updatepj', 'LA\BriefsController@pj');



	/* ================== Assets ================== */
    Route::resource(config('laraadmin.adminRoute') . '/assets', 'LA\AssetsController');
    Route::get(config('laraadmin.adminRoute') . '/assets/{id}/{type}', 'LA\AssetsController@index');
	Route::get(config('laraadmin.adminRoute') . '/asset_dt_ajax', 'LA\AssetsController@dtajax');

	/* ================== Validations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/validations', 'LA\ValidationsController');
	Route::get(config('laraadmin.adminRoute') . '/validation_dt_ajax', 'LA\ValidationsController@dtajax');

	/* ================== Taches ================== */
	Route::resource(config('laraadmin.adminRoute') . '/taches', 'LA\TachesController');
	Route::get(config('laraadmin.adminRoute') . '/tach_dt_ajax', 'LA\TachesController@dtajax');

	/* ================== Notifications ================== */
	Route::resource(config('laraadmin.adminRoute') . '/notifications', 'LA\NotificationsController');
	Route::get(config('laraadmin.adminRoute') . '/notification_dt_ajax', 'LA\NotificationsController@dtajax');

	/* ================== Bcommandes ================== */
	Route::resource(config('laraadmin.adminRoute') . '/bcommandes', 'LA\BcommandesController');
	Route::get(config('laraadmin.adminRoute') . '/bcommande_dt_ajax', 'LA\BcommandesController@dtajax');

	/* ================== Soumissions ================== */
	Route::resource(config('laraadmin.adminRoute') . '/soumissions', 'LA\SoumissionsController');
	Route::get(config('laraadmin.adminRoute') . '/soumission_dt_ajax', 'LA\SoumissionsController@dtajax');

	/* ================== Devis ================== */
	Route::resource(config('laraadmin.adminRoute') . '/devis', 'LA\DevisController');
	Route::get(config('laraadmin.adminRoute') . '/devi_dt_ajax', 'LA\DevisController@dtajax');

	/* ================== Debriefs ================== */
	Route::resource(config('laraadmin.adminRoute') . '/debriefs', 'LA\DebriefsController');
	Route::get(config('laraadmin.adminRoute') . '/debrief_dt_ajax', 'LA\DebriefsController@dtajax');
    Route::post(config('laraadmin.adminRoute') . '/debriefs/updatepj', 'LA\DebriefsController@pj');

	/* ================== Mails ================== */
	Route::resource(config('laraadmin.adminRoute') . '/mails', 'LA\MailsController');
	Route::get(config('laraadmin.adminRoute') . '/mail_dt_ajax', 'LA\MailsController@dtajax');


	/* ================== Assetops ================== */
	Route::resource(config('laraadmin.adminRoute') . '/assetops', 'LA\AssetopsController');
	Route::get(config('laraadmin.adminRoute') . '/assetop_dt_ajax', 'LA\AssetopsController@dtajax');

	/* ================== Biblios ================== */
	Route::resource(config('laraadmin.adminRoute') . '/biblios', 'LA\BibliosController');
	Route::get(config('laraadmin.adminRoute') . '/biblio_dt_ajax', 'LA\BibliosController@dtajax');

	/* ================== Checklists ================== */
	Route::resource(config('laraadmin.adminRoute') . '/checklists', 'LA\ChecklistsController');
	Route::get(config('laraadmin.adminRoute') . '/checklist_dt_ajax', 'LA\ChecklistsController@dtajax');

	/* ================== Assetypes ================== */
	Route::resource(config('laraadmin.adminRoute') . '/assetypes', 'LA\AssetypesController');
	Route::get(config('laraadmin.adminRoute') . '/assetype_dt_ajax', 'LA\AssetypesController@dtajax');

	//Gestion des validations
    Route::get(config('laraadmin.adminRoute') . '/assign/validation', 'LA\DashboardController@validassign');
    Route::get(config('laraadmin.adminRoute') . '/assign/asset_waiting', 'AssetsvalidController@index');
    Route::get(config('laraadmin.adminRoute') . '/assign/asset_valider', 'AssetsvalidController@assetvalider');
    Route::get(config('laraadmin.adminRoute') . '/assign/asset_rejeter', 'AssetsvalidController@assetrejeter');
    Route::get(config('laraadmin.adminRoute'). '/assign/assign/waiting/{id}/{type}/valid','AssetsvalidController@validAsset');
    Route::post(config('laraadmin.adminRoute'). '/assign/assign/waiting/reject','AssetsvalidController@rejectAsset');
    //Route::get(config('laraadmin.adminRoute') . '/assign/{assetype}/asset_waiting', 'AssetsvalidController@checkAssetValidation');
    //Route::post(config('laraadmin.adminRoute') . '/assign/validation', 'LA\DashboardController@storeAssignValid');

	/* ================== Assetvalidations ================== */
	Route::resource(config('laraadmin.adminRoute') . '/assetvalidations', 'LA\AssetvalidationsController');
	Route::get(config('laraadmin.adminRoute') . '/assetvalidation_dt_ajax', 'LA\AssetvalidationsController@dtajax');

	/* ================== Assetvalidations ================== */
	/*Route::resource(config('laraadmin.adminRoute') . '/assetvalidations', 'LA\AssetvalidationsController');
	Route::get(config('laraadmin.adminRoute') . '/assetvalidation_dt_ajax', 'LA\AssetvalidationsController@dtajax');*/
});
