<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assetop extends Model
{
    use SoftDeletes;
	
	protected $table = 'assetops';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];


    public function userOP(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function project(){
        return $this->belongsTo('App\Models\Projet','project_id');
    }
}
