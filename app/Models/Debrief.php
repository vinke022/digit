<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Debrief extends Model
{
    use SoftDeletes;
	
	protected $table = 'debriefs';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public function userDebrief(){
        return $this->belongsTo('App\User','user_id');
    }

    public function brief(){
        return $this->belongsTo('App\Models\Brief','brief_id');
    }

    public function project(){
        return $this->belongsTo('App\Models\Projet');
    }

    public function validationReject(){
        return $this->hasMany('App\Models\Validation');
    }

    public function lastReject(){
        return $this->validationReject()->latest();
        // orderBy('id', 'desc')->take(1)
    }

    public function userBF(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
