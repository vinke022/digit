<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tach extends Model
{
    use SoftDeletes;
	
	protected $table = 'taches';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function userTache(){
	    return $this->belongsTo('App\User','assigne_id');
    }

    public function userAssignant(){
        return $this->belongsTo('App\User','assignant_id');
    }
}
