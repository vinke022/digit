<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Devi extends Model
{
    use SoftDeletes;
	
	protected $table = 'devis';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

    public function userDP(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function project(){
        return $this->belongsTo('App\Models\Projet','project_id');
    }

    public function validationReject(){
        return $this->hasMany('App\Models\Validation');
    }

    public function lastReject(){
        return $this->validationReject()->latest();
    }
}
