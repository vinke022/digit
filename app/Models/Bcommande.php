<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bcommande extends Model
{
    use SoftDeletes;
	
	protected $table = 'bcommandes';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function userBC(){
	    return $this->belongsTo('App\Models\User','user_id');
    }

    public function validationReject(){
        return $this->hasMany('App\Models\Validation','bc_id');
    }

    public function lastReject(){
        return $this->validationReject()->latest();
    }

    public function project(){
        return $this->belongsTo('App\Models\Projet','project_id');
    }
}
