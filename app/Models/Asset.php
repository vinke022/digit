<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
    use SoftDeletes;
	
	protected $table = 'assets';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function debrief(){
	    return $this->belongsTo('App\Models\Debrief');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function validationReject(){
        return $this->hasMany('App\Models\Validation');
    }

    public function lastReject(){
        return $this->validationReject()->latest();
    }
}
