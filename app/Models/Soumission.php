<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Soumission extends Model
{
    use SoftDeletes;
	
	protected $table = 'soumissions';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function userSomi(){
	    return $this->belongsTo('App\Models\User','user_id');
    }

    public function projectSomi(){
        return $this->belongsTo('App\Models\Projet','project_id');
    }

    public function validationReject(){
        return $this->hasMany('App\Models\Validation','soumi_id');
    }

    public function lastReject(){
        return $this->validationReject()->latest();
    }

}
