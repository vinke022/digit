<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brief extends Model
{
    use SoftDeletes;
	
	protected $table = 'briefs';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function project(){
	    return $this->belongsTo('App\Models\Projet');
    }

    public function validationReject(){
        return $this->hasMany('App\Models\Validation');
    }

    public function lastReject(){
        return $this->validationReject()->latest();
    }

    public function userBF(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
