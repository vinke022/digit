<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;
	
	protected $table = 'users';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function tacheUsers(){
	    return $this->hasMany('App\Models\Tach','assigne_id');
    }

    public function tacheUsersAssignant(){
        return $this->hasMany('App\Models\Tach','assignant_id');
    }

    public function tacheUsersEncoursAssignant(){
        return $this->tacheUsersAssignant()->where('status','1');
    }

    public function tacheUsersEncours(){
        return $this->tacheUsers()->where('status','1');
    }

    public function tacheUsersAssigne(){
        return $this->tacheUsers()->where('status','0');
    }

    public function departUsers(){
        return $this->belongsTo('App\Models\Department','departement_id');
    }
}
