<?php
/**
 * Model genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;
	
	protected $table = 'notifications';
	
	protected $hidden = [
        
    ];

	protected $guarded = [];

	protected $dates = ['deleted_at'];

	public function userAssigneNotif(){
	    return $this->belongsTo('App\Models\User','send_id');
    }

    public function devis(){
        return $this->belongsTo('App\Models\Devi','devi_id');
    }

    public function asset(){
        return $this->belongsTo('App\Models\Asset','asset_id');
    }

    /*public function devisuserDP(){
        return $this->devis()->belongsTo('App\Models\User','user_id');
    }

    public function devisproject(){
        return $this->devis()->belongsTo('App\Models\Projet','project_id');
    }*/

}
