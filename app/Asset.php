<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }
}
