<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class Notification{
    public static function CountNotifs(){
        $nbNotif = \App\Models\Notification::where('recip_id',Auth::user()->id)->where('vue','0')->count();
        return $nbNotif;
    }
}