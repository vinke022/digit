@extends('admin_lead.layouts.app')

@push('styles')
<style>
    .btn-success {
        background-color: #10cfbd;
        border-color: #0eb7a7;
    }
</style>
@endpush

@section('htmlheader_title') Tableau de bord  @endsection
@section('contentheader_title') Tableau de bord  @endsection

@section('main-content')
    <section class="content">

        <div class="row">
            <div class="col-md-12 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-tasks"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text" style="font-size: 23px;">Projets en cours</span>
                        <span class="info-box-number" style="font-size: 32px;">{{$projectEncours}}</span>
                    </div>
                </div>
            </div>
            {{--<div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-clock-o"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"> Temps moyens de validation </span>
                        <span class="info-box-number"> 10 min </span>
                    </div>
                </div>
            </div>--}}
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2 class="page-header"> </h2>
            </div>

            <div class="col-md-12">
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-warning" style="min-height: 300px">
                    <div class="box-header with-border">
                        <h3 class="box-title">Briefs en attente de validation </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    <th>Annonceur</th>
                                    <th>Project</th>
                                    <th>Libelle</th>
                                    <th>Date Entrée</th>
                                    <th>Date Fin</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allbriefsBriefsAttente as $attent)
                                    <tr>
                                        <td>{{ (new \App\Http\Controllers\AdminleadController)->showAnnonceurPjt($attent->project->annonceur)}}</td>
                                        <td>{{$attent->project->operation}}</td>
                                        <td>{{$attent->libelle}}</td>
                                        <td>{{date("d/m/Y",strtotime($attent->dateDentree))}}</td>
                                        <td>{{date("d/m/Y",strtotime($attent->dateDeSortie))}}</td>
                                        <td><span class="label label-info">En attente</span></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-body -->
                    @if(count($allbriefsBriefsAttente)!=0)
                        <div class="box-footer clearfix">
                            <a href="{{url(config('laraadmin.adminRoute'). '/lead/brief/waitingsbrief')}}" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-eye"></i> Consultés les projets en attente</a>
                        </div>
                @endif
                <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>

            <div class="col-md-12">
                <div class="col-md-12" style="padding: 0px;">
                    <!-- TABLE: LATEST ORDERS -->
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Debriefs en attente de validation </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Annonceur</th>
                                        <th>Project</th>
                                        <th>Libelle</th>
                                        <th>Date Entrée</th>
                                        <th>Date Fin</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($allbriefsAttente as $attent)
                                        <tr>
                                            <td>{{ (new \App\Http\Controllers\AdminleadController)->showAnnonceurPjt($attent->project->annonceur)}}</td>
                                            <td>{{$attent->project->operation}}</td>
                                            <td>{{$attent->libelle}}</td>
                                            <td>{{date("d/m/Y",strtotime($attent->dateDentree))}}</td>
                                            <td>{{date("d/m/Y",strtotime($attent->dateDeSortie))}}</td>
                                            <td><span class="label label-warning">En attente</span></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-body -->
                        @if(count($allbriefsAttente)!=0)
                            <div class="box-footer clearfix">
                                <a href="{{url(config('laraadmin.adminRoute'). '/lead/brief/waiting')}}" class="btn btn-sm btn-warning btn-flat pull-left"><i class="fa fa-eye"></i> Consultés les projets en attente</a>
                            </div>
                    @endif
                    <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-12" style="padding: 0px;">
                    <!-- TABLE: LATEST ORDERS -->
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Debriefs en cours </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <thead>
                                    <tr>
                                        <th>Annonceur</th>
                                        <th>Project</th>
                                        <th>Libelle</th>
                                        <th>Date Entrée</th>
                                        <th>Date Fin</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($allbriefsvalid as $attent)
                                        <tr>
                                            <td>{{ (new \App\Http\Controllers\AdminleadController)->showAnnonceurPjt($attent->project->annonceur)}}</td>
                                            <td>{{$attent->project->operation}}</td>
                                            <td>{{$attent->libelle}}</td>
                                            <td>{{date("d/m/Y",strtotime($attent->dateDentree))}}</td>
                                            <td>{{date("d/m/Y",strtotime($attent->dateDeSortie))}}</td>
                                            <td><span class="label label-success">En cours</span></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-body -->
                        @if(count($allbriefsvalid)!=0)
                            <div class="box-footer clearfix">
                                <a href="{{url(config('laraadmin.adminRoute'). '/lead/brief/inprogress')}}" class="btn btn-sm btn-info btn-success pull-left"><i class="fa fa-eye"></i> Consultés les projets en cours</a>
                            </div>
                    @endif
                    <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <h2 class="page-header"> </h2>
            </div>

            {{--<div class="col-md-12">--}}
                {{--<!-- TABLE: LATEST ORDERS -->--}}
                {{--<div class="box box-danger">--}}
                    {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">Assets en attente de validation </h3>--}}
                    {{--</div>--}}
                    {{--<!-- /.box-header -->--}}
                    {{--<div class="box-body">--}}
                        {{--<div class="table-responsive">--}}
                            {{--<table class="table no-margin">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>Id</th>--}}
                                    {{--<th>Libelle</th>--}}
                                    {{--<th>Debrief</th>--}}
                                    {{--<th>Type d'asset</th>--}}
                                    {{--<th>Fichier</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--@foreach($assetWaiting as $asset)--}}
                                    {{--<tr>--}}
                                        {{--<td><a href="javascript:void(0)">{{$asset->id}}</a></td>--}}
                                        {{--<td><a href="javascript:void(0)">{{$asset->libelle}}</a></td>--}}
                                        {{--<td>{{$asset->debrief->libelle}}</td>--}}
                                        {{--<td>{{ucfirst($asset->asset_type)}}</td>--}}
                                        {{--<td>--}}
                                            {{--<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>--}}
                                        {{--</td>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                                {{--@foreach($deviWaiting as $asset)--}}
                                    {{--<tr>--}}
                                        {{--<td><a href="javascript:void(0)">{{$asset->id}}</a></td>--}}
                                        {{--<td><a href="javascript:void(0)">{{$asset->libelle}}</a></td>--}}
                                        {{--<td></td>--}}
                                        {{--<td>DEVIS</td>--}}
                                        {{--<td>--}}
                                            {{--<a href="{{url('devisFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>--}}
                                        {{--</td>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}
                        {{--<!-- /.table-responsive -->--}}
                    {{--</div>--}}
                    {{--<!-- /.box-body -->--}}
                    {{--@if(count($assetWaiting)!=0 or count($deviWaiting)!=0)--}}
                        {{--<div class="box-footer clearfix">--}}
                            {{--<a href="{{url(config('laraadmin.adminRoute'). '/lead/asset/waiting')}}" class="btn btn-sm btn-info btn-success pull-left"><i class="fa fa-eye"></i> Consultés les assets en attente de validation</a>--}}
                        {{--</div>--}}
                {{--@endif--}}
                {{--<!-- /.box-footer -->--}}
                {{--</div>--}}
                {{--<!-- /.box -->--}}
            {{--</div>--}}

            <div class="col-md-12">
                <!-- TABLE: LATEST ORDERS -->
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Etat des projects </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <thead>
                                <tr>
                                    {{--<th>Id</th>--}}
                                    <th>Libelle</th>
                                    <th>Phase</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($projectEtat as $project)
                                    <tr>
                                        {{--<td><a href="javascript:void(0)">{{$project->id}}</a></td>--}}
                                        <td><a href="javascript:void(0)">{{$project->operation}}</a></td>
                                        <td>
                                            @if($project->niveau==1)
                                                <span class="label label-danger">Phase 1</span>
                                            @elseif($project->niveau== 2)
                                                <span class="label label-warning">Phase 2</span>
                                            @else
                                                <span class="label label-success">Phase 3</span>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- ChartJS -->
<script src="{{ asset('la-assets/plugins/chartjs/Chart.js') }}"></script>
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')

@endpush