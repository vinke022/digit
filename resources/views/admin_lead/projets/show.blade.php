@extends('admin_lead.layouts.app')

@section('htmlheader_title')
	Projet View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<div class="bg-primary clearfix">
		<div class="col-md-1">
			<div class="row">
				<div class="col-md-12">
					<!--<img class="profile-image" src="{{ asset('la-assets/img/avatar5.png') }}" alt="">-->
					<div class="profile-icon text-primary"><i class="fa {{ $module->fa_icon }}"></i></div>
				</div>
				<!-- <div class="col-md-9">
					
				</div> -->
			</div>
		</div>
		<!-- <div class="col-md-3">
			<div class="dats1"><div class="label2">Admin</div></div>
			<div class="dats1"><i class="fa fa-envelope-o"></i> superadmin@gmail.com</div>
			<div class="dats1"><i class="fa fa-map-marker"></i> Pune, India</div>
		</div> -->
		<div class="col-md-10">
			<h4 class="name">{{ $projet->$view_col }}</h4>
		</div>
		<div class="col-md-1 actions">
			@la_access("Projets", "edit")
				<a href="{{ url(config('laraadmin.adminRoute') . '/projets/'.$projet->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br>
			@endla_access
			
			@la_access("Projets", "delete")
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.projets.destroy', $projet->id], 'method' => 'delete', 'style'=>'display:inline']) }}
					<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
				{{ Form::close() }}
			@endla_access
		</div>
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/projets') }}" data-toggle="tooltip" data-placement="right" title="Back to Projets"><i class="fa fa-chevron-left"></i></a></li>
		<li class=""><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Informations générales</a></li>
		<li class="active"><a role="tab" data-toggle="tab" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-clock-o"></i> Briefs</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>General Info</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'operation')
						@la_display($module, 'annonceur')
						@la_display($module, 'dateDentree')
						@la_display($module, 'userId')
						@la_display($module, 'dateDeSortie')
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane active fade in p20 bg-white" id="tab-timeline">
			<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
				@la_access("Briefs", "create")
				@if(count($briefs)==0)
					<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">
						Ajouter un brief
					</button>
				@endif
				@endla_access
			</div>
			<div class="box box-success">
				<div class="box-body">
					<table id="example1" class="table table-bordered">
					<thead>
					<tr class="success">
						@foreach( $listing_cols_brief as $col )
						<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
						@endforeach
						@if($show_actions)
						<th>Actions</th>
						@endif
					</tr>
					</thead>
					<tbody>
						@foreach($briefs as $k => $brief)
						<tr role="row" class="odd">
							<td class="sorting_1">{{$k+1}}</td>
							<td>
								<a href="{{url(config('laraadmin.adminRoute') . '/briefs/'.$brief->id)}}">
									{{$brief->libelle}}
								</a>
							</td>
							<td>{{$brief->taf}}</td>
							<td>{{$brief->dateDentree}}</td>
							<td>{{$brief->dateDeSortie}}</td>
							<td>
								<a href="{{url(config('laraadmin.adminRoute') . '/briefs/'.$brief->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
								{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.briefs.destroy', $brief->id], 'method' => 'delete', 'style'=>'display:inline']) }}
								<button class="btn btn-danger btn-xs" type="submit">
									<i class="fa fa-times"></i>
								</button>
								{{Form::close()}}
							</td>
						</tr>
						@endforeach
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>

	@la_access("Briefs", "create")
	<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Ajouter un Brief</h4>
				</div>
				{!! Form::open(['action' => 'LA\BriefsController@store', 'id' => 'brief-add-form']) !!}
				<div class="modal-body">
					<div class="box-body">
	                   {{--@la_form($module)
						@la_input($moduleBrief, 'userId')
						@la_input($moduleBrief, 'project_id')
	                   	--}}
						
						
						@la_input($moduleBrief, 'libelle')
						@la_input($moduleBrief, 'taf')
						@la_input($moduleBrief, 'dateDentree')
						@la_input($moduleBrief, 'dateDeSortie')
						@la_input($moduleBrief, 'dep')
						@la_input($moduleBrief, 'pj')
						<input type="hidden" name="project_id" value="{{$projet->id}}">
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	@endla_access

</div>
@endsection

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Recherche",
			sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
		    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
		    sInfoPostFix:    "",
		    sLoadingRecords: "Chargement en cours...",
		    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
		    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
		    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
		    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
			oPaginate: {
	            "sFirst":    "Premier",
	            "sLast":    "Dernier",
	            "sNext":    "Suivant",
	            "sPrevious": "Pr&eacute;c&eacute;den"
	        },
		}
	});
	$("#brief-add-form").validate({
		
	});
});
</script>
@endpush
