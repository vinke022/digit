@extends("admin_lead.layouts.app")

@section("contentheader_title", "Projets")
@section("contentheader_description", "liste")
@section("section", "Projets")
@section("sub_section", "liste")
@section("htmlheader_title", "Projets Listing")

@section("headerElems")
@la_access("Projets", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">
		Ajouter projet
	</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Projets", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Ajouter projet</h4>
			</div>
			{!! Form::open(['action' => 'LA\ProjetsController@store', 'id' => 'projet-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
                    @la_form($module)
					
					{{--
					@la_input($module, 'operation')
					@la_input($module, 'annonceur')
					@la_input($module, 'dateDentree')
					@la_input($module, 'userId')
					@la_input($module, 'dateDeSortie')
					--}}
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
				{!! Form::submit( 'Enregistrer', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/projet_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Recherche",
			sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
		    sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
		    sInfoPostFix:    "",
		    sLoadingRecords: "Chargement en cours...",
		    sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
		    sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
		    sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
		    sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
			oPaginate: {
	            "sFirst":    "Premier",
	            "sLast":    "Dernier",
	            "sNext":    "Suivant",
	            "sPrevious": "Pr&eacute;c&eacute;den"
	        },
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#projet-add-form").validate({
		
	});
});
</script>
@endpush
