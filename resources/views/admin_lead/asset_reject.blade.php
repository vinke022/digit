@extends('admin_lead.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Asset rejeté @endsection
@section('contentheader_title') Asset rejeté @endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="exampledevis" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Project</th>
                                <th>Libelle</th>
                                <th>Fichier</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingDevis as $asset)
                                <tr>
                                    <td>{{$asset->project->operation}}</td>
                                    <td>{{$asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">RECO</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="examplereco" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Brief</th>
                                <th>Fichier</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingReco as $asset)
                                <tr>
                                    <td>{{$asset->debrief->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="box">--}}
                    {{--<div class="box-header">--}}
                        {{--<h4><span class="label label-danger">ASSET OP</span></h4>--}}
                    {{--</div>--}}
                    {{--<!-- /.box-header -->--}}
                    {{--<div class="box-body">--}}
                        {{--<table id="example7" class="table table-bordered">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th>Id</th>--}}
                                {{--<th>Libelle</th>--}}
                                {{--<th>Project</th>--}}
                                {{--<th>Type d'asset</th>--}}
                                {{--<th>Fichier</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}
                            {{--@foreach($assetWaitingOP as $asset)--}}
                                {{--<tr>--}}
                                    {{--<td><a href="javascript:void(0)">{{$asset->id}}</a></td>--}}
                                    {{--<td>{{$asset->libelle}}</td>--}}
                                    {{--<td>{{$asset->project->operation}}</td>--}}
                                    {{--<td>{{ucfirst($asset->type)}}</td>--}}
                                    {{--<td>--}}
                                        {{--<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>--}}
                                    {{--</td>--}}

                                {{--</tr>--}}
                            {{--@endforeach--}}
                            {{--</tbody>--}}

                        {{--</table>--}}
                    {{--</div>--}}
                    {{--<!-- /.box-body -->--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">CREA</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="examplecrea" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Brief</th>
                                <th>Fichier</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingCrea as $asset)
                                <tr>
                                    <td>{{$asset->debrief->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">DIGITAL</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="exampledigit" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Brief</th>
                                <th>Fichier</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingDigit as $asset)
                                <tr>
                                    <td>{{$asset->debrief->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">FLOW PLAN</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="exampleflow" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Brief</th>
                                <th>Fichier</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingFlow as $asset)
                                <tr>
                                    <td>{{$asset->debrief->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">PLAN MEDIA</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="exampleplan" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Brief</th>
                                <th>Fichier</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingPlan as $asset)
                                <tr>
                                    <td>{{$asset->debrief->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Libelle</th>
                                <th>Brief</th>
                                <th>Type d'asset</th>
                                <th>Fichier</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetReject as $asset)
                                <tr>
                                    <td><a href="javascript:void(0)">{{$asset->id}}</a></td>
                                    <td>{{$asset->libelle}}</td>
                                    <td>{{$asset->debrief->libelle}}</td>
                                    <td>{{ucfirst($asset->asset_type)}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@endsection

@push('scripts')

<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example7").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

    $("#examplereco").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#examplecrea").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#exampledigit").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#exampledevis").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#exampleplan").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#exampleflow").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

</script>
@endpush