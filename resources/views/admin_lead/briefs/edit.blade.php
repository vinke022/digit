@extends("admin_lead.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/briefs') }}">Brief</a> :
@endsection
@section("contentheader_description", $brief->$view_col)
@section("section", "Briefs")
@section("section_url", url(config('laraadmin.adminRoute') . '/briefs'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Briefs Edit : ".$brief->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($brief, ['route' => [config('laraadmin.adminRoute') . '.briefs.update', $brief->id ], 'method'=>'PUT', 'id' => 'brief-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'libelle')
					@la_input($module, 'dateDentree')
					@la_input($module, 'dateDeSortie')
					@la_input($module, 'taf')
					@la_input($module, 'pj')
					@la_input($module, 'dep')
					@la_input($module, 'project_id')
					@la_input($module, 'userId')
					--}}
					<input type="hidden" name="status" value="En attente">
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/briefs') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#brief-edit-form").validate({
		
	});
});
</script>
@endpush
