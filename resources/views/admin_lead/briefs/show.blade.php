@extends('admin_lead.layouts.app')

@push('styles')
	<link href="{{ asset('la-assets/css/dropzone.css') }}" type="text/css" rel="stylesheet" />

	<style type="text/css">
		.inner p {
		    font-size: 24px;
		    font-weight: bold;
		}
	</style>
@endpush

@section('htmlheader_title')
	Brief View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<div class="bg-primary clearfix">
		<div class="col-md-1">
			<div class="row">
				<div class="col-md-12">
					<div class="profile-icon text-primary"><i class="fa {{ $module->fa_icon }}"></i></div>
				</div>
			</div>
		</div>
		<div class="col-md-10">
			<h4 class="name">{{ $brief->$view_col }}</h4>
		</div>
		<div class="col-md-1 actions">
			@la_access("Briefs", "edit")
				<a href="{{ url(config('laraadmin.adminRoute') . '/projets/'.$brief->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br>
			@endla_access
			
			@la_access("Briefs", "delete")
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.briefs.destroy', $brief->id], 'method' => 'delete', 'style'=>'display:inline']) }}
					<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
				{{ Form::close() }}
			@endla_access
		</div>
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/projets/'.$project_id) }}" data-toggle="tooltip" data-placement="right" title="Back to Briefs"><i class="fa fa-chevron-left"></i></a></li>
		<li class=""><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Brief</a></li>
		<li class="active"><a role="tab" data-toggle="tab" href="#see-asset" data-target="#see-asset"><i class="fa fa-folder"></i> Assets</a></li> 
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Information Générale</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'libelle')
						<div class="form-group">
							<label for="libelle" class="col-md-2">Travail à faire :</label>
							<div class="col-md-10 fvalue">
								{{ $brief->taf }}
							</div>
						</div>
						@la_display($module, 'dateDentree')
						@la_display($module, 'dateDeSortie')
						@la_display($module, 'taf')
						@la_display($module, 'pj')
						@la_display($module, 'dep')
						@la_display($module, 'project_id')
						@la_display($module, 'userId')
						<div class="form-group">
							<label for="dateDeSortie" class="col-md-2">Statut :</label>
							<div class="col-md-10 fvalue">{{$brief->status}}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane active fade in p20 bg-white" id="see-asset">
			<div class="row">
	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h3>{{$assetDevis}}</h3>
	                  <p>Devis</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-file"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/devis')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->

	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-maroon">
	                <div class="inner">
	                  <h3>{{$assetReco}}</h3>
	                  <p>Reco</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-comment-o"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/reco')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->

				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-green">
						<div class="inner">
							<h3>{{$assetCrea}}</h3>
							<p>Créa</p>
						</div>
						<div class="icon">
							<i class="fa fa-lightbulb-o"></i>
						</div>
						<a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/crea')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->

	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-yellow">
	                <div class="inner">
	                  <h3>{{$assetFPlane}}</h3>
	                  <p>Flow plan</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-file-excel-o"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/flowplane')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->
	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-red">
	                <div class="inner">
	                  <h3>{{$assetMedia}}</h3>
	                  <p>Plan Media</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-television"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/planmedia')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->

	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-navy">
	                <div class="inner">
	                  <h3>{{$assetDigit}}</h3>
	                  <p>Digital</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-star"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/digital')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->
          	</div><!-- /.row -->

		</div>

		
		
	</div>
	</div>
	</div>
</div>
@endsection
