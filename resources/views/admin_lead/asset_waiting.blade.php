@extends('admin_lead.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Asset en attente de validation @endsection
@section('contentheader_title') Asset en attente de validation @endsection

@section('main-content')
    @if(Auth::user()->post=='DGA')
        <section class="content">

            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                            <div class="col-md-12">
                                <div id="loaderdevisDGA" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampledevis" class="table table-bordered">
                                <thead>
                                <tr>
                                    {{--<th>Project</th>--}}
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingDevis as $asset)
                                    <tr>
                                        {{--<td>{{$asset->project->operation}}</td>--}}
                                        <td>{{$asset->devis->libelle}}</td>
                                        <td>
                                            <a href="{{url('devisFiles/'.$asset->devis->path)}}" download="{{$asset->devis->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                        <td>
                                            <div id="sectbtndevisDGA{{$asset->id}}">
                                            <a onclick="devisDGAValid('{{url(config('laraadmin.adminRoute') . '/lead/assetDevis/waiting/'.$asset->id.'/valid')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalDevis" href="#" onclick="displayModalDevis('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">BON COMMANDE PRESTA</span></h4>
                            <div class="col-md-12">
                                <div id="loaderbcpDGA" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampledigit" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                    <th>Actions</th>
                                    {{--<th>Action</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingPresta as $asset)
                                    <tr>
                                        <td>{{$asset->asset->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                        <td>
                                            <div id="sectbtnbcpDGA{{$asset->id}}">
                                            <a onclick="bcpDGAValid('{{url(config('laraadmin.adminRoute') . '/lead/assetBcp/waiting/'.$asset->id.'/valid')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalBcp" href="#" onclick="displayModalBcp('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">RETRO PLANNING</span></h4>
                            <div class="col-md-12">
                                <div id="loaderretroDGA" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampleretro" class="table table-bordered">
                                <thead>
                                <tr>
                                    {{--<th>Project</th>--}}
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingRetro as $asset)
                                    <tr>
                                        {{--<td>{{$asset->project->operation}}</td>--}}
                                        <td>{{$asset->asset->libelle}}</td>
                                        <td>
                                            <a href="{{url('devisFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                        <td>
                                            <div id="sectbtnretroDGA{{$asset->id}}">
                                            <a onclick="retroDGAValid('{{url(config('laraadmin.adminRoute') . '/lead/assetDevis/waiting/'.$asset->id.'/valid/retro')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalRetro" href="#" onclick="displayModalRetro('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                {{--<div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">RECO</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="examplereco" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Brief</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingReco as $asset)
                                    <tr>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>--}}
            </div>

            {{--<div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">CREA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="examplecrea" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Brief</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingCrea as $asset)
                                    <tr>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">FLOW PLAN</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampleflow" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Brief</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingFlow as $asset)
                                    <tr>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">PLAN MEDIA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampleplan" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Brief</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingPlan as $asset)
                                    <tr>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Libelle</th>
                                    <th>Brief</th>
                                    <th>Type d'asset</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaiting as $asset)
                                    <tr>
                                        <td><a href="javascript:void(0)">{{$asset->id}}</a></td>
                                        <td>{{$asset->libelle}}</td>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>{{ucfirst($asset->asset_type)}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>--}}
        </section>
    @endif

    @if(Auth::user()->post=='CONTROLE')
        <section class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                            <div class="col-md-12">
                                <div id="loaderdevisCTR" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampledevis" class="table table-bordered">
                                <thead>
                                <tr>
                                    {{--<th>Project</th>--}}
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingDevis as $asset)
                                    <tr>
                                        {{--<td>{{$asset->project->operation}}</td>--}}
                                        <td>{{$asset->devis->libelle}}</td>
                                        <td>
                                            <a href="{{url('devisFiles/'.$asset->devis->path)}}" download="{{$asset->devis->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                        <td>
                                            <div id="sectbtndevisCTR{{$asset->id}}">
                                            <a onclick="devisCTRValid('{{url(config('laraadmin.adminRoute') . '/lead/assetDevis_controle/waiting/'.$asset->id.'/valid')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalDevisControle" href="#" onclick="displayModalDevisControle('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">BON COMMANDE PRESTA</span></h4>
                            <div class="col-md-12">
                                <div id="loaderbcpCTR" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampledigit" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                    <th>Actions</th>
                                    {{--<th>Action</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingPresta as $asset)
                                    <tr>
                                        <td>{{$asset->asset->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                        <td>
                                            <div id="sectbtnbcpCTR{{$asset->id}}">
                                            <a onclick="bcpCTRValid('{{url(config('laraadmin.adminRoute') . '/lead/assetBcp_controle/waiting/'.$asset->id.'/valid')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalBcpControle" href="#" onclick="displayModalBcpControle('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">RETRO PLANNING</span></h4>
                            <div class="col-md-12">
                                <div id="loaderretroCTR" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampleretro" class="table table-bordered">
                                <thead>
                                <tr>
                                    {{--<th>Project</th>--}}
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingRetro as $asset)
                                    <tr>
                                        {{--<td>{{$asset->project->operation}}</td>--}}
                                        <td>{{$asset->asset->libelle}}</td>
                                        <td>
                                            <a href="{{url('devisFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                        <td>
                                            <div id="sectbtnretroCTR{{$asset->id}}">
                                            <a onclick="retroCTRValid('{{url(config('laraadmin.adminRoute') . '/lead/assetDevis_controle/waiting/'.$asset->id.'/valid/retro')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalRetroControle" href="#" onclick="displayModalRetroControle('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                {{--<div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">RECO</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="examplereco" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Brief</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingReco as $asset)
                                    <tr>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>--}}
            </div>
        </section>
    @endif

    @if(Auth::user()->post!='DGA' AND Auth::user()->post!='CONTROLE')
        <section class="content">

            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampledevis" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Project</th>
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingDevis as $asset)
                                    <tr>
                                        <td>{{$asset->project->operation}}</td>
                                        <td>{{$asset->libelle}}</td>
                                        <td>
                                            <a href="{{url('devisFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">RECO</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="examplereco" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Brief</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingReco as $asset)
                                    <tr>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">CREA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="examplecrea" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Brief</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingCrea as $asset)
                                    <tr>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">FLOW PLAN</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampleflow" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Brief</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingFlow as $asset)
                                    <tr>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">PLAN MEDIA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="exampleplan" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Brief</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingPlan as $asset)
                                    <tr>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Libelle</th>
                                    <th>Brief</th>
                                    <th>Type d'asset</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaiting as $asset)
                                    <tr>
                                        <td><a href="javascript:void(0)">{{$asset->id}}</a></td>
                                        <td>{{$asset->libelle}}</td>
                                        <td>{{$asset->debrief->libelle}}</td>
                                        <td>{{ucfirst($asset->asset_type)}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </section>
    @endif

    <div class="modal fade" id="versionModalDevis" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeDevis"  method="post" action="{{url(config('laraadmin.adminRoute'). '/lead/assetDevis/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderdevisDGA2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="idDevis" name="idDevis"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetDevisDGA" onclick="submitDevisDGA()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalRetro" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeRetro"  method="post" action="{{url(config('laraadmin.adminRoute'). '/lead/assetDevis/waiting/rejet/retro')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderretroDGA2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="fileid" name="fileid"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetRetroDGA" onclick="submitRetroDGA()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalBcp" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeBcp"  method="post" action="{{url(config('laraadmin.adminRoute'). '/lead/assetBcp/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderbcpDGA2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="idbcp" name="idbcp"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetBcpDGA" onclick="submitBcpDGA()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalDevisControle" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeDevisControle"  method="post" action="{{url(config('laraadmin.adminRoute'). '/lead/assetDevis_controle/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderdevisCTR2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="idDevisControle" name="idDevisControle"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetDevisCTR" onclick="submitDevisCTR()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="versionModalRetroControle" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeRetroControle"  method="post" action="{{url(config('laraadmin.adminRoute'). '/lead/assetDevis_controle/waiting/rejet/retro')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderretroCTR2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="fileidControle" name="fileidControle"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetRetroCTR" onclick="submitRetroCTR()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="versionModalBcpControle" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeBcpControle"  method="post" action="{{url(config('laraadmin.adminRoute'). '/lead/assetBcp_controle/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderbcpCTR2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="idbcpControle" name="idbcpControle"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetBcpCTR" onclick="submitBcpCTR()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection



@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

    $("#example7").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

    $("#exampleretro").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

    $("#examplereco").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#examplecrea").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#exampledigit").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#exampledevis").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#exampleplan").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#exampleflow").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

    function displayModalDevis(id){
        $("#formTypeDevis")[0].reset();
        $("#idDevis").val(id);
    }

    function displayModalRetro(id){
        $("#formTypeRetro")[0].reset();
        $("#fileid").val(id);
    }

    function displayModalBcp(id){
        $("#formTypeBcp")[0].reset();
        $("#idbcp").val(id);
    }

    function displayModalDevisControle(id){
        $("#formTypeDevisControle")[0].reset();
        $("#idDevisControle").val(id);
    }

    function displayModalRetroControle(id){
        $("#formTypeRetroControle")[0].reset();
        $("#fileidControle").val(id);
    }

    function displayModalBcpControle(id){
        $("#formTypeBcpControle")[0].reset();
        $("#idbcpControle").val(id);
    }

    //LOADER DGA
        //DEVIS
        function devisDGAValid(lien,id) {
            $('#sectbtndevisDGA'+id).hide();
            $('#loaderdevisDGA').show();
            //alert("ici");
            window.location=lien;
        }
        function submitDevisDGA() {
            $("#commitRejetDevisDGA").prop('disabled', true);
            $('#loaderdevisDGA2').show();
            //alert("ici");
            $("#formTypeDevis").submit();
            //console.log($("#idbrief").val());
        }
        //BCP
        function bcpDGAValid(lien,id) {
            $('#sectbtnbcpDGA'+id).hide();
            $('#loaderbcpDGA').show();
            //alert("ici");
            window.location=lien;
        }
        function submitBcpDGA() {
            $("#commitRejetBcpDGA").prop('disabled', true);
            $('#loaderbcpDGA2').show();
            //alert("ici");
            $("#formTypeBcp").submit();
            //console.log($("#idbrief").val());
        }
        //RETRO PLAN
        function retroDGAValid(lien,id) {
            $('#sectbtnretroDGA'+id).hide();
            $('#loaderretroDGA').show();
            //alert("ici");
            window.location=lien;
        }
        function submitRetroDGA() {
            $("#commitRejetRetroDGA").prop('disabled', true);
            $('#loaderretroDGA2').show();
            //alert("ici");
            $("#formTypeRetro").submit();
            //console.log($("#idbrief").val());
        }

    //LOADER CONTROL
        //DEVIS
        function devisCTRValid(lien,id) {
            $('#sectbtndevisCTR'+id).hide();
            $('#loaderdevisCTR').show();
            //alert("ici");
            window.location=lien;
        }
        function submitDevisCTR() {
            $("#commitRejetDevisCTR").prop('disabled', true);
            $('#loaderdevisCTR2').show();
            //alert("ici");
            $("#formTypeDevisControle").submit();
            //console.log($("#idbrief").val());
        }
        //BCP
        function bcpCTRValid(lien,id) {
            $('#sectbtnbcpCTR'+id).hide();
            $('#loaderbcpCTR').show();
            //alert("ici");
            window.location=lien;
        }
        function submitBcpCTR() {
            $("#commitRejetBcpCTR").prop('disabled', true);
            $('#loaderbcpCTR2').show();
            //alert("ici");
            $("#formTypeBcpControle").submit();
            //console.log($("#idbrief").val());
        }
        //RETRO PLAN
        function retroCTRValid(lien,id) {
            $('#sectbtnretroCTR'+id).hide();
            $('#loaderretroCTR').show();
            //alert("ici");
            window.location=lien;
        }
        function submitRetroCTR() {
            $("#commitRejetRetroCTR").prop('disabled', true);
            $('#loaderretroCTR2').show();
            //alert("ici");
            $("#formTypeRetroControle").submit();
            //console.log($("#idbrief").val());
        }

</script>
@endpush