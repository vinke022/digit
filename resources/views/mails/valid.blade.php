<html>
<head>
    <style>
        @media  only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media  only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>
    <body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #ffffff; color: #000; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">

        <table cellpadding="0" width="100%" cellspacing="0" border="0" id="backgroundTable" class="bgBody">
        <tbody>
            <tr>
                <td>
                    <table cellpadding="0" width="620" class="container" align="center" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td>
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                        <tbody>
                                            <tr>
                                                <td class="movableContentContainer bgItem">

                                                    <div class="movableContent">
                                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                            <tbody><tr height="40">
                                                                <td width="200">&nbsp;</td>
                                                                <td width="200">&nbsp;</td>
                                                                <td width="200">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td width="200" valign="top">&nbsp;</td>
                                                                <td width="200" valign="top" align="center">
                                                                    <div class="contentEditableContainer contentImageEditable">
                                                                        <div class="contentEditable" align="center">
                                                                            <img src="{{url('la-assets/img/logo2.jpg')}}" height="155" alt="Logo" data-default="placeholder">
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="200" valign="top">&nbsp;</td>
                                                            </tr>
                                                            <tr height="25">
                                                                <td width="200">&nbsp;</td>
                                                                <td width="200">&nbsp;</td>
                                                                <td width="200">&nbsp;</td>
                                                            </tr>
                                                            </tbody></table>
                                                    </div>

                                                    <div class="movableContent">
                                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container" style="font-size: 20px">
                                                            <tbody>
                                                            <tr>
                                                                <td width="10">&nbsp;</td>
                                                                <td width="400" align="center">
                                                                    <div class="contentEditableContainer contentTextEditable">
                                                                        <div class="contentEditable" align="center">
                                                                            <br>
                                                                            {{--<p><strong>Bonjour Nom$Prenom, </strong>--}}
                                                                                {{--<br>--}}
                                                                                {{--<br>--}}
                                                                            <p style="line-height: 1.5">
                                                                                {{$notifmessage}}
                                                                            </p>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td width="10">&nbsp;</td>
                                                            </tr>
                                                            </tbody></table>
                                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                            <tbody><tr>
                                                                <td width="200">&nbsp;</td>
                                                                <td width="200" align="center" style="padding-top:25px;">
                                                                    <table cellpadding="0" cellspacing="0" border="0" align="center" width="200" height="50">
                                                                        <tbody><tr>
                                                                            <td bgcolor="#3097D1" align="center" style="border-radius:4px;" width="200" height="50">
                                                                                <div class="contentEditableContainer contentTextEditable">
                                                                                    <div class="contentEditable" align="center">
                                                                                        <a target="_blank" href="{{url('/')}}" class="link2" style="color: #FFF;text-decoration: none;">VOIR PLUS</a>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tbody></table>
                                                                </td>
                                                                <td width="200">&nbsp;</td>
                                                            </tr>
                                                            </tbody></table>
                                                    </div>

                                                    <div class="movableContent">
                                                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" class="container">
                                                            <tbody><tr>
                                                                <td width="100%" colspan="2" style="padding-top:65px;">
                                                                    <hr style="height:1px;border:none;color:#333;background-color:#ddd;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="60%" height="70" valign="middle" style="padding-bottom:20px;">
                                                                    <div class="contentEditableContainer contentTextEditable">
                                                                        <div class="contentEditable" align="left">
                                                                            <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;">Envoyé par support@agence6sens.fr <br> DIGITALISATION</span>
                                                                            <br>
                                                                            <span style="font-size:11px;color:#555;font-family:Helvetica, Arial, sans-serif;line-height:200%;">© 2019 Digitalisation. All rights reserved.</span>
                                                                            <br>
                                                                            <span style="font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;"></span></div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    </body>
</html>
