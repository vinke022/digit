@extends('la.layouts.auth')

@section('htmlheader_title')
    Login
@endsection

@push('styles')
<style>

    .banner-section {
        padding-right: 0;
    }
    .d-md-flex {
        display: flex !important;
    }
    .banner-section .slide-content {
        width: 100%;
    }

    .bg-1{
        background: url({{url('la-assets/img/loginbackground.png')}}) no-repeat center center;
        background-size: cover;
        width: 100%;
    }
    .h-100 {
        height: 100% !important;
    }
    .auto-form-wrapper {
        position: relative;
        height: 100vh;
        min-height: 100vh;
        max-height: 100vh;
        padding: 110px 22% 5%;
        border-radius: 4px;
    }
    .align-items-center{
        align-items: center !important;
    }
    .auto-form-wrapper form {
        width: 50%;
        min-width: 300px;
        max-width: 480px;
    }
    .auto-form-wrapper form .form-group .input-group{
        height: 44px;
    }

    .input-group {
        position: relative;
        display: flex;
        flex-wrap: wrap;
        align-items: stretch;
        width: 100%;
    }
    .auto-form-wrapper form .form-group {
        width: 100%;
        margin-bottom: 25px;
    }

    .auto-form-wrapper form .form-group .input-group .form-control{
        border: 1px solid #e5e5e5;
        border-radius: 6px 6px 6px 6px;
    }
    .mb-5 {
        margin-bottom: 3rem !important;
    }
    .mh10{
        min-height: 100vh;
    }


</style>
@endpush

@section('content')
    <body class="hold-transition login-page">
    <div class="row d-flex align-items-stretch">
        <div class="col-md-4 mh10 banner-section d-none d-md-flex align-items-stretch justify-content-center">
            <div class="slide-content bg-1">

            </div>
        </div>
        <div class="col-md-8 h-100 bg-white">
            <div class="auto-form-wrapper d-flex align-items-center justify-content-center flex-column">

                <form action="{{ url('/login') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <h3 class="mr-auto">Bienvenue !</h3>
                    <p class="mb-5 mr-auto">Connectez-vous pour démarrer votre session</p>

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Il y a eu quelques problèmes.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group has-feedback">
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Email" name="email"/>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group has-feedback">
                            <input type="password" class="form-control" placeholder="Password" name="password"/>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember"> Se souvenir de moi.
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary submit-btn"><i class="fa fa-user"></i> Connexion</button>
                    </div>
                    <div class="wrapper mt-5 text-gray">
                        <p class="footer-text">Copyright © 2019</p>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('la.layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    </body>

@endsection
