@extends('la.layouts.v1-1.auth')

@section('htmlheader_title')
    Connexion
@endsection

@push('styles')
    <style>
        .btn-brand-02 {
            background-color: #bc0060;
            border-color: #bc0060;
            color: #fff;
            font-size: 16px;
        }

        .btn-brand-02:hover, .btn-brand-02:focus {
            background-color: #000000;
            border-color: #000000;
            color: #fff;
        }
    </style>
@endpush

@section('content')
<div class="content content-fixed content-auth">
    <div class="container">
        <div class="media align-items-stretch justify-content-center ht-100p pos-relative">
        <div class="media-body align-items-center d-none d-lg-flex">
            <img src="{{asset('version-1.1-libs/images/login.jpg')}}" class="img-fluid" alt="">
        </div><!-- media-body -->
            <div class="sign-wrapper mg-lg-l-50 mg-xl-l-60">
                <div class="wd-100p">
                <h3 class="tx-color-01 mg-b-5">Connexion</h3>
                <p class="tx-color-03 tx-16 mg-b-40">Bienvenue! Connectez-vous s'il vous plaît pour continuer.</p>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Il y a eu quelques problèmes.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ url('/login') }}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label>Adresse email </label>
                            <input type="email"  name="email" class="form-control" placeholder="yourname@yourmail.com">
                        </div>
                        <div class="form-group">
                            <div class="d-flex justify-content-between mg-b-5">
                            <label class="mg-b-0-f">Mot de passe</label>
                            <a href="#" class="tx-13">Mot de passe oublié?</a>
                            </div>
                            <input type="password" name="password" class="form-control" placeholder="Entrer votre mot de passe">
                        </div>
                        <button type="submit" class="btn btn-brand-02 btn-block">Demarrez</button>
                    </form>
                </div>
            </div><!-- sign-wrapper -->
        </div><!-- media -->
    </div><!-- container -->
</div><!-- content -->
@endsection
