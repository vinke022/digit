@extends('user_trafic.layouts.app')

@push('styles')
<style>
  .small-box p {
    font-size: 25px;
  }
</style>
@endpush

@section('htmlheader_title') Tableau de bord  @endsection
@section('contentheader_title') Tableau de bord @endsection

@section('main-content')
  <hr>
  <section class="content">
    <div class="row">
      {{--<h2 class="page-header">Teams</h2>--}}
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3>Asset validé</h3>
            <p>{{$assetValid}}</p>
          </div>
          <div class="icon">
            <i class="fa fa-check"></i>
          </div>
          <a href="#" class="small-box-footer">{{-- more info <i class="fa fa-arrow-circle-right"></i>--}}</a>
        </div>
      </div>
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>Asset rejeté</h3>
            <p>{{$assetReject}}</p>
          </div>
          <div class="icon">
            <i class="fa fa-times"></i>
          </div>
          <a href="#" class="small-box-footer">{{-- more info <i class="fa fa-arrow-circle-right"></i>--}}</a>
        </div>
      </div>

    </div>
    <div class="row">
      <div class="col-md-6">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Briefs et Debriefs assignés </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                <tr>
                  <th>Libelle</th>
                  <th>Annonceur</th>
                  <th>Project</th>
                  <th>Date Entrée</th>
                  <th>Date Fin</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @if(count($briefAssigne)>0)
                  @foreach($briefAssigne as $brief)
                    <tr>
                      <td>
                        <a href="{{url(config('laraadmin.adminRoute') . '/debriefs/'.$brief->id.'#tab-info')}}">
                          {{$brief->libelle}}
                        </a>
                      </td>
                      <td>{{ (new \App\Http\Controllers\HeadDepartmentController)->showAnnonceurPjt($brief->project->annonceur)}}</td>
                      <td>{{$brief->project->operation}}</td>

                      <td>{{date("d/m/Y",strtotime($brief->dateDentree))}}</td>
                      <td>{{date("d/m/Y",strtotime($brief->dateDeSortie))}}</td>
                      <td>
                        @if($brief->status =='2')
                          <span class="label label-success">accepté</span>
                        @else
                          <span class="label label-danger">En attente</span>
                        @endif
                      </td>
                    </tr>
                  @endforeach
                @else
                  <tr><td>Aucun briefs ou debriefs assignés</td></tr>
                @endif
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            <a href="{{url('admin/dashboard/brief/assign')}}" class="btn btn-sm btn-info pull-left"><i class="fa fa-eye"></i> Consultés les briefs ou debriefs assignés</a>
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-6">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Assets en cours de validation </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                <tr>
                  {{--<th>Id</th>--}}
                  <th>Libelle</th>
                  <th>Debrief</th>
                  <th>Asset</th>
                  <th>Fichier</th>
                </tr>
                </thead>
                <tbody>
                @if(count($assetEncours)>0)
                  @foreach($assetEncours as $asset)
                    <tr>
                      {{--<td><a href="javascript:void(0)">{{$asset->id}}</a></td>--}}
                      <td><a href="javascript:void(0)">{{$asset->libelle}}</a></td>
                      <td>{{$asset->debrief->libelle}}</td>
                      <td>{{(new \App\Http\Controllers\LA\AssetypesController())->getmylibelle($asset->asset_type)}}</td>
                      <td>
                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                      </td>
                    </tr>
                  @endforeach
                @else
                  <tr><td>Aucun asset en cours de validation</td></tr>
                @endif
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            {{--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>--}}
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>

      <div class="col-md-6">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Tâches en cours</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Libelle</th>
                  <th>Assigné</th>
                  <th>Status</th>
                  <th>Assignant</th>
                </tr>
                </thead>
                <tbody>
                @if(count($taches)>0)
                  @foreach($taches as $tache)
                    @php $ast = (new \App\Http\Controllers\TraficUserController())->checkTachAssetTraf($tache->id) @endphp
                    @if($ast=='0')
                      <tr>
                        <td>{{$tache->id}}</td>
                        <td><a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id)}}">{{$tache->libelle}}</a></td>
                        <td>{{$tache->userTache->name}}</td>
                        <td><span class="text-success">En cours</span></td>
                        <td>{{$tache->userAssignant->name}}</td>
                      </tr>
                    @endif
                  @endforeach
                @else
                  <tr><td>Aucune tâche en cours</td></tr>
                @endif
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            {{--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>--}}
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-6">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Tâches assignées </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Libelle</th>
                  <th>Assigné</th>
                  <th>Status</th>
                  <th>Assignant</th>
                </tr>
                </thead>
                <tbody>
                @if(count($tachesAssign)>0)
                  @foreach($tachesAssign as $tache)
                    <tr>
                      <td>{{$tache->id}}</td>
                      <td><a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id)}}">{{$tache->libelle}}</a></td>
                      <td>{{$tache->userTache->name}}</td>
                      <td><span class="text-success">En attente</span></td>
                      <td>{{$tache->userAssignant->name}}</td>
                    </tr>
                  @endforeach
                @else
                  <tr><td>Aucune tâche en cours</td></tr>
                @endif
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            {{--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>--}}
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
    </div>

  </section>
@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')

@endpush