@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url('/') }}">Tâche</a> :
@endsection
@section("contentheader_description", $tach->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($tach, ['route' => [config('laraadmin.adminRoute') . '.taches.update', $tach->id ], 'method'=>'PUT', 'id' => 'tach-edit-form']) !!}
				{{--@la_form($module)@la_input($module, 'assigne_id')
					@la_input($module, 'assignant_id') @la_input($module, 'brief_id') @la_input($module, 'status')--}}

					@la_input($module, 'libelle')
					@la_input($module, 'description')

					<div class="form-group">
						<label for="assigne_id">Assigner</label>
						<select name="assigne_id" id="assigne_id" class="form-control">
							@foreach($usersDepartmts as $user)
								<option value="{{$user->id}}" <?= $tach->assigne_id==$user->id ? "selected" : "" ?>>{{$user->name}}</option>
							@endforeach
						</select>
					</div>
					<input type="hidden" name="debrief_id" value="{{$tach->debrief_id}}">
					<input type="hidden" name="assignant_id" value="{{$tach->assignant_id}}">

                    <br>
					<div class="form-group">
						{!! Form::submit( 'MISE A JOUR', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/debriefs/'.$tach->debrief_id.'#tache') }}">ANNULER</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#tach-edit-form").validate({
		
	});
});
</script>
@endpush
