@extends('la.layouts.app')

@section('htmlheader_title')
	Tache
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('la-assets/plugins/dropify/dropify.css') }}">
<link rel="stylesheet" href="{{ asset('la-assets/plugins/lightbox/lightbox.css') }}">
<style>
	.imgasset {
		height: 101px!important;
		width: 100%;
	}
	.mailbox-attachment-info{
		height: 90px;
	}
</style>
@endpush

@section('main-content')

@if (count($errors) > 0)
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<div id="page-content" class="profile2">
	<div class="bg-primary clearfix nav nav-tabs profile">
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-3">
					<!--<img class="profile-image" src="{{ asset('la-assets/img/avatar5.png') }}" alt="">-->
					<div class="profile-icon text-primary"><i class="fa fa-tasks"></i></div>
				</div>
				<div class="col-md-9">
					<h4 class="name">{{ $tach->$view_col }}</h4>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			{{--<div class="dats1"><div class="label2">Admin</div></div>
			<div class="dats1"><i class="fa fa-envelope-o"></i> superadmin@gmail.com</div>
			<div class="dats1"><i class="fa fa-map-marker"></i> Pune, India</div>--}}
		</div>
		<div class="col-md-4">

		</div>
		{{--<div class="col-md-1 actions">--}}
			{{--@la_access("Taches", "edit")--}}
				{{--<a href="{{ url(config('laraadmin.adminRoute') . '/taches/'.$tach->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br>--}}
			{{--@endla_access--}}
			{{----}}
			{{--@la_access("Taches", "delete")--}}
				{{--{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.taches.destroy', $tach->id], 'method' => 'delete', 'style'=>'display:inline']) }}--}}
					{{--<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>--}}
				{{--{{ Form::close() }}--}}
			{{--@endla_access--}}
		{{--</div>--}}
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url('/') }}" data-toggle="tooltip" data-placement="right" title="Cliquer pour revenir en arrière"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i> Information tâche</a></li>
		@if($module->row['assigne_id']==Auth::user()->id and $module->row['status']!='0')
		<li class=""><a role="tab" data-toggle="tab" href="#terminer-tache" data-target="#terminer-tache"><i class="fa fa-download"></i> Télécharger les assets</a></li>
		@endif
		<li class=""><a role="tab" data-toggle="tab" class="active" href="#tab-info-brief" data-target="#tab-info-brief"><i class="fa fa-bars"></i> Information Debrief</a></li>
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="row panel-default panel-heading">
						<div class="col-md-6"><h4>Information tâche</h4></div>
						@if($module->row['assigne_id']==Auth::user()->id and $module->row['status']=='0')
						<div class="col-md-6">
							<a href="{{url(config('laraadmin.adminRoute'). '/acceptache/'.$module->row['id'])}}" class="btn btn-success btn-sm pull-right"><i class="fa fa-pencil-square-o"></i> Débuter la Tache</a>
						</div>
						@endif

						@if($module->row['assigne_id']==Auth::user()->id and $module->row['status']=='1')
							<div class="col-md-6">
								<a href="{{url(config('laraadmin.adminRoute'). '/user/'.$module->row['id'].'/termine')}}" class="btn btn-danger btn-sm pull-right"><i class="fa fa-check"></i> Terminer la Tache</a>
							</div>
						@endif
					</div>
					<div class="panel-body">
						@la_display($module, 'libelle')
						<div class="form-group">
							<label for="description" class="col-md-2">Description :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($module->row['description']) !!}
							</div>
						</div>
						{{--@la_display($module, 'description')--}}
						@la_display($module, 'brief_id')
						<div class="form-group">
							<label for="description" class="col-md-2">Assigné :</label>
							<div class="col-md-10 fvalue">
								{{ $mytaches->userTache->name }}
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="col-md-2">Assignant :</label>
							<div class="col-md-10 fvalue">
								{{ $mytaches->userAssignant->name   }}
							</div>
						</div>
						{{--@la_display($module, 'assigne_id')
						@la_display($module, 'assignant_id')
						@la_display($module, 'status')--}}
						<div class="form-group">
							<label for="status" class="col-md-2">Status :</label>
							<div class="col-md-10 fvalue">
								@if($module->row['status']=='0')
									<span class="label label-danger">En attente</span>
								@elseif($module->row['status']=='1')
									<span class="label label-warning">En cours</span>
								@else
									<span class="label label-success">Terminer</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane fade in" id="tab-info-brief">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="row panel-default panel-heading">
						<div class="col-md-6"><h4>Information debrief</h4></div>
					</div>
					<div class="panel-body">

						<div class="form-group">
							<label for="description" class="col-md-2">Libelle :</label>
							<div class="col-md-10 fvalue">
								{{$debrieftache->libelle}}
							</div>
						</div>

						<div class="form-group">
							<label for="description" class="col-md-2">Proposition :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($debrieftache->proposition) !!}
							</div>
						</div>
						<div class="form-group">
							<label for="description" class="col-md-2">Cible :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($debrieftache->cible) !!}
							</div>
						</div>

						<div class="form-group">
							<label for="description" class="col-md-2">Travail attendu :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($debrieftache->taf) !!}
							</div>
						</div>

						<div class="form-group">
							<label for="description" class="col-md-2">Date d'entrée :</label>
							<div class="col-md-10 fvalue">
								{{$debrieftache->dateDentree}}
							</div>
						</div>

						<div class="form-group">
							<label for="description" class="col-md-2">Date de sortie :</label>
							<div class="col-md-10 fvalue">
								{{$debrieftache->dateDeSortie}}
							</div>
						</div>

						@if($debrieftache->pj)
							<div class="form-group">
								<label for="piece" class="col-md-2">Pièces jointes :</label>
								<div class="col-md-10 fvalue">
									<a href="{{url('briefFiles/'.$debrieftache->pj)}}" download="{{$debrieftache->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
								</div>
							</div>
						@endif

						<div class="form-group">
							<label for="dateDeSortie" class="col-md-2">Statut :</label>
							<div class="col-md-10 fvalue">
								@if($debrieftache->status=='0')
									<span class="label label-danger">Rejeter</span>
								@elseif($debrieftache->status=='1')
									<span class="label label-warning">En attente</span>
								@else
									<span class="label label-success">Valider</span>
								@endif
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		@if($module->row['assigne_id']==Auth::user()->id and $module->row['status']!='0')
		<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="terminer-tache">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="row panel-default panel-heading">
						<div class="col-md-6"><h4>Asset téléchargés</h4></div>
						<div class="col-md-6">
							<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal"><i class="fa fa-plus"></i> Ajouter un asset</button>
						</div>
					</div>
					<div class="panel-body">
						<div class="box box-success">
							<!--<div class="box-header"></div>-->
							<div class="box-body">
								{{--@php($extensions_check=array('jpeg','jpg','gif','tiff','bmp','png'))--}}
								<div class="box-footer">
									<ul class="mailbox-attachments clearfix">
										@foreach($assetUser as $asset )
											{{--<div class="col-md-4">--}}

											@if(in_array($asset->ext, ['jpeg','jpg','gif','tiff','bmp','png']))

												<li>
													{{--<span class="mailbox-attachment-icon" style="max-height:150px;">--}}
														{{--<i class="fa fa-file-image-o"></i>--}}
													{{--</span>--}}
													<span class="mailbox-attachment-icon has-img" style="max-height:150px;">
														<a href="{{url('assetFiles/'.$asset->path)}}" data-lightbox="image-1" data-title="{{$asset->libelle}}">
															<img src="{{url('assetFiles/'.$asset->path)}}" alt="{{$asset->libelle}}" style="width: 100%">
														</a>
													</span>
													<div class="mailbox-attachment-info">
														<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
														<span class="mailbox-attachment-size">
															<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
															<a href="{{url('assetFiles/'.$asset->path)}}" data-lightbox="image-1" data-title="{{$asset->libelle}}" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
															<br>
															@if($asset->status=='0')
																<span class="label label-danger">Asset rejeté</span>
															@elseif($asset->status=='1')
																<span class="label label-warning">En attente de validation</span>
															@else
																<span class="label label-success">Asset validé</span>
															@endif

															<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
																<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
																{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
																<button type="submit" class="btn btn-danger btn-xs pull-right">
																	<i class="fa fa-trash"></i>
																</button>
																{{ Form::close() }}
															</form>
														</span>
													</div>
												</li>
											@endif

											@if(in_array($asset->ext, ['htm','html','xml']))
												<li>
													<span class="mailbox-attachment-icon" style="max-height:150px;">
														<i class="fa fa-code"></i>
													</span>
													<div class="mailbox-attachment-info">
														<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
														<span class="mailbox-attachment-size">
															<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
															<a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
															<br>
															@if($asset->status=='0')
																<span class="label label-danger">Asset rejeté</span>
															@elseif($asset->status=='1')
																<span class="label label-warning">En attente de validation</span>
															@else
																<span class="label label-success">Asset validé</span>
															@endif
															<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
																<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
																{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
																<button type="submit" class="btn btn-danger btn-xs pull-right">
																	<i class="fa fa-trash"></i>
																</button>
																{{ Form::close() }}
															</form>
														</span>
													</div>
												</li>

												{{--<h3>{{$asset->libelle}}</h3>
                                                <object width="100%" height="500px" data="{{url('assetFiles/'.$asset->path)}}"></object>--}}
											@endif

											@if(in_array($asset->ext, ['mp3','wma','aac','ogg']))
												<li>
													<span class="mailbox-attachment-icon" style="max-height:150px;">
														<i class="fa fa-file-audio-o"></i>
													</span>
													<div class="mailbox-attachment-info">
														<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
														<span class="mailbox-attachment-size">
															<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
															<a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        					<br>
															@if($asset->status=='0')
																<span class="label label-danger">Asset rejeté</span>
															@elseif($asset->status=='1')
																<span class="label label-warning">En attente de validation</span>
															@else
																<span class="label label-success">Asset validé</span>
															@endif
															<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
																<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
																{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
																<button type="submit" class="btn btn-danger btn-xs pull-right">
																	<i class="fa fa-trash"></i>
																</button>
																{{ Form::close() }}
															</form>
														</span>
													</div>
												</li>
												{{--<h3>{{$asset->libelle}}</h3>
                                                <audio src="{{url('assetFiles/'.$asset->path)}}" controls></audio>--}}
											@endif

											@if(in_array($asset->ext, ['avi','mpeg','mov','mp4','flv']))
												<li>
													<span class="mailbox-attachment-icon" style="max-height:150px;">
														<i class="fa fa-file-video-o"></i>
													</span>
													<div class="mailbox-attachment-info">
														<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
														<span class="mailbox-attachment-size">
															<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
															<a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        					<br>
															@if($asset->status=='0')
																<span class="label label-danger">Asset rejeté</span>
															@elseif($asset->status=='1')
																<span class="label label-warning">En attente de validation</span>
															@else
																<span class="label label-success">Asset validé</span>
															@endif
															<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
																<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
																{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
																<button type="submit" class="btn btn-danger btn-xs pull-right">
																	<i class="fa fa-trash"></i>
																</button>
																{{ Form::close() }}
															</form>
														</span>
													</div>
												</li>
												{{--<h3>{{$asset->libelle}}</h3>
                                                <video src="{{url('assetFiles/'.$asset->path)}}" controls poster="assets/img/movie.png" width="100%"></video>--}}
											@endif

											@if(in_array($asset->ext, ['rar','zip','tar','EndNote']))
												<li>
													<span class="mailbox-attachment-icon" style="max-height:150px;">
														<i class="fa fa-file-archive-o"></i>
													</span>
													<div class="mailbox-attachment-info">
														<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
														<span class="mailbox-attachment-size">
															<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
															<br>
															@if($asset->status=='0')
																<span class="label label-danger">Asset rejeté</span>
															@elseif($asset->status=='1')
																<span class="label label-warning">En attente de validation</span>
															@else
																<span class="label label-success">Asset validé</span>
															@endif
															<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
																<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
																{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
																<button type="submit" class="btn btn-danger btn-xs pull-right">
																	<i class="fa fa-trash"></i>
																</button>
																{{ Form::close() }}
															</form>
														</span>
													</div>
												</li>
												{{--<h3>{{$asset->libelle}}</h3>
                                                <img class="img-responsive" src="{{url('assetFiles/'.$asset->path)}}" alt="{{$asset->libelle}}">--}}
											@endif

											@if(in_array($asset->ext, ['pdf']))
												<li>
													<span class="mailbox-attachment-icon" style="max-height:150px;">
														<i class="fa fa-file-pdf-o"></i>
													</span>
													<div class="mailbox-attachment-info">
														<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
														<span class="mailbox-attachment-size">
															<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
															<a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        					<br>
															@if($asset->status=='0')
																<span class="label label-danger">Asset rejeté</span>
															@elseif($asset->status=='1')
																<span class="label label-warning">En attente de validation</span>
															@else
																<span class="label label-success">Asset validé</span>
															@endif
															<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
																<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
																{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
																<button type="submit" class="btn btn-danger btn-xs pull-right">
																		<i class="fa fa-trash"></i>
																	</button>
																{{ Form::close() }}
															</form>
														</span>
													</div>
												</li>
											@endif

											@if(in_array($asset->ext, ['ppt','pptx']))
												<li>
													<span class="mailbox-attachment-icon" style="max-height:150px;">
														<i class="fa fa-file-powerpoint-o"></i>
													</span>
													<div class="mailbox-attachment-info">
														<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
														<span class="mailbox-attachment-size">
															<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
															<a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        					<br>
															@if($asset->status=='0')
																<span class="label label-danger">Asset rejeté</span>
															@elseif($asset->status=='1')
																<span class="label label-warning">En attente de validation</span>
															@else
																<span class="label label-success">Asset validé</span>
															@endif
															<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
																<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
																{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
																<button type="submit" class="btn btn-danger btn-xs pull-right">
																	<i class="fa fa-trash"></i>
																</button>
																{{ Form::close() }}
															</form>
														</span>
													</div>
												</li>
											@endif

											@if(in_array($asset->ext, ['xls','xlsx','csv']))
												<li>
													<span class="mailbox-attachment-icon" style="max-height:150px;">
														<i class="fa fa-file-excel-o"></i>
													</span>
													<div class="mailbox-attachment-info">
														<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
														<span class="mailbox-attachment-size">
															<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
															<a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        					<br>
															@if($asset->status=='0')
																<span class="label label-danger">Asset rejeté</span>
															@elseif($asset->status=='1')
																<span class="label label-warning">En attente de validation</span>
															@else
																<span class="label label-success">Asset validé</span>
															@endif
															<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
																<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
																{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
																<button type="submit" class="btn btn-danger btn-xs pull-right">
																	<i class="fa fa-trash"></i>
																</button>
																{{ Form::close() }}
															</form>
														</span>
													</div>
												</li>
											@endif

											{{--</div>--}}
										@endforeach
									</ul>
								</div>

							</div>
							{{--<div class="box-body">
                                <table id="example1" class="table table-bordered">
                                <thead>
                                <tr class="success">
                                    @foreach( $listing_cols as $col )
                                    <th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
                                    @endforeach
                                    @if($show_actions)
                                    <th>Actions</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                </table>
                            </div>--}}
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
	</div>
	</div>

@la_access("Assets", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">AJOUTER UN ASSET</h4>
			</div>
			{!! Form::open(['action' => 'LA\AssetsController@store', 'id' => 'asset-add-form','files' => true]) !!}
			<div class="modal-body">
				<div class="box-body">
					{{--@la_form($module)
                    @la_input($module, 'asset_type')
                    @la_input($module, 'brief_id')
                    @la_input($module, 'path')
                    @la_input($module, 'ext')--}}
					<div class="form-group">
						<label for="libelle">Libelle :</label>
						<input class="form-control" required placeholder="Entrer le Libelle" data-rule-maxlength="255" name="libelle" type="text" value="">
					</div>

					<div class="form-group">
						<label for="asset_type">Type d'asset :</label>
						<select class="form-control" required="" id="asset_type" name="asset_type" tabindex="-1" aria-hidden="true" aria-required="true">
							@foreach($assetypes as $asset)
								<option value="{{$asset->name}}">{{$asset->libelle}}</option>
							@endforeach

							{{--<option value="reco">Reco</option>
							<option value="crea">Créa</option>
							<option value="flowplane">Flow plan</option>
							<option value="planmedia">Plan Media</option>
							--}}{{--<option value="digital">Digital</option>--}}{{--
							<option value="fichefabric">Fiche de fabrication</option>
							<option value="retroplanning">Retroplanning</option>
							<option value="bcp">Bon de commande prestataire</option>
							<option value="rapportop">Rapport opérationnel</option>
							<option value="livrable">Livrable</option>
							<option value="rapport">Rapport Digital Mensuel</option>
							<option value="rapportrim">Rapport Digital Trimestriel</option>
							<option value="production">Production</option>
							<option value="planpubli">Planning de publication</option>
							<option value="gglads">Visuel Google Ads</option>
							<option value="prototype">Prototype & Rendu</option>--}}
						</select>
					</div>
					<p class="help-block" style="color: red!important;">Si vous devez ajouter plusieurs fichiers veuillez le zip avant de le charger</p>
					<div class="form-group">
						<label for="file"><strong>Fichier</strong></label>
						<input type="file" id="file" name="file" class="dropify" required/>
					</div>

					<input type="hidden" name="status" value="1">
					{{--<input type="hidden" name="asset_type" value="{{$asset_type}}">--}}
					<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
					<input type="hidden" name="debrief_id" value="{{$tach->debrief_id}}">
					<input type="hidden" name="tache_id" value="{{$tach->id}}">
				</div>

				<div class="col-md-12">
					<div id="loader" class="pull-right" style="display: none;">
						<img src="{{ URL::asset('la-assets/img/load.gif')}}">
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">FERMER</button>
				{!! Form::submit( 'ENREGISTER', ['class'=>'btn btn-success','onClick'=>'submitDetailsForm()','id'=>'formaddasset']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access


	</div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>
<script src="{{ asset('la-assets/plugins/lightbox/lightbox.js') }}"></script>
<script>
    $('.dropify').dropify();

    function submitDetailsForm() {
        $("#formaddasset").prop('disabled', true);
        $('#loader').show();
        //alert("ici");
        $("#asset-add-form").submit();
    }

</script>
@endpush
