@extends("la.layouts.app")

@section("contentheader_title", "Job bag")
@section("htmlheader_title", "Job bag Liste")

@section("headerElems")
    @la_access("Projets", "create")
    <a href="{{url(config('laraadmin.adminRoute').'/projets_add')}}" class="btn btn-success btn-sm pull-right">
        Ajouter un Job bag
    </a>
    @endla_access
@endsection

@section("main-content")

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">PHASE 1</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered">
                        <thead>
                        <tr>
                            {{--<th>Id</th>--}}
                            <th>Opération</th>
                            <th>Annonceur</th>
                            <th>Commerciale</th>
                            <th>Date d'entrée</th>
                            @if(Auth::user()->role_id=='4' or Auth::user()->role_id=='1')
                            <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($project1 as $projet)
                            <tr>
                                {{--<td>{{$projet->id}}</td>--}}
                                <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                                <td>{{$projet->annonceurProject->name}}</td>
                                <td>{{$projet->userProject->name}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                                @if(Auth::user()->id=='4' or Auth::user()->role_id=='1')
                                <td>
                                    <a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">PHASE 2</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered">
                        <thead>
                        <tr>
                            {{--<th>Id</th>--}}
                            <th>Opération</th>
                            <th>Annonceur</th>
                            <th>Commerciale</th>
                            <th>Date d'entrée</th>
                            {{--<th>Date de sortie</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($project2 as $projet)
                            <tr>
                                {{--<td>{{$projet->id}}</td>--}}
                                <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                                <td>{{$projet->annonceurProject->name}}</td>
                                <td>{{$projet->userProject->name}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                                {{--<td>{{date("d/m/Y",strtotime($projet->dateDeSortie))}}</td>--}}
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">PHASE 3</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example3" class="table table-bordered">
                        <thead>
                        <tr>
                            {{--<th>Id</th>--}}
                            <th>Opération</th>
                            <th>Annonceur</th>
                            <th>Commerciale</th>
                            <th>Date d'entrée</th>
                            {{--<th>Date de sortie</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($project3 as $projet)
                            <tr>
                                {{--<td>{{$projet->id}}</td>--}}
                                <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                                <td>{{$projet->annonceurProject->name}}</td>
                                <td>{{$projet->userProject->name}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                                {{--<td>{{date("d/m/Y",strtotime($projet->dateDeSortie))}}</td>--}}
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });

        $("#example2").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });

        $("#example3").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });
    });
</script>
@endpush
