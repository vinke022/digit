@extends("la.layouts.app")

@section("contentheader_title", "Job Bag")
@section("contentheader_description", "Phase 3")
@section("htmlheader_title", "Job Bag phase 3")

@section("headerElems")

@endsection

@section("main-content")

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box box-success">
        <!--<div class="box-header"></div>-->
        <div class="box-body">
            <table id="example1" class="table table-bordered">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Opération</th>
                    <th>Annonceur</th>
                    <th>Date d'entrée</th>
                    <th>Date de sortie</th>
                    {{--<th>Action</th>--}}
                </tr>
                </thead>
                <tbody>
                @if(count($projects)>0)
                    @foreach($projects as $projet)
                        <tr>
                            <td>{{$projet->slug}}</td>
                            <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                            <td>{{$projet->annonceurProject->name}}</td>
                            <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                            <td>{{date("d/m/Y",strtotime($projet->dateDeSortie))}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr><td>Aucun Job bag en phase 3 </td></tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;den"
                },
            }
        });
    });
</script>
@endpush
