@extends('la.layouts.app')

@section('htmlheader_title')
	Projet View
@endsection
@push('styles')
<style>
	.table thead tr.success td{
		background-color: #263238!important;
		color: #fff;
	}
</style>
@endpush

@section('main-content')
	<div id="page-content" class="profile2">
		<div class="nav nav-tabs profile clearfix">
			<div class="col-md-1">
				<div class="row">
					<div class="col-md-12">
					<!--<img class="profile-image" src="{{ asset('la-assets/img/avatar5.png') }}" alt="">-->
						<div class="profile-icon text-primary"><i class="fa {{ $module->fa_icon }}"></i></div>
					</div>
					<!-- <div class="col-md-9">

                    </div> -->
				</div>
			</div>
		<!-- <div class="col-md-3">
			<div class="dats1"><div class="label2">Admin</div></div>
			<div class="dats1"><i class="fa fa-envelope-o"></i> superadmin@gmail.com</div>
			<div class="dats1"><i class="fa fa-map-marker"></i> Pune, India</div>
		</div> -->
			<div class="col-md-10">
				<h4 class="name">{{ $projet->$view_col }}</h4>
			</div>
			{{--<div class="col-md-1 actions">
				@la_access("Projets", "edit")
				<a href="{{ url(config('laraadmin.adminRoute') . '/projets/'.$projet->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br>
				@endla_access

				@la_access("Projets", "delete")
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.projets.destroy', $projet->id], 'method' => 'delete', 'style'=>'display:inline']) }}
				<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
				{{ Form::close() }}
				@endla_access
			</div>--}}
		</div>

		<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
			<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/projets') }}" data-toggle="tooltip" data-placement="right" title="Cliquer pour revenir en arrière"><i class="fa fa-chevron-left"></i></a></li>
			<li class="active"><a role="tab" data-toggle="tab" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-bars"></i> Brief du project</a></li>
			<li class=""><a role="tab" data-toggle="tab" class="active" href="#op" data-target="#op"><i class="fa fa-folder"></i> ASSET OP </a></li>
			<li class=""><a role="tab" data-toggle="tab" class="active" href="#dp" data-target="#dp"><i class="fa fa-file-o"></i> Devis et Proforma</a></li>
			<li class=""><a role="tab" data-toggle="tab" class="active" href="#mail" data-target="#mail"><i class="fa fa-reply-all"></i> Mail client</a></li>
			{{--<li class=""><a role="tab" data-toggle="tab" class="active" href="#ds" data-target="#ds"><i class="fa fa-globe"></i> Divers</a></li>
			<li class=""><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa {{ $module->fa_icon }}"></i> Information project</a></li>--}}
			<li class=""><a role="tab" data-toggle="tab" class="active" href="#assets" data-target="#assets"><i class="fa fa-folder"></i> Assets</a></li>
			<li class=""><a role="tab" data-toggle="tab" class="active" href="#checklist" data-target="#checklist"><i class="fa fa-check-square-o"></i> Check-list</a></li>

			@if($projet->niveau == 1)
				<li class=""><a role="tab" data-toggle="tab" class="" href="#bc" data-target="#bc"><i class="fa fa-refresh"></i>  Passer à la phase 1</a></li>
			@endif

			@if($projet->niveau == 2)
				<li class=""><a role="tab" data-toggle="tab" class="" href="#nv3" data-target="#nv3"><i class="fa fa-refresh"></i>  Passer à la phase 2</a></li>
			@endif
			<li class=""><a role="tab" data-toggle="tab" class="active" href="#teamwork" data-target="#teamwork"><i class="fa fa-users"></i> Equipes Project</a></li>

		</ul>

		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in" id="tab-info">
				<div class="tab-content">
					<div class="panel infolist">
						<div class="panel-default panel-heading">
							<h4>Information project</h4>
						</div>
						<div class="panel-body">
							@la_display($module, 'operation')
							@la_display($module, 'annonceur')
							@la_display($module, 'dateDentree')
							{{--@la_display($module, 'userId')
							@la_display($module, 'dateDeSortie')--}}
						</div>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane fade in" id="teamwork">
				<div class="tab-content">
					<div class="panel infolist">
						<div class="panel-default panel-heading">
							<h4>Equipes project</h4>
						</div>
						<div class="panel-body">
							<div class="col-md-12">
								<div class="box-body table-responsive no-padding">
									<table class="table table-hover">
										<tbody><tr>
											<th>Departement</th>
											<th>Utilisateur</th>
										</tr>
										@foreach($userComms as $userTeam)
										<tr>
											<td>{{$userTeam->departUsers->name}}</td>
											<td>{{$userTeam->name}}</td>
										</tr>
										@endforeach
										@foreach($userAsserts as $userTeam)
											<tr>
												<td>{{$userTeam->departUsers->name}}</td>
												<td>{{$userTeam->name}}</td>
											</tr>
										@endforeach
										@foreach($userDep as $userTeam)
											<tr>
												<td>{{$userTeam->departUsers->name}}</td>
												<td>{{$userTeam->name}}</td>
											</tr>
										@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="assets">
				<div class="tab-content">
					<div class="panel infolist">
						<div class="panel-default panel-heading">
							<div class="row">
								<div class="col-md-6"><h4>Assets </h4></div>
								<div class="col-md-6">
									<form class="form-inline" style="display: inline-flex" action="{{route('searchassetspjt')}}" method="GET">
										{{ csrf_field() }}
										<div class="">
												<select class="form-control" name="debrief" id="debrief">
													<option value="">Tous les debriefs</option>
													@foreach($debriefs as $debrief)
														<option value="{{$debrief->id}}">{{$debrief->libelle}}</option>
													@endforeach
												</select>
											</div>
											<div class="col-md-3">
												<select class="form-control" name="niveau" id="niveau">
													<option value="">Tous</option>
													<option value="1">Phase 1</option>
													<option value="2">Phase 2</option>
													<option value="3">Phase 3</option>
												</select>
											</div>
											<div class="col-md-3">
												<input type="hidden" name="idpjt" value="{{$projet->id}}">
												<button type="submit" name="send" class="btn btn-success btn-md"><i class="fa fa-eye"></i> Afficher</button>
											</div>
										{{--<div class="form-group" style="border-bottom:none">
											<input type="text" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
										</div>
										<div class="form-group" style="border-bottom:none">
											<input type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com">
										</div>
										<button type="submit" name="send" class="btn btn-success btn-bloc btn-sm">VOIR</button>--}}
									</form>
								</div>
							</div>
						</div>

						<div class="panel-body">
							<div class="row">
								@php $assetypes1 = (new \App\Http\Controllers\AssetsvalidController())->typAssetForVue();@endphp

								@foreach($assetypes1 as $k => $assetype)
									@php $assetnb = (new \App\Http\Controllers\LA\ProjetsController())->nbAssetByBrief($projet->id,$assetype['libelle']);@endphp
									<div class="col-lg-3 col-xs-6">
										<!-- small box -->
										<div class="small-box bg-{{$assetcolors[$k]}}">
											<div class="inner">
												<h4 style="font-weight: bold;margin: 0 0 10px 0;white-space: nowrap;padding: 0;">{{$assetype['libelle']}}</h4>
												<p>{{$assetnb}}</p>
											</div>
											<a href="{{url(config('laraadmin.adminRoute') . '/projets/assets/'.$projet->id.'/'.$assetype['name'])}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
										</div>
									</div><!-- ./col -->
								@endforeach
							</div><!-- /.row -->
						</div>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="checklist">
				@if(count($listsFormCheck)>0)
					<div class="box box-success">
						<div class="box-body">

							<div class="col-md-6">
								@foreach($listsFormCheck as $k => $check)
									<div class="form-group">
										<label class="checkbox-inline">
											@php
												$exist = $checklistsShow[$k][0]->checklist_id;
											@endphp
											@if($exist == $check->id)
												<input type="checkbox" class="check-list-input" name="check" value="{{$check->id}}" checked> {{$check->libelle}}
											@else
												<input type="checkbox" class="check-list-input" name="check" value="{{$check->id}}"> {{$check->libelle}}
											@endif
										</label>
									</div>
								@endforeach
							</div>

							<div class="col-md-6">
								<h4 style="border-bottom: solid 1px #eee; padding-bottom: 5px;">Décochés</h4>
								@foreach($uncheck as $v)
									<div class="form-group">
										<label class="checkbox-inline" style="color: #a29f9f;">
											<input type="checkbox" class="check-list-input" name="check" disabled>
											<?php print_r($v[0]->libelle); ?>
										</label>
									</div>
								@endforeach
							</div>

						</div>
					</div>
				@else
					<div class="box box-success">
						<div class="box-body">
							@foreach(array_chunk($checklistAlluser,6) as $checks)
								<div class="col-md-4">
									@foreach($checks as $check)
									<div class="form-group">
										<label class="checkbox-inline">
											<input type="checkbox" class="check-list-inputt" checked disabled> {{$check->libelle}}
												@if($check->niveau==1)
													<small class="label label-danger">Phase 1</small>
												@elseif($check->niveau==2)
													<small class="label label-warning">Phase 2</small>
												@else
													<small class="label label-success">Phase 3</small>
												@endif
										</label>
									</div>
									@endforeach
								</div>
							@endforeach
						</div>
					</div>
				@endif
			</div>

			<div role="tabpanel" class="tab-pane active fade in p20 bg-white" id="tab-timeline">
				<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
					@la_access("Briefs", "create")
					@if(count($briefs)==0)
						<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">
							Ajouter un brief
						</button>
					@endif
					@endla_access
				</div>
				<div class="box box-success">
					<div class="box-body">
						<table id="example1" class="table table-bordered">
							<thead>
							<tr class="success">
								@foreach( $listing_cols_brief as $col )
									<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
								@endforeach
								@if($show_actions)
									<th>Actions</th>
								@endif
							</tr>
							</thead>
							<tbody>
							@foreach($briefs as $k => $brief)
								<tr role="row" class="odd">
									<td class="sorting_1">{{$k+1}}</td>
									<td>
										<a href="{{url(config('laraadmin.adminRoute') . '/briefs/'.$brief->id)}}">
											{{$brief->libelle}}
										</a>
									</td>
									<td>
                                        @php $tab= explode(' ', strip_tags($brief->proposition), 16);unset($tab[15]); @endphp
										{{implode(' ', $tab).'...'}}
										{{--{!! nl2br($brief->proposition) !!}--}}
									</td>
									<td>
										@php $tab= explode(' ', strip_tags($brief->cible), 16);unset($tab[15]); @endphp
										{{implode(' ', $tab).'...'}}
										{{--{!! nl2br($brief->cible) !!}--}}
									</td>
									<td>
										@php $tab= explode(' ', strip_tags($brief->taf), 16);unset($tab[15]); @endphp
										{{implode(' ', $tab).'...'}}
										{{--{!! nl2br($brief->taf) !!}--}}
									</td>
									<td>{{$brief->dateDentree}}</td>
									{{--<td>{{$brief->dateDeSortie}}</td>--}}
									<td>
										@if($brief->status=='0')
											<span class="label label-danger">Rejeter</span>
										@elseif($brief->status=='1')
											<span class="label label-warning">En attente</span>
										@else
											<span class="label label-success">Valider</span>
										@endif
									</td>
									<td>
										@if(Module::hasAccess("Briefs", "edit"))
											<a href="{{url(config('laraadmin.adminRoute') . '/briefs/'.$brief->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
											{{--{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.briefs.destroy', $brief->id], 'method' => 'delete', 'style'=>'display:inline']) }}--}}
											{{--<button class="btn btn-danger btn-xs" type="submit">--}}
												{{--<i class="fa fa-times"></i>--}}
											{{--</button>--}}
											{{--{{Form::close()}}--}}
										@endif
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="op">
				@if($projet->niveau != 2)
				<div class="tab-content">
					<div class="panel infolist">
						<div class="panel-default panel-heading">
							<h4>Asset opérationnel</h4>
						</div>
						<div class="panel-body">
							<p>Disponible à l'etape 2</p>
						</div>
					</div>
				</div>
				@else
					<div class="tab-content">
						<div class="panel infolist">
							<div class="panel-default panel-heading">
								{{--<h4>Assets opérationnel </h4>--}}
								<div class="row">
									<div class="col-md-6"><h4>Assets opérationnel</h4></div>
									<div class="col-md-6">
										<form class="form-inline" style="display: inline-flex" action="{{route('searchassetspjt')}}" method="GET">
											{{ csrf_field() }}
											<div class="">
												<select class="form-control" name="debrief" id="debrief">
													<option value="">Tous les debriefs</option>
													@foreach($debriefs as $debrief)
														<option value="{{$debrief->id}}">{{$debrief->libelle}}</option>
													@endforeach
												</select>
											</div>
											<div class="col-md-3">
												<select class="form-control" name="niveau" id="niveau">
													<option value="">Tous</option>
													<option value="1">Phase 1</option>
													<option value="2">Phase 2</option>
													<option value="3">Phase 3</option>
												</select>
											</div>
											<div class="col-md-3">
												<input type="hidden" name="idpjt" value="{{$projet->id}}">
												<button type="submit" name="send" class="btn btn-success btn-md"><i class="fa fa-eye"></i> Afficher</button>
											</div>
											{{--<div class="form-group" style="border-bottom:none">
                                                <input type="text" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
                                            </div>
                                            <div class="form-group" style="border-bottom:none">
                                                <input type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com">
                                            </div>
                                            <button type="submit" name="send" class="btn btn-success btn-bloc btn-sm">VOIR</button>--}}
										</form>
									</div>
								</div>
							</div>
							<div class="panel-body">
								<div class="row">
									@php $assetypes2 = (new \App\Http\Controllers\LA\ProjetsController())->typAssetOP();@endphp

									@foreach($assetypes2 as $k => $assetype)
										@php $assetnb = (new \App\Http\Controllers\LA\ProjetsController())->nbAssetByBrief($projet->id,$assetype['libelle']);@endphp
										<div class="col-lg-3 col-xs-6">
											<!-- small box -->
											<div class="small-box bg-{{$assetcolors[$k]}}">
												<div class="inner">
													<h4 style="font-weight: bold;margin: 0 0 10px 0;white-space: nowrap;padding: 0;">{{$assetype['libelle']}}</h4>
													<p>{{$assetnb}}</p>
												</div>
												<a href="{{url(config('laraadmin.adminRoute') . '/projets/assets/'.$projet->id.'/'.$assetype['name'])}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
											</div>
										</div><!-- ./col -->
									@endforeach
								</div><!-- /.row -->
							</div>
						</div>
					</div>
				@endif
			</div>

			<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="dp">
				<div class="tab-content">
					<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
						@la_access("Devis", "create")
						<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModalDP">
							Ajouter un devis ou proforma
						</button>
						@endla_access
					</div>
					<div class="box box-success">
						<div class="box-body">
							<table id="example2" class="table table-bordered">
								<thead>
								<tr class="success">
									<td>Id</td>
									<td>Libelle</td>
									<td>Commercial</td>
									<td>Telecharger</td>
									<td>Status</td>
									<td>Action</td>
								</tr>
								</thead>
								<tbody>

								@foreach($dp as $k => $dp)
									<tr>
										<td class="sorting_1">{{$k+1}}</td>
										<td>{{$dp->libelle}}</td>
										<td>{{$dp->userDP->name}}</td>
										<td><a href="{{url('devisFiles/'.$dp->path)}}" download="{{$dp->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a></td>
										<td>
											@if($dp->status=='0')
												<span class="label label-danger">Rejeter</span>
											@elseif($dp->status=='1')
												<span class="label label-warning">En attente</span>
											@elseif($dp->status=='3')
												<span class="label bg-navy">En attente de suppression</span>
											@else
												<span class="label label-success">Valider</span>
											@endif
										</td>
										<td>
											@if($dp->user_id == Auth::user()->id)
												{{--{!! Form::open(['route' => [config('laraadmin.adminRoute') . '.devis.destroy', $dp->id], 'method' => 'delete', 'style'=>'display:inline']) !!}--}}
												{{--<button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>--}}
												{{--{!! Form::close() !!}--}}
												@if($dp->status != '3')
												<a data-toggle="modal" data-target="#versionModalRapport" href="#" onclick="displayModalRapport('{{$dp->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
												@endif
											@endif
										</td>
									</tr>
								@endforeach

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			{{--<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="ds">
				<div class="tab-content">
					<div class="panel infolist">
						<div class="panel-default panel-heading">
							<h4>Divers</h4>
						</div>
						<div class="panel-body">

						</div>
					</div>
				</div>
			</div>--}}

			<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="mail">
				<div class="tab-content">
					<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
						@la_access("Mails", "create")
						<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModalMail">
							Ajouter un mail client
						</button>
						@endla_access
					</div>
					<div class="box box-success">
						<div class="box-body">
							<ul class="mailbox-attachments clearfix">
								@foreach($mails as $asset)
								<li>
									<span class="mailbox-attachment-icon has-img" style="max-height:150px;">
										<a href="{{url('mailFiles/'.$asset->path)}}" data-lightbox="image-1" data-title="{{$asset->libelle}}">
											<img src="{{url('mailFiles/'.$asset->path)}}" alt="{{$asset->libelle}}" style="width: 100%">
										</a>
									</span>
									<div class="mailbox-attachment-info">
										<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
										<span class="mailbox-attachment-size">
											<a href="{{url('mailFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
											<br>
											@if(Auth::user()->id ==$asset->user_id )
												{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assetops.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
												<button type="submit" class="btn btn-danger btn-xs pull-right">
																<i class="fa fa-trash"></i>
															</button>
												{{ Form::close() }}
											@endif
										</span>
									</div>
								</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
			</div>

			@if($projet->niveau == 1)
				<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="bc">
					<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
						@la_access("Briefs", "create")
							@if(count($briefs)!=0)
								<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModalBC">
									Ajouter un bon de commande
								</button>
							@else
								<p class="pull-right text-danger">Ajouter un brief avant de pouvoir ajouter un bon de commande</p>
							@endif
						@endla_access
					</div>
					<div class="box box-success">
						<div class="box-body">
							<table id="example2" class="table table-bordered">
								<thead>
								<tr class="success">
									<td>Id</td>
									<td>Libelle</td>
									<td>Commercial</td>
									<td>Télécharger</td>
									<td>Status</td>
									<td>Action</td>
								</tr>
								</thead>
								<tbody>

								@foreach($bc as $k => $bc)
									<tr>
										<td class="sorting_1">{{$k+1}}</td>
										<td>{{$bc->libelle}}</td>
										<td>{{$bc->userBC->name}}</td>
										<td><a href="{{url('assetFiles/'.$bc->path)}}" download="{{$bc->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a></td>
										<td>
											@if($bc->status=='0')
												<span class="label label-danger">Rejeter</span>
											@elseif($bc->status=='1')
												<span class="label label-warning">En attente</span>
											@else
												<span class="label label-success">Valider</span>
											@endif
										</td>
										<td>
											@if($bc->user_id == Auth::user()->id)
												{!! Form::open(['route' => [config('laraadmin.adminRoute') . '.bcommandes.destroy', $bc->id], 'method' => 'delete', 'style'=>'display:inline']) !!}
												<button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>
												{!! Form::close() !!}
											@endif
										</td>
									</tr>
								@endforeach

								</tbody>
							</table>
						</div>
					</div>
				</div>
			@endif

			@if($projet->niveau == 2)
				<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="nv3">

					<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
						@la_access("Briefs", "create")
						<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModalNV3">
							<i class="fa fa-refresh"></i> SOUMETTRE
						</button>
						@endla_access
					</div>
					<div class="box box-success">
						<div class="box-body">
							<table id="example2" class="table table-bordered">
								<thead>
								<tr class="success">
									<td>Id</td>
									<td>Libelle</td>
									<td>Commercial</td>
									<td>Télécharger</td>
									<td>Status</td>
									<td>Action</td>
								</tr>
								</thead>
								<tbody>

								@foreach($soumis as $k => $nv)
									<tr>
										<td class="sorting_1">{{$k+1}}</td>
										<td>{{$nv->libelle}}</td>
										<td>{{$nv->userSomi->name}}</td>
										<td><a href="{{url('assetFiles/'.$nv->path)}}" download="{{$nv->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a></td>
										<td>
											@if($nv->status=='0')
												<span class="label label-danger">Rejeter</span>
											@elseif($nv->status=='1')
												<span class="label label-warning">En attente</span>
											@else
												<span class="label label-success">Valider</span>
											@endif
										</td>
										<td>
											@if($nv->user_id == Auth::user()->id)
												{!! Form::open(['route' => [config('laraadmin.adminRoute') . '.soumissions.destroy', $nv->id], 'method' => 'delete', 'style'=>'display:inline']) !!}
												<button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>
												{!! Form::close() !!}
											@endif
										</td>
									</tr>
								@endforeach

								</tbody>
							</table>
						</div>
					</div>

				</div>
			@endif
		</div>
	</div>


	<div class="modal fade" id="AddModalCkeck" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Ajouter un Check-list</h4>
				</div>
				{!! Form::open(['action' => 'LA\ProjetsController@addcheck', 'id' => 'addcheck-add-form']) !!}
				<div class="modal-body">
					<div class="box-body">

						@foreach($listsFormCheck as $check)
							<div class="col-md-6">
							<div class="form-group">
								<label class="checkbox-inline">
									<input type="checkbox" id="check" name="check[]" value="{{$check->id}}"> {{$check->libelle}}
								</label>
							</div>
							</div>
						@endforeach
						<input type="hidden" name="project_id" value="{{$projet->id}}">

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">FERMER</button>
					{!! Form::submit( 'AJOUTER', ['class'=>'btn btn-success']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>


	@la_access("Briefs", "create")
	<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Ajouter un Brief</h4>
				</div>
				{!! Form::open(['action' => 'LA\BriefsController@store', 'id' => 'brief-add-form','files' => true]) !!}
				<div class="modal-body">
					<div class="box-body">
						{{--@la_form($module)
                         @la_input($moduleBrief, 'userId')
                         @la_input($moduleBrief, 'project_id')
                         @la_input($moduleBrief, 'pj')
                            --}}


						@la_input($moduleBrief, 'libelle')
						<div class="form-group">
							<label for="taf">La Proposition :</label>
							<textarea class="form-control" placeholder="1.Sur quel produit/service/offre ce brief va-t-il concentrer?
2.Dans cette proposition qu'est-ce qui est nouveau? En quoi est-ce différent de nos concurrents?" cols="30" rows="3" name="proposition" aria-required="true"></textarea>
						</div>
						<div class="form-group">
							<label for="taf">La Cible :</label>
							<textarea class="form-control" placeholder="Enter La Cible" cols="30" rows="3" name="cible" aria-required="true"></textarea>
						</div>
						@la_input($moduleBrief, 'taf')
						@la_input($moduleBrief, 'dateDentree')
						@la_input($moduleBrief, 'dateDeSortie')
						<i class="fa fa-info" data-toggle="tooltip" data-placement="top" title="Si le brief concerne plus d'un département veuillez l'assigner au département Crea/Strat"></i>
						{{--@la_input($moduleBrief, 'dep')--}}
						<div class="form-group">
							<label for="dep">Departement* :</label>
							<select class="form-control select2-hidden-accessible" required="1" data-placeholder="Enter Departement" rel="select2" name="dep" tabindex="-1" aria-hidden="true">
								@foreach($alldepartments as $dep)
									<option value="{{$dep->id}}">{{$dep->name}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="">Assets attendus</label>
							<p>Selectionnez les assets attendus</p>
							@php $assetattendus = (new \App\Http\Controllers\AssetsvalidController())->typAssetForVue();@endphp
							@foreach($assetattendus as $assetattendu)
								<label class="checkbox-inline">
									<input type="checkbox" id="division_id" name="asset[]" value="{{$assetattendu['name']}}"> {{strtoupper($assetattendu['libelle'])}}
								</label>
							@endforeach
						</div>

						<div class="form-group">
							<label for="file"><strong>Pièces jointes</strong></label>
							<input type="file" id="file" name="file" class="dropify"/>
						</div>
						<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
						<input type="hidden" name="project_id" value="{{$projet->id}}">

					</div>
					<div class="col-md-12">
						<div id="loader" class="pull-right" style="display: none;">
							<img src="{{ URL::asset('la-assets/img/load.gif')}}">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="formaddassetcan" class="btn btn-default" data-dismiss="modal">FERMER</button>
					{!! Form::submit( 'AJOUTER', ['class'=>'btn btn-success','onClick'=>'submitDetailsForm()','id'=>'formaddasset']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>



	<div class="modal fade" id="AddModalBC" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Ajouter un bon de commande</h4>
				</div>
				{!! Form::open(['action' => 'LA\BcommandesController@store', 'id' => 'bcommande-add-form','files' => true]) !!}
				<div class="modal-body">
					<div class="box-body">

						<div class="form-group">
							<label for="libelle">Libelle* :</label>
							<input class="form-control" placeholder="Enter libelle" data-rule-maxlength="250" required="1" name="libelle" type="text" value="" aria-required="true">
						</div>

						<div class="form-group">
							<label for="file"><strong>Pièces jointes</strong></label>
							<input type="file" id="file" name="file" class="dropify"/>
						</div>
						<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
						<input type="hidden" name="project_id" value="{{$projet->id}}">

					</div>
					<div class="col-md-12">
						<div id="loaderbc" class="pull-right" style="display: none;">
							<img src="{{ URL::asset('la-assets/img/load.gif')}}">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="formaddbccan" class="btn btn-default" data-dismiss="modal">FERMER</button>
					{!! Form::submit( 'VALIDER', ['class'=>'btn btn-success','onClick'=>'submitBCForm()','id'=>'formaddbc']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div class="modal fade" id="AddModalDP" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Ajouter un devis ou proforma</h4>
				</div>
				{!! Form::open(['action' => 'LA\DevisController@store', 'id' => 'devi-add-form','files' => true]) !!}
				<div class="modal-body">
					<div class="box-body">

						<div class="form-group">
							<label for="libelle">Libelle* :</label>
							<input class="form-control" placeholder="Enter libelle" data-rule-maxlength="250" required="1" name="libelle" type="text" value="" aria-required="true">
						</div>

						<div class="form-group">
							<label for="file"><strong>Pièces jointes</strong></label>
							<input type="file" id="file" name="file" class="dropify"/>
						</div>
						<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
						<input type="hidden" name="project_id" value="{{$projet->id}}">

					</div>
					<div class="col-md-12">
						<div id="loaderdp" class="pull-right" style="display: none;">
							<img src="{{ URL::asset('la-assets/img/load.gif')}}">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="formadddpcan" class="btn btn-default" data-dismiss="modal">FERMER</button>
					{!! Form::submit( 'VALIDER', ['class'=>'btn btn-success','onClick'=>'submitDPForm()','id'=>'formadddp']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div class="modal fade" id="AddModalNV3" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Soumettre au niveau 3</h4>
				</div>
				{!! Form::open(['action' => 'LA\SoumissionsController@store', 'id' => 'soumission-add-form','files' => true]) !!}
				<div class="modal-body">
					<div class="box-body">

						<div class="form-group">
							<label for="libelle">Libelle* :</label>
							<input class="form-control" placeholder="Enter libelle" data-rule-maxlength="250" required="1" name="libelle" type="text" value="" aria-required="true">
						</div>

						<div class="form-group">
							<label for="file"><strong>Pièces jointes</strong></label>
							<input type="file" id="file" name="file" class="dropify"/>
						</div>
						<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
						<input type="hidden" name="project_id" value="{{$projet->id}}">

					</div>
				</div>
				<div class="modal-footer">
					{!! Form::submit( 'SOUMETTRE', ['class'=>'btn btn-success']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	@endla_access

	@la_access("Mails", "create")
	<div class="modal fade" id="AddModalMail" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Ajouter un mail client</h4>
				</div>
				{!! Form::open(['action' => 'LA\MailsController@store', 'id' => 'mail-add-form','files' => true]) !!}
				<div class="modal-body">
					<div class="box-body">
						{{--@la_form($module)@la_input($moduleMail, 'project_id')
                        @la_input($moduleMail, 'debrief_id')@la_input($moduleMail, 'user_id')@la_input($moduleMail, 'path')
                        @la_input($moduleMail, 'ext') --}}

                        @la_input($moduleMail, 'libelle')

						<div class="form-group">
							<label for="file"><strong>Pièces jointes</strong></label>
							<input type="file" id="file" name="file" class="dropify"/>
						</div>
						<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
						<input type="hidden" name="project_id" value="{{$projet->id}}">
						<input type="hidden" name="me" value="0">

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">FERMER</button>
					{!! Form::submit( 'ENREGISTRER', ['class'=>'btn btn-success']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	@endla_access

	@la_access("Assetops", "create")
	<div class="modal fade" id="AddModalOP" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Add Assetop</h4>
				</div>
				{!! Form::open(['action' => 'LA\AssetopsController@store', 'id' => 'assetop-add-form','files' => true]) !!}
				<div class="modal-body">
					<div class="box-body">
						{{--@la_form($module)
                        @la_input($moduleOP, 'project_id')
                        @la_input($moduleOP, 'user_id')@la_input($moduleOP, 'path')
                        @la_input($moduleOP, 'ext')
                        @la_input($moduleOP, 'status')--}}
                        @la_input($moduleOP, 'libelle')
                        @la_input($moduleOP, 'type')
						<div class="form-group">
							<label for="file"><strong>Pièces jointes</strong></label>
							<input type="file" id="file" name="file" class="dropify"/>
						</div>
						<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
						<input type="hidden" name="project_id" value="{{$projet->id}}">

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">FERME</button>
					{!! Form::submit( 'ENREGISTRER', ['class'=>'btn btn-success']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	@endla_access

	<div class="modal fade" id="versionModalRapport" role="dialog">
		<div class="modal-dialog modal-md">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Motif de la suppression</h4>
					</div>

					<div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
						<form class="form-horizontal" id="formTypeRapport"  method="post" action="{{url(config('laraadmin.adminRoute'). '/marketing/delete/devis')}}" autocomplete="off">
							<div class="form-group">
								{{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
								<div class="col-md-12">
									<textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
								</div>
							</div>
							<input type="hidden" id="idrapport" name="idrapport"/>
							<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
							<button type="submit" id="commitRejet" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
						</form>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('la-assets/plugins/dropify/dropify.css') }}">
<link rel="stylesheet" href="{{ asset('la-assets/plugins/lightbox/lightbox.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/lightbox/lightbox.js') }}"></script>
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>
<script>
    function displayModalRapport(id){
        $("#formTypeRapport")[0].reset();
        $("#idrapport").val(id);
    }

    function submitBCForm() {
        $("#formaddbc").prop('disabled', true);
        $("#formaddbccan").prop('disabled', true);
        $('#loaderbc').show();
        //alert("ici");
        $("#bcommande-add-form").submit();
    }

    function submitDPForm() {
        $("#formadddp").prop('disabled', true);
        $("#formadddpcan").prop('disabled', true);
        $('#loaderdp').show();
        //alert("ici");
        $("#devi-add-form").submit();
    }

    function submitDetailsForm() {
        $("#formaddasset").prop('disabled', true);
        $("#formaddassetcan").prop('disabled', true);
        $('#loader').show();
        //alert("ici");
        $("#brief-add-form").submit();
    }

    $('.dropify').dropify();
    $(function () {
        $("#example1").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });
        $("#exampleckeck").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });

        $("#brief-add-form").validate({

        });

        $("#example2").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Précédent"
                },
            }
        });
    });
</script>
@endpush
