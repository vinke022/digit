@extends("la.layouts.app")

@section("contentheader_title", "Job bag")
@section("contentheader_description", "Phase 1")

@section("headerElems")
	@la_access("Projets", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">
		Ajouter un Job bag
	</button>
	@endla_access
@endsection

@section("main-content")

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<div class="box box-success">
		<!--<div class="box-header"></div>-->
		<div class="box-body">
			<table id="example1" class="table table-bordered">
				<thead>
				<tr class="success">
					<th>Id</th>
					<th>Opération</th>
					<th>Annonceur</th>
					<th>Date d'entéee</th>
					<th>Action</th>
					@if(Auth::user()->post =='DSTRAT')
					<th>Passer en phase 2</th>
					@endif
				</tr>
				</thead>
				<tbody>
				@foreach($projects as $project)
					<tr>
						<td class="sorting_1">{{$project->slug}}</td>
						<td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$project->id)}}">{{$project->operation}}</a></td>
						<td>{{$project->annonceurProject->name}}</td>
						<td>{{$project->dateDentree}}</td>
						<td>
							@if(Module::hasAccess("Projets", "edit"))
								<a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$project->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
								{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.projets.destroy', $project->id], 'method' => 'delete', 'style'=>'display:inline']) }}
								<button class="btn btn-danger btn-xs" type="submit">
									<i class="fa fa-times"></i>
								</button>
								{{Form::close()}}
							@endif
						</td>
						@if(Auth::user()->post =='DSTRAT')
							<td>
								<button type="button" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-passphase{{$project->id}}">Passer en phase 2</button>
							</td>
						@endif
					</tr>

					<div class="modal modal-danger fade" id="modal-passphase{{$project->id}}">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title">Phase 2</h4>
								</div>
								<div class="modal-body">
									<p>Voulez-vous passer en phase 2 ?</p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Annuler</button>
									<a href="{{url(config('laraadmin.adminRoute') . '/projets/change/'.$project->id.'/phase')}}" class="btn btn-outline">Passer</a>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>

				@endforeach
				</tbody>
			</table>
		</div>
	</div>

	@la_access("Projets", "create")
	<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Ajouter un Job bag</h4>
				</div>
				{!! Form::open(['action' => 'LA\ProjetsController@store', 'id' => 'projet-add-form']) !!}
				<div class="modal-body">
					<div class="box-body">
						{{--@la_form($module)@la_input($module, 'userId')@la_input($module, 'dateDeSortie')--}}

						@la_input($module, 'operation')
						@la_input($module, 'annonceur')
						@la_input($module, 'dateDentree')

						<input type="hidden" name="userId" value="{{Auth::user()->id}}">

					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">FERMER</button>
					{!! Form::submit( 'ENREGISTRER', ['class'=>'btn btn-success']) !!}
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Précédent"
                },
            }
        });
		/*$("#projet-add-form").validate({

		 });*/
    });
</script>
@endpush
