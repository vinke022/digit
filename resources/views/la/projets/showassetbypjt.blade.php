@extends("la.layouts.app")

@push('styles')
<link rel="stylesheet" href="{{ asset('la-assets/plugins/dropify/dropify.css') }}">
<style>
    .imgasset {
        height: 101px!important;
        width: 100%;
    }
    .mailbox-attachment-info{
        height: 90px;
    }
</style>
@endpush

@section("contentheader_title", "Assets")
@section("contentheader_description", "")
{{--@section("section", "Assets")
@section("sub_section", "Listing")--}}
@section("htmlheader_title", "Assets")

@section("headerElems")

@endsection

@section("main-content")
    <div class="row float-right" style="margin-left: 0;margin-bottom: 4px;">
        <a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$idd.'#assets' )}}" class="btn btn-danger btn-lrg" title="Retour" style="padding: 3px 12px;">
            <i class="fa fa-backward"></i> Retour
        </a>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box box-success">
        <!--<div class="box-header"></div>-->
        <div class="box-body">
            <div class="box-footer">
                <ul class="mailbox-attachments clearfix">
                @if(count($asset_data)>0)
                    @foreach($asset_data as $asset )

                        @if(in_array($asset->ext, ['jpeg','jpg','gif','tiff','bmp','png']))
                            <li>
                                {{--<span class="mailbox-attachment-icon" style="max-height:150px;">--}}
                                    {{--<i class="fa fa-file-image-o"></i>--}}
                                {{--</span>--}}
                                <span class="mailbox-attachment-icon has-img" style="max-height:150px;">
                                    <a href="{{url('assetFiles/'.$asset->path)}}" data-lightbox="image-1" data-title="{{$asset->libelle}}">
                                        <img src="{{url('assetFiles/'.$asset->path)}}" alt="{{$asset->libelle}}" style="width: 100%">
                                    </a>
                                </span>
                                <div class="mailbox-attachment-info">
                                    <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                    <br><span class=""><i class=""></i>Type : {{$asset->asset_type}}</span>
                                    <span class="mailbox-attachment-size">
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" data-lightbox="image-1" data-title="{{$asset->libelle}}" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        <br>
                                        @if($asset->status=='0')
                                            <span class="label label-danger">Asset rejeté</span>
                                        @elseif($asset->status=='1')
                                            <span class="label label-warning">En attente de validation</span>
                                        @else
                                            <span class="label label-success">Asset validé</span>
                                        @endif
                                    </span>
                                </div>
                            </li>
                        @endif

                        @if(in_array($asset->ext, ['htm','html','xml']))
                            <li>
							<span class="mailbox-attachment-icon" style="max-height:150px;">
								<i class="fa fa-code"></i>
							</span>
                                <div class="mailbox-attachment-info">
                                    <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                    <br><span class=""><i class=""></i>Type : {{$asset->asset_type}}</span>
                                    <span class="mailbox-attachment-size">
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        <br>
                                        @if($asset->status=='0')
                                            <span class="label label-danger">Asset rejeté</span>
                                        @elseif($asset->status=='1')
                                            <span class="label label-warning">En attente de validation</span>
                                        @else
                                            <span class="label label-success">Asset validé</span>
                                        @endif
								</span>
                                </div>
                            </li>
                        @endif

                        @if(in_array($asset->ext, ['mp3','wma','aac','ogg']))
                            <li>
                                <span class="mailbox-attachment-icon" style="max-height:150px;">
                                    <i class="fa fa-file-audio-o"></i>
                                </span>
                                <div class="mailbox-attachment-info">
                                    <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                    <br><span class=""><i class=""></i>Type : {{$asset->asset_type}}</span>
                                    <span class="mailbox-attachment-size">
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        <br>
                                        @if($asset->status=='0')
                                            <span class="label label-danger">Asset rejeté</span>
                                        @elseif($asset->status=='1')
                                            <span class="label label-warning">En attente de validation</span>
                                        @else
                                            <span class="label label-success">Asset validé</span>
                                        @endif
								    </span>
                                </div>
                            </li>
                        @endif

                        @if(in_array($asset->ext, ['avi','mpeg','mov','mp4','flv']))
                            <li>
                                <span class="mailbox-attachment-icon" style="max-height:150px;">
                                    <i class="fa fa-file-video-o"></i>
                                </span>
                                <div class="mailbox-attachment-info">
                                    <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                    <br><span class=""><i class=""></i>Type : {{$asset->asset_type}}</span>
                                    <span class="mailbox-attachment-size">
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        <br>
                                        @if($asset->status=='0')
                                            <span class="label label-danger">Asset rejeté</span>
                                        @elseif($asset->status=='1')
                                            <span class="label label-warning">En attente de validation</span>
                                        @else
                                            <span class="label label-success">Asset validé</span>
                                        @endif
								    </span>
                                </div>
                            </li>
                        @endif

                        @if(in_array($asset->ext, ['rar','zip','tar','EndNote']))
                            <li>
                                <span class="mailbox-attachment-icon" style="max-height:150px;">
                                    <i class="fa fa-file-archive-o"></i>
                                </span>
                                <div class="mailbox-attachment-info">
                                    <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                    <br><span class=""><i class=""></i>Type : {{$asset->asset_type}}</span>
                                    <span class="mailbox-attachment-size">
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        <br>
                                        @if($asset->status=='0')
                                            <span class="label label-danger">Asset rejeté</span>
                                        @elseif($asset->status=='1')
                                            <span class="label label-warning">En attente de validation</span>
                                        @else
                                            <span class="label label-success">Asset validé</span>
                                        @endif
								    </span>
                                </div>
                            </li>
                        @endif

                        @if(in_array($asset->ext, ['pdf']))
                            <li>
                                <span class="mailbox-attachment-icon" style="max-height:150px;">
                                    <i class="fa fa-file-pdf-o"></i>
                                </span>
                                <div class="mailbox-attachment-info">
                                    <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                    <br><span class=""><i class=""></i>Type : {{$asset->asset_type}}</span>
                                    <span class="mailbox-attachment-size">
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        <br>
                                        @if($asset->status=='0')
                                            <span class="label label-danger">Asset rejeté</span>
                                        @elseif($asset->status=='1')
                                            <span class="label label-warning">En attente de validation</span>
                                        @else
                                            <span class="label label-success">Asset validé</span>
                                        @endif
							        </span>
                                </div>
                            </li>
                        @endif

                        @if(in_array($asset->ext, ['ppt','pptx']))
                            <li>
                                <span class="mailbox-attachment-icon" style="max-height:150px;">
                                    <i class="fa fa-file-powerpoint-o"></i>
                                </span>
                                <div class="mailbox-attachment-info">
                                    <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                    <br><span class=""><i class=""></i>Type : {{$asset->asset_type}}</span>
                                    <span class="mailbox-attachment-size">
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        <br>
                                        @if($asset->status=='0')
                                            <span class="label label-danger">Asset rejeté</span>
                                        @elseif($asset->status=='1')
                                            <span class="label label-warning">En attente de validation</span>
                                        @else
                                            <span class="label label-success">Asset validé</span>
                                        @endif
						            </span>
                                </div>
                            </li>
                        @endif

                        @if(in_array($asset->ext, ['xls','xlsx','csv']))
                            <li>
                                <span class="mailbox-attachment-icon" style="max-height:150px;">
                                    <i class="fa fa-file-excel-o"></i>
                                </span>
                                <div class="mailbox-attachment-info">
                                    <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                    <br><span class=""><i class=""></i>Type : {{$asset->asset_type}}</span>
                                    <span class="mailbox-attachment-size">
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" target="_blank" style="margin-left: 25%"><i class="fa fa-eye"></i> Voir</a>
                                        <br>
                                        @if($asset->status=='0')
                                            <span class="label label-danger">Asset rejeté</span>
                                        @elseif($asset->status=='1')
                                            <span class="label label-warning">En attente de validation</span>
                                        @else
                                            <span class="label label-success">Asset validé</span>
                                        @endif

                                    </span>
                                </div>
                            </li>
                        @endif

                    @endforeach
                @else
                    <p>Aucun assets pour le moment</p>
                @endif
                </ul>
            </div>

        </div>

    </div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('la-assets/plugins/lightbox/lightbox.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/lightbox/lightbox.js') }}"></script>
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>
<script>
    $('.dropify').dropify();
    /*$(function () {
        $("#example1").DataTable({
            processing: true,
            serverSide: true,
            ajax: "",
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Search"
            },
            @if($show_actions)
            columnDefs: [ { orderable: false, targets: [-1] }],
            @endif
        });
        $("#asset-add-form").validate({

        });
    });*/
</script>
@endpush
