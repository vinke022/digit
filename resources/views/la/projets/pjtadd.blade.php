@extends("la.layouts.app")

@section("contentheader_title", "Ajouter un Job bag")
@section("contentheader_description", "")

@section("headerElems")

@endsection

@section("main-content")

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box">
        <div class="box-header">

        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                    {!! Form::open(['action' => 'LA\ProjetsController@store', 'id' => 'projet-add-form']) !!}
                    <div class="modal-body">
                        <div class="box-body">
                            {{--@la_form($module)@la_input($module, 'userId')@la_input($module, 'dateDeSortie')--}}

                            @la_input($module, 'operation')
                            {{--@la_input($module, 'annonceur')--}}
                            <div class="form-group">
                                <label for="annonceur">Annonceur* :</label>
                                <select class="form-control select2-hidden-accessible" required="1" data-placeholder="Enter Annonceur" rel="select2" name="annonceur" tabindex="-1" aria-hidden="true">
                                    @foreach($annoncs as $annoncs)
                                        <option value="{{$annoncs->id}}">{{$annoncs->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @la_input($module, 'dateDentree')

                            <input type="hidden" name="userId" value="{{Auth::user()->id}}">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">FERMER</button>
                        {!! Form::submit( 'ENREGISTRER', ['class'=>'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Précédent"
                },
            }
        });
        /*$("#projet-add-form").validate({

         });*/
    });
</script>
@endpush
