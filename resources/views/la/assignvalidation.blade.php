@extends('la.layouts.app')

@section('htmlheader_title') Validation  @endsection
@section('contentheader_title') Validation @endsection

@section('main-content')
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;padding: 3px 0px">
              <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">
                Ajouter une validation
              </button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered">
              <thead>
              <tr>
                <th>Ref No</th>
                <th>Type d'asset</th>
                <th>Libelle type asset</th>
                <th>Nombre de validateur</th>
                <th>Validateur</th>
                <th>Action</th>
              </tr>
              </thead>
              <tbody>
                  @foreach($assetvalidateur as $k => $asset)
                      <tr role="row" class="odd">
                          <td class="sorting_1">{{$k+1}}</td>
                          <td>
                              <a href="#">
                                  {{$asset->assetype}}
                              </a>
                          </td>
                          <td>{{(new \App\Http\Controllers\LA\DashboardController())->nameLibelle($asset->assetype)}}</td>
                          <td>{{$asset->nombre}}</td>
                          <td>
                            @php $arrayvalid = json_decode($asset->Post); @endphp
                            @foreach($arrayvalid as $valid)
                                {{$valid}} -
                            @endforeach
                          </td>
                          <td>
                              <a href="{{url(config('laraadmin.adminRoute') . '/assetvalidations/'.$asset->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
                              {!! Form::open(['route' => [config('laraadmin.adminRoute') . '.assetvalidations.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) !!}
                              <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></button>
                              {!! Form::close() !!}
                          </td>
                      </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->

        </div>
      </div>
    </div>

    <div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Ajouter une validation</h4>
          </div>
            {!! Form::open(['action' => 'LA\AssetvalidationsController@store', 'id' => 'assetvalidation-add-form']) !!}
          <div class="modal-body">
            <div class="box-body">

              <div class="form-group">
                <label for="libelle">Type d'asset* :</label>
                <select class="form-control" required="" id="asset_type" name="asset_type" tabindex="-1" aria-hidden="true" aria-required="true">
                  @foreach($assetypes as $asset)
                    <option value="{{$asset->name}}">{{$asset->libelle}}</option>
                  @endforeach
                </select>
              </div>

              <div class="form-group">
                <label for="libelle">Nombre de validateur* :</label>
                <input class="form-control" placeholder="Enter nombre" data-rule-maxlength="4" required="1" name="nombre" type="number" value="1" aria-required="true">
              </div>

              <div class="form-group">
                <label for="libelle">Validateur * :</label>
                {{--<select class="form-control js-example-basic-multiple" required="" id="valideur" name="valideur[]" tabindex="-1" multiple>
                  @foreach($posts as $post)
                    <option value="{{$post->post}}">{{$post->post}}</option>
                  @endforeach
                </select>--}}
                  <select class="form-control" required="" id="valideur" name="valideur[]" multiple data-role="tagsinput">
                      {{--@foreach($posts as $post)
                          <option value="{{$post->post}}">{{$post->post}}</option>
                      @endforeach--}}
                  </select>
              </div>

              <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

            </div>
            <div class="col-md-12">
              <div id="loaderdp" class="pull-right" style="display: none;">
                <img src="{{ URL::asset('la-assets/img/load.gif')}}">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" id="formadddpcan" class="btn btn-default" data-dismiss="modal">FERMER</button>
            {!! Form::submit( 'VALIDER', ['class'=>'btn btn-success']) !!}
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>

  </section>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('la-assets/plugins/chzn/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('la-assets/plugins/chzn/bootstrap-tagsinput.css')}}">
<style>
    .bootstrap-tagsinput {
        display: block;
    }
</style>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/lightbox/lightbox.js') }}"></script>
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('la-assets/plugins/chzn/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{ asset('la-assets/plugins/chzn/select2.full.min.js')}}"></script>

<script>
    $(document).ready(function () {
      $('.js-example-basic-multiple').select2({

      });
    });


    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example2").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

</script>
@endpush