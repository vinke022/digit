@extends("la.layouts.app")

@section("contentheader_title", "Profil")
@section("htmlheader_title", "Profil")

@section("headerElems")

@endsection

@section("main-content")

    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" alt="User profile picture">

                    <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>

                    <p class="text-muted text-center">{{Auth::user()->email}}</p>

                    {{--<ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Followers</b> <a class="pull-right">1,322</a>
                        </li>
                        <li class="list-group-item">
                            <b>Following</b> <a class="pull-right">543</a>
                        </li>
                        <li class="list-group-item">
                            <b>Friends</b> <a class="pull-right">13,287</a>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>--}}
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profiledit" data-toggle="tab" aria-expanded="false">Modifier mon profil</a></li>
                    <li class=""><a href="#passedit" data-toggle="tab" aria-expanded="false">Changer de mot de passe</a></li>
                    {{--<li class=""><a href="#avatar" data-toggle="tab" aria-expanded="true">Mon avatar</a></li>--}}
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="profiledit">
                        <form class="form-horizontal" method="post" action="{{url(config('laraadmin.adminRoute') . '/profil/update/info')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="nom" class="col-sm-2 control-label">Nom</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nom" value="{{Auth::user()->name}}" name="nom">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger">ENREGISTRER</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="passedit">
                        <!-- The timeline -->
                        <form class="form-horizontal" method="post" action="{{url(config('laraadmin.adminRoute') . '/profil/update/pass')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="motpass" class="col-sm-3 control-label">Mot de passe actuel</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="motpass" id="motpass" placeholder="Mot de passe actuel">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="newpass" class="col-sm-3 control-label">Nouveau mot de passe</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="newpass" id="newpass" placeholder="Nouveau mot de passe">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="confirmpass" class="col-sm-3 control-label">Confirmer mot de passe</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="confirmpass" id="confirmpass" placeholder="Confirmer mot de passe">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-9">
                                    <button type="submit" class="btn btn-danger">ENREGISTRER</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="avatar">
                        <form class="form-horizontal" enctype="multipart/form-data" action="" method="post">
                            {{ csrf_field() }}
                            <div class="form-group col-sm-12">
                                <input type="file" id="file" name="fileUser" class="dropify" required/>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-danger">ENREGISTRER</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/dropify/dropify.css') }}">
@endpush


@push('scripts')
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
    $('.dropify').dropify();
</script>
@endpush
