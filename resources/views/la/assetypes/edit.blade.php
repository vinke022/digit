@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/assetypes') }}">Assetype</a> :
@endsection
@section("contentheader_description", $assetype->$view_col)
@section("section", "Assetypes")
@section("section_url", url(config('laraadmin.adminRoute') . '/assetypes'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Assetypes Edit : ".$assetype->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($assetype, ['route' => [config('laraadmin.adminRoute') . '.assetypes.update', $assetype->id ], 'method'=>'PUT', 'id' => 'assetype-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'libelle')
					@la_input($module, 'name')
					@la_input($module, 'status')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/assetypes') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#assetype-edit-form").validate({
		
	});
});
</script>
@endpush
