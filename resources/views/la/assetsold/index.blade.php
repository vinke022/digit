@extends("la.layouts.app")

@push('styles')
<link href="{{ asset('la-assets/css/dropzone.css') }}" type="text/css" rel="stylesheet" />
@endpush

@section("contentheader_title", "Assets")
@section("contentheader_description", "Assets liste")
@section("section", "Assets")
@section("sub_section", "Liste")
@section("htmlheader_title", "Assets liste")

@section("headerElems")
    <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Ajouter</button>
@endsection

@section("main-content")

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Ajouter un asset</h4>
                </div>
                <form action="" method="post" class="dropzone" id="my-awesome-dropzone">
                    {{csrf_field()}}
                    <div class="fallback">
                        <input type="file" name="file" enctype="multipart/form-data"/>
                    </div>
                    <input type="hidden" name="idbrief" value="{{$assets[0]->brief_id}}">
                    <input type="hidden" name="type" value="{{$assets[0]->type}}">
                </form>
            </div>
        </div>
    </div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>

</script>
@endpush
