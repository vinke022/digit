@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/checklists') }}">Checklist</a> :
@endsection
@section("contentheader_description", $checklist->$view_col)
@section("section", "Checklists")
@section("section_url", url(config('laraadmin.adminRoute') . '/checklists'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Checklists Edit : ".$checklist->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($checklist, ['route' => [config('laraadmin.adminRoute') . '.checklists.update', $checklist->id ], 'method'=>'PUT', 'id' => 'checklist-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'libelle')
					@la_input($module, 'departement')
					@la_input($module, 'niveau')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/checklists') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#checklist-edit-form").validate({
		
	});
});
</script>
@endpush
