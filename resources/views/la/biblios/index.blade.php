@extends("la.layouts.app")

@section("contentheader_title", "Biblios")
@section("contentheader_description", "Biblios listing")
@section("section", "Biblios")
@section("sub_section", "Listing")
@section("htmlheader_title", "Biblios Listing")

@section("headerElems")
@la_access("Biblios", "create")
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Add Biblio</button>
@endla_access
@endsection

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>
			
		</tbody>
		</table>
	</div>
</div>

@la_access("Biblios", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Add Biblio</h4>
			</div>
			{!! Form::open(['action' => 'LA\BibliosController@store', 'id' => 'biblio-add-form','files' => true]) !!}
			<div class="modal-body">
				<div class="box-body">
					{{--@la_form($module)@la_input($module, 'path')
                    @la_input($module, 'ext')--}}

                    @la_input($module, 'libelle')
                    @la_input($module, 'type')

					<div class="form-group">
						<label for="file"><strong>Pièces jointes</strong></label>
						<input type="file" id="file" name="file" class="dropify"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				{!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('la-assets/plugins/dropify/dropify.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>
<script>
    $('.dropify').dropify();
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/biblio_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#biblio-add-form").validate({
		
	});
});
</script>
@endpush
