@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/biblios') }}">Biblio</a> :
@endsection
@section("contentheader_description", $biblio->$view_col)
@section("section", "Biblios")
@section("section_url", url(config('laraadmin.adminRoute') . '/biblios'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Biblios Edit : ".$biblio->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($biblio, ['route' => [config('laraadmin.adminRoute') . '.biblios.update', $biblio->id ], 'method'=>'PUT', 'id' => 'biblio-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'libelle')
					@la_input($module, 'type')
					@la_input($module, 'path')
					@la_input($module, 'ext')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/biblios') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#biblio-edit-form").validate({
		
	});
});
</script>
@endpush
