@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/devis') }}">Devi</a> :
@endsection
@section("contentheader_description", $devi->$view_col)
@section("section", "Devis")
@section("section_url", url(config('laraadmin.adminRoute') . '/devis'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Devis Edit : ".$devi->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($devi, ['route' => [config('laraadmin.adminRoute') . '.devis.update', $devi->id ], 'method'=>'PUT', 'id' => 'devi-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'libelle')
					@la_input($module, 'path')
					@la_input($module, 'project_id')
					@la_input($module, 'user_id')
					@la_input($module, 'ext')
					@la_input($module, 'status')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/devis') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#devi-edit-form").validate({
		
	});
});
</script>
@endpush
