@extends("la.layouts.app")

@section("contentheader_title")
	<a href="#">Brief</a> :
@endsection
@section("contentheader_description", $brief->$view_col)
@section("htmlheader_title", "Modifié un briefs  : ".$brief->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif



<div id="page-content" class="">
	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-edit"></i> Mise a jour</a></li>
		{{--<li class=""><a role="tab" data-toggle="tab" href="#tab-timeline" data-target="#tab-timeline"><i class="fa fa-file"></i> Mise a jour PJ</a></li>--}}
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active fade in p20 bg-white" id="tab-info">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
						{!! Form::model($brief, ['route' => [config('laraadmin.adminRoute') . '.briefs.update', $brief->id ], 'method'=>'PUT', 'id' => 'brief-edit-form']) !!}
						{{--@la_form($module)
                        @la_input($module, 'project_id')
                            @la_input($module, 'userId')
                            --}}

						@la_input($module, 'libelle')
					<div class="form-group">
						<label for="taf">La Proposition :</label>
						<textarea class="form-control" placeholder="1.Sur quel produit/service/offre ce brief va-t-il concentrer?
2.Dans cette proposition qu'est-ce qui est nouveau? En quoi est-ce différent de nos concurrents?" cols="30" rows="3" name="proposition" aria-required="true">{{$brief->proposition}}</textarea>
					</div>
					<div class="form-group">
						<label for="taf">La Cible :</label>
						<textarea class="form-control" placeholder="Enter La Cible" cols="30" rows="3" name="cible" aria-required="true">{{$brief->cible}}</textarea>
					</div>
					@la_input($module, 'taf')
						@la_input($module, 'dateDentree')
						@la_input($module, 'dateDeSortie')
						{{--@la_input($module, 'pj')--}}
						@la_input($module, 'dep')

						<div class="form-group">
							<label for="">Assets attendus</label>
							<p>Selectionnez les assets attendus</p>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="reco" {{ stristr($brief->asset, 'reco') == true ? 'checked' : '' }}> Reco
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="crea" {{ stristr($brief->asset, 'crea') == true ? 'checked' : '' }}> Créa
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="flowplane" {{ stristr($brief->asset, 'flowplane') == true ? 'checked' : '' }}> Flow plan
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="planmedia" {{ stristr($brief->asset, 'planmedia') == true ? 'checked' : '' }}> Plan media
							</label>
							{{--<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="digital" {{ stristr($brief->asset, 'digital') == true ? 'checked' : '' }}> Digital
							</label>--}}
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="fichefabric" {{ stristr($debrief->asset, 'fichefabric') == true ? 'checked' : '' }}> Fiche de fabrication
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="retroplanning" {{ stristr($debrief->asset, 'retroplanning') == true ? 'checked' : '' }}> Retroplanning
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="bcp" {{ stristr($debrief->asset, 'bcp') == true ? 'checked' : '' }}> Bon de commande prestataire
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="livrable" {{ stristr($debrief->asset, 'livrable') == true ? 'checked' : '' }}> Livrable
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="rapport" {{ stristr($debrief->asset, 'rapport') == true ? 'checked' : '' }}> Rapport
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="production" {{ stristr($debrief->asset, 'production') == true ? 'checked' : '' }}> Production
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="planpubli" {{ stristr($debrief->asset, 'planpubli') == true ? 'checked' : '' }}> Planning de publication
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="gglads" {{ stristr($debrief->asset, 'gglads') == true ? 'checked' : '' }}> Visuel Google Ads
							</label>
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="prototype" {{ stristr($debrief->asset, 'prototype') == true ? 'checked' : '' }}> Prototype & Rendu
							</label>
						</div>

						<input type="hidden" name="status" value="1">
						<input type="hidden" name="project_id" value="{{$brief->project_id}}">
						<br>
						<div class="form-group">
							{!! Form::submit( 'MISE A JOUR', ['class'=>'btn btn-success']) !!}
							<a class="btn btn-default pull-right" href="{{ url(config('laraadmin.adminRoute').'/projets/'.$brief->project_id) }}">ANNULER</a>
						</div>
						{!! Form::close() !!}
					</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="tab-timeline">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					{!! Form::open(['action' => 'LA\BriefsController@pj', 'id' => 'brief-edit-formpj','method'=>'POST','files' => true]) !!}
					{{--{!! Form::model($brief, ['route' => [config('laraadmin.adminRoute') . '.briefs.updatepj'], 'method'=>'POST', 'id' => 'brief-edit-form','files'=>true]) !!}--}}
					<div class="form-group">
						<label for="file"><strong>Pièce jointe</strong></label>
						<input type="file" id="file" name="pj" class="dropify" data-default-file="{{url('briefFiles/'.$brief->pj.'')}}"/>
					</div>

					<input type="hidden" name="status" value="1">
					<input type="hidden" name="project_id" value="{{$brief->project_id}}">
					<input type="hidden" name="brief_id" value="{{$brief->id}}">
					<br>
					<div class="form-group">
						{!! Form::submit( 'MISE A JOUR', ['class'=>'btn btn-success']) !!}
						<a class="btn btn-default pull-right" href="{{ url(config('laraadmin.adminRoute').'/projets/'.$brief->project_id) }}">ANNULER</a>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('la-assets/plugins/dropify/dropify.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>
<script>
$('.dropify').dropify();
$(function () {
	$("#brief-edit-form").validate({
		
	});
});
</script>
@endpush
