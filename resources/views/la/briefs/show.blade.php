@extends('la.layouts.app')

@push('styles')
	<link href="{{ asset('la-assets/css/dropzone.css') }}" type="text/css" rel="stylesheet" />

	<style type="text/css">
		.inner p {
		    font-size: 24px;
		    font-weight: bold;
		}
	</style>
@endpush

@section('htmlheader_title')
	Brief View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<div class="nav nav-tabs profile clearfix">
		<div class="col-md-1">
			<div class="row">
				<div class="col-md-12">
					<div class="profile-icon text-primary"><i class="fa fa-bars"></i></div>
				</div>
			</div>
		</div>
		<div class="col-md-10">
			<h4 class="name">{{ $brief->$view_col }}</h4>
		</div>
		<div class="col-md-1 actions">
			{{--@la_access("Briefs", "edit")
				<a href="{{ url(config('laraadmin.adminRoute') . '/briefs/'.$brief->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br>
			@endla_access
			
			@la_access("Briefs", "delete")
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.briefs.destroy', $brief->id], 'method' => 'delete', 'style'=>'display:inline']) }}
					<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
				{{ Form::close() }}
			@endla_access--}}
		</div>
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/projets/'.$project_id) }}" data-toggle="tooltip" data-placement="right" title="Cliquer pour revenir en arrière"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class="active" href="#debrief" data-target="#debrief"><i class="fa fa-edit"></i> Débrief</a></li>
		{{--<li class=""><a role="tab" data-toggle="tab" class=""  href="#see-asset" data-target="#see-asset"><i class="fa fa-folder"></i> Assets</a></li>--}}
		<li class=""><a role="tab" data-toggle="tab" href="#tab-general-info" data-target="#tab-info"><i class="fa fa-bars"></i>Information du debrief</a></li>
		{{--@if(Auth::user()->role_id == 5)
			<li class=""><a role="tab" data-toggle="tab" href="#tache" data-target="#tache"><i class="fa fa-tasks"></i>Assigner des tâches</a></li>
		@endif--}}
	</ul>

	<div class="tab-content">

		<div role="tabpanel" class="tab-pane active fade in p20 bg-white" id="debrief">
			<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
				@if(Auth::user()->role_id==1)
					<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModalDEBRIEF">
						Ajouter un Debrief
					</button>
				@else
					@if($debriefsCount<=3 and $nvproject->niveau!='1')
						@la_access("Debriefs", "create")
						<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModalDEBRIEF">
							Ajouter un Debrief
						</button>
						@endla_access
					@endif
					@if($debriefsCount<=1 and $nvproject->niveau=='1')
						@la_access("Debriefs", "create")
						<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModalDEBRIEF">
							Ajouter un Debrief
						</button>
						@endla_access
					@endif
				@endif

			</div>
			<div class="box box-success">
				<div class="box-body">
					<table id="example1" class="table table-bordered">
						<thead>
						<tr class="success">
							<td>Id</td>
							<td>Libelle</td>
							<td>Proposition</td>
							<td>Cible</td>
							<td>Travail attendu</td>
							<td>Date d'entrée</td>
							<td>Date de sortie</td>
							{{--<td>Brief</td>--}}
							<td>Status</td>
							<td>Actions</td>
						</tr>
						</thead>
						<tbody>
						@foreach($debriefs as $k => $brief)
							<tr role="row" class="odd">
								<td class="sorting_1">{{$k+1}}</td>
								<td>
									<a href="{{url(config('laraadmin.adminRoute') . '/debriefs/'.$brief->id)}}">
										{{$brief->libelle}}
									</a>
								</td>
								<td>
									@php $tab= explode(' ', strip_tags($brief->proposition), 16);unset($tab[15]); @endphp
									{{implode(' ', $tab).'...'}}
									{{--{!! nl2br($brief->proposition) !!}--}}
								</td>
								<td>
									@php $tab= explode(' ', strip_tags($brief->cible), 16);unset($tab[15]); @endphp
									{{implode(' ', $tab).'...'}}
									{{--{!! nl2br($brief->cible) !!}--}}
								</td>
								<td>
									@php $tab= explode(' ', strip_tags($brief->taf), 16);unset($tab[15]); @endphp
									{{implode(' ', $tab).'...'}}
									{{--{!! nl2br($brief->taf) !!}--}}
								</td>
								<td>{{$brief->dateDentree}}</td>
								<td>{{$brief->dateDeSortie}}</td>
								{{--<td>{{$brief->brief->libelle}}</td>--}}
								<td>
									@if($brief->status=='0')
										<span class="label label-danger">Rejeter</span>
									@elseif($brief->status=='1' or $brief->status=='3')
										<span class="label label-warning">En attente</span>
									@else
										<span class="label label-success">Valider</span>
									@endif
								</td>
								<td>
									@if(Module::hasAccess("Debriefs", "edit"))
										<a href="{{url(config('laraadmin.adminRoute') . '/debriefs/'.$brief->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
										{{--{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.debriefs.destroy', $brief->id], 'method' => 'delete', 'style'=>'display:inline']) }}--}}
										{{--<button class="btn btn-danger btn-xs" type="submit">--}}
											{{--<i class="fa fa-times"></i>--}}
										{{--</button>--}}
										{{--{{Form::close()}}--}}
									@endif
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Information Générale</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'libelle')
						<div class="form-group">
							<label for="libelle" class="col-md-2">Proposition :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($brief->proposition) !!}
							</div>
						</div>

						<div class="form-group">
							<label for="libelle" class="col-md-2">Cible :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($brief->cible) !!}
							</div>
						</div>

						<div class="form-group">
							<label for="libelle" class="col-md-2">Travail attendu :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($brief->taf) !!}
							</div>
						</div>
						@la_display($module, 'dateDentree')
						@la_display($module, 'dateDeSortie')

						{{--<div class="form-group">--}}
							{{--<label for="dateDeSortie" class="col-md-2">Assets attendus :</label>--}}
							{{--<div class="col-md-10 fvalue">--}}
								{{--@php $assets = explode( ',', $brief->asset ) @endphp--}}
								{{--@if(count($assets))--}}
									{{--@foreach($assets as $asset)--}}
										{{--<a href="#" role="button">--}}
											{{--<span class="label label-primary">{{$asset}}</span></a>--}}
									{{--@endforeach--}}
								{{--@endif--}}
							{{--</div>--}}
						{{--</div>--}}


						<div class="form-group">
							<label for="dateDeSortie" class="col-md-2">Statut :</label>
							<div class="col-md-10 fvalue">
								@if($brief->status=='0')
									<span class="label label-danger">Rejeter</span>
								@elseif($brief->status=='1' or $brief->status=='3')
									<span class="label label-warning">En attente</span>
								@else
									<span class="label label-success">Valider</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="libelle" class="col-md-2">Departement :</label>
							<div class="col-md-10 fvalue">
								@if($brief->dep=='1')
									Administration
								@elseif($brief->dep=='2')
									Service MEDIA
								@elseif($brief->dep=='3')
									Service Crea/Strat
								@elseif($brief->dep=='5')
									Service Digital
								@elseif($brief->dep=='6')
									Service Stratégie
								@elseif($brief->dep=='7')
									Service Commercial
								@elseif($brief->dep=='8')
									Service Opérationnel
								@else
									Service Trafic
								@endif
							</div>
						</div>

						@if($brief->pj)
						<div class="form-group">
							<label for="piece" class="col-md-2">Pièces jointes :</label>
							<div class="col-md-10 fvalue">
								<a href="{{url('briefFiles/'.$brief->pj)}}" download="{{$brief->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
							</div>
						</div>
						@endif
						{{--@la_display($module, 'taf')
						@la_display($module, 'pj')
						@la_display($module, 'dep')--}}
						{{--@la_display($module, 'project_id')
						@la_display($module, 'userId')
						<div class="form-group">
							<label for="dateDeSortie" class="col-md-2">Statut :</label>
							<div class="col-md-10 fvalue">{{$brief->status}}</div>
						</div>--}}
					</div>
				</div>
			</div>
		</div>

		{{--<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="see-asset">
			<div class="row">
	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-aqua">
	                <div class="inner">
	                  <h3>{{$assetDevis}}</h3>
	                  <p>Devis</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-file"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/devis')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->

	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-maroon">
	                <div class="inner">
	                  <h3>{{$assetReco}}</h3>
	                  <p>Reco</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-comment-o"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/reco')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->

				<div class="col-lg-3 col-xs-6">
					<!-- small box -->
					<div class="small-box bg-green">
						<div class="inner">
							<h3>{{$assetCrea}}</h3>
							<p>Créa</p>
						</div>
						<div class="icon">
							<i class="fa fa-lightbulb-o"></i>
						</div>
						<a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/crea')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
					</div>
				</div><!-- ./col -->

	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-yellow">
	                <div class="inner">
	                  <h3>{{$assetFPlane}}</h3>
	                  <p>Flow plan</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-file-excel-o"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/flowplane')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->
	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-red">
	                <div class="inner">
	                  <h3>{{$assetMedia}}</h3>
	                  <p>Plan Media</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-television"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/planmedia')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->

	            <div class="col-lg-3 col-xs-6">
	              <!-- small box -->
	              <div class="small-box bg-navy">
	                <div class="inner">
	                  <h3>{{$assetDigit}}</h3>
	                  <p>Digital</p>
	                </div>
	                <div class="icon">
	                  <i class="fa fa-star"></i>
	                </div>
	                <a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$brief->id.'/digital')}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
	              </div>
	            </div><!-- ./col -->
          	</div><!-- /.row -->

		</div>--}}

		{{--<div role="tabpanel" class="tab-pane fade in" id="tache">
			<div class="col-md-12">
				<div class="tab-content">
					<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
						@la_access("Taches", "create")
						<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">
							Ajouter une tâche
						</button>
						@endla_access
						<span class="taf">
							<h5 style="color: #bc0060;">TRAVAIL A FAIRE :</h5>
							<p>{!! nl2br($brief->taf) !!}</p>
						</span>
					</div>

					<div class="box box-success">
						<div class="box-body">
							<table id="example1" class="table table-bordered">
								<thead>
								<tr class="success">
									@foreach( $listing_cols_tache as $col )
										<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
									@endforeach
									@if($show_actions)
										<th>Actions</th>
									@endif
								</tr>
								</thead>
								<tbody>
								@foreach($taches as $k => $tache)
									<tr role="row" class="odd">
										<td class="sorting_1">{{$k+1}}</td>
										<td>
											{{$tache->libelle}}
											--}}{{--<a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id)}}"> </a>--}}{{--
										</td>
										<td>{{$tache->description}}</td>
										<td>{{$tache->userTache->name}}</td>
										<td>
											@if($tache->status=='0')
												<span class="label label-danger">En attente</span>
											@elseif($tache->status=='1')
												<span class="label label-warning">En cours</span>
											@else
												<span class="label label-success">Terminer</span>
											@endif
										</td>
										<td>
											<a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
											{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.taches.destroy', $tache->id], 'method' => 'delete', 'style'=>'display:inline']) }}
											<button class="btn btn-danger btn-xs" type="submit">
												<i class="fa fa-times"></i>
											</button>
											{{Form::close()}}
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>


				</div>
			</div>
		</div>--}}
	</div>
	</div>
	</div>


{{--@la_access("Taches", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Ajouter une tâche</h4>
			</div>
			{!! Form::open(['action' => 'LA\TachesController@store', 'id' => 'tach-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					--}}{{--@la_form($moduleTache)
					@la_input($module, 'status')
					@la_input($module, 'assignant_id')
					@la_input($moduleTache, 'brief_id') @la_input($moduleTache, 'assigne_id')
					--}}{{--

					@la_input($moduleTache, 'libelle')
					@la_input($moduleTache, 'description')

					<div class="form-group">
						<label for="assigne_id">Assigner</label>
						<select name="assigne_id" id="assigne_id" class="form-control">
							@foreach($usersDepartmts as $user)
							<option value="{{$user->id}}">{{$user->name}}</option>
							@endforeach
						</select>
					</div>

					<input type="hidden" name="brief_id" value="{{$brief->id}}">
					<input type="hidden" name="assignant_id" value="{{Auth::user()->id}}">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">FERME</button>
				{!! Form::submit( 'ENREGISTRER', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access--}}

<div class="modal fade" id="AddModalDEBRIEF" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Ajouter un Debrief</h4>
			</div>
			{!! Form::open(['action' => 'LA\DebriefsController@store', 'id' => 'debrief-add-form','files' => true]) !!}
			<div class="modal-body">
				<div class="box-body">
					{{--@la_form($module)@la_input($moduleDebrief, 'path')@la_input($moduleDebrief, 'project_id')@la_input($moduleDebrief, 'user_id')
					@la_input($moduleDebrief, 'brief_id')
					@la_input($moduleDebrief, 'status')
					@la_input($moduleDebrief, 'asset')--}}

					@la_input($moduleDebrief, 'libelle')
					<div class="form-group">
						<label for="taf">La Proposition :</label>
						<textarea class="form-control" placeholder="1.Sur quel produit/service/offre ce brief va-t-il concentrer?
2.Dans cette proposition qu'est-ce qui est nouveau? En quoi est-ce différent de nos concurrents?" cols="30" rows="3" name="proposition" aria-required="true"></textarea>
					</div>
					<div class="form-group">
						<label for="taf">La Cible :</label>
						<textarea class="form-control" placeholder="Enter La Cible" cols="30" rows="3" name="cible" aria-required="true"></textarea>
					</div>
					@la_input($moduleDebrief, 'taf')
					@la_input($moduleDebrief, 'dateDentree')
					@la_input($moduleDebrief, 'dateDeSortie')
					<i class="fa fa-info" data-toggle="tooltip" data-placement="top" title="Si le debrief concerne plus d'un département veuillez l'assigner au département Crea/Strat"></i>
					{{--@la_input($moduleDebrief, 'dep')--}}

					<div class="form-group">
						<label for="dep">Departement* :</label>
						<select class="form-control select2-hidden-accessible" required="1" data-placeholder="Enter Departement" rel="select2" name="dep" tabindex="-1" aria-hidden="true">
							@foreach($alldepartments as $dep)
							<option value="{{$dep->id}}">{{$dep->name}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label for="">Assets attendus</label>
						<p>Selectionnez les assets attendus</p>
						@php $assetattendus = (new \App\Http\Controllers\AssetsvalidController())->typAssetForVue();@endphp
						@foreach($assetattendus as $assetattendu)
							<label class="checkbox-inline">
								<input type="checkbox" id="division_id" name="asset[]" value="{{$assetattendu['name']}}"> {{strtoupper($assetattendu['libelle'])}}
							</label>
						@endforeach
					</div>

					<div class="form-group">
						<label for="file"><strong>Pièces jointes</strong></label>
						<input type="file" id="file" name="file" class="dropify"/>
					</div>

					<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
					<input type="hidden" name="brief_id" value="{{$briefID}}">
					<input type="hidden" name="project_id" value="{{$project_id}}">

				</div>
				<div class="col-md-12">
					<div id="loaderdb" class="pull-right" style="display: none;">
						<img src="{{ URL::asset('la-assets/img/load.gif')}}">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" id="formadddbcan" class="btn btn-default" data-dismiss="modal">ANNULER</button>
				{!! Form::submit( 'ENREGISTRER', ['class'=>'btn btn-success','onClick'=>'submitDBForm()','id'=>'formadddb']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

</div>
@endsection


@push('styles')
<link rel="stylesheet" href="{{ asset('la-assets/plugins/dropify/dropify.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>
<script>
    $('.dropify').dropify();

    function submitDBForm() {
        $("#formadddb").prop('disabled', true);
        $("#formadddbcan").prop('disabled', true);
        $('#loaderdb').show();
        //alert("ici");
        $("#debrief-add-form").submit();
    }


    $(function () {
        $("#example1").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Précédent"
                },
            }
        });
        $("#brief-add-form").validate({

        });

        $("#example2").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Précédent"
                },
            }
        });
    });
</script>
@endpush
