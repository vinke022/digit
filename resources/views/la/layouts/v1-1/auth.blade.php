<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">

        <title>@hasSection('htmlheader_title')@yield('htmlheader_title') - @endif{{ LAConfigs::getByKey('sitename') }}</title>

        <!-- vendor css -->
        <link href="{{ asset('version-1.1-libs/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet')}}">
        <link href="{{ asset('version-1.1-libs/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

        <!-- DashForge CSS -->
        <link rel="stylesheet" href="{{asset('version-1.1-libs/assets/css/dashforge.css')}}">
        <link rel="stylesheet" href="{{asset('version-1.1-libs/assets/css/dashforge.auth.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('version-1.1-libs/assets/css/style-v1.1.css')}}">
        @stack('styles')
    </head>
    <body>
        <header class="navbar navbar-header navbar-header-fixed">
            <div style="width:100%; text-align:center">
                <img src="{{asset('version-1.1-libs/images/logo.png')}}" alt="" 
                    style="width: 50px; margin-top: 5px;">
            </div>
        </header>

        @yield('content')

        <footer class="footer">
            <div>
                <span>&copy; 2019 Digitalisation v1.1.0. </span>
            </div>
        </footer>

        <script src="{{ asset('version-1.1-libs/jquery/jquery.min.js')}}"></script>
        <script src="{{ asset('version-1.1-libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ asset('version-1.1-libs/feather-icons/feather.min.js')}}"></script>
        <script src="{{ asset('version-1.1-libs/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>

        <script src="{{ asset('version-1.1-libs/assets/js/dashforge.js')}}"></script>

        <!-- append theme customizer -->
        <script src="{{ asset('version-1.1-libs/lib/js-cookie/js.cookie.js')}}"></script>
        <script src="{{ asset('version-1.1-libs/assets/js/dashforge.settings.js')}}"></script>
    </body>
</html>