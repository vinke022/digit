<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- search form (Optional) -->
        @if(LAConfigs::getByKey('sidebar_search'))
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search..."/>
                    <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
                </div>
            </form>
    @endif
    <!-- /.search form -->

    {{--MENU DU SUPER ADMIN--}}
    @if(Auth::user()->role_id==1)
        <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">MODULES</li>
                <!-- Optionally, you can add icons to the links -->
                <li>
                    <a href="{{ url('/') }}">
                        <i class='fas fa-home'></i>
                        <span>Tableau de bord</span>
                    </a>
                </li>

                <?php
                // $menuItems = Dwij\Laraadmin\Models\Menu::where("parent", 0)->orderBy('hierarchy', 'asc')->get();
                ?>
                @foreach ($menuItems as $menu)
                    @if($menu->type == "module")
                        <?php
                        // $temp_module_obj = Module::get($menu->name);
                        ?>
                        @la_access($temp_module_obj->id)
                        @if(isset($module->id) && $module->name == $menu->name)
                            <?php //echo LAHelper::print_menu($menu ,true); ?>
                            @else
                            <?php //echo LAHelper::print_menu($menu); ?>
                            @endif
                            @endla_access
                            @else
                            <?php //echo LAHelper::print_menu($menu); ?>
                            @endif
                            @endforeach

                            <li class="treeview">
                                <a href="#">
                                    <i class="far fa-folder"></i> <span>Job Bag</span>
                                    <i class="fas fa-angle-right  pull-right"></i>
                                </a>
                                <ul class="treeview-menu" style="display: none;">
                                    {{-- <li class="treeview">
                                        <a href="#"><i class="far fa-folder"></i> <span>Job Bag List</span> <i class="fas fa-angle-right  pull-right"></i></a>
                                        <ul class="treeview-menu" style="display: none;">
                                            <li>
                                                <a href="{{ url(config('laraadmin.adminRoute'). '/projets_list')}}">
                                                    <i class='far fa-folder'></i>
                                                    <span>list</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li> --}}
                                    <li>
                                        <a href="{{ url(config('laraadmin.adminRoute'). '/projets_list') }}">
                                            <i class="fas fa-list-ul"></i>
                                            <span>list</span>
                                        </a>
                                    </li>

                                    <li class="treeview">
                                        <a href="#">
                                            <i class="far fa-edit"></i>
                                            <span>Brief & Debrief</span>
                                            <i class="fas fa-angle-right  pull-right"></i>
                                        </a>
                                        <ul class="treeview-menu" style="display: none;">
                                            <li class="treeview">
                                                <a href="{{ url(config('laraadmin.adminRoute').'/lead/brief/waitingsbrief') }}">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                    <span>Brief</span>
                                                </a>
                                            </li>
                                            <li class="treeview">
                                                <a href="#">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                    <span>Debrief</span>
                                                    <i class="fas fa-angle-right  pull-right"></i>
                                                </a>
                                                <ul class="treeview-menu" style="display: none;">
                                                    <li>
                                                        <a href="{{ url(config('laraadmin.adminRoute').'/lead/brief/waiting') }}">
                                                            <i class="fas fa-spinner"></i>
                                                            <span>Debrief en attente</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="{{ url(config('laraadmin.adminRoute').'/lead/brief/inprogress') }}">
                                                            <i class="fas fa-bullhorn"></i>
                                                            <span>Debrief en cours</span>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="{{ url(config('laraadmin.adminRoute').'/lead/brief/rejected') }}">
                                                            <i class="far fa-window-close"></i>
                                                            <span>Debrief rejeté</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>

                                    <li class="treeview">
                                        <a href="#"><i class="far fa-file"></i> <span>Asset</span> <i class="fas fa-angle-right  pull-right"></i></a>
                                        <ul class="treeview-menu" style="display: none;">
                                            <li>
                                                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_waiting') }}">
                                                    <i class="fas fa-spinner"></i>
                                                    <span>Asset en attente</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_valider') }}">
                                                    <i class="far fa-check-square"></i>
                                                    <span>Asset validé</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_rejeter') }}">
                                                    <i class="far fa-window-close"></i>
                                                    <span>Asset rejeté</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>

                                    <li>
                                        <a href="{{ url(config('laraadmin.adminRoute').'/planning') }}">
                                            <i class='far fa-calendar-alt'></i>
                                            <span>Planning</span>
                                        </a>
                                    </li>

                                    <li class="treeview">
                                        <a href="#"><i class="far fa-folder"></i> <span>Phases</span> <i class="fas fa-angle-right  pull-right"></i></a>
                                        <ul class="treeview-menu" style="display: none;">
                                            <li>
                                                <a href="{{ url(config('laraadmin.adminRoute'). '/projets') }}">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                    <span>Phase 1</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ url(config('laraadmin.adminRoute').'/projets_second') }}">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                    <span>Phase 2</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="{{ url(config('laraadmin.adminRoute').'/projets_third') }}">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                    <span>Phase 3</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>


                            <li>
                                <a href="{{ url(config('laraadmin.adminRoute').'/asset/digital') }}">
                                    <i class='far fa-file'></i>
                                    <span>Digital</span>
                                </a>
                            </li>

                            <li class="treeview">
                                <a href="#">
                                    <i class="fas fa-users" aria-hidden="true"></i>
                                    <span>Team</span>
                                    <i class="fas fa-angle-right  pull-right" aria-hidden="true"></i>
                                </a>
                                <ul class="treeview-menu menu-open">
                                    <li>
                                        <a href="{{ url(config('laraadmin.adminRoute'). '/users') }}">
                                            <i class="fas fa-users" aria-hidden="true"></i>
                                            <span>Users</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url(config('laraadmin.adminRoute'). '/departments') }}">
                                            <i class="fas fa-tag" aria-hidden="true"></i>
                                            <span>Departments</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url(config('laraadmin.adminRoute'). '/employees') }}">
                                            <i class="fas fa-users" aria-hidden="true"></i>
                                            <span>Employees</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url(config('laraadmin.adminRoute'). '/roles') }}">
                                            <i class="fas fa-user-plus" aria-hidden="true"></i>
                                            <span>Roles</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url(config('laraadmin.adminRoute'). '/permissions') }}">
                                            <i class="fas fa-magic" aria-hidden="true"></i>
                                            <span>Permissions</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="{{url(config('laraadmin.adminRoute').'/bibliotheques')}}">
                                    <i class='far fa-bookmark'></i>
                                    <span>Mediatheque</span>
                                </a>
                            </li>

                            <!-- LAMenus -->
            </ul><!-- /.sidebar-menu -->
            @endif

            {{--MENU DU ADMIN--}}
            @if(Auth::user()->role_id==3)
            <!-- Sidebar Menu -->
            @include('include.sidebar.adminlead')
            <!-- /.sidebar-menu -->
            @endif

            {{--MENU DU ADMIN MAKETING--}}
            @if(Auth::user()->role_id==4)
            @include('include.sidebar.maketing')
            @endif

            {{--MENU DU ADMIN HEAD DEPARTEMENT --}}
            @if(Auth::user()->role_id==5)
            @include('include.sidebar.head')
            @endif

            {{--MENU DU ADMIN CURRENT--}}
            @if(Auth::user()->role_id==6)
            @include('include.sidebar.current')
            @endif

            {{--MENU DU ADMIN VALIDATEUR--}}
            @if(Auth::user()->role_id==7)
            @include('include.sidebar.adminleadvalid')
            @endif

            {{--MENU DU ADMIN VALIDATEUR--}}
            @if(Auth::user()->role_id==8)
            @include('include.sidebar.trafic')
            @endif

            @if (! Auth::guest())
            <ul class="sidebar-menu">
                <li class="header">COMPTE</li>
                <li class="user-avatar">
                    <a href="#">
                        <img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}"
                             class="img-circle" alt="User Image" />
                        <p style="display: inline-block">{{ Auth::user()->name }}</p>
                    </a>
                </li>
                <li>
                    <a href="{{url(config('laraadmin.adminRoute').'/profil')}}"><i class="far fa-user"></i> Profil</a>
                </li>
            </ul>
            @endif

    </section>
    <!-- /.sidebar -->
</aside>
