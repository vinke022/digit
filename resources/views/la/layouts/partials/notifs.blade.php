<!-- Navbar Right Menu -->
<div class="navbar-custom-menu">
	<ul class="nav navbar-nav" id="navright">
		<!-- Messages: style can be found in dropdown.less-->
		@if(LAConfigs::getByKey('show_messages'))
			<li class="dropdown messages-menu">
				<!-- Menu toggle button -->
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-envelope-o"></i>
					<span class="label label-success">4</span>
				</a>
				<ul class="dropdown-menu">
					<li class="header">You have 4 messages</li>
					<li>
						<!-- inner menu: contains the messages -->
						<ul class="menu">
							<li><!-- start message -->
								<a href="#">
									<div class="pull-left">
										<!-- User Image -->
										<img src="@if(isset(Auth::user()->email)) {{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }} @else asset('/img/user2-160x160.jpg' @endif" class="img-circle" alt="User Image"/>
									</div>
									<!-- Message title and timestamp -->
									<h4>
										Support Team
										<small><i class="fa fa-clock-o"></i> 5 mins</small>
									</h4>
									<!-- The message -->
									<p>Why not buy a new awesome theme?</p>
								</a>
							</li><!-- end message -->
						</ul><!-- /.menu -->
					</li>
					<li class="footer"><a href="#">See All Messages</a></li>
				</ul>
			</li><!-- /.messages-menu -->
		@endif
		@if(LAConfigs::getByKey('show_notifications'))
		<!-- Notifications Menu -->
			<li class="dropdown notifications-menu">
				<!-- Menu toggle button -->
				@inject('nbnotif','App\Helpers\Notification')
				@if(Auth::user()->role_id==4)
					<a href="{{url(config('laraadmin.adminRoute') . '/marketing/notif')}}" class="">
						<i class="fa fa-bell-o"></i>
						<span class="label label-warning">{{$nbnotif::CountNotifs()>0 ? $nbnotif::CountNotifs() : ''}}</span>
					</a>
				@endif
				@if(Auth::user()->role_id==5)
					<a href="{{url(config('laraadmin.adminRoute') . '/dashboard/notif')}}" class="">
						<i class="fa fa-bell-o"></i>
						<span class="label label-warning">{{$nbnotif::CountNotifs()>0 ? $nbnotif::CountNotifs() : ''}}</span>
					</a>
				@endif
				@if(Auth::user()->role_id==6)
					<a href="{{url(config('laraadmin.adminRoute') . '/user/notif')}}" class="">
						<i class="fa fa-bell-o"></i>
						<span class="label label-warning">{{$nbnotif::CountNotifs()>0 ? $nbnotif::CountNotifs() : ''}}</span>
					</a>
				@endif
				@if(Auth::user()->role_id==7)
					<a href="{{url(config('laraadmin.adminRoute') . '/admin/notif')}}" class="">
						<i class="fa fa-bell-o"></i>
						<span class="label label-warning">{{$nbnotif::CountNotifs()>0 ? $nbnotif::CountNotifs() : ''}}</span>
					</a>
				@endif

				@if(Auth::user()->post=='DGA')
					<a href="{{url(config('laraadmin.adminRoute') . '/lead/notif')}}" class="">
						<i class="fa fa-bell-o"></i>
						<span class="label label-warning">{{$nbnotif::CountNotifs()>0 ? $nbnotif::CountNotifs() : ''}}</span>
					</a>
				@endif

				{{--<ul class="dropdown-menu">
                    <li class="header">You have 10 notifications</li>
                    <li>
                        <!-- Inner Menu: contains the notifications -->
                        <ul class="menu">
                            <li><!-- start notification -->
                                <a href="#">
                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                </a>
                            </li><!-- end notification -->
                        </ul>
                    </li>
                    <li class="footer"><a href="#">View all</a></li>
                </ul>--}}
			</li>
		@endif
		@if(LAConfigs::getByKey('show_tasks'))
		<!-- Tasks Menu -->
			<li class="dropdown tasks-menu">
				<!-- Menu Toggle Button -->
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-flag-o"></i>
					<span class="label label-danger">9</span>
				</a>
				<ul class="dropdown-menu">
					<li class="header">You have 9 tasks</li>
					<li>
						<!-- Inner menu: contains the tasks -->
						<ul class="menu">
							<li><!-- Task item -->
								<a href="#">
									<!-- Task title and progress text -->
									<h3>
										Design some buttons
										<small class="pull-right">20%</small>
									</h3>
									<!-- The progress bar -->
									<div class="progress xs">
										<!-- Change the css width attribute to simulate progress -->
										<div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
											<span class="sr-only">20% Complete</span>
										</div>
									</div>
								</a>
							</li><!-- end task item -->
						</ul>
					</li>
					<li class="footer">
						<a href="#">View all tasks</a>
					</li>
				</ul>
			</li>
		@endif
		@if (Auth::guest())
			<li><a href="{{ url('/login') }}">Login</a></li>
			<li><a href="{{ url('/register') }}">Register</a></li>
		@else
			<li>
				<a href="#">
					<i class="fas fa-search fa-lg" aria-hidden="true"></i>
				</a>
			</li>
			{{--<li class="bell">--}}
				{{--<a href="#">--}}
					{{--<i class="far fa-bell fa-lg"></i>--}}
					{{--<span>3</span>--}}
				{{--</a>--}}
			{{--</li>--}}
			<li>
				<a href="{{ url('/logout') }}">
					<i class="fas fa-power-off fa-lg"></i>
				</a>
			</li>
			<!-- User Account Menu -->
			{{-- <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!-- The user image in the navbar-->
                    <img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" class="user-image" alt="User Image"/>
                    <!-- hidden-xs hides the username on small devices so only the image appears. -->
                </a>
                <ul class="dropdown-menu">
                    <!-- The user image in the menu -->
                    <li class="user-header">
                        <img src="{{ Gravatar::fallback(asset('la-assets/img/user2-160x160.jpg'))->get(Auth::user()->email) }}" class="img-circle" alt="User Image" />
                    </li>
                    <!-- Menu Body -->
                    @role("SUPER_ADMIN")
                    <li class="user-body">
                        <!-- <div class="col-xs-6 text-center mb10">
                            <a href="{{ url(config('laraadmin.adminRoute') . '/lacodeeditor') }}"><i class="fa fa-code"></i> <span>Editor</span></a>
                        </div> -->
                        <div class="col-xs-6 text-center mb10">
                            <a href="{{ url(config('laraadmin.adminRoute') . '/modules') }}"><i class="fa fa-cubes"></i> <span>Modules</span></a>
                        </div>
                        <div class="col-xs-6 text-center mb10">
                            <a href="{{ url(config('laraadmin.adminRoute') . '/la_menus') }}"><i class="fas fa-bars"></i> <span>Menus</span></a>
                        </div>
                        <div class="col-xs-6 text-center mb10">
                            <a href="{{ url(config('laraadmin.adminRoute') . '/la_configs') }}"><i class="fa fa-cogs"></i> <span>Configure</span></a>
                        </div>
                        <!-- <div class="col-xs-6 text-center">
                            <a href="{{ url(config('laraadmin.adminRoute') . '/backups') }}"><i class="fa fa-hdd-o"></i> <span>Backups</span></a>
                        </div> -->
                        <div class="col-xs-6 text-center mb10">
                            <a href="{{ url(config('laraadmin.adminRoute') . '/annonceurs') }}">
                                <i class="fa fa-users"></i>
                                <span>Annonceurs</span>
                            </a>
                        </div>
                        <div class="col-xs-6 text-center mb10">
                            <a href="{{ url(config('laraadmin.adminRoute') . '/biblios') }}">
                                <i class="far fa-bookmark"></i>
                                <span>Biblios</span>
                            </a>
                        </div>
                        <div class="col-xs-6 text-center mb10">
                            <a href="{{ url(config('laraadmin.adminRoute') . '/checklists') }}">
                                <i class="fa fa-check"></i>
                                <span>check-list</span>
                            </a>
                        </div>
                    </li>
                    @endrole
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{ url(config('laraadmin.adminRoute') . '/profil')}}" class="btn btn-default btn-flat">Profil</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">
                                Déconnexion
                            </a>
                        </div>
                    </li>
                </ul>
            </li> --}}
		@endif
		@if(LAConfigs::getByKey('show_rightsidebar'))
		<!-- Control Sidebar Toggle Button -->
			<li>
				<a href="#" data-toggle="control-sidebar"><i class="fa fa-comments-o"></i> <span class="label label-warning">10</span></a>

			</li>
		@endif
	</ul>
</div>