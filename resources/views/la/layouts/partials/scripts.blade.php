<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset('la-assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('la-assets/js/bootstrap.min.js') }}" type="text/javascript"></script>

<!-- jquery.validate + select2 -->
<script src="{{ asset('la-assets/plugins/jquery-validation/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('la-assets/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('la-assets/plugins/bootstrap-datetimepicker/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('la-assets/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="{{ asset('la-assets/js/app.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('la-assets/plugins/stickytabs/jquery.stickytabs.js') }}" type="text/javascript"></script>
<script src="{{ asset('la-assets/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script>
    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('#navright').fadeOut();
        } else {
            $('#navright').fadeIn();
        }
    });
</script>
//checklist ajax
<script type="text/javascript">
    $(".check-list-input").change(function() {
        self = $(this);
        var project_id = {{$projet->id}};
        if(self.is(':checked')) {
            var id_checked = self.val();
            $.ajax({
                method: "POST",
                url: "{{url('ajaxChecklist')}}",
                data: {
                    id_checked: id_checked,
                    project_id: project_id
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
                }
            })
                .done(function(data) {
                    console.log(data);
                    location.reload();
                });
        } else if(self.attr('checked', false)){
            var id_checked = self.val();
            $.ajax({
                method: "POST",
                url: "{{url('ajaxChecklistUpdate')}}",
                data: {
                    id_checked: id_checked,
                    project_id: project_id
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
                }
            })
                .done(function(data) {
                    console.log(data);
                    location.reload();
                });
        }
    });
</script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->

@stack('scripts')