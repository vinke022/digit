@extends("la.layouts.app")

@push('styles')
<link rel="stylesheet" href="{{ asset('la-assets/plugins/dropify/dropify.css') }}">
<link rel="stylesheet" href="{{ asset('la-assets/plugins/lightbox/lightbox.css') }}">
<style>
	.imgasset {
		height: 101px!important;
		width: 100%;
	}
	.mailbox-attachment-info{
		height: 90px;
	}
</style>
@endpush

@section("contentheader_title", "Assets")
@section("contentheader_description", "$asset_type")
{{--@section("section", "Assets")
@section("sub_section", "Listing")--}}
@section("htmlheader_title", "$asset_type")

@section("headerElems")
@la_access("Assets", "create")
	@if(Auth::user()->role_id == 4 and $asset_type=='devis')
	<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">Ajouter un asset</button>
	@endif
@endla_access
@endsection

@section("main-content")
	<div class="row float-right" style="margin-left: 0;margin-bottom: 4px;">
		<a href="{{url(config('laraadmin.adminRoute') . '/debriefs/'.$asset_id.'#see-asset' )}}" class="btn btn-danger btn-lrg" title="Retour" style="padding: 3px 12px;">
			<i class="fa fa-backward"></i> Retour
		</a>
	</div>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box box-success">
	<!--<div class="box-header"></div>-->
	<div class="box-body">
		{{--@php($extensions_check=array('jpeg','jpg','gif','tiff','bmp','png'))--}}
		<div class="box-footer">
			<ul class="mailbox-attachments clearfix">
		@foreach($asset_data as $asset )
			{{--<div class="col-md-4">--}}

				@if(in_array($asset->ext, ['jpeg','jpg','gif','tiff','bmp','png']))

					<li>
						{{--<span class="mailbox-attachment-icon" style="max-height:150px;">--}}
							{{--<i class="fa fa-file-image-o"></i>--}}
						{{--</span>--}}
						<span class="mailbox-attachment-icon has-img" style="max-height:150px;">
							<a href="{{url('assetFiles/'.$asset->path)}}" data-lightbox="image-1" data-title="{{$asset->libelle}}">
								<img src="{{url('assetFiles/'.$asset->path)}}" alt="{{$asset->libelle}}" style="width: 100%">
							</a>
						</span>
						<div class="mailbox-attachment-info">
							<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
							<span class="mailbox-attachment-size">
								<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
								<a href="{{url('assetFiles/'.$asset->path)}}" data-lightbox="image-1" data-title="{{$asset->libelle}}" style="margin-left:10px"><i class="fa fa-eye"></i>Voir</a>
								<br>
								@if($asset->status=='0')
									<span class="label label-danger">Asset rejeté</span>
								@elseif($asset->status=='1')
									<span class="label label-warning">En attente de validation</span>
								@else
									<span class="label label-success">Asset validé</span>
								@endif
								{{--<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
									<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
									{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
									<button type="submit" class="btn btn-danger btn-xs pull-right">
										<i class="fa fa-trash"></i>
									</button>
									{{ Form::close() }} 
								</form>--}}
							</span>
						</div>
					</li>

					{{--<h3>{{$asset->libelle}}</h3>
					<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
						<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
						<button class="btn btn-danger btn-xs" type="submit" style="position: absolute;right: 10px"><i class="fa fa-close"></i></button></form>
					--}}{{--<a href="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}"><i class="fa fa-close" style="position: absolute;right: 10px"></i></a>--}}{{--
					<img class="img-responsive" style="max-height: 100px" src="{{url('assetFiles/'.$asset->path)}}" alt="{{$asset->libelle}}">--}}
				@endif

				@if(in_array($asset->ext, ['htm','html','xml']))
						<li>
							<span class="mailbox-attachment-icon" style="max-height:150px;">
								<i class="fa fa-code"></i>
							</span>
							<div class="mailbox-attachment-info">
								<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
								<span class="mailbox-attachment-size">
									<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
									<br>
									@if($asset->status=='0')
										<span class="label label-danger">Asset rejeté</span>
									@elseif($asset->status=='1')
										<span class="label label-warning">En attente de validation</span>
									@else
										<span class="label label-success">Asset validé</span>
									@endif
									{{--<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
										<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
										{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
										<button type="submit" class="btn btn-danger btn-xs pull-right">
											<i class="fa fa-trash"></i>
										</button>
										{{ Form::close() }} 
									</form>--}}
								</span>
							</div>
						</li>

					{{--<h3>{{$asset->libelle}}</h3>
					<object width="100%" height="500px" data="{{url('assetFiles/'.$asset->path)}}"></object>--}}
				@endif

				@if(in_array($asset->ext, ['mp3','wma','aac','ogg']))
						<li>
							<span class="mailbox-attachment-icon" style="max-height:150px;">
								<i class="fa fa-file-audio-o"></i>
							</span>
							<div class="mailbox-attachment-info">
								<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
								<span class="mailbox-attachment-size">
									<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
									<br>
									@if($asset->status=='0')
										<span class="label label-danger">Asset rejeté</span>
									@elseif($asset->status=='1')
										<span class="label label-warning">En attente de validation</span>
									@else
										<span class="label label-success">Asset validé</span>
									@endif
									{{--<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
										<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
										{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
										<button type="submit" class="btn btn-danger btn-xs pull-right">
											<i class="fa fa-trash"></i>
										</button>
										{{ Form::close() }} 
									</form>--}}
								</span>
							</div>
						</li>
					{{--<h3>{{$asset->libelle}}</h3>
					<audio src="{{url('assetFiles/'.$asset->path)}}" controls></audio>--}}
				@endif

				@if(in_array($asset->ext, ['avi','mpeg','mov','mp4','flv']))
						<li>
							<span class="mailbox-attachment-icon" style="max-height:150px;">
								<i class="fa fa-file-video-o"></i>
							</span>
							<div class="mailbox-attachment-info">
								<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
								<span class="mailbox-attachment-size">
									<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
									<br>
									@if($asset->status=='0')
										<span class="label label-danger">Asset rejeté</span>
									@elseif($asset->status=='1')
										<span class="label label-warning">En attente de validation</span>
									@else
										<span class="label label-success">Asset validé</span>
									@endif
									{{--<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
										<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
										{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
										<button type="submit" class="btn btn-danger btn-xs pull-right">
											<i class="fa fa-trash"></i>
										</button>
										{{ Form::close() }}
									</form>--}}
								</span>
							</div>
						</li>
					{{--<h3>{{$asset->libelle}}</h3>
					<video src="{{url('assetFiles/'.$asset->path)}}" controls poster="assets/img/movie.png" width="100%"></video>--}}
				@endif

				@if(in_array($asset->ext, ['rar','zip','tar','EndNote']))
						<li>
							<span class="mailbox-attachment-icon" style="max-height:150px;">
								<i class="fa fa-file-archive-o"></i>
							</span>
							<div class="mailbox-attachment-info">
								<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
								<span class="mailbox-attachment-size">
									<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
									<br>
									@if($asset->status=='0')
										<span class="label label-danger">Asset rejeté</span>
									@elseif($asset->status=='1')
										<span class="label label-warning">En attente de validation</span>
									@else
										<span class="label label-success">Asset validé</span>
									@endif
									{{--<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
										<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
										{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
										<button type="submit" class="btn btn-danger btn-xs pull-right">
											<i class="fa fa-trash"></i>
										</button>
										{{ Form::close() }}
									</form>--}}
								</span>
							</div>
						</li>
					{{--<h3>{{$asset->libelle}}</h3>
					<img class="img-responsive" src="{{url('assetFiles/'.$asset->path)}}" alt="{{$asset->libelle}}">--}}
				@endif

				@if(in_array($asset->ext, ['pdf']))
					<li>
						<span class="mailbox-attachment-icon" style="max-height:150px;">
							<i class="fa fa-file-pdf-o"></i>
						</span>
						<div class="mailbox-attachment-info">
							<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
							<span class="mailbox-attachment-size">
								<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
								<br>
								@if($asset->status=='0')
									<span class="label label-danger">Asset rejeté</span>
								@elseif($asset->status=='1')
									<span class="label label-warning">En attente de validation</span>
								@else
									<span class="label label-success">Asset validé</span>
								@endif
								@if(Auth::user()->id ==$asset->user_id )
								<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
									<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
									{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
										<button type="submit" class="btn btn-danger btn-xs pull-right">
											<i class="fa fa-trash"></i>
										</button>
									{{ Form::close() }}
								</form>
								@endif
							</span>
						</div>
					</li>
				@endif

				@if(in_array($asset->ext, ['ppt','pptx']))
					<li>
					<span class="mailbox-attachment-icon" style="max-height:150px;">
						<i class="fa fa-file-powerpoint-o"></i>
					</span>
						<div class="mailbox-attachment-info">
							<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
							<span class="mailbox-attachment-size">
							<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
								<br>
								@if($asset->status=='0')
									<span class="label label-danger">Asset rejeté</span>
								@elseif($asset->status=='1')
									<span class="label label-warning">En attente de validation</span>
								@else
									<span class="label label-success">Asset validé</span>
								@endif
							{{--<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
								<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
								{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
								<button type="submit" class="btn btn-danger btn-xs pull-right">
									<i class="fa fa-trash"></i>
								</button>
								{{ Form::close() }}
							</form>--}}
						</span>
						</div>
					</li>
				@endif

				@if(in_array($asset->ext, ['xls','xlsx','csv']))
					<li>
						<span class="mailbox-attachment-icon" style="max-height:150px;">
							<i class="fa fa-file-excel-o"></i>
						</span>
						<div class="mailbox-attachment-info">
							<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
							<span class="mailbox-attachment-size">
								<a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
								<br>
								@if($asset->status=='0')
									<span class="label label-danger">Asset rejeté</span>
								@elseif($asset->status=='1')
									<span class="label label-warning">En attente de validation</span>
								@else
									<span class="label label-success">Asset validé</span>
								@endif
								{{--<form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/assets/'.$asset->id) }}" accept-charset="UTF-8">
									<input name="_method" type="hidden" value="DELETE"> {{csrf_field()}}
									{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assets.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
									<button type="submit" class="btn btn-danger btn-xs pull-right">
										<i class="fa fa-trash"></i>
									</button>
									{{ Form::close() }}
								</form>--}}
							</span>
						</div>
					</li>
				@endif

			{{--</div>--}}
		@endforeach
			</ul>
		</div>

	</div>
	{{--<div class="box-body">
		<table id="example1" class="table table-bordered">
		<thead>
		<tr class="success">
			@foreach( $listing_cols as $col )
			<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
			@endforeach
			@if($show_actions)
			<th>Actions</th>
			@endif
		</tr>
		</thead>
		<tbody>

		</tbody>
		</table>
	</div>--}}
</div>

@la_access("Assets", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Ajouter un asset</h4>
			</div>
			{!! Form::open(['action' => 'LA\AssetsController@store', 'id' => 'asset-add-form','files' => true]) !!}
			<div class="modal-body">
				<div class="box-body">
                    {{--@la_form($module)
					@la_input($module, 'asset_type')
					@la_input($module, 'brief_id')
					@la_input($module, 'path')
					@la_input($module, 'ext')
					@la_input($module, 'libelle')--}}
					<div class="form-group">
						<label for="libelle"><strong>Libelle</strong></label>
						<input type="text" id="libelle" name="libelle" class="form-control" required/>
					</div>
					<input type="hidden" name="status" value="1">
					<div class="form-group">
						<label for="file"><strong>Fichier</strong></label>
						<input type="file" id="file" name="file" class="dropify" required/>
					</div>

					<input type="hidden" name="asset_type" value="{{$asset_type}}">
					<input type="hidden" name="brief_id" value="{{$asset_id}}">
					<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">FERMER</button>
				{!! Form::submit( 'ENREGISTRER', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/lightbox/lightbox.js') }}"></script>
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>
<script>
    $('.dropify').dropify();
$(function () {
	$("#example1").DataTable({
		processing: true,
        serverSide: true,
        ajax: "{{ url(config('laraadmin.adminRoute') . '/asset_dt_ajax') }}",
		language: {
			lengthMenu: "_MENU_",
			search: "_INPUT_",
			searchPlaceholder: "Search"
		},
		@if($show_actions)
		columnDefs: [ { orderable: false, targets: [-1] }],
		@endif
	});
	$("#asset-add-form").validate({
		
	});
});
</script>
@endpush
