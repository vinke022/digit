@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/annonceurs') }}">Annonceur</a> :
@endsection
@section("contentheader_description", $annonceur->$view_col)

@section("htmlheader_title", "Annonceurs Edit : ".$annonceur->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($annonceur, ['route' => [config('laraadmin.adminRoute') . '.annonceurs.update', $annonceur->id ], 'method'=>'PUT', 'id' => 'annonceur-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'name')
					@la_input($module, 'adresse')
					@la_input($module, 'email')
					@la_input($module, 'activites')
					@la_input($module, 'mobile')
					@la_input($module, 'fixe')
					@la_input($module, 'type')
					--}}
                    <br>
					<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
					<div class="form-group">
						{!! Form::submit( 'MISE A JOUR', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url('/') }}">ANNULER</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#annonceur-edit-form").validate({
		
	});
});
</script>
@endpush
