@extends("la.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/assign/validation') }}">Assetvalidation</a> :
@endsection
@section("contentheader_description", $assetvalidation->$view_col)
@section("htmlheader_title", "Assetvalidations Edit")

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($assetvalidation, ['route' => [config('laraadmin.adminRoute') . '.assetvalidations.update', $assetvalidation->id ], 'method'=>'PUT', 'id' => 'assetvalidation-edit-form']) !!}
					{{--@la_form($module)
					@la_input($module, 'name')@la_input($module, 'assetype')
					--}}

					<div class="form-group">
						<label for="libelle">Type asset :</label>
						<select class="form-control" required="" id="assetype" name="assetype">
							@foreach($assetypes as $asset)
								<option value="{{$asset->name}}" {{$asset->name ==$assetvalidation->assetype ? 'selected' : '' }}>{{$asset->libelle}}</option>
							@endforeach
						</select>
					</div>

					@la_input($module, 'nombre')

					<div class="form-group">
						<label for="libelle">Validateur * :</label>
						<select class="form-control" required="" id="valideur" name="valideur[]" multiple data-role="tagsinput">
							@php $arrayvalid = json_decode($assetvalidation->Post); @endphp
							@foreach($arrayvalid as $post)
								<option value="{{$post}}">{{$post}}</option>
							@endforeach
						</select>
					</div>
                    <br>
					<div class="form-group">
						{!! Form::submit( 'MISE A JOUR', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/assetvalidations') }}">ANNULER</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection


@push('styles')
<link rel="stylesheet" href="{{asset('la-assets/plugins/chzn/bootstrap-tagsinput.css')}}">
<style>
	.bootstrap-tagsinput {
		display: block;
	}
</style>
@endpush

@push('scripts')
<!-- Select2 -->
<script src="{{ asset('la-assets/plugins/chzn/bootstrap-tagsinput.min.js')}}"></script>

<script>
$(function () {
	$("#assetvalidation-edit-form").validate({
		
	});
});
</script>
@endpush
