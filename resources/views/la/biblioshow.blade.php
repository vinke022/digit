@extends('la.layouts.app')

@section('htmlheader_title') Bibliotheque @if($type== 'docsadmin') Documents administratifs @else {{ucfirst($type)}} @endif @endsection
@section('contentheader_title') Bibliotheque @if($type== 'docsadmin') Documents administratifs @else {{ucfirst($type)}} @endif @endsection
@section('contentheader_description') @endsection

@section('main-content')
    <section class="content">
        @if(count($biblio) != 0)
        <div class="box box-success">
            <!--<div class="box-header"></div>-->
            <div class="box-body">
                <div class="box-footer">
                    <ul class="mailbox-attachments clearfix">
                        @foreach($biblio as $asset )

                            @if(in_array($asset->ext, ['jpeg','jpg','gif','tiff','bmp','png']))

                                <li>
                                    <span class="mailbox-attachment-icon" style="max-height:150px;">
                                        <i class="fa fa-file-image-o"></i>
                                    </span>
                                    <div class="mailbox-attachment-info">
                                        <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                        <span class="mailbox-attachment-size">
                                            <a href="{{url('bibliosFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                            <br>
							            </span>
                                    </div>
                                </li>

                            @endif

                            @if(in_array($asset->ext, ['htm','html','xml']))
                                <li>
                                    <span class="mailbox-attachment-icon" style="max-height:150px;">
                                        <i class="fa fa-code"></i>
                                    </span>
                                    <div class="mailbox-attachment-info">
                                        <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                        <span class="mailbox-attachment-size">
                                            <a href="{{url('bibliosFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                            <br>
								        </span>
                                    </div>
                                </li>
                            @endif


                            @if(in_array($asset->ext, ['pdf']))
                                <li>
                                    <span class="mailbox-attachment-icon" style="max-height:150px;">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </span>
                                    <div class="mailbox-attachment-info">
                                        <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                        <span class="mailbox-attachment-size">
                                            <a href="{{url('bibliosFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                            <br>
                                        </span>
                                    </div>
                                </li>
                            @endif

                            @if(in_array($asset->ext, ['ppt','pptx']))
                                <li>
                                    <span class="mailbox-attachment-icon" style="max-height:150px;">
                                        <i class="fa fa-file-powerpoint-o"></i>
                                    </span>
                                    <div class="mailbox-attachment-info">
                                        <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                        <span class="mailbox-attachment-size">
							                <a href="{{url('bibliosFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                            <br>
                                        </span>
                                    </div>
                                </li>
                            @endif

                            @if(in_array($asset->ext, ['xls','xlsx','csv']))
                                <li>
                                    <span class="mailbox-attachment-icon" style="max-height:150px;">
                                        <i class="fa fa-file-excel-o"></i>
                                    </span>
                                    <div class="mailbox-attachment-info">
                                        <span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
                                        <span class="mailbox-attachment-size">
                                            <a href="{{url('bibliosFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                            <br>
                                        </span>
                                    </div>
                                </li>
                            @endif

                        @endforeach
                    </ul>
                </div>

            </div>

        </div>
        @else
            <p>Aucun document pour l'instant</p>
        @endif
    </section>

@endsection

@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush


@push('scripts')
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Pr&eacute;c&eacute;den"
            },
        }
    });
</script>
@endpush