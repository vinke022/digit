@extends('la.layouts.app')

@push('styles')
<link href="{{ asset('la-assets/css/dropzone.css') }}" type="text/css" rel="stylesheet" />

<style type="text/css">
	.inner p {
		font-size: 24px;
		font-weight: bold;
	}
</style>
@endpush

@section('htmlheader_title')
	Debrief View
@endsection


@section('main-content')
<div id="page-content" class="profile2">
	<div class="nav nav-tabs profile clearfix">
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-3">
					<div class="profile-icon text-primary"><i class="fa {{ $module->fa_icon }}"></i></div>
				</div>
				<div class="col-md-9">
					<h4 class="name">{{ $debrief->$view_col }}</h4>
				</div>
			</div>
		</div>


		<div class="col-md-1 actions">
			{{--@la_access("Debriefs", "edit")
				<a href="{{ url(config('laraadmin.adminRoute') . '/debriefs/'.$debrief->id.'/edit') }}" class="btn btn-xs btn-edit btn-default"><i class="fa fa-pencil"></i></a><br>
			@endla_access
			
			@la_access("Debriefs", "delete")
				{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.debriefs.destroy', $debrief->id], 'method' => 'delete', 'style'=>'display:inline']) }}
					<button class="btn btn-default btn-delete btn-xs" type="submit"><i class="fa fa-times"></i></button>
				{{ Form::close() }}
			@endla_access--}}
		</div>
	</div>

	<ul data-toggle="ajax-tab" class="nav nav-tabs profile" role="tablist">
		<li class=""><a href="{{ url(config('laraadmin.adminRoute') . '/briefs/'.$brief_id) }}" data-toggle="tooltip" data-placement="right" title="Cliquer pour revenir en arrière"><i class="fa fa-chevron-left"></i></a></li>
		<li class="active"><a role="tab" data-toggle="tab" class=""  href="#see-asset" data-target="#see-asset"><i class="fa fa-folder"></i> Assets</a></li>
		<li class=""><a role="tab" data-toggle="tab" href="#tab-info" data-target="#tab-info"><i class="fa fa-edit"></i> Information du debrief</a></li>
		<li class=""><a role="tab" data-toggle="tab" class="active" href="#mail" data-target="#mail"><i class="fa fa-reply-all"></i> Mail client</a></li>
		@if(Auth::user()->role_id == 5 or Auth::user()->role_id == 8)
			@if(Auth::user()->departement_id == $debrief->dep)
			<li class=""><a role="tab" data-toggle="tab" href="#tache" data-target="#tache"><i class="fa fa-tasks"></i>Assigner des tâches</a></li>
			@endif
			@if(Auth::user()->post!= 'TRAFIC' AND Auth::user()->post !='RSTRAT')
				<li class=""><a role="tab" data-toggle="tab" href="#creatache" data-target="#creatache"><i class="fa fa-tasks"></i>Créer une tâche</a></li>
			@endif
		@endif
	</ul>

	<div class="tab-content">
		<div role="tabpanel" class="tab-pane fade in" id="tab-info">
			<div class="tab-content">
				<div class="panel infolist">
					<div class="panel-default panel-heading">
						<h4>Information Générale</h4>
					</div>
					<div class="panel-body">
						@la_display($module, 'libelle')
						<div class="form-group">
							<label for="libelle" class="col-md-2">Proposition :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($debrief->proposition) !!}
							</div>
						</div>

						<div class="form-group">
							<label for="libelle" class="col-md-2">Cible :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($debrief->cible) !!}
							</div>
						</div>

						<div class="form-group">
							<label for="libelle" class="col-md-2">Travail attendu :</label>
							<div class="col-md-10 fvalue">
								{!! nl2br($debrief->taf) !!}
							</div>
						</div>
						@la_display($module, 'dateDentree')
						@la_display($module, 'dateDeSortie')

						<div class="form-group">
							<label for="dateDeSortie" class="col-md-2">Assets attendus :</label>
							<div class="col-md-10 fvalue">
								@php $assets = explode( ',', $debrief->asset ) @endphp
								@if(count($assets))
									@foreach($assets as $asset)
										@if($asset != 'Array')
										<a href="#" role="button">
											<span class="label label-primary">{{$asset}}</span></a>
										@endif
									@endforeach
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="dateDeSortie" class="col-md-2">Statut :</label>
							<div class="col-md-10 fvalue">
								@if($debrief->status=='0')
									<span class="label label-danger">Rejeter</span>
								@elseif($debrief->status=='1' or $debrief->status=='3')
									<span class="label label-warning">En attente</span>
								@else
									<span class="label label-success">Valider</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="libelle" class="col-md-2">Departement :</label>
							<div class="col-md-10 fvalue">
								@if($debrief->dep=='1')
									Administration
								@elseif($debrief->dep=='2')
									Service MEDIA
								@elseif($debrief->dep=='3')
									Service Crea/Strat
								@elseif($debrief->dep=='5')
									Service Digital
								@elseif($debrief->dep=='6')
									Service Stratégie
								@elseif($debrief->dep=='7')
									Service Commercial
								@elseif($debrief->dep=='8')
									Service Opérationnel
								@else
									Service Trafic
								@endif
							</div>
						</div>

						@if($debrief->pj)
							<div class="form-group">
								<label for="piece" class="col-md-2">Pièces jointes :</label>
								<div class="col-md-10 fvalue">
									<a href="{{url('briefFiles/'.$debrief->pj)}}" download="{{$debrief->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
								</div>
							</div>
						@endif
						{{--@la_display($module, 'path')
						@la_display($module, 'dep')
						@la_display($module, 'project_id')
						@la_display($module, 'user_id')
						@la_display($module, 'brief_id')
						@la_display($module, 'status')
						@la_display($module, 'asset')--}}
					</div>
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane active fade in p20 bg-white" id="see-asset">
			<div class="row">
				@php $assetypes1 = (new \App\Http\Controllers\AssetsvalidController())->typAssetForVue();@endphp
				@foreach($assetypes1 as $k => $assetype)
					@php $assetnb = (new \App\Http\Controllers\LA\DebriefsController())->nbAssetByBrief($debrief->id,$assetype['libelle']);@endphp
					<div class="col-lg-3 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-{{$assetcolors[$k]}}">
							<div class="inner">
								<h3>{{$assetnb}}</h3>
								<p style="font-size: 15px!important;">{{$assetype['libelle']}}</p>
							</div>

							<a href="{{url(config('laraadmin.adminRoute') . '/assets/'.$debrief->id.'/'.$assetype['name'])}}" class="small-box-footer">Voir <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					</div>
				@endforeach
			</div><!-- /.row -->

		</div>

		<div role="tabpanel" class="tab-pane fade in" id="tache">
			@if($debrief->status=='2')
				<div class="col-md-12">
				<div class="tab-content">
					<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
						@la_access("Taches", "create")
						<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">
							Ajouter une tâche
						</button>
						@endla_access
						<span class="taf">
							<h5 style="color: #bc0060;">TRAVAIL ATTENDU :</h5>
							<p>{!! nl2br($debrief->taf) !!}</p>
						</span>
					</div>

					<div class="box box-success">
						<div class="box-body">
							<table id="example1" class="table table-bordered">
								<thead>
								<tr class="success">
									@foreach( $listing_cols_tache as $col )
										<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
									@endforeach
									@if($show_actions)
										<th>Actions</th>
									@endif
								</tr>
								</thead>
								<tbody>
								@foreach($taches as $k => $tache)
									<tr role="row" class="odd">
										<td class="sorting_1">{{$k+1}}</td>
										<td>
											{{$tache->libelle}}
											{{--<a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id)}}"> </a>--}}
										</td>
										<td>{{$tache->description}}</td>
										<td>{{$tache->userTache->name}}</td>
										<td>
											@if($tache->status=='0')
												<span class="label label-danger">En attente</span>
											@elseif($tache->status=='1')
												<span class="label label-warning">En cours</span>
											@else
												<span class="label label-success">Terminer</span>
											@endif
										</td>
										<td>
											<a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
											@if($tache->status=='0')
											{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.taches.destroy', $tache->id], 'method' => 'delete', 'style'=>'display:inline']) }}
											<button class="btn btn-danger btn-xs" type="submit">
												<i class="fa fa-times"></i>
											</button>
											{{Form::close()}}
											@endif
										</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>


				</div>
			</div>
			@else
				<div class="col-md-12">
					<div class="tab-content">
						<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
							<a class="btn btn-success btn-sm" href="{{url(config('laraadmin.adminRoute').'/dashboard/brief/assign')}}">
								En attente de validation
							</a>
							{{--<span class="taf">--}}
							{{--<h5 style="color: #bc0060;">TRAVAIL ATTENDU :</h5>--}}
							{{--<p>{!! nl2br($debrief->taf) !!}</p>--}}
							{{--</span>--}}
						</div>
					</div>
				</div>
			@endif
		</div>

		<div role="tabpanel" class="tab-pane fade in p20 bg-white" id="mail">
			<div class="tab-content">
				<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
					@la_access("Mails", "create")
					<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModalMail">
						Ajouter un mail client
					</button>
					@endla_access
				</div>
				<div class="box box-success">
					<div class="box-body">
						<ul class="mailbox-attachments clearfix">
							@foreach($mails as $asset)
								<li>
									<span class="mailbox-attachment-icon has-img" style="max-height:150px;">
										<a href="{{url('mailFiles/'.$asset->path)}}" data-lightbox="image-1" data-title="{{$asset->libelle}}">
											<img src="{{url('mailFiles/'.$asset->path)}}" alt="{{$asset->libelle}}" style="width: 100%">
										</a>
									</span>
									<div class="mailbox-attachment-info">
										<span href="#" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> {{$asset->libelle}}</span>
										<span class="mailbox-attachment-size">
											<a href="{{url('mailFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
											<br>
											@if(Auth::user()->id ==$asset->user_id )
												{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.assetops.destroy', $asset->id], 'method' => 'delete', 'style'=>'display:inline']) }}
												<button type="submit" class="btn btn-danger btn-xs pull-right">
																<i class="fa fa-trash"></i>
															</button>
												{{ Form::close() }}
											@endif
										</span>
									</div>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div role="tabpanel" class="tab-pane fade in" id="creatache">
			{{--@if($debrief->status=='2')--}}
				<div class="col-md-12">
					<div class="tab-content">
						<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">
							@la_access("Taches", "create")
							<button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">
								Créer une tâche
							</button>
							@endla_access

						</div>

						<div class="box box-success">
							<div class="box-body">
								<table id="example1" class="table table-bordered">
									<thead>
									<tr class="success">
										@foreach( $listing_cols_tache as $col )
											<th>{{ $module->fields[$col]['label'] or ucfirst($col) }}</th>
										@endforeach
										@if($show_actions)
											<th>Actions</th>
										@endif
									</tr>
									</thead>
									<tbody>
									@foreach($taches as $k => $tache)
										<tr role="row" class="odd">
											<td class="sorting_1">{{$k+1}}</td>
											<td>
												{{$tache->libelle}}
												{{--<a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id)}}"> </a>--}}
											</td>
											<td>{{$tache->description}}</td>
											<td>{{$tache->userTache->name}}</td>
											<td>
												@if($tache->status=='0')
													<span class="label label-danger">En attente</span>
												@elseif($tache->status=='1')
													<span class="label label-warning">En cours</span>
												@else
													<span class="label label-success">Terminer</span>
												@endif
											</td>
											<td>
												<a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
												@if($tache->status=='0')
												{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.taches.destroy', $tache->id], 'method' => 'delete', 'style'=>'display:inline']) }}
												<button class="btn btn-danger btn-xs" type="submit">
													<i class="fa fa-times"></i>
												</button>
												{{Form::close()}}
												@endif
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>


					</div>
				</div>
			{{--@else--}}
				{{--<div class="col-md-12">--}}
					{{--<div class="tab-content">--}}
						{{--<div class="row bloc-btn-show" style="margin-right: 0px!important;margin-bottom: 5px;">--}}
							{{--<p>--}}
								{{--Le debrief doit être validé avant que vous ne créiez une tache--}}
							{{--</p>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--@endif--}}
		</div>

	</div>
	</div>
	</div>
</div>


@la_access("Taches", "create")
<div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Ajouter une tâche</h4>
			</div>
			{!! Form::open(['action' => 'LA\TachesController@store', 'id' => 'tach-add-form']) !!}
			<div class="modal-body">
				<div class="box-body">
					{{--@la_form($moduleTache)
					@la_input($module, 'status')
					@la_input($module, 'assignant_id')
					@la_input($moduleTache, 'brief_id') @la_input($moduleTache, 'assigne_id')
					--}}

					@la_input($moduleTache, 'libelle')
					@la_input($moduleTache, 'description')

					<div class="form-group">
						<label for="assigne_id">Assigner</label>
						<select name="assigne_id" id="assigne_id" class="form-control">
							@foreach($usersDepartmts as $user)
								<option value="{{$user->id}}">{{$user->name}}</option>
							@endforeach
						</select>
					</div>

					<input type="hidden" name="debrief_id" value="{{$debrief->id}}">
					<input type="hidden" name="assignant_id" value="{{Auth::user()->id}}">
				</div>

				<div class="col-md-12">
					<div id="loader" class="pull-right" style="display: none;">
						<img src="{{ URL::asset('la-assets/img/load.gif')}}">
					</div>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default formaddtache" data-dismiss="modal">FERME</button>
				{!! Form::submit( 'ENREGISTRER', ['class'=>'btn btn-success','onClick'=>'submitTacheForm()','id'=>'formaddtache']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access

@la_access("Mails", "create")
<div class="modal fade" id="AddModalMail" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Ajouter un mail client</h4>
			</div>
			{!! Form::open(['action' => 'LA\MailsController@store', 'id' => 'mail-add-form','files' => true]) !!}
			<div class="modal-body">
				<div class="box-body">
					{{--@la_form($module)@la_input($moduleMail, 'project_id')
                    @la_input($moduleMail, 'debrief_id')@la_input($moduleMail, 'user_id')@la_input($moduleMail, 'path')
                    @la_input($moduleMail, 'ext') --}}

					@la_input($moduleMail, 'libelle')

					<div class="form-group">
						<label for="file"><strong>Pièces jointes</strong></label>
						<input type="file" id="file" name="file" class="dropify"/>
					</div>
					<input type="hidden" name="user_id" value="{{Auth::user()->id}}">
					<input type="hidden" name="debrief_id" value="{{$debrief->id}}">
					<input type="hidden" name="me" value="1">

				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">FERMER</button>
				{!! Form::submit( 'ENREGISTRER', ['class'=>'btn btn-success']) !!}
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endla_access


@endsection


@push('styles')
<link rel="stylesheet" href="{{ asset('la-assets/plugins/dropify/dropify.css') }}">
<link rel="stylesheet" href="{{ asset('la-assets/plugins/lightbox/lightbox.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/lightbox/lightbox.js') }}"></script>
<script src="{{ asset('la-assets/plugins/dropify/dropify.js')}}"></script>
<script>
    $('.dropify').dropify();

    function submitTacheForm() {
        $("#formaddtache").prop('disabled', true);
        $(".formaddtache").prop('disabled', true);
        $('#loader').show();
        //alert("ici");
        $("#tach-add-form").submit();
    }

    $(function () {
        $("#example1").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });

        $("#example2").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Précédent"
                },
            }
        });
    });
</script>
@endpush