@extends('la.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Asset rejeté @endsection
@section('contentheader_title') Asset rejeté @endsection

@section('main-content')
    <section class="content">

        <div class="row">
            @foreach($assetypes as $k=>$assetype)
                @php $result = (new \App\Http\Controllers\AssetsvalidController())->checkAssetValidation($assetype['name']); @endphp
                @if($result)
                    @php $assetValider = (new \App\Http\Controllers\AssetsvalidController())->getAssetReject($assetype['name']); @endphp
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-header">
                                <h4><span class="label label-danger">{{$assetype['libelle']}}</span></h4>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example" class="table table-bordered nbasset">
                                    <thead>
                                    <tr>
                                        <th>Brief</th>
                                        <th>Libelle</th>
                                        <th>Fichier</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($assetValider as $asset)
                                        <tr>
                                            <td>{{$asset->debrief->libelle}}</td>
                                            <td>{{$asset->libelle}}</td>
                                            <td>
                                                <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                @endif
            @endforeach

            @if(Auth::user()->post=='DSTRAT' )
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example" class="table table-bordered nbasset">
                                <thead>
                                <tr>
                                    <th>Project</th>
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingDevis as $asset)
                                    <tr>
                                        <td>{{$asset->project->operation}}</td>
                                        <td>{{$asset->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif

            @if(Auth::user()->post=='CONTROLE')
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example" class="table table-bordered nbasset">
                                <thead>
                                <tr>
                                    <th>Project</th>
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingDevis as $asset)
                                    <tr>
                                        <td>{{$asset->project->operation}}</td>
                                        <td>{{$asset->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif

            @if(Auth::user()->post=='DGA')
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example" class="table table-bordered nbasset">
                                <thead>
                                <tr>
                                    <th>Project</th>
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingDevis as $asset)
                                    <tr>
                                        <td>{{$asset->project->operation}}</td>
                                        <td>{{$asset->libelle}}</td>
                                        <td>
                                            <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif

            @if(Auth::user()->post=='DG' or Auth::user()->post=='SA')
                @foreach($assetypes as $k=>$assetype)
                    @php $assetWaiting = (new \App\Http\Controllers\AssetsvalidController())->getAssetReject($assetype['name']); @endphp
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-header">
                                <h4><span class="label label-danger">{{$assetype['libelle']}}</span></h4>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example" class="table table-bordered nbasset">
                                    <thead>
                                    <tr>
                                        <th>Brief</th>
                                        <th>Libelle</th>
                                        <th>Fichier</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($assetWaiting as $asset)
                                        <tr>
                                            <td>{{$asset->debrief->libelle}}</td>
                                            <td>{{$asset->libelle}}</td>
                                            <td>
                                                <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                @endforeach
            @endif
        </div>


    </section>

@endsection



@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    $.widget.bridge('uibutton', $.ui.button);
    $(".nbasset").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
</script>
@endpush