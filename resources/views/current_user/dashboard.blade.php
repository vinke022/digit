@extends('current_user.layouts.app')

@push('styles')
<style>
  .small-box p {
    font-size: 25px;
  }
</style>
@endpush

@section('htmlheader_title') Tableau de bord  @endsection
@section('contentheader_title') Tableau de bord @endsection

@section('main-content')
  <hr>
  <section class="content">
    <div class="row">
      {{--<h2 class="page-header">Teams</h2>--}}
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-success">
          <div class="inner">
            <h3>Asset validé</h3>
            <p>{{$assetValid}}</p>
          </div>
          <div class="icon">
            <i class="fa fa-check"></i>
          </div>
          <a href="#" class="small-box-footer">{{-- more info <i class="fa fa-arrow-circle-right"></i>--}}</a>
        </div>
      </div>
      <div class="col-md-6">
        <!-- small box -->
        <div class="small-box bg-red">
          <div class="inner">
            <h3>Asset rejeté</h3>
            <p>{{$assetReject}}</p>
          </div>
          <div class="icon">
            <i class="fa fa-times"></i>
          </div>
          <a href="#" class="small-box-footer">{{-- more info <i class="fa fa-arrow-circle-right"></i>--}}</a>
        </div>
      </div>
      {{--<div class="col-md-4">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3>Te assignées</h3>
            <p>10hr 40min</p>
          </div>
          <div class="icon">
            <i class="fa fa-clock-o"></i>
          </div>
          <a href="#" class="small-box-footer">--}}{{-- more info <i class="fa fa-arrow-circle-right"></i>--}}{{--</a>
        </div>
      </div>--}}
    </div>
    <div class="row">
      <div class="col-md-8">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Tâches en cours</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Libelle</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @if(count($taches)>0)
                  @foreach($taches as $tache)
                      <tr>
                        <td>{{$tache->id}}</td>
                        <td><a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id)}}">{{$tache->libelle}}</a></td>
                        <td><span class="text-success">En cours</span></td>
                      </tr>
                  @endforeach
                @else
                  <tr><td>Aucune tâche en cours</td></tr>
                @endif
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            {{--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>--}}
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-4">
        <!-- TABLE: LATEST ORDERS -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Tâches assignées </h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive">
              <table class="table no-margin">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Libelle</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @if(count($tachesAssign)>0)
                  @foreach($tachesAssign as $tache)
                    <tr>
                      <td>{{$tache->id}}</td>
                      <td><a href="{{url(config('laraadmin.adminRoute') . '/taches/'.$tache->id)}}">{{$tache->libelle}}</a></td>
                      <td><span class="text-success">En attente</span></td>
                    </tr>
                  @endforeach
                @else
                  <tr><td>Aucune tâche en cours</td></tr>
                @endif
                </tbody>
              </table>
            </div>
            <!-- /.table-responsive -->
          </div>
          <!-- /.box-body -->
          <div class="box-footer clearfix">
            {{--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>--}}
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
    </div>

  </section>
@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')

@endpush