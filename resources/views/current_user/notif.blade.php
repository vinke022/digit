@extends('current_user.layouts.app')

@push('styles')
<style>
    .notif ul li {
        list-style: none;
        padding: 10px;
        margin-bottom: 6px;
        border-bottom: 1px solid #dddfe2;
    }
    .notif ul {
        padding: 10px;
        list-style-type: none;
        background-color: #fff;
        margin: 0;
        padding: 0;
    }
    .post-meta{
        margin-left: 15px;
    }

    .pas_vue{
        background-color: #dbdee2;
    }
</style>
@endpush

@section('htmlheader_title') Notification @endsection
@section('contentheader_title') Notification @endsection

@section('main-content')
    <section class="content">

        <div class="row notif">
            <div class="col-md-offset-1 col-md-10">
                <!-- /.box-header -->
                <ul>
                    @php $assetypes = (new \App\Http\Controllers\AssetsvalidController())->typAssetForVue(); @endphp
                    @if(count($notifs)>0)
                        @foreach($notifs as $notif)
                            @if($notif->libelle == 'brief_a_valide')
                                <li class="<?=$notif->vue == '0' ? 'pas_vue' : '' ?>">
                                    <i class="fa fa-bars"></i> <b>BRIEF</b> - Brief en attente de validation
                                    <div class="post-meta">
                                        <span> <i class="fa fa-clock-o"></i> {{date("d M Y H:i:s",strtotime($notif->created_at))}} </span>
                                        {{--<br><a href="#"><i class="fa fa-eye"></i> Voir plus</a>--}}
                                        <br><a href="{{url('/')}}"><i class="fa fa-eye"></i> Voir plus</a>
                                    </div>

                                </li>
                            @endif


                            @if($notif->libelle == 'tache_assigne')
                                <li class="<?=$notif->vue == '0' ? 'pas_vue' : '' ?>">
                                    <i class="fa fa-tasks"></i> <b>Tâche</b> - Une tâche vous êtes assignée
                                    <div class="post-meta">
                                        <span> <i class="fa fa-clock-o"></i> {{date("d M Y H:i:s",strtotime($notif->created_at))}} </span>
{{--                                            <br><a href="{{url(config('laraadmin.adminRoute'). '/user/assigntach') }}"><i class="fa fa-eye"></i> Voir plus</a>--}}
                                        <br><a href="{{url('/')}}"><i class="fa fa-eye"></i> Voir plus</a>
                                    </div>

                                </li>
                            @endif

                            @if($notif->libelle == 'brief_rejete')
                                <li class="<?=$notif->vue == '0' ? 'pas_vue' : '' ?>">
                                    <i class="fa fa-tasks"></i> <b>BRIEF</b> - Le brief a été rejeté
                                    <div class="post-meta">
                                        <span> <i class="fa fa-clock-o"></i> {{date("d M Y H:i:s",strtotime($notif->created_at))}} </span>
                                        {{--<br><a href="#"><i class="fa fa-eye"></i> Voir plus</a>--}}
                                        <br><a href="{{url('/')}}"><i class="fa fa-eye"></i> Voir plus</a>
                                    </div>

                                </li>
                            @endif

                            @if($notif->libelle == 'brief_valide')
                                <li class="<?=$notif->vue == '0' ? 'pas_vue' : '' ?>">
                                    <i class="fa fa-tasks"></i> <b>BRIEF</b> - Le brief a été accepté
                                    <div class="post-meta">
                                        <span> <i class="fa fa-clock-o"></i> {{date("d M Y H:i:s",strtotime($notif->created_at))}} </span>
                                        {{--<br><a href="#"><i class="fa fa-eye"></i> Voir plus</a>--}}
                                        <br><a href="{{url('/')}}"><i class="fa fa-eye"></i> Voir plus</a>
                                    </div>

                                </li>
                            @endif

                            @foreach($assetypes as $assetype)

                                @if($notif->libelle == $assetype['name'].'_valide')
                                    <li class="<?=$notif->vue == '0' ? 'pas_vue' : '' ?>">
                                        <i class="fa fa-bars"></i> <b>{{$assetype['libelle']}}</b> - {{$assetype['libelle']}} a été validé
                                        <div class="post-meta">
                                            <span> <i class="fa fa-clock-o"></i> {{date("d M Y H:i:s",strtotime($notif->created_at))}} </span>
                                            <br><a href="{{url('/')}}"><i class="fa fa-eye"></i> Voir plus</a>
                                        </div>
                                    </li>
                                @endif

                                @if($notif->libelle == $assetype['name'].'_rejete')
                                    <li class="<?=$notif->vue == '0' ? 'pas_vue' : '' ?>">
                                        <i class="fa fa-bars"></i> <b>{{$assetype['libelle']}}</b> - {{$assetype['libelle']}} a été rejeté
                                        <div class="post-meta">
                                            <span> <i class="fa fa-clock-o"></i> {{date("d M Y H:i:s",strtotime($notif->created_at))}} </span>
                                            <br><a href="{{url('/')}}"><i class="fa fa-eye"></i> Voir plus</a>
                                        </div>
                                    </li>
                                @endif
                            @endforeach


                        @endforeach
                    @else
                        <p style="padding: 5px">Aucune notification pour le moment...</p>
                    @endif
                </ul>
                <!-- /. box -->
            </div>
        </div>

    </section>
@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- ChartJS -->
<script src="{{ asset('la-assets/plugins/chartjs/Chart.js') }}"></script>
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')

@endpush