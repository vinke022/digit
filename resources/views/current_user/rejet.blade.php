@extends('current_user.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Asset rejeté @endsection
@section('contentheader_title') Asset rejeté @endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Libelle</th>
                            <th>Type d'asset</th>
                            <th>Observation</th>
                            <th>Utilisateur</th>
                            <th>Fichier</th>
                            {{--<th>Action</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($tachesRejet)>0)
                            @foreach($tachesRejet as $tache)
                                <tr>
                                    <td><a href="javascript:void(0)">{{$tache->id}}</a></td>
                                    <td><a href="{{url(config('laraadmin.adminRoute') .'/taches/'. $tache->tache_id.'#terminer-tache')}}">{{$tache->libelle}} </a></td>
                                    <td>{{ucfirst($tache->asset_type)}}</td>
                                    <td>
                                        {!! nl2br($tache->lastReject[0]->observe) !!}
                                    </td>
                                    <td>
                                        {{(new \App\Http\Controllers\LA\DashboardController())->itemUser($tache->lastReject[0]->user_id)}}
                                    </td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$tache->path)}}" download="{{$tache->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    {{--<td>
                                        <a href="{{url(config('laraadmin.adminRoute') . '/brief/waiting/'.$asset->id.'/valid')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModal" href="#" onclick="displayModal('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                    </td>--}}
                                </tr>
                            @endforeach
                        @else
                            <tr><td>Aucun asset rejeté </td></tr>
                        @endif
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>
@endsection



@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Pr&eacute;c&eacute;dent"
            },
        }
    });

    function displayModal(id){
        $("#formType")[0].reset();
        $("#idbrief").val(id);
    }

</script>
@endpush
