@extends('head_department.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Asset en attente de validation @endsection
@section('contentheader_title') Asset en attente de validation @endsection

@section('main-content')
    <section class="content">

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">PLANNING PUBLICATION</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingPlanning as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <a href="{{url(config('laraadmin.adminRoute') . '/dashboard/cpd/asset/waiting/'.$asset->id.'/valid/plan')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModalPlan" href="#" onclick="displayModalPlan('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">VISUEL GOOGLE ADS</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingGoogle as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <a href="{{url(config('laraadmin.adminRoute') . '/dashboard/cpd/asset/waiting/'.$asset->id.'/valid/ggle')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModalGgle" href="#" onclick="displayModalGgle('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">RAPPORT</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example3" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingRapport as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <a href="{{url(config('laraadmin.adminRoute') . '/dashboard/cpd/asset/waiting/'.$asset->id.'/valid/rapport')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModalRapport" href="#" onclick="displayModalRapport('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">RAPPORT TRIMESTRIEL</span></h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example4" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingRapportTrim as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <a href="{{url(config('laraadmin.adminRoute') . '/dashboard/cpd/asset/waiting/'.$asset->id.'/valid/rapportrim')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModalRapportrim" href="#" onclick="displayModalRapportrim('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>



    </section>

    <div class="modal fade" id="versionModalPlan" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypePlan" method="post" action="{{url(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/reject/plan')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="idplan" name="idplan"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalGgle" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeGgle" method="post" action="{{url(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/reject/ggle')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="ggleid" name="ggleid"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalRapport" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeRapport"  method="post" action="{{url(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/reject/rapport')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="idrapport" name="idrapport"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalRapportrim" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeRapportrim"  method="post" action="{{url(config('laraadmin.adminRoute'). '/dashboard/cpd/asset/reject/rapportrim')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="idrapportrim" name="idrapportrim"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection



@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example2").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example3").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example4").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example5").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example6").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example7").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example8").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

    function displayModalPlan(id){
        $("#formTypePlan")[0].reset();
        $("#idplan").val(id);
    }

    function displayModalGgle(id){
        $("#formTypeGgle")[0].reset();
        $("#ggleid").val(id);
    }

    function displayModalRapport(id){
        $("#formTypeRapport")[0].reset();
        $("#idrapport").val(id);
    }

    function displayModalRapportrim(id){
        $("#formTypeRapportrim")[0].reset();
        $("#idrapportrim").val(id);
    }



</script>
@endpush