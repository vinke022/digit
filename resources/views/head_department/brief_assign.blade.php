@extends('head_department.layouts.app')

@push('styles')
<style>

</style>
@endpush

@section('htmlheader_title') Briefs et Debriefs assignés @endsection
@section('contentheader_title') Briefs et Debriefs assignés @endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Annonceur</th>
                            <th>Project</th>
                            <th>Libelle</th>
                            <th>Travail attendu </th>
                            <th>Date Entrée</th>
                            <th>Date Fin</th>
                            <th>Pièce jointe</th>
                            @if(Auth::user()->post=='RSTRAT' or Auth::user()->post=='DCOMM')
                            <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($briefAssigne)>0)
                            @foreach($briefAssigne as $brief)
                                <tr>
                                    <td><a href="javascript:void(0)">{{ (new \App\Http\Controllers\HeadDepartmentController)->showAnnonceurPjt($brief->project->annonceur)}}</a></td>
                                    <td>{{$brief->project->operation}}</td>
                                    <td>
                                        <a href="{{url(config('laraadmin.adminRoute') . '/debriefs/'.$brief->id.'#tab-info')}}">
                                            {{$brief->libelle}}
                                        </a>
                                    </td>
                                    <td>
                                        @php $tab= explode(' ', strip_tags($brief->taf), 16);unset($tab[15]); @endphp
                                        {{implode(' ', $tab).'...'}}
                                        {{--{!! nl2br($brief->taf) !!}--}}
                                    </td>
                                    <td>{{date("d/m/Y",strtotime($brief->dateDentree))}}</td>
                                    <td>{{date("d/m/Y",strtotime($brief->dateDeSortie))}}</td>
                                    <td>
                                        @if($brief->pj)
                                            <a href="{{url('briefFiles/'.$brief->pj)}}" download="{{$brief->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        @else
                                            Aucune pièce jointe
                                        @endif
                                    </td>
                                    @if(Auth::user()->post=='DCOMM')
                                    <td>
                                        @if($brief->status=='3')
                                        <a href="{{url(config('laraadmin.adminRoute') . '/dashboard/brief/waiting/'.$brief->id.'/valid')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModal" href="#" onclick="displayModal('{{$brief->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        @elseif($brief->status=='0')
                                            <span class="label label-danger">Debrief rejeté</span>
                                        @else
                                            <span class="label label-success">Debrief accepté</span>
                                        @endif
                                    </td>
                                    @endif

                                    @if(Auth::user()->post=='RSTRAT')
                                        <td>
                                            <a href="{{url(config('laraadmin.adminRoute') . '/dashboard/brief/'.$brief->id.'/reassignation')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i> Requalifier le brief</a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <tr><td>Aucun Briefs assignés</td></tr>
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>

    <div class="modal fade" id="versionModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formType"  method="post" action="{{url(config('laraadmin.adminRoute'). '/dashboard/brief/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="idbrief" name="idbrief"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger  pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Pr&eacute;c&eacute;dent"
            },
        }
    });

    function displayModal(id){
        $("#formType")[0].reset();
        $("#idbrief").val(id);
    }

</script>
@endpush
