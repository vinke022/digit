@extends('head_department.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Editer une tâche assignée @endsection
@section('contentheader_title') Editer une tâche assignée @endsection

@section('main-content')
    <section class="content">
        <div class="box">
            <div class="box-header">

            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <form method="POST" action="{{ url(config('laraadmin.adminRoute') . '/dashboard/tacheAssign/update') }}" accept-charset="UTF-8">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="assigne_id">Assigner</label>
                                <input class="form-control" readonly name="libelle" type="text" value="{{$taches->libelle}}">
                            </div>
                            <div class="form-group">
                                <label for="assigne_id">Assigner</label>
                                <textarea class="form-control" readonly cols="30" rows="3" name="description">{{$taches->description}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="assigne_id">Assigner</label>
                                <select name="assigne_id" id="assigne_id" class="form-control">
                                    @foreach($usersDepartmts as $user)
                                        <option value="{{$user->id}}" <?= $taches->assigne_id==$user->id ? "selected" : "" ?>>{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="id" value="{{$taches->id}}">
                            <input type="hidden" name="debrief_id" value="{{$taches->debrief_id}}">
                            <input type="hidden" name="assignant_id" value="{{$taches->assignant_id}}">
                            <br>
                            <div class="form-group">
                                <input class="btn btn-success" type="submit" value="MISE A JOUR">
                                <button class="btn btn-default pull-right">
                                    <a href="{{url('/')}}">ANNULER</a>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection



@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Pr&eacute;c&eacute;dent"
            },
        }
    });

    function displayModal(id){
        $("#formType")[0].reset();
        $("#idbrief").val(id);
    }

</script>
@endpush
