<ul class="sidebar-menu">
    {{--<li class="header">MODULES</li>--}}
    <!-- Optionally, you can add icons to the links -->
    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/dashboard/trafic') }}">
            <i class='fa fa-home'></i> <span>Tableau de bord</span>
        </a>
    </li>

    <li class="treeview">
        <a href="#"><i class="fa fa-folder"></i> <span>Job Bag</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li class="treeview">
                <a href="#"><i class="fa fa-folder"></i> <span>Job Bag List</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute'). '/projets_list') }}">
                            <i class='fa fa-folder'></i>
                            <span>list</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-folder"></i> <span>Phase</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute'). '/projets') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 1</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/projets_second') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 2</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/projets_third') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 3</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/dashboard/brief/assign') }}">
            <i class='fa fa-bars'></i>
            <span>Briefs et Debriefs assignés</span>
        </a>
    </li>

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/trafic/tacheassign') }}">
            <i class="fa fa-edit"></i> <span>Tâches assignées</span>
        </a>
    </li>

    <li class="treeview">
        <a href="#"><i class="fa fa-file"></i> <span>Asset</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_waiting') }}">
                    <i class='fa fa-file'></i>
                    <span>Asset en attente</span>
                </a>
            </li>

            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_valider') }}">
                    <i class='fa fa-file'></i>
                    <span>Asset validé</span>
                </a>
            </li>

            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_rejeter') }}">
                    <i class='fa fa-file'></i>
                    <span>Asset rejeté</span>
                </a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/asset/digital') }}">
            <i class='fa fa-file'></i>
            <span>Asset Digital</span>
        </a>
    </li>

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/planning') }}">
            <i class='fa fa-calendar'></i>
            <span>Planning</span>
        </a>
    </li>

    <li>
        <a href="{{url(config('laraadmin.adminRoute').'/bibliotheques')}}">
            <i class='fa fa-book'></i>
            <span>Bibliothèques</span>
        </a>
    </li>
    <!-- LAMenus -->
</ul>