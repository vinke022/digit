<ul class="sidebar-menu">
    {{--<li class="header">MODULES</li>--}}
    <!-- Optionally, you can add icons to the links -->
    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/dashboard') }}">
            <i class='fa fa-home'></i> <span>Tableau de bord</span>
        </a>
    </li>
    <li class="treeview">
        <a href="#"><i class="fa fa-folder"></i> <span>Job Bag</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li class="treeview">
                <a href="#"><i class="fa fa-folder"></i> <span>Job Bag List</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute'). '/projets_list') }}">
                            <i class='fa fa-folder'></i>
                            <span>list</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-folder"></i> <span>Phase</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute'). '/projets') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 1</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/projets_second') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 2</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/projets_third') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 3</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/dashboard/brief/assign') }}">
            <i class='fa fa-bars'></i>
            <span>Briefs et Debriefs assignés</span>
        </a>
    </li>

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/dashboard/tacheAssign') }}">
            <i class='fa fa-edit'></i>
            <span>Tâches assignées</span>
        </a>
    </li>

    @if(in_array(Auth::user()->post,array('DA','OP','DCREA','RSTRAT','RD','CPD')))
    {{--@if(Auth::user()->post=='DA' or Auth::user()->post=='OP' or Auth::user()->post=='DCREA' or Auth::user()->post=='RSTRAT' or Auth::user()->post=='RD' or Auth::user()->post=='CPD')--}}
        <li class="treeview">
            <a href="#"><i class="fa fa-file"></i> <span>ASSET</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu" style="display: none;">
                <li>
                    <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_waiting') }}">
                        <i class='fa fa-file'></i>
                        <span>Asset a valider</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url(config('laraadmin.adminRoute').'/dashboard/myasset/rejet') }}">
                        <i class='fa fa-file'></i>
                        <span>Mes assets rejetés</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    {{--@if(Auth::user()->post=='OP')--}}
        {{--<li class="treeview">--}}
            {{--<a href="#"><i class="fa fa-file"></i> <span>ASSET</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
            {{--<ul class="treeview-menu" style="display: none;">--}}
                {{--<li>--}}
                    {{--<a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_waiting') }}">--}}
                        {{--<i class='fa fa-file'></i>--}}
                        {{--<span>Asset a valider</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="{{ url(config('laraadmin.adminRoute').'/dashboard/myasset/rejet') }}">--}}
                        {{--<i class='fa fa-file'></i>--}}
                        {{--<span>Mes assets rejetés</span>--}}
                    {{--</a>--}}
                {{--</li>--}}

                {{--<li>--}}
                    {{--<a href="{{ url(config('laraadmin.adminRoute').'/marketing/asset/valid') }}">--}}
                        {{--<i class='fa fa-file'></i>--}}
                        {{--<span>Asset validé</span>--}}
                    {{--</a>--}}
                {{--</li>--}}

                {{--<li>--}}
                    {{--<a href="{{ url(config('laraadmin.adminRoute').'/marketing/asset/reject') }}">--}}
                        {{--<i class='fa fa-file'></i>--}}
                        {{--<span>Asset rejeté</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
    {{--@endif--}}

    @if(Auth::user()->post=='CPD')
        {{--<li class="treeview">--}}
            {{--<a href="#"><i class="fa fa-file"></i> <span>ASSET</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
            {{--<ul class="treeview-menu" style="display: none;">--}}
                {{--<li>--}}
                    {{--<a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_waiting') }}">--}}
                        {{--<i class='fa fa-file'></i>--}}
                        {{--<span>Asset a valider</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="{{ url(config('laraadmin.adminRoute').'/dashboard/myasset/rejet') }}">--}}
                        {{--<i class='fa fa-file'></i>--}}
                        {{--<span>Mes assets rejetés</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
        <li class="treeview">
            <a href="#"><i class="fa fa-file"></i> <span>ASSET DIGITAL</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu" style="display: none;">
                <li>
                    <a href="{{ url(config('laraadmin.adminRoute').'/dashboard/cpd/digit') }}">
                        <i class='fa fa-file'></i>
                        <span>Assets</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url(config('laraadmin.adminRoute').'/dashboard/cpd/digit/createtache') }}">
                        <i class='fa fa-file'></i>
                        <span>Créer une tâche</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    {{--@if(Auth::user()->post=='RD')--}}
        {{--<li class="treeview">--}}
            {{--<a href="#"><i class="fa fa-file"></i> <span>ASSET</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
            {{--<ul class="treeview-menu" style="display: none;">--}}
                {{--<li>--}}
                    {{--<a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_waiting') }}">--}}
                        {{--<i class='fa fa-file'></i>--}}
                        {{--<span>Asset a valider</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="{{ url(config('laraadmin.adminRoute').'/dashboard/myasset/rejet') }}">--}}
                        {{--<i class='fa fa-file'></i>--}}
                        {{--<span>Mes assets rejetés</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</li>--}}
    {{--@endif--}}

    {{--@if(Auth::user()->post=='RSTRAT')--}}
        {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-file"></i> <span>ASSET</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
                {{--<ul class="treeview-menu" style="display: none;">--}}
                    {{--<li>--}}
                        {{--<a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_waiting') }}">--}}
                            {{--<i class='fa fa-file'></i>--}}
                            {{--<span>Asset a valider</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="{{ url(config('laraadmin.adminRoute').'/dashboard/myasset/rejet') }}">--}}
                            {{--<i class='fa fa-file'></i>--}}
                            {{--<span>Mes assets rejetés</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
    {{--@endif--}}

    @if(Auth::user()->post!='CPD')
        <li>
            <a href="{{ url(config('laraadmin.adminRoute').'/asset/digital') }}">
                <i class='fa fa-file'></i>
                <span>Asset Digital</span>
            </a>
        </li>
    @endif

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/dashboard/users_dep') }}">
            <i class='fa fa-users'></i>
            <span>Employées</span>
        </a>
    </li>

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/planning') }}">
            <i class='fa fa-calendar'></i>
            <span>Planning</span>
        </a>
    </li>

    <li>
        <a href="{{url(config('laraadmin.adminRoute').'/bibliotheques')}}">
            <i class='fa fa-book'></i>
            <span>Bibliothèques</span>
        </a>
    </li>
{{--<li>
    <a href="{{ url(config('laraadmin.adminRoute')) }}">
        <i class='fa fa-users'></i>
        <span>Arret maladie</span>
    </a>
</li>
<li>
    <a href="{{ url(config('laraadmin.adminRoute')) }}">
        <i class='fa fa-users'></i>
        <span>Demande de permission</span>
    </a>
</li>--}}
<!-- LAMenus -->
</ul>