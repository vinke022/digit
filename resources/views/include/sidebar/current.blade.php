<ul class="sidebar-menu">
    {{--<li class="header">MODULES</li>--}}
    <!-- Optionally, you can add icons to the links -->
    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/dashboard/user') }}">
            <i class='fa fa-home'></i> <span>Tableau de bord</span>
        </a>
    </li>
    {{-- <li>
         <a href="#">
             <i class="fa fa-folder"></i> <span>Job Bag List</span>
         </a>
     </li>--}}
    <li class="treeview">
        <a href="#"><i class="fa fa-bars"></i> <span>Tâches</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/user/assigntach') }}">
                    <i class="fa fa-bars"></i> <span>Tâches assignées</span>
                </a>
            </li>
            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/user/inprogresstach') }}">
                    <i class="fa fa-bars"></i> <span>Tâches en cours</span>
                </a>
            </li>
            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/user/attent/finish') }}">
                    <i class="fa fa-bars"></i> <span>Tâches terminer en validation</span>
                </a>
            </li>
            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/user/finish') }}">
                    <i class="fa fa-bars"></i> <span>Tâches terminées</span>
                </a>
            </li>
        </ul>
    </li>


    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/user/rejet') }}">
            <i class="fa fa-file"></i> <span>Asset Rejeté</span>
        </a>
    </li>

    @if(Auth::user()->departement_id==5)
        <li>
            <a href="{{ url(config('laraadmin.adminRoute').'/asset/digital') }}">
                <i class='fa fa-file'></i>
                <span>Asset Digital</span>
            </a>
        </li>
    @endif

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/planning') }}">
            <i class='fa fa-calendar'></i>
            <span>Planning</span>
        </a>
    </li>

    <li>
        <a href="{{url(config('laraadmin.adminRoute').'/bibliotheques')}}">
            <i class='fa fa-book'></i>
            <span>Bibliothèques</span>
        </a>
    </li>
    <!-- LAMenus -->
</ul>