<ul class="sidebar-menu">
    {{--<li class="header">MODULES</li>--}}
    <!-- Optionally, you can add icons to the links -->
    <li>
        <a href="{{ url('/')}}">
            <i class='fa fa-home'></i> <span>Tableau de bord</span>
        </a>
    </li>
    {{--<li>--}}
    {{--<a href="{{ url(config('laraadmin.adminRoute'). '/projets') }}">--}}
    {{--<i class="fa fa-folder"></i> <span>Job Bag List</span>--}}
    {{--</a>--}}
    {{--</li>--}}

    <li class="treeview">
        <a href="#"><i class="fa fa-folder"></i> <span>Job Bag</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li class="treeview">
                <a href="#"><i class="fa fa-folder"></i> <span>Job Bag List</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute'). '/projets_list') }}">
                            <i class='fa fa-folder'></i>
                            <span>list</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-folder"></i> <span>Phase</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute'). '/projets') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 1</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/projets_second') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 2</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/projets_third') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 3</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/planning') }}">
            <i class='fa fa-calendar'></i>
            <span>Planning</span>
        </a>
    </li>

    <li class="treeview">
        <a href="#"><i class="fa fa-bars"></i> <span>Brief & Debrief</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li>
                <a href="{{ url(config('laraadmin.adminRoute'). '/marketing/brief/attente') }}">
                    <i class="fa fa-bars"></i> <span>Brief & Debrief en attente</span>
                </a>
            </li>

            <li>
                <a href="{{ url(config('laraadmin.adminRoute'). '/marketing/brief/valid') }}">
                    <i class="fa fa-bars"></i> <span>brief & Debrief valider</span>
                </a>
            </li>

            <li>
                <a href="{{ url(config('laraadmin.adminRoute'). '/marketing/brief/reject') }}">
                    <i class="fa fa-bars"></i> <span>Brief & Debrief rejeter</span>
                </a>
            </li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#"><i class="fa fa-file"></i> <span>Devis & Proforma</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li>
                <a href="{{ url(config('laraadmin.adminRoute'). '/marketing/asset/attente') }}">
                    <i class="fa fa-file"></i> <span>Devis & Proforma en attente</span>
                </a>
            </li>

            <li>
                <a href="{{ url(config('laraadmin.adminRoute'). '/marketing/asset/valid') }}">
                    <i class="fa fa-file"></i> <span>Devis & Proforma valider</span>
                </a>
            </li>

            <li>
                <a href="{{ url(config('laraadmin.adminRoute'). '/marketing/asset/reject') }}">
                    <i class="fa fa-file"></i> <span>Devis & Proforma rejeter</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-file-o"></i> <span>Bon de commande</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/marketing/bc/waiting') }}">
                            <i class='fa fa-file-o'></i>
                            <span>BC en attente</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/marketing/bc/valid') }}">
                            <i class='fa fa-file-o'></i>
                            <span>BC validé</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/marketing/bc/reject') }}">
                            <i class='fa fa-file-o'></i>
                            <span>BC rejeté</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#"><i class="fa fa-file"></i> <span>ASSET</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_waiting') }}">
                    <i class='fa fa-file'></i>
                    <span>Asset en attente</span>
                </a>
            </li>

            {{--<li>
                <a href="{{ url(config('laraadmin.adminRoute').'/marketing/asset/valid') }}">
                    <i class='fa fa-file'></i>
                    <span>Asset validé</span>
                </a>
            </li>

            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/marketing/asset/reject') }}">
                    <i class='fa fa-file'></i>
                    <span>Asset rejeté</span>
                </a>
            </li>--}}
        </ul>
    </li>

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/asset/digital') }}">
            <i class='fa fa-file'></i>
            <span>Asset Digital</span>
        </a>
    </li>

    <li>
        <a href="{{url(config('laraadmin.adminRoute').'/marketing/annonceurs')}}">
            <i class='fa fa-users'></i>
            <span>Annonceurs</span>
        </a>
    </li>


        <li>
        <a href="{{url(config('laraadmin.adminRoute').'/bibliotheques')}}">
            <i class='fa fa-book'></i>
            <span>Bibliothèques</span>
        </a>
    </li>

    <!-- LAMenus -->
</ul>