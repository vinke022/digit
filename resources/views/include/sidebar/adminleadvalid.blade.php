<ul class="sidebar-menu">
    {{--<li class="header">MODULES</li>--}}
    <!-- Optionally, you can add icons to the links -->
    <li>
        <a href="{{ url('/') }}">
            <i class='fa fa-home'></i> <span>Tableau de bord</span>
        </a>
    </li>
    <li class="treeview">
        <a href="#"><i class="fa fa-folder"></i> <span>Job Bag</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li class="treeview">
                <a href="#"><i class="fa fa-folder"></i> <span>Job Bag List</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute'). '/admin/projets_list') }}">
                            <i class='fa fa-folder'></i>
                            <span>list</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-folder"></i> <span>Phase</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute'). '/projets') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 1</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/projets_second') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 2</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/projets_third') }}">
                            <i class='fa fa-folder'></i>
                            <span>Phase 3</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </li>
    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/planning') }}">
            <i class='fa fa-calendar'></i>
            <span>Planning</span>
        </a>
    </li>
    {{--<li class="treeview">--}}
        {{--<a href="#"><i class="fa fa-bars"></i> <span>Brief & Debrief</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
        {{--<ul class="treeview-menu" style="display: none;">--}}
            {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-bars"></i> <span>Brief</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
                {{--<ul class="treeview-menu" style="display: none;">--}}
                    {{--<li>--}}
                        {{--<a href="{{ url(config('laraadmin.adminRoute').'/briefwaitg/waitingsbrief') }}">--}}
                            {{--<i class='fa fa-bars'></i>--}}
                            {{--<span>Brief</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="treeview">--}}
                {{--<a href="#"><i class="fa fa-bars"></i> <span>Debrief</span> <i class="fa fa-angle-left pull-right"></i></a>--}}
                {{--<ul class="treeview-menu" style="display: none;">--}}
                    {{--<li>--}}
                        {{--<a href="{{ url(config('laraadmin.adminRoute').'/brief/waiting') }}">--}}
                            {{--<i class='fa fa-bars'></i>--}}
                            {{--<span>Debrief en attente</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                        {{--<a href="{{ url(config('laraadmin.adminRoute').'/brief/inprogress') }}">--}}
                            {{--<i class='fa fa-bars'></i>--}}
                            {{--<span>Debrief en cours</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                        {{--<a href="{{ url(config('laraadmin.adminRoute').'/brief/rejected') }}">--}}
                            {{--<i class='fa fa-bars'></i>--}}
                            {{--<span>Debrief rejeté</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        {{--</ul>--}}
    {{--</li>--}}

    <li class="treeview">
        <a href="#"><i class="fa fa-file"></i> <span>Asset</span> <i class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu" style="display: none;">
            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_waiting') }}">
                    <i class='fa fa-file'></i>
                    <span>Asset en attente</span>
                </a>
            </li>

            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_valider') }}">
                    <i class='fa fa-file'></i>
                    <span>Asset validé</span>
                </a>
            </li>

            <li>
                <a href="{{ url(config('laraadmin.adminRoute').'/assign/asset_rejeter') }}">
                    <i class='fa fa-file'></i>
                    <span>Asset rejeté</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-file-o"></i> <span>Bon de commande</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu" style="display: none;">
                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/bc/waiting') }}">
                            <i class='fa fa-file-o'></i>
                            <span>BC en attente</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/bc/valid') }}">
                            <i class='fa fa-file-o'></i>
                            <span>BC validé</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url(config('laraadmin.adminRoute').'/bc/reject') }}">
                            <i class='fa fa-file-o'></i>
                            <span>BC rejeté</span>
                        </a>
                    </li>
                </ul>
            </li>

            {{--<li>--}}
                {{--<a href="{{ url(config('laraadmin.adminRoute').'/devis/attent/del') }}">--}}
                    {{--<i class='fa fa-file'></i>--}}
                    {{--<span>Devis&Proforma en Suppression</span>--}}
                {{--</a>--}}
            {{--</li>--}}

        </ul>
    </li>

    <li>
        <a href="{{ url(config('laraadmin.adminRoute').'/asset/digital') }}">
            <i class='fa fa-file'></i>
            <span>Asset Digital</span>
        </a>
    </li>

    {{--<li>
        <a href="{{ url(config('laraadmin.adminRoute'). '/niveau3/waiting') }}">
            <i class='fa fa-refresh'></i>
            <span>Projet Niveau 3 en attente</span>
        </a>
    </li>--}}

    <li>
        <a href="{{url(config('laraadmin.adminRoute').'/bibliotheques')}}">
            <i class='fa fa-book'></i>
            <span>Bibliothèques</span>
        </a>
    </li>

    <li>
        <a href="{{url(config('laraadmin.adminRoute').'/annonceurs')}}">
            <i class='fa fa-users'></i>
            <span>Annonceurs</span>
        </a>
    </li>


    <!-- LAMenus -->
</ul>