<!--AFFICHER UN SET_FLASH DE SUCCESS-->
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
        <h4><i class="icon fa fa-check"></i> Success !</h4>
        {{Session::get('success')}}
    </div>
@endif
<!--AFFICHER UN SET_FLASH DE SUCCESS-->

<!--AFFICHER UN SET_FLASH DE ERRORS-->
@if(Session::has('error'))
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="color: #fff">×</span></button>
        <h4><i class="icon fa fa-ban"></i> Alert !</h4>
        {{Session::get('error')}}
    </div>
@endif
<!--AFFICHER UN SET_FLASH DE ERROS-->

@if($errors->any())
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" style="color: #fff">×</span></button>
        @foreach($errors->all() as $errorr)
            {{ $errorr }}<br/>
        @endforeach
    </div>
@endif