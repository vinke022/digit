@extends('marketing.layouts.app')

@push('styles')
<style>
  .small-box p {
    font-size: 20px;
  }
</style>
@endpush

@section('htmlheader_title') Tableau de bord  @endsection
@section('contentheader_title') Tableau de bord  @endsection

@section('main-content')
  <hr>
  <section class="content">
    @if(Auth::user()->post!='DCOMM')
      <div class="row">

        <div class="col-md-8">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Brief et Debrief en attente de validation</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                    <tr>
                      <th>Annonceur</th>
                      <th>Project</th>
                      <th>Libelle</th>
                      <th>Date Entrée</th>
                      <th>Date Fin</th>
                      <th>Departement</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($allbriefsAttente)>0)
                  @foreach($allbriefsAttente as $attent)
                    <tr>
                      <td><a href="javascript:void(0)">{{ (new \App\Http\Controllers\MarketingController)->showAnnonceurPjt($attent->project->annonceur)}}</a></td>
                      <td>{{$attent->project->operation}}</td>
                      <td>{{$attent->libelle}}</td>
                      <td>{{date("d/m/Y",strtotime($attent->dateDentree))}}</td>
                      <td>{{date("d/m/Y",strtotime($attent->dateDeSortie))}}</td>
                      <td>
                        @if($attent->dep=='1')
                          Administration
                        @elseif($attent->dep=='2')
                          Service MEDIA
                        @elseif($attent->dep=='3')
                          Service Crea/Strat
                        @elseif($attent->dep=='5')
                          Service Digital
                        @elseif($attent->dep=='6')
                          Service Digitalisation
                        @elseif($attent->dep=='7')
                          Service Commercial
                        @elseif($attent->dep=='8')
                          Service Opérationnel
                        @else
                          Service Trafic
                        @endif
                      </td>
                      <td><span class="label label-warning">En attente</span></td>
                    </tr>
                  @endforeach
                  @else
                    <tr><td>Aucun Brief ou Debrief en cours de validation</td></tr>
                  @endif
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            @if(count($allbriefsAttente)>0)
            <div class="box-footer clearfix">
              <a href="{{url(config('laraadmin.adminRoute'). '/marketing/brief/attente')}}" class="btn btn-sm btn-info btn-success pull-left"><i class="fa fa-eye"></i> Consultés les Brief et Debrief en attente de validation</a>
            </div>
            @endif
            <!-- /.box-footer -->
          </div>

          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Brief et Debrief rejeté</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Annonceur</th>
                    <th>Project</th>
                    <th>Libelle</th>
                    <th>Date Entrée</th>
                    <th>Date Fin</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if(count($allbriefsRejects)>0)
                    @foreach($allbriefsRejects as $attent)
                      <tr>
                        <td><a href="javascript:void(0)">{{ (new \App\Http\Controllers\MarketingController)->showAnnonceurPjt($attent->project->annonceur)}}</a></td>
                        <td>{{$attent->project->operation}}</td>
                        <td>{{$attent->libelle}}</td>
                        <td>{{date("d/m/Y",strtotime($attent->dateDentree))}}</td>
                        <td>{{date("d/m/Y",strtotime($attent->dateDeSortie))}}</td>
                        <td><span class="label label-danger">Rejeté</span></td>
                      </tr>
                    @endforeach
                  @else
                    <tr><td>Aucun brief ou debrief rejeté</td></tr>
                  @endif
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            @if(count($allbriefsRejects)>0)
            <div class="box-footer clearfix">
              <a href="{{url(config('laraadmin.adminRoute'). '/marketing/brief/reject')}}" class="btn btn-sm btn-info btn-success pull-left"><i class="fa fa-eye"></i> Consultés les Brief et Debrief rejeté</a>
            </div>
            @endif
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-4">
        <div class="col-md-12">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Debriefs Rejeté</h3>
              <p>{{$allbriefsReject}}</p>
            </div>
            <div class="icon">
              <i class="fa fa-times"></i>
            </div>
            <a href="#" class="small-box-footer">{{-- more info <i class="fa fa-arrow-circle-right"></i>--}}</a>
          </div>
        </div>

        <div class="col-md-12">
          <!-- small box -->
          <div class="small-box bg-primary">
              <div class="inner">
                <h3>Tâches en cours</h3>
                <p>{{$tachesencours}}</p>
              </div>
              <div class="icon">
                <i class="fa fa-tasks"></i>
              </div>
              <a href="#" class="small-box-footer">{{-- more info <i class="fa fa-arrow-circle-right"></i>--}}</a>
            </div>
        </div>
      </div>
      </div>

    @else
      <div class="row">
        {{--<div class="col-md-12">--}}
          {{--<div class="col-md-12" style="padding: 0px;">--}}
            {{--<!-- TABLE: LATEST ORDERS -->--}}
            {{--<div class="box box-warning" style="min-height: 300px">--}}
              {{--<div class="box-header with-border">--}}
                {{--<h3 class="box-title">Briefs en attente de validation </h3>--}}
              {{--</div>--}}
              {{--<!-- /.box-header -->--}}
              {{--<div class="box-body">--}}
                {{--<div class="table-responsive">--}}
                  {{--<table class="table no-margin">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}
                      {{--<th>Annonceur</th>--}}
                      {{--<th>Project</th>--}}
                      {{--<th>Libelle</th>--}}
                      {{--<th>Date Entrée</th>--}}
                      {{--<th>Date Fin</th>--}}
                      {{--<th>Departement</th>--}}
                      {{--<th>Status</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                    {{--<tbody>--}}
                    {{--@foreach($allbriefsBriefsAttente as $attent)--}}
                      {{--<tr>--}}
                        {{--<td>{{ (new \App\Http\Controllers\AdminleadvalidController)->showAnnonceurPjt($attent->project->annonceur)}}</td>--}}
                        {{--<td>{{$attent->project->operation}}</td>--}}
                        {{--<td>{{$attent->libelle}}</td>--}}
                        {{--<td>{{date("d/m/Y",strtotime($attent->dateDentree))}}</td>--}}
                        {{--<td>{{date("d/m/Y",strtotime($attent->dateDeSortie))}}</td>--}}
                        {{--<td>--}}
                          {{--@if($attent->dep=='6')--}}
                            {{--Service Crea/Strat--}}
                          {{--@else--}}
                            {{--Service Trafic--}}
                          {{--@endif--}}
                        {{--</td>--}}
                        {{--<td><span class="label label-info">En attente</span></td>--}}
                      {{--</tr>--}}
                    {{--@endforeach--}}
                    {{--</tbody>--}}
                  {{--</table>--}}
                {{--</div>--}}
                {{--<!-- /.table-responsive -->--}}
              {{--</div>--}}
              {{--<!-- /.box-body -->--}}
              {{--@if(count($allbriefsBriefsAttente)!=0)--}}
                {{--<div class="box-footer clearfix">--}}
                  {{--<a href="{{url(config('laraadmin.adminRoute'). '/briefwaitg/waitingsbrief')}}" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-eye"></i> Consultés les projets en attente</a>--}}
                {{--</div>--}}
            {{--@endif--}}
            {{--<!-- /.box-footer -->--}}
            {{--</div>--}}
            {{--<!-- /.box -->--}}
          {{--</div>--}}
        {{--</div>--}}

        <div class="col-md-12">
            <!-- TABLE: LATEST ORDERS -->
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Brief et Debrief en attente de validation </h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="table-responsive">
                  <table class="table no-margin">
                    <thead>
                    <tr>
                      <th>Annonceur</th>
                      <th>Project</th>
                      <th>Libelle</th>
                      <th>Date Entrée</th>
                      <th>Date Fin</th>
                      <th>Departement</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($allbriefsAttente as $attent)
                      <tr>
                        <td>{{ (new \App\Http\Controllers\AdminleadvalidController)->showAnnonceurPjt($attent->project->annonceur)}}</td>
                        <td>{{$attent->project->operation}}</td>
                        <td>{{$attent->libelle}}</td>
                        <td>{{date("d/m/Y",strtotime($attent->dateDentree))}}</td>
                        <td>{{date("d/m/Y",strtotime($attent->dateDeSortie))}}</td>
                        <td>
                          @if($attent->dep=='6')
                          Service Stratégie
                          @else
                          Service Trafic
                          @endif
                        </td>
                        <td><span class="label label-info">En attente</span></td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.box-body -->
              @if(count($allbriefsAttente)!=0)
                <div class="box-footer clearfix">
                  <a href="{{url(config('laraadmin.adminRoute'). '/marketing/brief/attente')}}" class="btn btn-sm btn-warning btn-flat pull-left"><i class="fa fa-eye"></i> Consultés les brief et debrief en attente de validation</a>
                </div>
            @endif
            <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </div>

        <div class="col-md-12">
          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Brief et Debrief validé </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>Annonceur</th>
                    <th>Project</th>
                    <th>Libelle</th>
                    <th>Date Entrée</th>
                    <th>Date Fin</th>
                    <th>Departement</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($allbriefsvalid as $attent)
                    <tr>
                      <td>{{ (new \App\Http\Controllers\AdminleadvalidController)->showAnnonceurPjt($attent->project->annonceur)}}</td>
                      <td>{{$attent->project->operation}}</td>
                      <td>{{$attent->libelle}}</td>
                      <td>{{date("d/m/Y",strtotime($attent->dateDentree))}}</td>
                      <td>{{date("d/m/Y",strtotime($attent->dateDeSortie))}}</td>
                      <td>
                        @if($attent->dep=='6')
                          Service Stratégie
                        @else
                          Service Trafic
                        @endif
                      </td>
                      <td><span class="label label-success">Validé</span></td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            @if(count($allbriefsvalid)!=0)
              <div class="box-footer clearfix">
                <a href="{{url(config('laraadmin.adminRoute'). '/brief/inprogress')}}" class="btn btn-sm btn-info btn-success pull-left"><i class="fa fa-eye"></i> Consultés les brief et debrief validé</a>
              </div>
          @endif
          <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>

      </div>
    @endif

  </section>
@endsection

@push('styles')
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/morris/morris.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Date Picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/datepicker/datepicker3.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush


@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{ asset('la-assets/plugins/morris/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('la-assets/plugins/knob/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('la-assets/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('la-assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
@endpush

@push('scripts')

@endpush