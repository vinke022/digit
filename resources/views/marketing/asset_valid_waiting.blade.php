@extends('marketing.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Asset en attente de validation @endsection
@section('contentheader_title') Asset en attente de validation @endsection

@section('main-content')
    <section class="content">

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">CREA</span></h4>
                        <div class="col-md-12">
                            <div id="loadercrea" class="pull-right" style="display: none;">
                                <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingCrea as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <div id="sectbtncrea{{$asset->id}}">
                                        <a onclick="creaValid('{{url(config('laraadmin.adminRoute') . '/marketing/asset/waiting/'.$asset->id.'/valid')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModal" href="#" onclick="displayModal('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">PRODUCTION</span></h4>
                        <div class="col-md-12">
                            <div id="loaderprod" class="pull-right" style="display: none;">
                                <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingProd as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <div id="sectbtnprod{{$asset->id}}">
                                        <a onclick="prodValid('{{url(config('laraadmin.adminRoute') . '/marketing/asset/waiting/'.$asset->id.'/valid/prod')}}','{{$asset->id}}')" href="{{url(config('laraadmin.adminRoute') . '/marketing/asset/waiting/'.$asset->id.'/valid/prod')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModalProd" href="#" onclick="displayModalProd('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">RETRO PLANNING</span></h4>
                        <div class="col-md-12">
                            <div id="loaderretro" class="pull-right" style="display: none;">
                                <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example3" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingRetro as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <div id="sectbtnretro{{$asset->id}}">
                                        <a onclick="retroValid('{{url(config('laraadmin.adminRoute') . '/marketing/asset/waiting/'.$asset->id.'/valid/retro')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModalRetro" href="#" onclick="displayModalRetro('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">LIVRABLE</span></h4>
                        <div class="col-md-12">
                            <div id="loaderlivre" class="pull-right" style="display: none;">
                                <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example4" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingLivr as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <div id="sectbtnlivre{{$asset->id}}">
                                        <a onclick="livreValid('{{url(config('laraadmin.adminRoute') . '/marketing/asset/waiting/'.$asset->id.'/valid/livre')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModalLivre" href="#" onclick="displayModalLivre('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">PLANNING PUBLICATION</span></h4>
                        <div class="col-md-12">
                            <div id="loaderplan" class="pull-right" style="display: none;">
                                <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example5" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingPlanning as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <div id="sectbtnplan{{$asset->id}}">
                                        <a onclick="planValid('{{url(config('laraadmin.adminRoute') . '/marketing/asset/waiting/'.$asset->id.'/valid/plan')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModalPlan" href="#" onclick="displayModalPlan('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">VISUEL GOOGLE ADS</span></h4>
                        <div class="col-md-12">
                            <div id="loaderggle" class="pull-right" style="display: none;">
                                <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example6" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingGoogle as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <div id="sectbtnggle{{$asset->id}}">
                                        <a onclick="ggleValid('{{url(config('laraadmin.adminRoute') . '/marketing/asset/waiting/'.$asset->id.'/valid/ggle')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                        <a data-toggle="modal" data-target="#versionModalGgle" href="#" onclick="displayModalGgle('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h4><span class="label label-danger">RAPPORT OPERATIONNEL</span></h4>
                        <div class="col-md-12">
                            <div id="loaderrapportop" class="pull-right" style="display: none;">
                                <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example8" class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Libelle</th>
                                <th>Fichier</th>
                                <th>Actions</th>
                                {{--<th>Action</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($assetWaitingRapportOp as $asset)
                                <tr>
                                    <td>{{$asset->asset->libelle}}</td>
                                    <td>
                                        <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                    </td>
                                    <td>
                                        <div id="sectbtnrapportop{{$asset->id}}">
                                            <a onclick="rapportopValid('{{url(config('laraadmin.adminRoute') . '/marketing/asset/waiting/'.$asset->id.'/valid/rapportop')}}','{{$asset->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalRapportOp" href="#" onclick="displayModalRapportOp('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

    </section>

    <div class="modal fade" id="versionModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypecrea"  method="post" action="{{url(config('laraadmin.adminRoute'). '/marketing/asset/reject')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loadercrea2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="idAsset" name="idAsset"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetCrea" onclick="submitCrea()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalProd" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeProd"  method="post" action="{{url(config('laraadmin.adminRoute'). '/marketing/asset/reject/prod')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderprod2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="prodfile" name="prodfile"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetProd" onclick="submitProd()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalOP" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeOp"  method="post" action="{{url(config('laraadmin.adminRoute'). '/assetOp/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="idOp" name="idOp"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalRetro" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeRetro" method="post" action="{{url(config('laraadmin.adminRoute'). '/marketing/asset/reject/retro')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderretro2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="idretro" name="idretro"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetRetro" onclick="submitRetro()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalLivre" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeLivre" method="post" action="{{url(config('laraadmin.adminRoute'). '/marketing/asset/reject/livre')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderlivre2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="livrefile" name="livrefile"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetLivre" onclick="submitLivre()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalPlan" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypePlan" method="post" action="{{url(config('laraadmin.adminRoute'). '/marketing/asset/reject/plan')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderplan2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="idplan" name="idplan"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetPlan" onclick="submitPlan()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalGgle" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeGgle" method="post" action="{{url(config('laraadmin.adminRoute'). '/marketing/asset/reject/ggle')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderggle2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="ggleid" name="ggleid"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetGgle" onclick="submitGgle()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalRapportOp" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeRapportOp" method="post" action="{{url(config('laraadmin.adminRoute'). '/marketing/asset/reject/rapportop')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loaderrapportop2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="rapportopid" name="rapportopid"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejetRapportOp" onclick="submitRapportOp()" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection



@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example2").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example3").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example4").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example5").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example6").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example7").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });
    $("#example8").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });


    function displayModal(id){
        $("#formType")[0].reset();
        $("#idAsset").val(id);
    }

    function displayModalProd(id){
        $("#formTypeProd")[0].reset();
        $("#prodfile").val(id);
    }

    function displayModalRetro(id){
        $("#formTypeRetro")[0].reset();
        $("#idretro").val(id);
    }

    function displayModalLivre(id){
        //console.log(id);
        $("#formTypeLivre")[0].reset();
        $("#livrefile").val(id);
    }

    function displayModalPlan(id){
        $("#formTypePlan")[0].reset();
        $("#idplan").val(id);
    }

    function displayModalGgle(id){
        $("#formTypeGgle")[0].reset();
        $("#ggleid").val(id);
    }

    function displayModalRapportOp(id){
        $("#formTypeRapportOp")[0].reset();
        $("#rapportopid").val(id);
    }

    //LOADER
        //CREA
        function creaValid(lien,id) {
            //console.log(lien);
            $('#sectbtncrea'+id).hide();
            $('#loadercrea').show();
            //alert("ici");
            window.location=lien;
        }

        function submitCrea() {
            $("#commitRejetCrea").prop('disabled', true);
            $('#loadercrea2').show();
            //alert("ici");
            $("#formTypecrea").submit();
            //console.log($("#idbrief").val());
        }

        //PROD
        function prodValid(lien,id) {
            //console.log(lien);
            $('#sectbtnprod'+id).hide();
            $('#loaderprod').show();
            //alert("ici");
            window.location=lien;
        }

        function submitProd() {
            $("#commitRejetProd").prop('disabled', true);
            $('#loaderprod2').show();
            //alert("ici");
            $("#formTypeProd").submit();
            //console.log($("#idbrief").val());
        }

        //RETRO
        function retroValid(lien,id) {
            //console.log(lien);
            $('#sectbtnretro'+id).hide();
            $('#loaderretro').show();
            //alert("ici");
            window.location=lien;
        }

        function submitRetro() {
            $("#commitRejetRetro").prop('disabled', true);
            $('#loaderretro2').show();
            //alert("ici");
            $("#formTypeRetro").submit();
            //console.log($("#idbrief").val());
        }

        //LIVRABLE
        function livreValid(lien,id) {
            //console.log(lien);
            $('#sectbtnlivre'+id).hide();
            $('#loaderlivre').show();
            //alert("ici");
            window.location=lien;
        }

        function submitLivre() {
            $("#commitRejetLivre").prop('disabled', true);
            $('#loaderlivre2').show();
            //alert("ici");
            $("#formTypeLivre").submit();
            //console.log($("#idbrief").val());
        }

        //PLANMEDIA
        function planValid(lien,id) {
            //console.log(lien);
            $('#sectbtnplan'+id).hide();
            $('#loaderplan').show();
            //alert("ici");
            window.location=lien;
        }

        function submitPlan() {
            $("#commitRejetPlan").prop('disabled', true);
            $('#loaderplan2').show();
            //alert("ici");
            $("#formTypePlan").submit();
            //console.log($("#idbrief").val());
        }

        //GOOGLE ADS
        function ggleValid(lien,id) {
            //console.log(lien);
            $('#sectbtnggle'+id).hide();
            $('#loaderggle').show();
            //alert("ici");
            window.location=lien;
        }

        function submitGgle() {
            $("#commitRejetPlan").prop('disabled', true);
            $('#loaderggle2').show();
            //alert("ici");
            $("#formTypeGgle").submit();
            //console.log($("#idbrief").val());
        }

        //RAPPORT OP
        function rapportopValid(lien,id) {
            //console.log(lien);
            $('#sectbtnrapportop'+id).hide();
            $('#loaderrapportop').show();
            //alert("ici");
            window.location=lien;
        }

        function submitRapportOp() {
            $("#commitRejetRapportOp").prop('disabled', true);
            $('#loaderrapportop2').show();
            //alert("ici");
            $("#formTypeRapportOp").submit();
            //console.log($("#idbrief").val());
        }
</script>
@endpush