@extends('marketing.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Brief & Debrief rejeté@endsection
@section('contentheader_title') Brief & Debrief rejeté @endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Annonceur</th>
                            <th>Project</th>
                            <th>Libelle</th>
                            <th>Date Entrée</th>
                            <th>Date Fin</th>
                            <th>Observation</th>
                            <th>Utilisateur</th>
                            @if(Auth::user()->post!='DCOMM')
                            <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($allbriefsreject)>0)
                            @foreach($allbriefsreject as $attent)
                                <tr>
                                    <td><a href="javascript:void(0)">{{ (new \App\Http\Controllers\MarketingController)->showAnnonceurPjt($attent->project->annonceur)}}</a></td>
                                    <td>{{$attent->project->operation}}</td>
                                    <td>{{$attent->libelle}}</td>
                                    <td>{{date("d/m/Y",strtotime($attent->dateDentree))}}</td>
                                    <td>{{date("d/m/Y",strtotime($attent->dateDeSortie))}}</td>
                                    <td>
                                        {!! nl2br($attent->lastReject[0]->observe) !!}
                                    </td>
                                    <td>
                                        {{(new \App\Http\Controllers\LA\DashboardController())->itemUser($attent->lastReject[0]->user_id)}}
                                    </td>
                                    @if(Auth::user()->post!='DCOMM')
                                    <td>
                                        <a href="{{url(config('laraadmin.adminRoute') . '/debriefs/'.$attent->id.'/edit')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Modifier"><i class="fa fa-edit"></i> Modifier</a>
                                        {{ Form::open(['route' => [config('laraadmin.adminRoute') . '.debriefs.destroy', $attent->id], 'method' => 'delete', 'style'=>'display:inline']) }}
                                        <button class="btn btn-danger btn-xs" type="submit">
                                            <i class="fa fa-remove"></i> Supprimer
                                        </button>
                                        {{Form::close()}}
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <tr><td>Aucun brief en cours de validation</td></tr>
                        @endif
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>

    <div class="modal fade" id="versionModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}

                        <div style="float: right; margin-right: 10px">

                        </div>

                        <h4 class="modal-title">Rejet du fichier</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formType"  method="post" action="{{url(config('laraadmin.adminRoute'). '/brief/waiting/rejet')}}" autocomplete="off">

                            <div class="form-group">
                                <label for="note" id="note" class="col-md-offset-4 control-label">Motif du brief <span class="obligatoire">*</span></label><br>
                                <div class="col-md-8">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required style="width: 10cm;margin-left: 2.1cm"></textarea>
                                </div>
                            </div>

                            <input type="hidden" id="idbrief" name="idbrief"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                            <button type="button" id="downloadBtn" class="btn btn-default" data-dismiss="modal" onclick="ErrorMsg()"><i class="glyph-icon icon-times-circle mrg5R"></i> Annuler</button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection



@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

    function displayModal(id){
        $("#formType")[0].reset();
        $("#idbrief").val(id);
    }

</script>
@endpush
