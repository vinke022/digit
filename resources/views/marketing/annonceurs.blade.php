@extends("marketing.layouts.app")

@section('htmlheader_title') Annonceurs @endsection
@section('contentheader_title') Annonceurs @endsection

@section("headerElems")
    <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#AddModal">
        Ajouter Annonceur
    </button>
@endsection

@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@section("main-content")

    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nom</th>
                            <th>Adresse</th>
                            <th>Email</th>
                            <th>Domaine d'activités</th>
                            <th>Fix</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($annonceurs)>0)
                            @foreach($annonceurs as $annonceur)
                                <tr>
                                    <td><a href="javascript:void(0)">{{$annonceur->id}}</a></td>
                                    <td>{{$annonceur->name}}</td>
                                    <td>{{$annonceur->adresse}}</td>
                                    <td>{{$annonceur->email }} </td>
                                    <td>{{$annonceur->activites }} </td>
                                    <td>{{$annonceur->fixe }} </td>
                                    <td>{{$annonceur->type }} </td>
                                    <td>
                                        <a href="{{url(config('laraadmin.adminRoute') . '/annonceurs/'.$annonceur->id.'/edit')}}" class="btn btn-warning btn-xs" style="display:inline;padding:2px 5px 3px 5px;"><i class="fa fa-edit"></i></a>
                                        {{--{{ Form::open(['route' => [config('laraadmin.adminRoute') . '.annonceurs.destroy', $annonceur->id], 'method' => 'delete', 'style'=>'display:inline']) }}--}}
                                        {{--<button class="btn btn-danger btn-xs" type="submit">--}}
                                            {{--<i class="fa fa-times"></i>--}}
                                        {{--</button>--}}
                                        {{--{{Form::close()}}--}}
                                    </td>

                                </tr>
                            @endforeach
                        @else
                            <tr><td>Aucun annonceur ajouté </td></tr>
                        @endif
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>

    <div class="modal fade" id="AddModal" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Ajouter Annonceur</h4>
                </div>
                {!! Form::open(['action' => 'LA\AnnonceursController@store', 'id' => 'annonceur-add-form']) !!}
                <div class="modal-body">
                    <div class="modal-body">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Nom* :</label>
                                <input class="form-control" placeholder="Enter Nom" data-rule-maxlength="255" required="1" name="name" type="text" value="" aria-required="true"></div>
                            <div class="form-group">
                                <label for="adresse">Adresse* :</label>
                                <textarea class="form-control" placeholder="Enter Adresse" data-rule-maxlength="1000" required="1" cols="30" rows="3" name="adresse" aria-required="true"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="email">Email :</label>
                                <input class="form-control" placeholder="Enter Email" data-rule-email="true" name="email" type="email" value="">
                            </div>
                            <div class="form-group">
                                <label for="activites">Domaine d'activités :</label>
                                <input class="form-control" placeholder="Enter Domaine d'activités" data-rule-maxlength="255" name="activites" type="text" value="">
                            </div>
                            <div class="form-group">
                                <label for="fixe">Fixe :</label>
                                <input class="form-control" placeholder="Enter Fixe" data-rule-minlength="8" data-rule-maxlength="255" name="fixe" type="text" value="">
                            </div>
                            <div class="form-group">
                                <label for="type">Type* :</label>
                                <select class="form-control select2-hidden-accessible" required="1" data-placeholder="Enter Type" rel="select2" name="type" tabindex="-1" aria-hidden="true" aria-required="true">
                                    <option value="Entreprise" selected="selected">Entreprise</option>
                                    <option value="Groupe">Groupe</option>
                                </select>
                            </div>
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    {!! Form::submit( 'Enregistrer', ['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection


@push('scripts')

<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>

<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

</script>
@endpush
