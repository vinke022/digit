@extends('admin_lead_valid.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Bon de commande en attente de validation @endsection
@section('contentheader_title') Bon de commande en attente de validation @endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="box">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Annonceur</td>
                            <td>Libelle</td>
                            <td>Project</td>
                            <td>Commerciale</td>
                            <td>Fichier</td>
                            <td>Status</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bcommandes as $bc)
                            <tr>
                                <td>{{ (new \App\Http\Controllers\AdminleadvalidController)->showAnnonceurPjt($bc->project->annonceur)}}</td>
                                <td>{{$bc->libelle}}</td>
                                <td>{{$bc->project->operation}}</td>
                                <td>{{$bc->userBC->name}}</td>
                                <td><a href="{{url('assetFiles/'.$bc->path)}}" download="{{$bc->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a></td>
                                <td>
                                    <span class="label label-danger">En attente</span>
                                </td>
                                <td>
                                    <a href="{{url(config('laraadmin.adminRoute') . '/bc/waiting/'.$bc->id.'/valid')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                    <a data-toggle="modal" data-target="#versionModal" href="#" onclick="displayModal('{{$bc->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>

    <div class="modal fade" id="versionModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formType"  method="post" action="{{url(config('laraadmin.adminRoute'). '/bc/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif du brief <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="idbc" name="idbc"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger  pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- DataTables -->
{{--<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/js/dataTables.bootstrap.min.js') }}"></script>--}}

<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>


<script>
    $("#example1").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Pr&eacute;c&eacute;den"
            },
        }
    });

    function displayModal(id){
        $("#formType")[0].reset();
        $("#idbc").val(id);
    }

</script>
@endpush