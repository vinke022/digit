@extends("admin_lead_valid.layouts.app")

@section("contentheader_title", "Projets")
@section("htmlheader_title", "Projets List")

@section("headerElems")

@endsection

@section("main-content")

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">PHASE 1</span><span class="info-box-text"> en attente de validation</span></h4>

                    <div class="col-md-12">
                        <div id="loaderp1" class="pull-right" style="display: none;">
                            <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Id</td>
                            <td>Opération</td>
                            <td>Commercial</td>
                            <td>Status</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($bcommandes as $k=>$bc)
                            <tr>
                                <td class="sorting_1">{{$k+1}}</td>
                                <td>{{$bc->project->operation}}</td>
                                <td>{{$bc->userBC->name}}</td>
                                <td>
                                    <span class="label label-success">En attente</span>
                                </td>
                                <td>
                                    <div id="sectbtn{{$bc->id}}">
                                    <a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$bc->project_id)}}" class="btn btn-default btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Voir"><i class="fa fa-eye"></i></a>
                                    <a onclick="validphase1('{{url(config('laraadmin.adminRoute') . '/bc/waiting/'.$bc->id.'/valid')}}','{{$bc->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                    <a data-toggle="modal" data-target="#versionModal" href="#" onclick="displayModal('{{$bc->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">PHASE 1</span> <span class="info-box-text"> en cours</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Opération</th>
                            <th>Annonceur</th>
                            <th>Date d'entrée</th>
                            {{--<th>Date de sortie</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($project1 as $projet)
                            <tr>
                                <td>{{$projet->id}}</td>
                                <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                                <td>{{$projet->annonceurProject->name}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                                {{--<td>{{date("d/m/Y",strtotime($projet->dateDeSortie))}}</td>--}}
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">PHASE 2</span><span class="info-box-text"> en attente de validation</span></h4>
                    <div class="col-md-12">
                        <div id="loaderp2" class="pull-right" style="display: none;">
                            <img src="{{ URL::asset('la-assets/img/load.gif')}}" style="width: 100%">
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example3" class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Id</td>
                            <td>Opération</td>
                            <td>Commercial</td>
                            <td>Status</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($soumettres as $k=>$bc)
                            <tr>
                                <td class="sorting_1">{{$k+1}}</td>
                                <td>{{$bc->projectSomi->operation}}</td>
                                <td>{{$bc->userSomi->name}}</td>
                                <td>
                                    <span class="label label-success">En attente</span>
                                </td>
                                <td>
                                    <div id="sect2btn{{$bc->id}}">
                                    <a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$bc->project_id)}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Voir"><i class="fa fa-eye"></i></a>
                                    <a onclick="validphase2('{{url(config('laraadmin.adminRoute') . '/soumi/waiting/'.$bc->id.'/valid')}}','{{$bc->id}}')" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                    <a data-toggle="modal" data-target="#versionModalSoumi" href="#" onclick="displayModalSoumi('{{$bc->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">PHASE 2</span> <span class="info-box-text"> en cours</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example4" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Opération</th>
                            <th>Annonceur</th>
                            <th>Date d'entrée</th>
                            {{--<th>Date de sortie</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        {{--@foreach($project1 as $projet)
                            <tr>
                                <td>{{$projet->id}}</td>
                                <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                                <td>{{$projet->annonceurProject->name}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDeSortie))}}</td>
                            </tr>
                        @endforeach--}}
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    <div class="row">
        {{--<div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">PHASE 3</span><span class="info-box-text"> en attente de validation</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example5" class="table table-bordered">
                        <thead>
                        <tr>
                            <td>Id</td>
                            <td>Opération</td>
                            <td>Commercial</td>
                            <td>Status</td>
                            <td>Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        --}}{{--@foreach($bcommandes as $k=>$bc)
                            <tr>
                                <td class="sorting_1">{{$k+1}}</td>
                                <td>{{$bc->project->operation}}</td>
                                <td>{{$bc->userBC->name}}</td>
                                <td>
                                    <span class="label label-success">En attente</span>
                                </td>
                                <td>
                                    <a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$bc->project_id)}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                    --}}{{----}}{{--<a href="{{url(config('laraadmin.adminRoute') . '/bc/waiting/'.$bc->id.'/valid')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                    <a data-toggle="modal" data-target="#versionModal" href="#" onclick="displayModal('{{$bc->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>--}}{{----}}{{--
                                </td>
                            </tr>
                        @endforeach--}}{{--
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>--}}
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">PHASE 3</span> <span class="info-box-text"> en cours</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example6" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Opération</th>
                            <th>Annonceur</th>
                            <th>Date d'entrée</th>
                            {{--<th>Date de sortie</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        {{--@foreach($project1 as $projet)
                            <tr>
                                <td>{{$projet->id}}</td>
                                <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                                <td>{{$projet->annonceurProject->name}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDeSortie))}}</td>
                            </tr>
                        @endforeach--}}
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    {{--<div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">ETAPE 1</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Opération</th>
                            <th>Annonceur</th>
                            <th>Date d'entrée</th>
                            <th>Date de sortie</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($project1 as $projet)
                            <tr>
                                <td>{{$projet->id}}</td>
                                <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                                <td>{{$projet->annonceurProject->name}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDeSortie))}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">ETAPE 2</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Opération</th>
                            <th>Annonceur</th>
                            <th>Date d'entrée</th>
                            <th>Date de sortie</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($project2 as $projet)
                            <tr>
                                <td>{{$projet->id}}</td>
                                <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                                <td>{{$projet->annonceurProject->name}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDeSortie))}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="box">
                <div class="box-header">
                    <h4><span class="label label-danger">ETAPE 3</span></h4>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example3" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Opération</th>
                            <th>Annonceur</th>
                            <th>Date d'entrée</th>
                            <th>Date de sortie</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($project3 as $projet)
                            <tr>
                                <td>{{$projet->id}}</td>
                                <td><a href="{{url(config('laraadmin.adminRoute') . '/projets/'.$projet->id)}}">{{$projet->operation}}</a></td>
                                <td>{{$projet->annonceurProject->name}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDentree))}}</td>
                                <td>{{date("d/m/Y",strtotime($projet->dateDeSortie))}}</td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>--}}

    <div class="modal fade" id="versionModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formType"  method="post" action="{{url(config('laraadmin.adminRoute'). '/bc/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif du brief <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loader2" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>
                            <input type="hidden" id="idbc" name="idbc"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button onclick="submitPhase1()" type="submit" id="commitRejet" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalSoumi" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeSoumi"  method="post" action="{{url(config('laraadmin.adminRoute'). '/soumi/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif du brief <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="loader3" class="pull-right" style="display: none;">
                                    <img src="{{ URL::asset('la-assets/img/load.gif')}}">
                                </div>
                            </div>

                            <input type="hidden" id="idsoumi" name="idsoumi"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button onclick="submitPhase2()" type="submit" id="commitRejet2" class="btn btn-danger  pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('la-assets/plugins/datatables/datatables.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
<script>
    function displayModal(id){
        $("#formType")[0].reset();
        $("#idbc").val(id);
    }

    function displayModalSoumi(id){
        $("#formTypeSoumi")[0].reset();
        $("#idsoumi").val(id);
    }

    function validphase1(lien,id) {
        $('#sectbtn'+id).hide();
        $('#loaderp1').show();
        //alert('test');
        window.location=lien;
    }

    function submitPhase1() {
        $("#commitRejet").prop('disabled', true);
        $('#loader2').show();
        $("#formType").submit();
    }

    function validphase2(lien,id) {
        $('#sect2btn'+id).hide();
        $('#loaderp2').show();
        //alert('test');
        window.location=lien;
    }

    function submitPhase2() {
        $("#commitRejet2").prop('disabled', true);
        $('#loader3').show();
        //alert('rrr');
        $("#formTypeSoumi").submit();
    }


    $(function () {
        $("#example1").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });

        $("#example2").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });

        $("#example3").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });

        $("#example4").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });
        $("#example5").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });
        $("#example6").DataTable({
            language: {
                lengthMenu: "_MENU_",
                search: "_INPUT_",
                searchPlaceholder: "Recherche",
                sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                sInfoPostFix:    "",
                sLoadingRecords: "Chargement en cours...",
                sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
                sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                oPaginate: {
                    "sFirst":    "Premier",
                    "sLast":    "Dernier",
                    "sNext":    "Suivant",
                    "sPrevious": "Pr&eacute;c&eacute;dent"
                },
            }
        });

    });
</script>
@endpush
