@extends("admin_lead_valid.layouts.app")

@section("contentheader_title")
	<a href="{{ url(config('laraadmin.adminRoute') . '/assets') }}">Asset</a> :
@endsection
@section("contentheader_description", $asset->$view_col)
@section("section", "Assets")
@section("section_url", url(config('laraadmin.adminRoute') . '/assets'))
@section("sub_section", "Edit")

@section("htmlheader_title", "Assets Edit : ".$asset->$view_col)

@section("main-content")

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="box">
	<div class="box-header">
		
	</div>
	<div class="box-body">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				{!! Form::model($asset, ['route' => [config('laraadmin.adminRoute') . '.assets.update', $asset->id ], 'method'=>'PUT', 'id' => 'asset-edit-form']) !!}
					@la_form($module)
					
					{{--
					@la_input($module, 'ext')
					@la_input($module, 'libelle')
					@la_input($module, 'asset_type')
					@la_input($module, 'brief_id')
					@la_input($module, 'path')
					--}}
                    <br>
					<div class="form-group">
						{!! Form::submit( 'Update', ['class'=>'btn btn-success']) !!} <button class="btn btn-default pull-right"><a href="{{ url(config('laraadmin.adminRoute') . '/assets') }}">Cancel</a></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
$(function () {
	$("#asset-edit-form").validate({
		
	});
});
</script>
@endpush
