@extends('la.layouts.app')


@push('styles')

<link rel="stylesheet" href="{{ asset('la-assets/plugins/datatables/DataTables-1.10.12/css/dataTables.bootstrap.min.css') }}">
<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{ asset('la-assets/plugins/daterangepicker/daterangepicker-bs3.css') }}">
@endpush

@section('htmlheader_title') Asset en attente de validation @endsection
@section('contentheader_title') Asset en attente de validation @endsection

@section('main-content')
    <section class="content">

        <div class="row">
            @foreach($assetypes as $k=>$assetype)
                @php $result = (new \App\Http\Controllers\AssetsvalidController())->checkAssetValidation($assetype['name']); @endphp
                @if($result)
                    @php $assetWaiting = (new \App\Http\Controllers\AssetsvalidController())->getNotif($assetype['name']); @endphp
                    <div class="col-md-6">
                        <div class="box">
                            <div class="box-header">
                                <h4><span class="label label-danger">{{$assetype['libelle']}}</span></h4>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="example" class="table table-bordered nbasset">
                                    <thead>
                                    <tr>
                                        <th>Libelle</th>
                                        <th>Fichier</th>
                                        <th>Actions</th>
                                        {{--<th>Action</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($assetWaiting as $asset)
                                        <tr>
                                            <td>{{$asset->asset->libelle}}</td>
                                            <td>
                                                <a href="{{url('assetFiles/'.$asset->asset->path)}}" download="{{$asset->asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                            </td>
                                            <td>
                                                <a href="{{url(config('laraadmin.adminRoute') . '/assign/assign/waiting/'.$asset->id.'/'.$assetype['name'].'/valid')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                                <a data-toggle="modal" data-target="#versionModal" href="#" onclick="displayModal('{{$asset->id}}','{{$assetype['name']}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                @endif
            @endforeach

            @if(Auth::user()->post=='DSTRAT' )
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example" class="table table-bordered nbasset">
                                <thead>
                                <tr>
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingDevis as $asset)
                                    <tr>
                                        <td>{{$asset->devis->libelle}}</td>
                                        <td>
                                            <a href="{{url('devisFiles/'.$asset->devis->path)}}" download="{{$asset->devis->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                        <td>
                                            <a href="{{url(config('laraadmin.adminRoute') . '/assetDevis/waiting/'.$asset->id.'/valid')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalDevis" href="#" onclick="displayModalDevis('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif

            @if(Auth::user()->post=='CONTROLE')
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example" class="table table-bordered nbasset">
                                <thead>
                                <tr>
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingDevis as $asset)
                                    <tr>
                                        <td>{{$asset->devis->libelle}}</td>
                                        <td>
                                            <a href="{{url('devisFiles/'.$asset->devis->path)}}" download="{{$asset->devis->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                        <td>
                                            <a href="{{url(config('laraadmin.adminRoute') . '/lead/assetDevis_controle/waiting/'.$asset->id.'/valid')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalDevis" href="#" onclick="displayModalDevis('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif

            @if(Auth::user()->post=='DGA')
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-header">
                            <h4><span class="label label-danger">DEVIS ET PROFORMA</span></h4>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example" class="table table-bordered nbasset">
                                <thead>
                                <tr>
                                    <th>Libelle</th>
                                    <th>Fichier</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($assetWaitingDevis as $asset)
                                    <tr>
                                        <td>{{$asset->devis->libelle}}</td>
                                        <td>
                                            <a href="{{url('devisFiles/'.$asset->devis->path)}}" download="{{$asset->devis->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                        </td>
                                        <td>
                                            <a href="{{url(config('laraadmin.adminRoute') . '/lead/assetDevis/waiting/'.$asset->id.'/valid')}}" class="btn btn-success btn-xs" style="display:inline;padding:2px 5px 3px 5px;" title="Valider"><i class="fa fa-check"></i></a>
                                            <a data-toggle="modal" data-target="#versionModalDevis" href="#" onclick="displayModalDevis('{{$asset->id}}')" class="btn btn-danger btn-xs" title="Rejeter"> <i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif

            @if(Auth::user()->post=='DG' or Auth::user()->post=='SA')
                @foreach($assetypes as $k=>$assetype)
                    @php $assetWaiting = (new \App\Http\Controllers\AssetsvalidController())->getAssetWaiting($assetype['name']); @endphp
                    <div class="col-md-6">
                            <div class="box">
                                <div class="box-header">
                                    <h4><span class="label label-danger">{{$assetype['libelle']}}</span></h4>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <table id="example1" class="table table-bordered nbassetdg">
                                        <thead>
                                        <tr>
                                            <th>Brief</th>
                                            <th>Libelle</th>
                                            <th>Fichier</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($assetWaiting as $asset)
                                            <tr>
                                                <td>{{$asset->debrief->libelle}}</td>
                                                <td>{{$asset->libelle}}</td>
                                                <td>
                                                    <a href="{{url('assetFiles/'.$asset->path)}}" download="{{$asset->libelle}}"><i class="fa fa-cloud-download"></i>Télécharger</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                @endforeach
            @endif
        </div>


    </section>

    <div class="modal fade" id="versionModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formType"  method="post" action="{{url(config('laraadmin.adminRoute'). '/assign/assign/waiting/reject')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="idAsset" name="idAsset"/>
                            <input type="hidden" id="typeAsset" name="typeAsset"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="versionModalDevis" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Motif de rejet</h4>
                    </div>

                    <div class="modal-body scrollable-content scrollable-xs scrollable-nice" style="height: 200px" >
                        <form class="form-horizontal" id="formTypeDevis"  method="post" action="{{url(config('laraadmin.adminRoute'). '/assetDevis/waiting/rejet')}}" autocomplete="off">
                            <div class="form-group">
                                {{--<label for="note" id="note" class="col-md-offset-4 control-label">Motif de rejet <span class="obligatoire">*</span></label><br>--}}
                                <div class="col-md-12">
                                    <textarea name="message" id="message" rows="4" cols="50" class="form-control" required placeholder="Motif du rejet"></textarea>
                                </div>
                            </div>
                            <input type="hidden" id="idDevis" name="idDevis"/>
                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}"/>
                            <button type="submit" id="commitRejet" class="btn btn-danger pull-right"><i class="fa fa-check mrg5R"></i> Confirmer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection



@push('scripts')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- Sparkline -->
<script src="{{ asset('la-assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('la-assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{ asset('la-assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('la-assets/plugins/fastclick/fastclick.js') }}"></script>
<!-- dashboard -->
<script src="{{ asset('la-assets/js/pages/dashboard.js') }}"></script>
<script src="{{ asset('la-assets/plugins/datatables/datatables.min.js') }}"></script>
@endpush

@push('scripts')
<script>
    $.widget.bridge('uibutton', $.ui.button);

    $(".nbasset").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });

    $(".nbassetdg").DataTable({
        language: {
            lengthMenu: "_MENU_",
            search: "_INPUT_",
            searchPlaceholder: "Recherche",
            sInfoEmpty:      "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
            sInfoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            sInfoPostFix:    "",
            sLoadingRecords: "Chargement en cours...",
            sZeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
            sEmptyTable:     "Aucune donn&eacute;e disponible dans le tableau",
            sLengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
            sInfo:         "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            oPaginate: {
                "sFirst":    "Premier",
                "sLast":    "Dernier",
                "sNext":    "Suivant",
                "sPrevious": "Précédent"
            },
        }
    });



    function displayModal(id,type){
        //console.log(id);
        //console.log(type);
        $("#formType")[0].reset();
        $("#idAsset").val(id);
        $("#typeAsset").val(type);
    }

    function displayModalDevis(id){
        $("#formTypeDevis")[0].reset();
        $("#idDevis").val(id);
    }


</script>
@endpush